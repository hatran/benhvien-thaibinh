var lstData = [
    {tenthuoc: "Aristin - C",tdk: 32028750000,tck: 6405750000,tenhoatchat: "Ciprofloxacin",nhom: "Kháng Sinh"},
    {tenthuoc: "Vimotram",tdk: 645000000,tck: 129000000,tenhoatchat: "Amoxicilin+Suib actam",nhom: "Kháng Sinh"},
    {
      tenthuoc: "Vimotram",
      tdk: 731500000,
      tck: 146300000,
      tenhoatchat: "Amoxicilin + Sulbactam",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Vimotram",
      tdk: 783750000,
      tck: 156750000,
      tenhoatchat: "Amoxicilin+ Sulbactam",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Vimotram",
      tdk: 806250000,
      tck: 161250000,
      tenhoatchat: "Amoxicilin + Sulbactam",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Vimotram",
      tdk: 1075000000,
      tck: 215000000,
      tenhoatchat: "Amoxicilin + Sulbactam",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Vimotram",
      tdk: 1567500000,
      tck: 313500000,
      tenhoatchat: "Amoxicilin + Sulbactam",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Vimotram",
      tdk: 1881250000,
      tck: 376250000,
      tenhoatchat: "Amoxilin+Sulbactam",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Vimotram",
      tdk: 2769250000,
      tck: 553850000,
      tenhoatchat: "Amoxicilin + Sulbactam",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Vimotram",
      tdk: 2848750000,
      tck: 569750000,
      tenhoatchat: "Amoxicilin + Sulbactam",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Vimotram",
      tdk: 3285000338,
      tck: 6570000675,
      tenhoatchat: "Amoxicillin + Sulbactam",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Xalvobin 500mg",
      tdk: 7426125000,
      tck: 1485225000,
      tenhoatchat: "Capecitabin",
      nhom: "Thuốc Ung Thư - Miễn Dịch"
    },
    {
      tenthuoc: "Paxus PM 100 mg",
      tdk: 4891800000,
      tck: 978360000,
      tenhoatchat: "Paclitaxel",
      nhom: "Thuốc Ung Thư - Miễn Dịch"
    },
    {
      tenthuoc: "Duoplavin 75mg+100mg",
      tdk: 3644379213,
      tck: 7288758425,
      tenhoatchat: "Acetylsalic acid + Clopidogrel",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Meronem Inj 1g 10's",
      tdk: 312500,
      tck: 62500,
      tenhoatchat: "Meropenem",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Meronem Inj 1g 10's",
      tdk: 1808376750,
      tck: 361675350,
      tenhoatchat: "Meropenem",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Meronem Inj 1g 10's",
      tdk: 1808376750,
      tck: 361675350,
      tenhoatchat: "Meropenem",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Rifaxon",
      tdk: 1628562500,
      tck: 325712500,
      tenhoatchat: "Paracetamol",
      nhom: "Thuốc Hạ nhiệt - Giảm đau"
    },
    {
      tenthuoc: "Rifaxon",
      tdk: 1903812500,
      tck: 380762500,
      tenhoatchat: "Paracetamol",
      nhom: "Thuốc Hạ nhiệt - Giảm đau"
    },
    {
      tenthuoc: "Taxotere 80mg/1ml",
      tdk: 3504059375,
      tck: 700811875,
      tenhoatchat: "Docetacel",
      nhom: "Thuốc Ung Thư - Miễn Dịch"
    },
    {
      tenthuoc: "Concor 5mg",
      tdk: 5903999325,
      tck: 1180799865,
      tenhoatchat: "Bisoprolol",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Concor 5mg",
      tdk: 2696060573,
      tck: 5392121145,
      tenhoatchat: "Bisoprolol",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Scilin M30 (30/70)",
      tdk: 3671250,
      tck: 734250,
      tenhoatchat: "Insulin trộn",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Scilin M30 (30/70)",
      tdk: 4500000,
      tck: 900000,
      tenhoatchat: "Insulin trộn, hỗn hợp",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Scilin M30 (30/70)",
      tdk: 7725000,
      tck: 1545000,
      tenhoatchat: "Insulin trộn",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Scilin M30 (30/70)",
      tdk: 8937500,
      tck: 1787500,
      tenhoatchat: "Insulin trộn, hỗn hợp",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Scilin M30 (30/70)",
      tdk: 64375000,
      tck: 12875000,
      tenhoatchat: "Insulin trộn, hỗn hợp (Mixtard-acting, Dual-acting)",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Scilin M30 (30/70)",
      tdk: 112500000,
      tck: 22500000,
      tenhoatchat: "Insulin trộn, hỗn hợp",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Scilin M30 (30/70)",
      tdk: 167375000,
      tck: 33475000,
      tenhoatchat: "Insulin trộn, hỗn hợp (Mixtard-acting, Dual-acting)",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Scilin M30 (30/70)",
      tdk: 231750000,
      tck: 46350000,
      tenhoatchat: "Insulin trộn, hỗn hợp (Mixtard-acting, Dual-acting)",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Scilin M30 (30/70)",
      tdk: 257500000,
      tck: 51500000,
      tenhoatchat: "Insulin trộn, hỗn hợp (Mixtard-acting, Dual-acting)",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Scilin M30 (30/70)",
      tdk: 309000000,
      tck: 61800000,
      tenhoatchat: "Insulin trộn, hỗn hợp",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Scilin M30 (30/70)",
      tdk: 386250000,
      tck: 77250000,
      tenhoatchat: "Insulin trộn, hỗn hợp (Mixtard-acting, Dual-acting)",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Scilin M30 (30/70)",
      tdk: 1552500000,
      tck: 310500000,
      tenhoatchat: "Insulin trộn, hỗn hợp (30/70)",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "REDITUX",
      tdk: 3025000000,
      tck: 605000000,
      tenhoatchat: "Rituximab",
      nhom: "Thuốc Ung Thư - Miễn Dịch"
    },
    {
      tenthuoc: "Scilin N",
      tdk: 12875000,
      tck: 2575000,
      tenhoatchat: "Insulin tác dụng trung bình, trung gian",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Scilin N",
      tdk: 15450000,
      tck: 3090000,
      tenhoatchat: "Insulin chậm tác dụng kéo dài",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Scilin N",
      tdk: 15450000,
      tck: 3090000,
      tenhoatchat: "Insulin tác dụng chậm, kéo dài (Slow-acting, Long-acting)",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Scilin N",
      tdk: 25750000,
      tck: 5150000,
      tenhoatchat: "Insulin tác dụng chậm, kéo dài (Slow-acting, Long-acting)",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Scilin N",
      tdk: 46475000,
      tck: 9295000,
      tenhoatchat: "Insulin ( tác dụng chậm )",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Scilin N",
      tdk: 51500000,
      tck: 10300000,
      tenhoatchat: "Insulin chậm tác dụng kéo dài",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Scilin N",
      tdk: 1382775000,
      tck: 276555000,
      tenhoatchat: "Insulin tác dụng trung bình, trung gian",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Scilin N",
      tdk: 1389487500,
      tck: 277897500,
      tenhoatchat: "Insulin tác dụng trung bình, trung gian",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Ebitac 25",
      tdk: 86250000,
      tck: 17250000,
      tenhoatchat: "Enalapril maleat + Hydrochlorothiazid",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Ebitac 25",
      tdk: 129375000,
      tck: 25875000,
      tenhoatchat: "Enalapril maleat+ Hydroclorothiazid",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Ebitac 25",
      tdk: 517500000,
      tck: 103500000,
      tenhoatchat: "Enalapril maleat + Hydrochlorothiazid",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Ebitac 25",
      tdk: 776250000,
      tck: 86250000,
      tenhoatchat: "Enalapril maleat +Hydrochlorothiazid",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Ebitac 25",
      tdk: 1401562500,
      tck: 280312500,
      tenhoatchat: "Enalapril+ Hydroclorothiazid",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Taxotere 20mg/1ml",
      tdk: 2803248000,
      tck: 560649600,
      tenhoatchat: "Docetacel",
      nhom: "Thuốc Ung Thư - Miễn Dịch"
    },
    {
      tenthuoc: "Heparin 25000 UI/5ml",
      tdk: 11812500,
      tck: 2362500,
      tenhoatchat: "Heparin (Natri)",
      nhom: "Thuốc ảnh hưởng đến Máu"
    },
    {
      tenthuoc: "Heparin 25000 UI/5ml",
      tdk: 122062500,
      tck: 24412500,
      tenhoatchat: "Heparin (Natri)",
      nhom: "Thuốc ảnh hưởng đến Máu"
    },
    {
      tenthuoc: "Heparin 25000 UI/5ml",
      tdk: 354375000,
      tck: 70875000,
      tenhoatchat: "Heparin (Natri)",
      nhom: "Thuốc ảnh hưởng đến Máu"
    },
    {
      tenthuoc: "Heparin 25000 UI/5ml",
      tdk: 2279812500,
      tck: 455962500,
      tenhoatchat: "Heparin (Natri)",
      nhom: "Thuốc ảnh hưởng đến Máu"
    },
    {
      tenthuoc: "Unasyn Inj 1500mg 1's",
      tdk: 5000000,
      tck: 1000000,
      tenhoatchat: "Ampicilin + sulbactam",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Unasyn Inj 1500mg 1's",
      tdk: 123750000,
      tck: 24750000,
      tenhoatchat: "Ampicillin Sodium (Ampicillin 1g),Sulbactam sodium (Sulbactam 500mg)",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Unasyn Inj 1500mg 1's",
      tdk: 165000000,
      tck: 33000000,
      tenhoatchat: "Ampicilin +Sulbactam",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Unasyn Inj 1500mg 1's",
      tdk: 165000000,
      tck: 33000000,
      tenhoatchat: "Ampicillin Sodium (Ampicillin 1g),Sulbactam sodium (Sulbactam 500mg)",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Unasyn Inj 1500mg 1's",
      tdk: 214500000,
      tck: 42900000,
      tenhoatchat: "Ampicillin + Sulbactam",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Unasyn Inj 1500mg 1's",
      tdk: 330000000,
      tck: 66000000,
      tenhoatchat: "Ampicillin 1g, Sulbactam 500mg",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Unasyn Inj 1500mg 1's",
      tdk: 371250000,
      tck: 74250000,
      tenhoatchat: "Amoxicilin + Sulbactam",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Unasyn Inj 1500mg 1's",
      tdk: 412500000,
      tck: 82500000,
      tenhoatchat: "Ampicilin + sulbactam",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Unasyn Inj 1500mg 1's",
      tdk: 412500000,
      tck: 82500000,
      tenhoatchat: "Ampicilin + sulbactam",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Unasyn Inj 1500mg 1's",
      tdk: 528000000,
      tck: 105600000,
      tenhoatchat: "Ampicilin + sulbactam",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Rocephin 1g I.V.",
      tdk: 22680000000,
      tck: 4536000000,
      tenhoatchat: "Ceftriaxon",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Rocephin 1g I.V.",
      tdk: 34020000000,
      tck: 6804000000,
      tenhoatchat: "Ceftriaxon",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Rocephin 1g I.V.",
      tdk: 40824000000,
      tck: 4536000000,
      tenhoatchat: "Ceftriaxone",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Rocephin 1g I.V.",
      tdk: 49555800000,
      tck: 9911160000,
      tenhoatchat: "Ceftriaxon",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Rocephin 1g I.V.",
      tdk: 56700000000,
      tck: 11340000000,
      tenhoatchat: "Ceftriaxon*",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Rocephin 1g I.V.",
      tdk: 68040000000,
      tck: 13608000000,
      tenhoatchat: "Ceftriaxon",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Paclitaxelum Actavis",
      tdk: 270937500000,
      tck: 54187500000,
      tenhoatchat: "Paclitaxel",
      nhom: "Thuốc Ung Thư - Miễn Dịch"
    },
    {
      tenthuoc: "Coveram 5-5 Tab 5mg/5mg 30's",
      tdk: 815388750,
      tck: 163077750,
      tenhoatchat: "Perindopril + amlodipin",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Coveram 5-5 Tab 5mg/5mg 30's",
      tdk: 1235437500,
      tck: 247087500,
      tenhoatchat: "Perindopril Arginine 5mg, Amlodipine besylate 5mg",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Coveram 5-5 Tab 5mg/5mg 30's",
      tdk: 1630777500,
      tck: 326155500,
      tenhoatchat: "Perindopril + amlodipin",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Coveram 5-5 Tab 5mg/5mg 30's",
      tdk: 2446166250,
      tck: 489233250,
      tenhoatchat: "Perindopril Arginine 5mg, Amlodipine besylate 5mg",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Coveram 5-5 Tab 5mg/5mg 30's",
      tdk: 3286263750,
      tck: 657252750,
      tenhoatchat: "Perindopril + amlodipin",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Coveram 5-5 Tab 5mg/5mg 30's",
      tdk: 3286263750,
      tck: 657252750,
      tenhoatchat: "Perindopril+ Amlodipin",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Coveram 5-5 Tab 5mg/5mg 30's",
      tdk: 4101652500,
      tck: 820330500,
      tenhoatchat: "Perindopril + amlodipin",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Coveram 5-5 Tab 5mg/5mg 30's",
      tdk: 4101652500,
      tck: 820330500,
      tenhoatchat: "Perindopril arginine; Amlodipine besilate",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Coveram 5-5 Tab 5mg/5mg 30's",
      tdk: 4101652503,
      tck: 820330501,
      tenhoatchat: "Perindopril + Amlodipine",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Coveram 5-5 Tab 5mg/5mg 30's",
      tdk: 4516759500,
      tck: 1235437500,
      tenhoatchat: "Perindopril Arginine 5mg, Amlodipine besylate 5mg",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Coveram 5-5 Tab 5mg/5mg 30's",
      tdk: 4941750000,
      tck: 988350000,
      tenhoatchat: "Perindopril arginine; Amlodipine besilate",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Coveram 5-5 Tab 5mg/5mg 30's",
      tdk: 12354375000,
      tck: 2470875000,
      tenhoatchat: "Amlodipin + Perindopril",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Coveram 5-5 Tab 5mg/5mg 30's",
      tdk: 16456027500,
      tck: 3291205500,
      tenhoatchat: "Perindopril Arginine 5mg, Amlodipine besylate 5mg",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Coveram 5-5 Tab 5mg/5mg 30's",
      tdk: 17296125000,
      tck: 3459225000,
      tenhoatchat: "Amlodipin +Perindopril",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Coveram 5-5 Tab 5mg/5mg 30's",
      tdk: 24708750000,
      tck: 4941750000,
      tenhoatchat: "Amlodipin +Perindopril",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Coveram 5-5 Tab 5mg/5mg 30's",
      tdk: 24708750000,
      tck: 4941750000,
      tenhoatchat: "Perindopril arginine 5mg (tương đương 3,395mg perindopril); Amlodipine (dưới dạng amlodipin besilate) 5mg",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Coveram 5-5 Tab 5mg/5mg 30's",
      tdk: 49417500000,
      tck: 9883500000,
      tenhoatchat: "Perindopril Arginine 5mg, Amlodipine besylate 5mg",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Coveram 5-5 Tab 5mg/5mg 30's",
      tdk: 74096599500,
      tck: 8232955500,
      tenhoatchat: "Perindopril Arginine 5mg, Amlodipine besylate 5mg",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Tiepanem 1g",
      tdk: 81250000000,
      tck: 16250000000,
      tenhoatchat: "Meropenem",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Tiepanem 1g",
      tdk: 154375000000,
      tck: 30875000000,
      tenhoatchat: "Meropenem",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "PAXUS PM 30MG",
      tdk: 234375000000,
      tck: 46875000000,
      tenhoatchat: "Paclitacel-Polymeric Micelle",
      nhom: "Thuốc Ung Thư - Miễn Dịch"
    },
    {
      tenthuoc: "Meiunem 0,5g",
      tdk: 64375001250,
      tck: 12875000250,
      tenhoatchat: "Meropenem",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Meiunem 0,5g",
      tdk: 74000000000,
      tck: 14800000000,
      tenhoatchat: "Meropenem",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Meiunem 0,5g",
      tdk: 95275000000,
      tck: 19055000000,
      tenhoatchat: "Meropenem",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Anzatax",
      tdk: 98175000000,
      tck: 19635000000,
      tenhoatchat: "Paclitacel",
      nhom: "Thuốc Ung Thư - Miễn Dịch"
    },
    {
      tenthuoc: "Anzatax",
      tdk: 126000000000,
      tck: 25200000000,
      tenhoatchat: "Paclitacel",
      nhom: "Thuốc Ung Thư - Miễn Dịch"
    },
    {
      tenthuoc: "Tadocel 80mg/2ml",
      tdk: 92475000000,
      tck: 18495000000,
      tenhoatchat: "Docetaxel",
      nhom: "Thuốc Ung Thư - Miễn Dịch"
    },
    {
      tenthuoc: "Tadocel 80mg/2ml",
      tdk: 124312500000,
      tck: 24862500000,
      tenhoatchat: "Docetaxel",
      nhom: "Thuốc Ung Thư - Miễn Dịch"
    },
    {
      tenthuoc: "Ceftriaxone panpharma",
      tdk: 5622500000,
      tck: 1124500000,
      tenhoatchat: "Ceftriaxon",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Ceftriaxone panpharma",
      tdk: 5622500000,
      tck: 1124500000,
      tenhoatchat: "Ceftriaxon",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Ceftriaxone panpharma",
      tdk: 14056250000,
      tck: 2811250000,
      tenhoatchat: "Ceftriaxon",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Ceftriaxone panpharma",
      tdk: 70281250000,
      tck: 14056250000,
      tenhoatchat: "Ceftriaxon",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Ceftriaxone panpharma",
      tdk: 112421847510,
      tck: 22484369502,
      tenhoatchat: "Ceftriaxon",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Proxacin 1%",
      tdk: 78200000000,
      tck: 15640000000,
      tenhoatchat: "Ciprofloxacin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Proxacin 1%",
      tdk: 128250000000,
      tck: 25650000000,
      tenhoatchat: "Ciprofloxacin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Paxus PM",
      tdk: 63281250240,
      tck: 12656250048,
      tenhoatchat: "Paclitaxel",
      nhom: "Thuốc Ung Thư - Miễn Dịch"
    },
    {
      tenthuoc: "Paxus PM",
      tdk: 142837500001,
      tck: 28567500000,
      tenhoatchat: "Paclitaxel",
      nhom: "Thuốc Ung Thư - Miễn Dịch"
    },
    {
      tenthuoc: "Meronem 1g",
      tdk: 200930748375,
      tck: 40186149675,
      tenhoatchat: "Meropenem",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Paxus PM 30 mg",
      tdk: 187616250000,
      tck: 37523250000,
      tenhoatchat: "Paclitaxel",
      nhom: "Thuốc Ung Thư - Miễn Dịch"
    },
    {
      tenthuoc: "Polhumin mix-2",
      tdk: 183750000,
      tck: 36750000,
      tenhoatchat: "Insulin trộn, hỗn hợp (Mixtard-acting, Dual- acting)",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Polhumin mix-2",
      tdk: 18375000000,
      tck: 3675000000,
      tenhoatchat: "Insulin trộn, hỗn hợp",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Polhumin mix-2",
      tdk: 18375000000,
      tck: 3675000000,
      tenhoatchat: "Insulin trộn, hỗn hợp",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Polhumin mix-2",
      tdk: 33075000000,
      tck: 3675000000,
      tenhoatchat: "Insulin trộn, hỗn hợp",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Polhumin mix-2",
      tdk: 45937500000,
      tck: 9187500000,
      tenhoatchat: "Insulin trộn, hỗn hợp",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Polhumin mix-2",
      tdk: 64312500000,
      tck: 12862500000,
      tenhoatchat: "Insulin trộn, hỗn hợp (Mixtard-acting, Dual-acting)",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Unasyn INJ",
      tdk: 1650000001,
      tck: 330000000,
      tenhoatchat: "Ampicilin + sulbactam",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Unasyn INJ",
      tdk: 173250000000,
      tck: 34650000000,
      tenhoatchat: "Ampicilin + sulbactam",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Klamentin 1g",
      tdk: 6231881250,
      tck: 1246376250,
      tenhoatchat: "Amoxicilin + Acid clavulanic",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Klamentin 1g",
      tdk: 24927525000,
      tck: 4985505000,
      tenhoatchat: "Amoxicilin+ Acid clavulanic",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Klamentin 1g",
      tdk: 142128787500,
      tck: 28425757500,
      tenhoatchat: "Amoxicilin + Acid clavulanic",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Vigamox",
      tdk: 2500000,
      tck: 500000,
      tenhoatchat: "Moxifloxacin",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Vigamox",
      tdk: 7875000000,
      tck: 1575000000,
      tenhoatchat: "Moxifloxacin",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Vigamox",
      tdk: 31499650000,
      tck: 6299930000,
      tenhoatchat: "Moxifloxacin",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Vigamox",
      tdk: 39375000000,
      tck: 7875000000,
      tenhoatchat: "Moxifloxacin",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Vigamox",
      tdk: 86939034000,
      tck: 17387806800,
      tenhoatchat: "Moxifloxacin",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Rocephin",
      tdk: 165564000000,
      tck: 33112800000,
      tenhoatchat: "Ceftriaxone",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Twynsta 40+5mg",
      tdk: 154480234926,
      tck: 30896046985,
      tenhoatchat: "Telmisartan + Amlodipine",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Ebitac 12.5",
      tdk: 2178750000,
      tck: 435750000,
      tenhoatchat: "Enalapril+ Hydrochlorothiazid",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Ebitac 12.5",
      tdk: 4312500000,
      tck: 862500000,
      tenhoatchat: "Enalapril +Hydroclorothiazid",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Ebitac 12.5",
      tdk: 8625000000,
      tck: 1725000000,
      tenhoatchat: "Enalapril maleat +Hydroclorothiazid",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Ebitac 12.5",
      tdk: 12937500000,
      tck: 2587500000,
      tenhoatchat: "Enalapril+ Hydrochlorothiazid",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Ebitac 12.5",
      tdk: 17250000000,
      tck: 3450000000,
      tenhoatchat: "Enalapril+ Hydroclorothiazid",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Ebitac 12.5",
      tdk: 23718750000,
      tck: 4743750000,
      tenhoatchat: "Enalapril maleat +Hydroclorothiazid",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Ebitac 12.5",
      tdk: 77625000000,
      tck: 8625000000,
      tenhoatchat: "Enalapril maleat +Hydroclorothiazid",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Bupivacaine 20mg 0.5% 5 mg/ml 0,5%/4ml",
      tdk: 33075000000,
      tck: 6615000000,
      tenhoatchat: "Bupivacain (hydroclorid)",
      nhom: "Thuốc Gây tê - Gây mê"
    },
    {
      tenthuoc: "Bupivacaine 20mg 0.5% 5 mg/ml 0,5%/4ml",
      tdk: 109992015000,
      tck: 21998403000,
      tenhoatchat: "Bupivacain (hydroclorid)",
      nhom: "Thuốc Gây tê - Gây mê"
    },
    {
      tenthuoc: "Mixtard 30 Flexpen 100IU/ml x 3ml",
      tdk: 7699950000,
      tck: 1539990000,
      tenhoatchat: "Insulin người",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Mixtard 30 Flexpen 100IU/ml x 3ml",
      tdk: 7699950000,
      tck: 1539990000,
      tenhoatchat: "Insulin người",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Mixtard 30 Flexpen 100IU/ml x 3ml",
      tdk: 7699950000,
      tck: 1539990000,
      tenhoatchat: "Insulin người (rDNA)",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Mixtard 30 Flexpen 100IU/ml x 3ml",
      tdk: 9624937500,
      tck: 1924987500,
      tenhoatchat: "Insulin trộn, hỗn hợp",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Mixtard 30 Flexpen 100IU/ml x 3ml",
      tdk: 28874790000,
      tck: 5774958000,
      tenhoatchat: "Insulin trộn, hỗn hợp",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Mixtard 30 Flexpen 100IU/ml x 3ml",
      tdk: 34649775000,
      tck: 3849975000,
      tenhoatchat: "Insulin hỗn hợp 30/70 100IU/ml 3ml",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Mixtard 30 Flexpen 100IU/ml x 3ml",
      tdk: 48124687500,
      tck: 9624937500,
      tenhoatchat: "Insulin trộn, hỗn hợp",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Levogol",
      tdk: 135000000000,
      tck: 27000000000,
      tenhoatchat: "Levofloxacin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Fudcime 200mg",
      tdk: 9125000000,
      tck: 1825000000,
      tenhoatchat: "Cefixim",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Fudcime 200mg",
      tdk: 29625000000,
      tck: 5925000000,
      tenhoatchat: "Cefixim",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Fudcime 200mg",
      tdk: 89475000563,
      tck: 17895000113,
      tenhoatchat: "Cefixim",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Levoflex",
      tdk: 62775000000,
      tck: 6975000000,
      tenhoatchat: "Levofloxacin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Levoflex",
      tdk: 64170001035,
      tck: 12834000207,
      tenhoatchat: "Levofloxacin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Vitamin B1-B6-B12",
      tdk: 596250000,
      tck: 119250000,
      tenhoatchat: "Vitamin B1+Vitamin B6+Vitamin B12.",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Vitamin B1-B6-B12",
      tdk: 1680000000,
      tck: 336000000,
      tenhoatchat: "Vitamin B1+Vitamin B6+Vitamin B12.",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Vitamin B1-B6-B12",
      tdk: 1800000000,
      tck: 360000000,
      tenhoatchat: "Vitamin B1+Vitamin B6+Vitamin B12.",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Vitamin B1-B6-B12",
      tdk: 4200000000,
      tck: 840000000,
      tenhoatchat: "Vitamin B1Vitamin B6",
      nhom: ""
    },
    {
      tenthuoc: "Vitamin B1-B6-B12",
      tdk: 6000000000,
      tck: 1200000000,
      tenhoatchat: "Vitamin B1 + B6 + B12",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Vitamin B1-B6-B12",
      tdk: 6000000000,
      tck: 1200000000,
      tenhoatchat: "Vitamin B1+Vitamin B6+Vitamin B12.",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Vitamin B1-B6-B12",
      tdk: 6762000000,
      tck: 1352400000,
      tenhoatchat: "Vitamin B1+Vitamin B6+Vitamin B12.",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Vitamin B1-B6-B12",
      tdk: 12000000000,
      tck: 2400000000,
      tenhoatchat: "Vitamin B1+Vitamin B6+Vitamin B12.",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Vitamin B1-B6-B12",
      tdk: 43200000000,
      tck: 4800000000,
      tenhoatchat: "Vitamin B1+Vitamin B6+Vitamin B12.",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Vitamin B1-B6-B12",
      tdk: 43922950000,
      tck: 8784590000,
      tenhoatchat: "Vitamin B1 + B6 + B12",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Insulin H Mix 100IU",
      tdk: 945000000,
      tck: 189000000,
      tenhoatchat: "Insulin trộn, hỗn hợp (Mixtard-acting, Dual-acting)",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Insulin H Mix 100IU",
      tdk: 1650000000,
      tck: 330000000,
      tenhoatchat: "Insulin trộn, hỗn hợp (Mixtard-acting, Dual-acting)",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Insulin H Mix 100IU",
      tdk: 6534000000,
      tck: 1306800000,
      tenhoatchat: "Insulin trộn, hỗn hợp",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Insulin H Mix 100IU",
      tdk: 13200000000,
      tck: 2640000000,
      tenhoatchat: "Insulin hỗn hợp 30/70 100IU/ml 10ml",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Insulin H Mix 100IU",
      tdk: 29700000000,
      tck: 3300000000,
      tenhoatchat: "Insulin trộn, hỗn hợp",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Insulin H Mix 100IU",
      tdk: 32868000000,
      tck: 6573600000,
      tenhoatchat: "Insulin trộn, hỗn hợp",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Insulin H Mix 100IU",
      tdk: 41250000000,
      tck: 8250000000,
      tenhoatchat: "Insulin trộn, hỗn hợp",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Lovenox 0,6ml,6000UI",
      tdk: 125328015855,
      tck: 25065603171,
      tenhoatchat: "Enoxaparin (natri)",
      nhom: "Thuốc ảnh hưởng đến Máu"
    },
    {
      tenthuoc: "Unasyn Inj 1500mg 1's Ampicillin 1g, Sulba",
      tdk: 123749996250,
      tck: 24749999250,
      tenhoatchat: "Ampicillin- sulbactam",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Tadocel 20mg/0,5ml",
      tdk: 25156250000,
      tck: 5031250000,
      tenhoatchat: "Docetacel",
      nhom: "Thuốc Ung Thư - Miễn Dịch"
    },
    {
      tenthuoc: "Tadocel 20mg/0,5ml",
      tdk: 95200000000,
      tck: 19040000000,
      tenhoatchat: "Docetaxel",
      nhom: "Thuốc Ung Thư - Miễn Dịch"
    },
    {
      tenthuoc: "PROPOFOL 1% KABI",
      tdk: 2185312500,
      tck: 437062500,
      tenhoatchat: "Propofol",
      nhom: "Thuốc Gây tê - Gây mê"
    },
    {
      tenthuoc: "PROPOFOL 1% KABI",
      tdk: 2622375000,
      tck: 524475000,
      tenhoatchat: "Propofol",
      nhom: "Thuốc Gây tê - Gây mê"
    },
    {
      tenthuoc: "PROPOFOL 1% KABI",
      tdk: 2913750000,
      tck: 582750000,
      tenhoatchat: "Propofol",
      nhom: "Thuốc Gây tê - Gây mê"
    },
    {
      tenthuoc: "PROPOFOL 1% KABI",
      tdk: 2913750000,
      tck: 582750000,
      tenhoatchat: "Propofol",
      nhom: "Thuốc Gây tê - Gây mê"
    },
    {
      tenthuoc: "PROPOFOL 1% KABI",
      tdk: 3858750000,
      tck: 771750000,
      tenhoatchat: "Propofol",
      nhom: "Thuốc Gây tê - Gây mê"
    },
    {
      tenthuoc: "PROPOFOL 1% KABI",
      tdk: 4734843750,
      tck: 946968750,
      tenhoatchat: "Propofol",
      nhom: "Thuốc Gây tê - Gây mê"
    },
    {
      tenthuoc: "PROPOFOL 1% KABI",
      tdk: 6431250000,
      tck: 1286250000,
      tenhoatchat: "Propofol",
      nhom: "Thuốc Gây tê - Gây mê"
    },
    {
      tenthuoc: "PROPOFOL 1% KABI",
      tdk: 17482500000,
      tck: 3496500000,
      tenhoatchat: "Propofol",
      nhom: "Thuốc Gây tê - Gây mê"
    },
    {
      tenthuoc: "PROPOFOL 1% KABI",
      tdk: 75888750000,
      tck: 15177750000,
      tenhoatchat: "Propofol, 10mg/ml (1%)",
      nhom: "Thuốc Gây tê - Gây mê"
    },
    {
      tenthuoc: "Cerecaps",
      tdk: 5625000000,
      tck: 1125000000,
      tenhoatchat: "Cao khô hồng hoa +cao khô đương quy +cao khô sinh địa +cao khô sài hồ +cao khô cam thảo +cao khô xuyên khung +cao khô xích thược +cao khô chỉ xác +cao khô ngưu tất +cao khô bạch quả",
      nhom: "Thuốc Đông dược"
    },
    {
      tenthuoc: "Cerecaps",
      tdk: 111928126679,
      tck: 22385625336,
      tenhoatchat: "Cao khô hồng hoa",
      nhom: "Thuốc Đông dược"
    },
    {
      tenthuoc: "Infartan 75",
      tdk: 5220000000,
      tck: 1044000000,
      tenhoatchat: "Clopidogrel",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Infartan 75",
      tdk: 10485000000,
      tck: 2097000000,
      tenhoatchat: "Clopidogrel",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Infartan 75",
      tdk: 100440000000,
      tck: 20088000000,
      tenhoatchat: "Clopidogrel",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Mixtard 30 FlexPen",
      tdk: 102083125,
      tck: 20416625,
      tenhoatchat: "Insulin trộn, hỗn hợp (Mixtard-acting, Dual- acting)",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Mixtard 30 FlexPen",
      tdk: 5832712125,
      tck: 1166542425,
      tenhoatchat: "Insulin người (rDNA)",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Mixtard 30 FlexPen",
      tdk: 5871211875,
      tck: 1174242375,
      tenhoatchat: "Insulin trộn, hỗn hợp",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Mixtard 30 FlexPen",
      tdk: 15399800000,
      tck: 3079960000,
      tenhoatchat: "Insulin người (rDNA)",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Mixtard 30 FlexPen",
      tdk: 19249875000,
      tck: 3849975000,
      tenhoatchat: "Insulin hỗn hợp 30/70 100IU/ml 3ml",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Mixtard 30 FlexPen",
      tdk: 28874812500,
      tck: 5774962500,
      tenhoatchat: "Insulin trộn, hỗn hợp (Mixtard-acting, Dual- acting)",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Mixtard 30 FlexPen",
      tdk: 38499750000,
      tck: 7699950000,
      tenhoatchat: "Insulin người (rDNA)",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Ceftriaxone  Panpharma [Chỉ dành cho kho Nhi]",
      tdk: 113574499899,
      tck: 22714899980,
      tenhoatchat: "Ceftriaxone",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Xeloda",
      tdk: 113529405000,
      tck: 22705881000,
      tenhoatchat: "Capecitabin",
      nhom: "Thuốc Ung Thư - Miễn Dịch"
    },
    {
      tenthuoc: "Rocephin 1g",
      tdk: 22680000000,
      tck: 4536000000,
      tenhoatchat: "Ceftriaxon",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Rocephin 1g",
      tdk: 90720000000,
      tck: 18144000000,
      tenhoatchat: "Ceftriaxon",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Scilin M30 400IU",
      tdk: 3090000060,
      tck: 618000012,
      tenhoatchat: "Insulin trộn (M) 30/70 400IU/10ml",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Scilin M30 400IU",
      tdk: 3090780000,
      tck: 618156000,
      tenhoatchat: "Insulin trộn (M) 30/70 400IU/10ml",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Scilin M30 400IU",
      tdk: 6179040000,
      tck: 1235808000,
      tenhoatchat: "Insulin trộn (M) 30/70 400IU/10ml",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Scilin M30 400IU",
      tdk: 6179040000,
      tck: 1235808000,
      tenhoatchat: "Insulin trộn (M) 30/70 400IU/10ml",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Scilin M30 400IU",
      tdk: 6179796000,
      tck: 1235959200,
      tenhoatchat: "Insulin trộn (M) 30/70 400IU/10ml",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Scilin M30 400IU",
      tdk: 6179796000,
      tck: 1235959200,
      tenhoatchat: "Insulin trộn (M) 30/70 400IU/10ml",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Scilin M30 400IU",
      tdk: 6181560000,
      tck: 1236312000,
      tenhoatchat: "Insulin trộn (M) 30/70 400IU/10ml",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Scilin M30 400IU",
      tdk: 9270000180,
      tck: 1854000036,
      tenhoatchat: "Insulin trộn (M) 30/70 400IU/10ml",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Scilin M30 400IU",
      tdk: 12360096000,
      tck: 2472019200,
      tenhoatchat: "Insulin trộn (M) 30/70 400IU/10ml",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Scilin M30 400IU",
      tdk: 14162032500,
      tck: 2832406500,
      tenhoatchat: "Insulin trộn (M) 30/70 400IU/10ml",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Scilin M30 400IU",
      tdk: 15450000300,
      tck: 3090000060,
      tenhoatchat: "Insulin trộn (M) 30/70 400IU/10ml",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Scilin M30 400IU",
      tdk: 23176125000,
      tck: 4635225000,
      tenhoatchat: "Insulin trộn (M) 30/70 400IU/10ml",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Scilin N 400UI/10ml",
      tdk: 108150000000,
      tck: 21630000000,
      tenhoatchat: "Insulin chậm, kéo dài",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Sanbeclaneksi (Amoxicillin+acid clavulanic) 1.2g",
      tdk: 50049943125,
      tck: 10009988625,
      tenhoatchat: "Amoxicilin+Acid Clavulanic",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Sanbeclaneksi (Amoxicillin+acid clavulanic) 1.2g",
      tdk: 57734593439,
      tck: 11546918688,
      tenhoatchat: "Amoxicilin+Acid Clavulanic",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Bloza",
      tdk: 2353050000,
      tck: 470610000,
      tenhoatchat: "Losartan",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Bloza",
      tdk: 7141680000,
      tck: 1428336000,
      tenhoatchat: "Losartan",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Bloza",
      tdk: 8583750000,
      tck: 1716750000,
      tenhoatchat: "Losartan",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Bloza",
      tdk: 12721117500,
      tck: 2544223500,
      tenhoatchat: "Losartan",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Bloza",
      tdk: 12841290000,
      tck: 2568258000,
      tenhoatchat: "Losartan",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Bloza",
      tdk: 14300527500,
      tck: 2860105500,
      tenhoatchat: "Losartan",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Bloza",
      tdk: 14300527500,
      tck: 2860105500,
      tenhoatchat: "Losartan",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Bloza",
      tdk: 34317832500,
      tck: 6863566500,
      tenhoatchat: "Losartan",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Heparin (natri) 5ml.5000UI/ml",
      tdk: 105984375000,
      tck: 21196875000,
      tenhoatchat: "Heparin",
      nhom: "Thuốc ảnh hưởng đến Máu"
    },
    {
      tenthuoc: "Twynsta",
      tdk: 7770045000,
      tck: 1554009000,
      tenhoatchat: "Amlodipin + Telmisartan 5mg+40mg",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Twynsta",
      tdk: 8037225000,
      tck: 1607445000,
      tenhoatchat: "Telmisartan + Amlodipine (dưới dạng Amlodipine besylate)",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Twynsta",
      tdk: 27741245000,
      tck: 5548249000,
      tenhoatchat: "Telmisartan + Amlodipine (dưới dạng Amlodipine besylate)",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Twynsta",
      tdk: 62409999750,
      tck: 12481999950,
      tenhoatchat: "Telmisartan + Amlodipine",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Adalat LA Tab 20mg 30's",
      tdk: 2650725000,
      tck: 294525000,
      tenhoatchat: "Nifedipine 20 mg",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Adalat LA Tab 20mg 30's",
      tdk: 5042625000,
      tck: 1008525000,
      tenhoatchat: "Nifedipine",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Adalat LA Tab 20mg 30's",
      tdk: 5935125000,
      tck: 1187025000,
      tenhoatchat: "Nifedipine",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Adalat LA Tab 20mg 30's",
      tdk: 7407750000,
      tck: 1481550000,
      tenhoatchat: "Nifedipine 20 mg",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Adalat LA Tab 20mg 30's",
      tdk: 7431550000,
      tck: 1487500000,
      tenhoatchat: "Nifedipine 20 mg",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Adalat LA Tab 20mg 30's",
      tdk: 9661312500,
      tck: 1932262500,
      tenhoatchat: "Nifedipin",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Adalat LA Tab 20mg 30's",
      tdk: 11156250000,
      tck: 2231250000,
      tenhoatchat: "Nifedipin",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Adalat LA Tab 20mg 30's",
      tdk: 11894050000,
      tck: 5950000000,
      tenhoatchat: "Nifedipine",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Adalat LA Tab 20mg 30's",
      tdk: 18586312500,
      tck: 3717262500,
      tenhoatchat: "Nifedipin",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Adalat LA Tab 20mg 30's",
      tdk: 26016375000,
      tck: 5203275000,
      tenhoatchat: "Nifedipin",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Bibiso",
      tdk: 1968750000,
      tck: 393750000,
      tenhoatchat: "Cao Actiso, Cao rau đắng đất, Cao bìm bìm",
      nhom: "Thuốc Đông dược"
    },
    {
      tenthuoc: "Bibiso",
      tdk: 8662500000,
      tck: 1732500000,
      tenhoatchat: "Cao khô Artiso +Cao khô Rau đắng đất +Cao khô Bìm bìm",
      nhom: "Thuốc Đông dược"
    },
    {
      tenthuoc: "Bibiso",
      tdk: 17325000000,
      tck: 3465000000,
      tenhoatchat: "Cao khô Artiso+Cao khô rau đắng đất+Cao khô bìm bìm",
      nhom: "Thuốc Đông dược"
    },
    {
      tenthuoc: "Bibiso",
      tdk: 17567550000,
      tck: 3513510000,
      tenhoatchat: "Cao khô Artiso, Cao khô, Rau đắng đất,Cao khô bìm bìm",
      nhom: "Thuốc Đông dược"
    },
    {
      tenthuoc: "Bibiso",
      tdk: 25987500000,
      tck: 5197500000,
      tenhoatchat: "Cao khô Artiso +Cao khô Rau đắng đất +Cao khô Bìm bìm",
      nhom: "Thuốc Đông dược"
    },
    {
      tenthuoc: "Bibiso",
      tdk: 32917500000,
      tck: 6583500000,
      tenhoatchat: "Cao Actiso, Cao rau đắng đất, Cao bìm bìm",
      nhom: "Thuốc Đông dược"
    },
    {
      tenthuoc: "Amlor 5mg",
      tdk: 1423687500,
      tck: 284737500,
      tenhoatchat: "Amlodipin 5mg",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Amlor 5mg",
      tdk: 1879267352,
      tck: 375853470,
      tenhoatchat: "Amlodipin",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Amlor 5mg",
      tdk: 3046874906,
      tck: 609374981,
      tenhoatchat: "Amlodipin",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Amlor 5mg",
      tdk: 97499934000,
      tck: 19499986800,
      tenhoatchat: "Amlodipine",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "LOVENOX 40mg Inj B/ 2 syringes x0,4ml",
      tdk: 533631250,
      tck: 106726250,
      tenhoatchat: "Enoxaparin natri",
      nhom: "Thuốc ảnh hưởng đến Máu"
    },
    {
      tenthuoc: "LOVENOX 40mg Inj B/ 2 syringes x0,4ml",
      tdk: 1072887500,
      tck: 214577500,
      tenhoatchat: "Natri  Enoxaparin",
      nhom: "Thuốc ảnh hưởng đến Máu"
    },
    {
      tenthuoc: "LOVENOX 40mg Inj B/ 2 syringes x0,4ml",
      tdk: 48880622500,
      tck: 9776124500,
      tenhoatchat: "Enoxaparin",
      nhom: "Thuốc ảnh hưởng đến Máu"
    },
    {
      tenthuoc: "LOVENOX 40mg Inj B/ 2 syringes x0,4ml",
      tdk: 48880622500,
      tck: 9776124500,
      tenhoatchat: "Enoxaparin",
      nhom: "Thuốc ảnh hưởng đến Máu"
    },
    {
      tenthuoc: "Meronem Inj 500mg 10's",
      tdk: 98679262500,
      tck: 19735852500,
      tenhoatchat: "Meropenem",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Merugold I.V 1g",
      tdk: 97500000938,
      tck: 19500000188,
      tenhoatchat: "Meropenem",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Remeclar 500",
      tdk: 48530125000,
      tck: 9706025000,
      tenhoatchat: "Clarithromycin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Remeclar 500",
      tdk: 48530125000,
      tck: 9706025000,
      tenhoatchat: "Clarithromycin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Scanneuron",
      tdk: 2062500000,
      tck: 412500000,
      tenhoatchat: "Vitamin B1 + Vitamin B6 + Vitamin B12",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Scanneuron",
      tdk: 4500000000,
      tck: 900000000,
      tenhoatchat: "Vitamin B1 + Vitamin B6 + Vitamin B12",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Scanneuron",
      tdk: 4575000000,
      tck: 915000000,
      tenhoatchat: "Vitamin B1+Vitamin B6+Vitamin B12.",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Scanneuron",
      tdk: 5400000000,
      tck: 1080000000,
      tenhoatchat: "Vitamin B1+Vitamin B6+Vitamin B12.",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Scanneuron",
      tdk: 6875000000,
      tck: 1375000000,
      tenhoatchat: "Vitamin Bl+Vitamin B6+Vitamin B12.",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Scanneuron",
      tdk: 7625000000,
      tck: 1525000000,
      tenhoatchat: "Vitamin B1 + B6 + B12",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Scanneuron",
      tdk: 7625000000,
      tck: 1525000000,
      tenhoatchat: "Vitamin B1 + Vitamin B6 + Vitamin B12",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Scanneuron",
      tdk: 8100000000,
      tck: 1620000000,
      tenhoatchat: "Vitamin B1 + Vitamin B6 + Vitamin B12",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Scanneuron",
      tdk: 9540000000,
      tck: 2484000000,
      tenhoatchat: "vitamin b1+b6+b12",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Scanneuron",
      tdk: 18000000000,
      tck: 3600000000,
      tenhoatchat: "Vitamin B1+Vitamin B6+Vitamin B12.",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Scanneuron",
      tdk: 20587500000,
      tck: 4117500000,
      tenhoatchat: "Vitamin B1 + B6 + B12",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "DBL Irinotecan Injection 100mg/5ml",
      tdk: 94500000000,
      tck: 18900000000,
      tenhoatchat: "Irinotecan",
      nhom: "Thuốc Ung Thư - Miễn Dịch"
    },
    {
      tenthuoc: "Fudcime",
      tdk: 13421250000,
      tck: 2684250000,
      tenhoatchat: "Cefixim",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Fudcime",
      tdk: 39500000000,
      tck: 7900000000,
      tenhoatchat: "Cefixim",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Fudcime",
      tdk: 40263750000,
      tck: 8052750000,
      tenhoatchat: "Cefixim",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Novomix 30 Flexpen",
      tdk: 14240625000,
      tck: 2848125000,
      tenhoatchat: "Insulin aspart Biphasic (DNA tái tổ hợp)",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Novomix 30 Flexpen",
      tdk: 78608250000,
      tck: 15721650000,
      tenhoatchat: "Insulin",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Indocollyre",
      tdk: 9900000000,
      tck: 1980000000,
      tenhoatchat: "Indomethacin",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Indocollyre",
      tdk: 9900000000,
      tck: 1980000000,
      tenhoatchat: "Indomethacin",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Indocollyre",
      tdk: 12375000000,
      tck: 2475000000,
      tenhoatchat: "Indomethacin",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Indocollyre",
      tdk: 57750000000,
      tck: 11550000000,
      tenhoatchat: "Indomethacin",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Sotstop",
      tdk: 250250000,
      tck: 50050000,
      tenhoatchat: "Ibuprofen",
      nhom: "Thuốc Hạ nhiệt - Giảm đau"
    },
    {
      tenthuoc: "Amlor cap",
      tdk: 24476562500,
      tck: 4895312500,
      tenhoatchat: "Amlodipin",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Systan Ultra",
      tdk: 24189847500,
      tck: 4837969500,
      tenhoatchat: "Polyethylen glycol + Propylen glycol",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Meiunem (meropenem) 500mg",
      tdk: 9999999844,
      tck: 1999999969,
      tenhoatchat: "Meropenem",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Meiunem (meropenem) 500mg",
      tdk: 14162500275,
      tck: 2832500055,
      tenhoatchat: "Meropenem",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Cloramphenicol",
      tdk: 31500000,
      tck: 6300000,
      tenhoatchat: "Cloramphenicol",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Cloramphenicol",
      tdk: 156250000,
      tck: 31250000,
      tenhoatchat: "Cloramphenicol",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Cloramphenicol",
      tdk: 619562500,
      tck: 123912500,
      tenhoatchat: "Cloramphenicol",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Cloramphenicol",
      tdk: 703125006,
      tck: 140625001,
      tenhoatchat: "Chloramphenicol",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Cloramphenicol",
      tdk: 843750000,
      tck: 168750000,
      tenhoatchat: "Cloramphenicol",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Cloramphenicol",
      tdk: 6100000000,
      tck: 1220000000,
      tenhoatchat: "Cloramphenicol",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Cloramphenicol",
      tdk: 15703125000,
      tck: 3140625000,
      tenhoatchat: "Cloramphenicol",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Tartriakson (Ceftriaxon) 1g",
      tdk: 24124996875,
      tck: 4824999375,
      tenhoatchat: "Ceftriaxon",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Toxaxine Inj",
      tdk: 24062500000,
      tck: 4812500000,
      tenhoatchat: "Tranexamic acid",
      nhom: "Thuốc ảnh hưởng đến Máu"
    },
    {
      tenthuoc: "Cozaar",
      tdk: 24045697644,
      tck: 4809139529,
      tenhoatchat: "Losartan",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Hometex",
      tdk: 23750000000,
      tck: 4750000000,
      tenhoatchat: "Cao đặc actiso",
      nhom: "Thuốc Đông dược"
    },
    {
      tenthuoc: "Thiên sứ hộ tâm đan",
      tdk: 23520002205,
      tck: 4704000441,
      tenhoatchat: "Cao Đan Sâm, Cao Tam Thất, Borneol",
      nhom: "Thuốc Đông dược"
    },
    {
      tenthuoc: "Mecefix (cefixim 150mg)",
      tdk: 23437500000,
      tck: 4687500000,
      tenhoatchat: "Cefixim",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Oflovid",
      tdk: 139680000,
      tck: 27936000,
      tenhoatchat: "Ofloxacin",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Oflovid",
      tdk: 3492000000,
      tck: 698400000,
      tenhoatchat: "Ofloxacin",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Oflovid",
      tdk: 9079200005,
      tck: 1815840001,
      tenhoatchat: "Ofloxacin",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Oflovid",
      tdk: 10476000000,
      tck: 2095200000,
      tenhoatchat: "Ofloxacin",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Concor Tab 5mg 30's",
      tdk: 10792500075,
      tck: 2158500015,
      tenhoatchat: "Bisoprolol",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Concor Tab 5mg 30's",
      tdk: 12295080000,
      tck: 2459016000,
      tenhoatchat: "Bisoprolol fumarate 5mg",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Campto Inj 100mg 5ml",
      tdk: 22995250000,
      tck: 4599050000,
      tenhoatchat: "Irinotecan",
      nhom: "Thuốc Ung Thư - Miễn Dịch"
    },
    {
      tenthuoc: "LOVENOX 60mg Inj B/ 2 syringes x  0,6ml",
      tdk: 22915507500,
      tck: 4583101500,
      tenhoatchat: "Enoxaparin",
      nhom: "Thuốc ảnh hưởng đến Máu"
    },
    {
      tenthuoc: "Lisonorm 5mg+10mg",
      tdk: 22554000000,
      tck: 4510800000,
      tenhoatchat: "Amlodipin + Lisinopril",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "EDNYT 10MG Tablet(s) nén",
      tdk: 22425373750,
      tck: 4485074750,
      tenhoatchat: "Enalapril",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Albis",
      tdk: 9562500000,
      tck: 1912500000,
      tenhoatchat: "Ranitidin + bismuth + sucralfat",
      nhom: "Thuốc Dạ dày - Ruột"
    },
    {
      tenthuoc: "Albis",
      tdk: 12750000000,
      tck: 2550000000,
      tenhoatchat: "Ranitidine + Bismuth + Sucralfate",
      nhom: "Thuốc Dạ dày - Ruột"
    },
    {
      tenthuoc: "Paracetamol",
      tdk: 430000000,
      tck: 86000000,
      tenhoatchat: "Paracetamol",
      nhom: "Thuốc Hạ nhiệt - Giảm đau"
    },
    {
      tenthuoc: "Paracetamol",
      tdk: 516000000,
      tck: 103200000,
      tenhoatchat: "Paracetamol",
      nhom: "Thuốc Hạ nhiệt - Giảm đau"
    },
    {
      tenthuoc: "Paracetamol",
      tdk: 645000000,
      tck: 129000000,
      tenhoatchat: "Paracetamol",
      nhom: "Thuốc Hạ nhiệt - Giảm đau"
    },
    {
      tenthuoc: "Paracetamol",
      tdk: 645000000,
      tck: 129000000,
      tenhoatchat: "Paracetamol",
      nhom: "Thuốc Hạ nhiệt - Giảm đau"
    },
    {
      tenthuoc: "Paracetamol",
      tdk: 652500000,
      tck: 130500000,
      tenhoatchat: "Paracetamol",
      nhom: "Thuốc Hạ nhiệt - Giảm đau"
    },
    {
      tenthuoc: "Paracetamol",
      tdk: 787500000,
      tck: 157500000,
      tenhoatchat: "Paracetamol",
      nhom: "Thuốc Hạ nhiệt - Giảm đau"
    },
    {
      tenthuoc: "Paracetamol",
      tdk: 1012500000,
      tck: 202500000,
      tenhoatchat: "Paracetamol",
      nhom: "Thuốc Hạ nhiệt - Giảm đau"
    },
    {
      tenthuoc: "Paracetamol",
      tdk: 1032000000,
      tck: 206400000,
      tenhoatchat: "Paracetamol",
      nhom: "Thuốc Hạ nhiệt - Giảm đau"
    },
    {
      tenthuoc: "Paracetamol",
      tdk: 1068750000,
      tck: 213750000,
      tenhoatchat: "Paracetamol",
      nhom: "Thuốc Hạ nhiệt - Giảm đau"
    },
    {
      tenthuoc: "Paracetamol",
      tdk: 1125000000,
      tck: 225000000,
      tenhoatchat: "Paracetamol",
      nhom: "Thuốc Hạ nhiệt - Giảm đau"
    },
    {
      tenthuoc: "Paracetamol",
      tdk: 1181250000,
      tck: 236250000,
      tenhoatchat: "Paracetamol",
      nhom: "Thuốc Hạ nhiệt - Giảm đau"
    },
    {
      tenthuoc: "Paracetamol",
      tdk: 1316250000,
      tck: 146250000,
      tenhoatchat: "Paracetamol",
      nhom: "Thuốc Hạ nhiệt - Giảm đau"
    },
    {
      tenthuoc: "Paracetamol",
      tdk: 1462500000,
      tck: 292500000,
      tenhoatchat: "Paracetamol",
      nhom: "Thuốc Hạ nhiệt - Giảm đau"
    },
    {
      tenthuoc: "Paracetamol",
      tdk: 1642600000,
      tck: 817000000,
      tenhoatchat: "Paracetamol",
      nhom: "Thuốc Hạ nhiệt - Giảm đau"
    },
    {
      tenthuoc: "Paracetamol",
      tdk: 1687500000,
      tck: 337500000,
      tenhoatchat: "Paracetamol",
      nhom: "Thuốc Hạ nhiệt - Giảm đau"
    },
    {
      tenthuoc: "Paracetamol",
      tdk: 2025000000,
      tck: 405000000,
      tenhoatchat: "Paracetamol",
      nhom: "Thuốc Hạ nhiệt - Giảm đau"
    },
    {
      tenthuoc: "Paracetamol",
      tdk: 2250000000,
      tck: 450000000,
      tenhoatchat: "Paracetamol",
      nhom: "Thuốc Hạ nhiệt - Giảm đau"
    },
    {
      tenthuoc: "Paracetamol",
      tdk: 2756250459,
      tck: 551250092,
      tenhoatchat: "Paracetamol",
      nhom: "Thuốc Hạ nhiệt - Giảm đau"
    },
    {
      tenthuoc: "Bactirid 40ml",
      tdk: 22125000000,
      tck: 4425000000,
      tenhoatchat: "Cefixim",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Adalat LA Tab 30mg 30's",
      tdk: 4608825000,
      tck: 921765000,
      tenhoatchat: "Nifedipin",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Adalat LA Tab 30mg 30's",
      tdk: 5885115000,
      tck: 1177023000,
      tenhoatchat: "Nifedipin",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Adalat LA Tab 30mg 30's",
      tdk: 11628420000,
      tck: 2325684000,
      tenhoatchat: "Nifedipin",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Vitamin B1+B6+B12",
      tdk: 1320000000,
      tck: 264000000,
      tenhoatchat: "Vitamin B1+ vitamin B6+ vitamin B12 (115mg+115mg+50mcg)",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Vitamin B1+B6+B12",
      tdk: 3300000000,
      tck: 660000000,
      tenhoatchat: "Bitamin B1+VitaminB6+VitaminB12",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Vitamin B1+B6+B12",
      tdk: 17483400000,
      tck: 3496680000,
      tenhoatchat: "vitamin b1+b6+b12",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Bisoprolol fumarat",
      tdk: 21945924000,
      tck: 4389184800,
      tenhoatchat: "Bisoprolol",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Propofol 1%/20mL.",
      tdk: 21853125000,
      tck: 4370625000,
      tenhoatchat: "Propofol",
      nhom: "Thuốc Gây tê - Gây mê"
    },
    {
      tenthuoc: "Zitromax 600mg/15ml",
      tdk: 21747749625,
      tck: 4349549925,
      tenhoatchat: "Azithromycin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Heparin 25000UI/5ml",
      tdk: 21735000000,
      tck: 4347000000,
      tenhoatchat: "Heparin (Natri)",
      nhom: "Thuốc ảnh hưởng đến Máu"
    },
    {
      tenthuoc: "Tartriakson 1g",
      tdk: 4825000000,
      tck: 965000000,
      tenhoatchat: "Ceftriaxon",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Tartriakson 1g",
      tdk: 4825000000,
      tck: 965000000,
      tenhoatchat: "Ceftriaxon",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Tartriakson 1g",
      tdk: 12062500000,
      tck: 2412500000,
      tenhoatchat: "Ceftriaxon",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "CIPROFLOXACIN KABI",
      tdk: 21656250000,
      tck: 4331250000,
      tenhoatchat: "Ciprofloxacin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Oflovid 3mg/ml x 5ml",
      tdk: 21650400000,
      tck: 4330080000,
      tenhoatchat: "Ofloxacin",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "PLAVIX 75mg B/ 1bl x 14 Tabs",
      tdk: 30625000,
      tck: 6125000,
      tenhoatchat: "Clopidogrel",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "PLAVIX 75mg B/ 1bl x 14 Tabs",
      tdk: 1275715000,
      tck: 255143000,
      tenhoatchat: "Clopidogrel",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "PLAVIX 75mg B/ 1bl x 14 Tabs",
      tdk: 6998208000,
      tck: 1399641600,
      tenhoatchat: "Clopidogrel",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "PLAVIX 75mg B/ 1bl x 14 Tabs",
      tdk: 13012293000,
      tck: 2602458600,
      tenhoatchat: "Clopidogrel",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Travatan",
      tdk: 4415232500,
      tck: 883046500,
      tenhoatchat: "Travoprost",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Travatan",
      tdk: 16714808750,
      tck: 3342961750,
      tenhoatchat: "Travoprost",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Vitamin B1",
      tdk: 14500000,
      tck: 14500000,
      tenhoatchat: "vitamin B1",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Vitamin B1",
      tdk: 275000000,
      tck: 55000000,
      tenhoatchat: "vitamin B1",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Vitamin B1",
      tdk: 283500000,
      tck: 56700000,
      tenhoatchat: "vitamin B1",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Vitamin B1",
      tdk: 405000000,
      tck: 81000000,
      tenhoatchat: "vitamin B1",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Vitamin B1",
      tdk: 417500000,
      tck: 417500000,
      tenhoatchat: "vitamin B1",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Vitamin B1",
      tdk: 420000000,
      tck: 84000000,
      tenhoatchat: "vitamin B1",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Vitamin B1",
      tdk: 630000000,
      tck: 126000000,
      tenhoatchat: "vitamin B1",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Vitamin B1",
      tdk: 8350000000,
      tck: 1670000000,
      tenhoatchat: "vitamin B1",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Vitamin B1",
      tdk: 10332000000,
      tck: 2066400000,
      tenhoatchat: "vitamin B1",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Bidilucil 500",
      tdk: 21112500000,
      tck: 4222500000,
      tenhoatchat: "Meclofenoxat hydroclorid",
      nhom: "Thuốc ảnh hưởng đến Máu"
    },
    {
      tenthuoc: "Lidocain 2% 10ml",
      tdk: 9253125000,
      tck: 1850625000,
      tenhoatchat: "Lidocain (hydroclorid)",
      nhom: "Thuốc Gây tê - Gây mê"
    },
    {
      tenthuoc: "Lidocain 2% 10ml",
      tdk: 11812500000,
      tck: 2362500000,
      tenhoatchat: "Lidocain",
      nhom: "Thuốc Gây tê - Gây mê"
    },
    {
      tenthuoc: "Enamigal",
      tdk: 974025000,
      tck: 194805000,
      tenhoatchat: "Enalapril",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Enamigal",
      tdk: 974025000,
      tck: 194805000,
      tenhoatchat: "Enalapril Maleat",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Enamigal",
      tdk: 9262012500,
      tck: 1852402500,
      tenhoatchat: "Enalapril",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Enamigal",
      tdk: 9745125750,
      tck: 1949025150,
      tenhoatchat: "Enalapril",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Mixtard Flexpen 30/70 100UI/ml,3ml",
      tdk: 20789848800,
      tck: 4157969760,
      tenhoatchat: "Insulin trộn (M) 30:70",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Enap (Enalapril) 5mg",
      tdk: 2410012500,
      tck: 482002500,
      tenhoatchat: "Enalapril",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Enap (Enalapril) 5mg",
      tdk: 3614995125,
      tck: 722999025,
      tenhoatchat: "Enalapril",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Enap (Enalapril) 5mg",
      tdk: 14580258000,
      tck: 2916051600,
      tenhoatchat: "Enalapril",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Tobradex 0.3% 3.5g",
      tdk: 124750000,
      tck: 24950000,
      tenhoatchat: "Tobramycin+Dexamethason",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Tobradex 0.3% 3.5g",
      tdk: 623750000,
      tck: 124750000,
      tenhoatchat: "Tobramycin + Dexamethason",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Tobradex 0.3% 3.5g",
      tdk: 5875725000,
      tck: 1175145000,
      tenhoatchat: "Tobramycin + Dexamethason",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Tobradex 0.3% 3.5g",
      tdk: 13722225000,
      tck: 2744445000,
      tenhoatchat: "Tobramycin + Dexamethason",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Plofed (Propofol) 1%/20ml",
      tdk: 1262500050,
      tck: 252500010,
      tenhoatchat: "Propofol",
      nhom: "Thuốc Gây tê - Gây mê"
    },
    {
      tenthuoc: "Plofed (Propofol) 1%/20ml",
      tdk: 18937499963,
      tck: 3787499993,
      tenhoatchat: "Propofol",
      nhom: "Thuốc Gây tê - Gây mê"
    },
    {
      tenthuoc: "Panadol caplet 500mg",
      tdk: 9558675000,
      tck: 1911735000,
      tenhoatchat: "Paracetamol (acetaminophen)",
      nhom: "Thuốc Hạ nhiệt - Giảm đau"
    },
    {
      tenthuoc: "Panadol caplet 500mg",
      tdk: 10511031300,
      tck: 2102206260,
      tenhoatchat: "Paracetamol",
      nhom: "Thuốc Hạ nhiệt - Giảm đau"
    },
    {
      tenthuoc: "Bloza (Losartan) 50mg",
      tdk: 4291875000,
      tck: 858375000,
      tenhoatchat: "Losartan",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Bloza (Losartan) 50mg",
      tdk: 15750000000,
      tck: 3150000000,
      tenhoatchat: "Losartan",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Duotrav",
      tdk: 20000000000,
      tck: 4000000000,
      tenhoatchat: "Travoprost+Timolol",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "3bpluzs (vitamin b1+b6+b12)",
      tdk: 19987500000,
      tck: 3997500000,
      tenhoatchat: "vitamin b1+b6+b12",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Utrogestan 100mg",
      tdk: 682500000,
      tck: 136500000,
      tenhoatchat: "Progesteron",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Utrogestan 100mg",
      tdk: 866250000,
      tck: 173250000,
      tenhoatchat: "Progesteron",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Utrogestan 100mg",
      tdk: 1312500000,
      tck: 262500000,
      tenhoatchat: "Progesterone",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Utrogestan 100mg",
      tdk: 1548750000,
      tck: 309750000,
      tenhoatchat: "Progesteron",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Utrogestan 100mg",
      tdk: 4042500000,
      tck: 808500000,
      tenhoatchat: "Progesteron",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Utrogestan 100mg",
      tdk: 4357500000,
      tck: 871500000,
      tenhoatchat: "Progesteron",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Utrogestan 100mg",
      tdk: 7061250000,
      tck: 1412250000,
      tenhoatchat: "Progesteron",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Tobradex",
      tdk: 31187500,
      tck: 6237500,
      tenhoatchat: "Tobramycin + Dexamethasone",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Tobradex",
      tdk: 991742625,
      tck: 198348525,
      tenhoatchat: "Tobramycin + Dexamethasone",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Tobradex",
      tdk: 1247500000,
      tck: 249500000,
      tenhoatchat: "Tobramycin + Dexamethasone",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Tobradex",
      tdk: 1691212500,
      tck: 338242500,
      tenhoatchat: "Tobramycin+Dexamethason",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Tobradex",
      tdk: 3382425000,
      tck: 676485000,
      tenhoatchat: "Tobramycin + Dexamethason",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Tobradex",
      tdk: 5637375000,
      tck: 1127475000,
      tenhoatchat: "Tobramycin + Dexamethasone",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Tobradex",
      tdk: 6861250000,
      tck: 1372250000,
      tenhoatchat: "Tobramycin + Dexamethasone",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Tobrin 0,3%/5ml",
      tdk: 243750000,
      tck: 48750000,
      tenhoatchat: "Tobramycin",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Tobrin 0,3%/5ml",
      tdk: 4875000113,
      tck: 975000023,
      tenhoatchat: "Tobramycin",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Tobrin 0,3%/5ml",
      tdk: 14625000000,
      tck: 2925000000,
      tenhoatchat: "Tobramycin",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Paclitaxel 100mg/16,7ml",
      tdk: 19696649963,
      tck: 3939329993,
      tenhoatchat: "Paclitaxel",
      nhom: "Thuốc Ung Thư - Miễn Dịch"
    },
    {
      tenthuoc: "Morphini Sulfas Wzf 0,1% 2mg 2ml Spinal 2mg/2ml",
      tdk: 19687500000,
      tck: 3937500000,
      tenhoatchat: "Morphin sulfat",
      nhom: "Thuốc Gây tê - Gây mê"
    },
    {
      tenthuoc: "Curam 625mg",
      tdk: 19666400775,
      tck: 3933280155,
      tenhoatchat: "Amoxicilin+Acid Clavulanic",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Dalacin C Cap 300mg 16's",
      tdk: 698926000,
      tck: 139785200,
      tenhoatchat: "Clindamycin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Dalacin C Cap 300mg 16's",
      tdk: 7034352000,
      tck: 1406870400,
      tenhoatchat: "Clindamycin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Dalacin C Cap 300mg 16's",
      tdk: 11836650000,
      tck: 2367330000,
      tenhoatchat: "Clindamycin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "DUOPLAVIN 75/100mg B/ 3bls  x 10 Tabs",
      tdk: 30000000,
      tck: 6000000,
      tenhoatchat: "Clopidogrel + acetylsalicylic",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "DUOPLAVIN 75/100mg B/ 3bls  x 10 Tabs",
      tdk: 7810500000,
      tck: 1562100000,
      tenhoatchat: "Clopidogrel 75mg; Acid acetylsalicylic 100mg",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "DUOPLAVIN 75/100mg B/ 3bls  x 10 Tabs",
      tdk: 11715749719,
      tck: 2343149944,
      tenhoatchat: "Clopidogrel+ Acid Acetylsalicylic",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Humulin 70/30",
      tdk: 2643750000,
      tck: 528750000,
      tenhoatchat: "Insulin người (rDNA)",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Humulin 70/30",
      tdk: 16687499994,
      tck: 3337499999,
      tenhoatchat: "Insulin (30/70)",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Neo-Tergynan",
      tdk: 8249999625,
      tck: 1649999925,
      tenhoatchat: "Metronidazol +Neomycin+ Nystatin",
      nhom: "Thuốc chống Nấm"
    },
    {
      tenthuoc: "Neo-Tergynan",
      tdk: 10999999500,
      tck: 2199999900,
      tenhoatchat: "Metronidazol +Neomycin+ Nystatin",
      nhom: "Thuốc chống Nấm"
    },
    {
      tenthuoc: "Mixtard 30 flexpen (insulin human)",
      tdk: 19249860000,
      tck: 3849972000,
      tenhoatchat: "Insulin người, rADN",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Tobrex Eye Ointment 3,5g",
      tdk: 618737500,
      tck: 123747500,
      tenhoatchat: "Tobramycin",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Tobrex Eye Ointment 3,5g",
      tdk: 1237475000,
      tck: 247495000,
      tenhoatchat: "Tobramycin",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Tobrex Eye Ointment 3,5g",
      tdk: 1856212500,
      tck: 371242500,
      tenhoatchat: "Tobramycin",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Tobrex Eye Ointment 3,5g",
      tdk: 3093687500,
      tck: 618737500,
      tenhoatchat: "Tobramycin",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Tobrex Eye Ointment 3,5g",
      tdk: 3774298750,
      tck: 754859750,
      tenhoatchat: "Tobramycin",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Tobrex Eye Ointment 3,5g",
      tdk: 8662325000,
      tck: 1732465000,
      tenhoatchat: "Tobramycin",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Lovenox 0,4ml,4000UI",
      tdk: 19210723088,
      tck: 3842144618,
      tenhoatchat: "Enoxaparin (natri)",
      nhom: "Thuốc ảnh hưởng đến Máu"
    },
    {
      tenthuoc: "Vigadexa 5ml",
      tdk: 18989820000,
      tck: 3797964000,
      tenhoatchat: "Moxifloxacin + Dexamethason",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Plofed",
      tdk: 18937500000,
      tck: 3787500000,
      tenhoatchat: "Propofol",
      nhom: "Thuốc Gây tê - Gây mê"
    },
    {
      tenthuoc: "Levonor 1mg/ml H10",
      tdk: 1968750000,
      tck: 393750000,
      tenhoatchat: "Nor epinephrin (Nor adrenalin)",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Levonor 1mg/ml H10",
      tdk: 16800000000,
      tck: 3360000000,
      tenhoatchat: "Nor epinephrin (Nor adrenalin)",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Mecefix (Cefixim) 150mg",
      tdk: 18749999625,
      tck: 3749999925,
      tenhoatchat: "Cefixim",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Heparin Belmed",
      tdk: 18699999998,
      tck: 3740000000,
      tenhoatchat: "Heparin",
      nhom: "Thuốc ảnh hưởng đến Máu"
    },
    {
      tenthuoc: "Clocardigel",
      tdk: 18659200000,
      tck: 3731840000,
      tenhoatchat: "Clopidogrel",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Unasyn",
      tdk: 825000000,
      tck: 165000000,
      tenhoatchat: "Ampicillin + Sulbactam",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Unasyn",
      tdk: 7425000000,
      tck: 1485000000,
      tenhoatchat: "Ampicilin sodium + sulbactam sodium",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Unasyn",
      tdk: 10233750000,
      tck: 2046750000,
      tenhoatchat: "Amoxicilin+Sulbactam",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Systane Ultra 5ml",
      tdk: 18405318750,
      tck: 3681063750,
      tenhoatchat: "Polyethylen glycol+ Propylen glycol",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Thiên sứ hộ tâm đan",
      tdk: 18375000000,
      tck: 3675000000,
      tenhoatchat: "Cao đan sâm, Cao tam thất, Borneol",
      nhom: "Thuốc Đông dược"
    },
    {
      tenthuoc: "Seduxen 5mg",
      tdk: 32812500,
      tck: 6562500,
      tenhoatchat: "Diazepam",
      nhom: "Thuốc chống Co Giật"
    },
    {
      tenthuoc: "Seduxen 5mg",
      tdk: 109250000,
      tck: 21850000,
      tenhoatchat: "Diazepam",
      nhom: "Thuốc chống Co Giật"
    },
    {
      tenthuoc: "Seduxen 5mg",
      tdk: 118125000,
      tck: 23625000,
      tenhoatchat: "Diazepam",
      nhom: "Thuốc chống Co Giật"
    },
    {
      tenthuoc: "Seduxen 5mg",
      tdk: 177187500,
      tck: 35437500,
      tenhoatchat: "Diazepam",
      nhom: "Thuốc chống Co Giật"
    },
    {
      tenthuoc: "Seduxen 5mg",
      tdk: 177187500,
      tck: 35437500,
      tenhoatchat: "Diazepam",
      nhom: "Thuốc chống Co Giật"
    },
    {
      tenthuoc: "Seduxen 5mg",
      tdk: 177375000,
      tck: 35475000,
      tenhoatchat: "Diazepam",
      nhom: "Thuốc chống Co Giật"
    },
    {
      tenthuoc: "Seduxen 5mg",
      tdk: 236250000,
      tck: 47250000,
      tenhoatchat: "Diazepam 5mg",
      nhom: "Thuốc chống Co Giật"
    },
    {
      tenthuoc: "Seduxen 5mg",
      tdk: 324843750,
      tck: 64968750,
      tenhoatchat: "Diazepam",
      nhom: "Thuốc chống Co Giật"
    },
    {
      tenthuoc: "Seduxen 5mg",
      tdk: 354375000,
      tck: 70875000,
      tenhoatchat: "Diazepam",
      nhom: "Thuốc chống Co Giật"
    },
    {
      tenthuoc: "Seduxen 5mg",
      tdk: 393750000,
      tck: 78750000,
      tenhoatchat: "Diazepam",
      nhom: "Thuốc chống Co Giật"
    },
    {
      tenthuoc: "Seduxen 5mg",
      tdk: 442968750,
      tck: 88593750,
      tenhoatchat: "Diazepam",
      nhom: "Thuốc chống Co Giật"
    },
    {
      tenthuoc: "Seduxen 5mg",
      tdk: 472500000,
      tck: 94500000,
      tenhoatchat: "Diazepam",
      nhom: "Thuốc chống Co Giật"
    },
    {
      tenthuoc: "Seduxen 5mg",
      tdk: 591250000,
      tck: 118250000,
      tenhoatchat: "Diazepam",
      nhom: "Thuốc chống Co Giật"
    },
    {
      tenthuoc: "Seduxen 5mg",
      tdk: 1181250000,
      tck: 236250000,
      tenhoatchat: "Diazepam",
      nhom: "Thuốc chống Co Giật"
    },
    {
      tenthuoc: "Seduxen 5mg",
      tdk: 1771875000,
      tck: 354375000,
      tenhoatchat: "Diazepam",
      nhom: "Thuốc chống Co Giật"
    },
    {
      tenthuoc: "Seduxen 5mg",
      tdk: 2126250000,
      tck: 425250000,
      tenhoatchat: "Diazepam",
      nhom: "Thuốc chống Co Giật"
    },
    {
      tenthuoc: "Seduxen 5mg",
      tdk: 3937500000,
      tck: 787500000,
      tenhoatchat: "Diazepam",
      nhom: "Thuốc chống Co Giật"
    },
    {
      tenthuoc: "Seduxen 5mg",
      tdk: 5610937500,
      tck: 1122187500,
      tenhoatchat: "Diazepam",
      nhom: "Thuốc chống Co Giật"
    },
    {
      tenthuoc: "I.V-globulin S 2500mg",
      tdk: 18187500000,
      tck: 3637500000,
      tenhoatchat: "I.V-globulin S 2500mg",
      nhom: "Chế phẩm từ Máu"
    },
    {
      tenthuoc: "Ambelin 5mg",
      tdk: 1260000000,
      tck: 252000000,
      tenhoatchat: "Amlodipin",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Ambelin 5mg",
      tdk: 1260000000,
      tck: 252000000,
      tenhoatchat: "Amlodipine",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Ambelin 5mg",
      tdk: 1418630456,
      tck: 283726091,
      tenhoatchat: "Amlodipine",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Ambelin 5mg",
      tdk: 3570000000,
      tck: 714000000,
      tenhoatchat: "Amlodipin",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Ambelin 5mg",
      tdk: 10500000000,
      tck: 2100000000,
      tenhoatchat: "Amlodipin",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Asgizole (Esomeprazol) 40mg",
      tdk: 18000000375,
      tck: 3600000075,
      tenhoatchat: "Esomeprazole",
      nhom: "Thuốc Dạ dày - Ruột"
    },
    {
      tenthuoc: "Oflovid Oint",
      tdk: 17576500000,
      tck: 3515300000,
      tenhoatchat: "Ofloxacin",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Taxotere (Docetaxel) 20mg/1ml",
      tdk: 17520300000,
      tck: 3504060000,
      tenhoatchat: "Docetaxel",
      nhom: "Thuốc Ung Thư - Miễn Dịch"
    },
    {
      tenthuoc: "Campto (Irinotecan) 40mg",
      tdk: 17475368708,
      tck: 3495073742,
      tenhoatchat: "Irinotecan",
      nhom: "Thuốc Ung Thư - Miễn Dịch"
    },
    {
      tenthuoc: "Transamin Injection",
      tdk: 468750000,
      tck: 93750000,
      tenhoatchat: "Tranexamic acid",
      nhom: "Thuốc ảnh hưởng đến Máu"
    },
    {
      tenthuoc: "Transamin Injection",
      tdk: 875000000,
      tck: 175000000,
      tenhoatchat: "Tranexamic acid",
      nhom: "Thuốc ảnh hưởng đến Máu"
    },
    {
      tenthuoc: "Transamin Injection",
      tdk: 2625000000,
      tck: 525000000,
      tenhoatchat: "Tranexamic acid",
      nhom: "Thuốc ảnh hưởng đến Máu"
    },
    {
      tenthuoc: "Transamin Injection",
      tdk: 6000000000,
      tck: 1200000000,
      tenhoatchat: "Tranexamic acid",
      nhom: "Thuốc ảnh hưởng đến Máu"
    },
    {
      tenthuoc: "Transamin Injection",
      tdk: 7350000000,
      tck: 1470000000,
      tenhoatchat: "Tranexamic acid",
      nhom: "Thuốc ảnh hưởng đến Máu"
    },
    {
      tenthuoc: "Ebitac",
      tdk: 17250000000,
      tck: 3450000000,
      tenhoatchat: "Enalapril maleat+ Hydroclorothiazid",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Medoclav 1g 875mg+ 125mg",
      tdk: 17062500000,
      tck: 3412500000,
      tenhoatchat: "Amoxycillin + Acid Clavulanic",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Clarithromycin 500mg",
      tdk: 3382499938,
      tck: 676499988,
      tenhoatchat: "Clarithromycin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Clarithromycin 500mg",
      tdk: 3727500000,
      tck: 745500000,
      tenhoatchat: "Clarithromycin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Clarithromycin 500mg",
      tdk: 9940000000,
      tck: 1988000000,
      tenhoatchat: "Clarithromycin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "VG5",
      tdk: 4839975000,
      tck: 967995000,
      tenhoatchat: "Cao diệp hạ châu, cao nhân trần, cao nhọ nồi, cao râu bắp",
      nhom: "Thuốc Đông dược"
    },
    {
      tenthuoc: "VG5",
      tdk: 12100000500,
      tck: 2420000100,
      tenhoatchat: "Cao diệp hạ châu, cao nhân trần, cao cỏ nhọ nồi, cao râu bắp",
      nhom: "Thuốc Đông dược"
    },
    {
      tenthuoc: "Levobact 0,5% eye drops",
      tdk: 16500005500,
      tck: 3300001100,
      tenhoatchat: "Levofloxacin",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Tobiwel",
      tdk: 3093750000,
      tck: 618750000,
      tenhoatchat: "Natri chondroitin sulfat + retinol palmitat + cholin hydrotartrat + riboflavin (vitamin B2) + thiamin hydroclorid (vitamin B1)",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Tobiwel",
      tdk: 4125000000,
      tck: 825000000,
      tenhoatchat: "Natri chondroitin sulfat + retinol palmitat + cholin hydrotartrat + riboflavin (vitamin B2) + thiamin hydroclorid (vitamin B1)",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Tobiwel",
      tdk: 4125000000,
      tck: 825000000,
      tenhoatchat: "Natri chondroitin sulfat + retinol palmitat + cholin hydrotartrat + riboflavin (vitamin B2) + thiamin hydroclorid (vitamin B1)",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Tobiwel",
      tdk: 5156250000,
      tck: 1031250000,
      tenhoatchat: "Natri chondroitin sulfat + retinol palmitat + cholin hydrotartrat + riboflavin (vitamin B2) + thiamin hydroclorid (vitamin B1)",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Enap 5",
      tdk: 512629688,
      tck: 102525938,
      tenhoatchat: "Enalapril",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Enap 5",
      tdk: 2590750000,
      tck: 518150000,
      tenhoatchat: "Enalapril",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Enap 5",
      tdk: 13375500000,
      tck: 2675100000,
      tenhoatchat: "Enalapril Maleat",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Aerius Syr 0.5mg/ ml 1's",
      tdk: 493125000,
      tck: 98625000,
      tenhoatchat: "Desloratadine",
      nhom: "Thuốc chống Dị ứng"
    },
    {
      tenthuoc: "Aerius Syr 0.5mg/ ml 1's",
      tdk: 3945000000,
      tck: 789000000,
      tenhoatchat: "Desloratadin",
      nhom: "Thuốc chống Dị ứng"
    },
    {
      tenthuoc: "Aerius Syr 0.5mg/ ml 1's",
      tdk: 6903750000,
      tck: 1380750000,
      tenhoatchat: "Desloratadine",
      nhom: "Thuốc chống Dị ứng"
    },
    {
      tenthuoc: "Losartan 25mg",
      tdk: 11249988750,
      tck: 2249997750,
      tenhoatchat: "Losartan",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Vigamox 0,5%/5ml",
      tdk: 11249831250,
      tck: 2249966250,
      tenhoatchat: "Moxifloxacin",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Tranecid 250",
      tdk: 210000000,
      tck: 42000000,
      tenhoatchat: "Tranexamic acid",
      nhom: "Thuốc ảnh hưởng đến Máu"
    },
    {
      tenthuoc: "Tranecid 250",
      tdk: 525000000,
      tck: 105000000,
      tenhoatchat: "Acid Tranexamic",
      nhom: "Thuốc ảnh hưởng đến Máu"
    },
    {
      tenthuoc: "Tranecid 250",
      tdk: 2800000000,
      tck: 560000000,
      tenhoatchat: "Acid Tranexamic",
      nhom: "Thuốc ảnh hưởng đến Máu"
    },
    {
      tenthuoc: "Tranecid 250",
      tdk: 7693000000,
      tck: 1538600000,
      tenhoatchat: "Acid Tranexamic",
      nhom: "Thuốc ảnh hưởng đến Máu"
    },
    {
      tenthuoc: "Exforge HCT 10mg/160mg/12.5mg",
      tdk: 11090536275,
      tck: 2218107255,
      tenhoatchat: "Amlodipin+Valsartan+Hydrochlorothiazide",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Clarithromycin 250mg",
      tdk: 3106250000,
      tck: 621250000,
      tenhoatchat: "Clarithromycin 250mg",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Clarithromycin 250mg",
      tdk: 7837507125,
      tck: 1567501425,
      tenhoatchat: "Clarithromycin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Vitamin E",
      tdk: 65625000,
      tck: 13125000,
      tenhoatchat: "Vitamin E",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Vitamin E",
      tdk: 131250000,
      tck: 26250000,
      tenhoatchat: "Vitamin E",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Vitamin E",
      tdk: 151250000,
      tck: 30250000,
      tenhoatchat: "Vitamin E",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Vitamin E",
      tdk: 285000000,
      tck: 57000000,
      tenhoatchat: "Vitamin E",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Vitamin E",
      tdk: 328125000,
      tck: 65625000,
      tenhoatchat: "Vitamin E",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Vitamin E",
      tdk: 328125000,
      tck: 65625000,
      tenhoatchat: "Vitamin E",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Vitamin E",
      tdk: 1562500000,
      tck: 312500000,
      tenhoatchat: "Vitamin E",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Vitamin E",
      tdk: 1852500000,
      tck: 370500000,
      tenhoatchat: "Vitamin E",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Vitamin E",
      tdk: 2999999625,
      tck: 599999925,
      tenhoatchat: "Vitamin E",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Vitamin E",
      tdk: 3215625000,
      tck: 643125000,
      tenhoatchat: "Vitamin E",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Paclitaxel Ebewe",
      tdk: 10826399745,
      tck: 2165279949,
      tenhoatchat: "Paclitaxel",
      nhom: "Thuốc Ung Thư - Miễn Dịch"
    },
    {
      tenthuoc: "Medsamic 250mg/5ml",
      tdk: 10608239250,
      tck: 2121647850,
      tenhoatchat: "Tranexamic acid",
      nhom: "Thuốc ảnh hưởng đến Máu"
    },
    {
      tenthuoc: "Hoạt huyết dưỡng não Cebraton S",
      tdk: 1209000000,
      tck: 241800000,
      tenhoatchat: "Cao đinh lăng, cao bạch quả",
      nhom: "Thuốc Đông dược"
    },
    {
      tenthuoc: "Hoạt huyết dưỡng não Cebraton S",
      tdk: 9375000000,
      tck: 1875000000,
      tenhoatchat: "Cao đinh lăng +Cao bạch quả",
      nhom: "Thuốc Đông dược"
    },
    {
      tenthuoc: "Midactam",
      tdk: 10552500000,
      tck: 2110500000,
      tenhoatchat: "Ampicilin +Subactam",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Midactam 1,5g",
      tdk: 10552500000,
      tck: 2110500000,
      tenhoatchat: "Ampicilin +Sulbactam",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Novorapid Flexpen 100U/ml",
      tdk: 10546869375,
      tck: 2109373875,
      tenhoatchat: "Insulin tác dụng ngắn (S) (Insulin aspart)",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Diopolol (Bisoprolol) 5mg",
      tdk: 10500052500,
      tck: 2100010500,
      tenhoatchat: "Bisoprolol",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Transamin Injection 250mg /5ml",
      tdk: 10500000000,
      tck: 2100000000,
      tenhoatchat: "Tranexamic acid",
      nhom: "Thuốc ảnh hưởng đến Máu"
    },
    {
      tenthuoc: "Fabamox 500",
      tdk: 3937500000,
      tck: 787500000,
      tenhoatchat: "Amoxicilin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Fabamox 500",
      tdk: 6559875000,
      tck: 1311975000,
      tenhoatchat: "Amoxicilin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Klamentin 250",
      tdk: 3009600000,
      tck: 601920000,
      tenhoatchat: "Amoxicilin + Acidclavulanic",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Klamentin 250",
      tdk: 7481250000,
      tck: 1496250000,
      tenhoatchat: "Amoxicilin + Acid clavulanic",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Systane Ultra",
      tdk: 10367077500,
      tck: 2073415500,
      tenhoatchat: "Polyethylene glycol 400 + Propylen glycol",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Tobrex 0.3% 5ml",
      tdk: 1499962500,
      tck: 299992500,
      tenhoatchat: "Tobramycin",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Tobrex 0.3% 5ml",
      tdk: 8849778750,
      tck: 1769955750,
      tenhoatchat: "Tobramycin",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Anginovag 10ml",
      tdk: 927500000,
      tck: 185500000,
      tenhoatchat: "Beta-glycyrrhetinic acid + Dequalinium chlorid + Tyrothricin + Hydrocortison acetat + Lidocain hydrochlorid",
      nhom: "Thuốc chống Dị ứng"
    },
    {
      tenthuoc: "Anginovag 10ml",
      tdk: 9274999913,
      tck: 1854999983,
      tenhoatchat: "Beta-glycyrrhetinic acid +dequalinium clorid + tyrothricin+ hydrocortison acetat +lidocain hydroclorid",
      nhom: "Thuốc chống Dị ứng"
    },
    {
      tenthuoc: "Esomeprazol Stada 40mg",
      tdk: 10199999250,
      tck: 2039999850,
      tenhoatchat: "Esomeprazole",
      nhom: "Thuốc Dạ dày - Ruột"
    },
    {
      tenthuoc: "Cesyrup",
      tdk: 3303750000,
      tck: 660750000,
      tenhoatchat: "Vitamin C",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Cesyrup",
      tdk: 6825000000,
      tck: 2625000000,
      tenhoatchat: "Vitamin C",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Erolin 10mg (Loratadin)",
      tdk: 10125000000,
      tck: 2025000000,
      tenhoatchat: "Loratadin",
      nhom: "Thuốc chống Dị ứng"
    },
    {
      tenthuoc: "Vitamin C",
      tdk: 168750000,
      tck: 33750000,
      tenhoatchat: "Vitamin C",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Vitamin C",
      tdk: 282000000,
      tck: 56400000,
      tenhoatchat: "Vitamin C",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Vitamin C",
      tdk: 337500000,
      tck: 67500000,
      tenhoatchat: "Vitamin C",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Vitamin C",
      tdk: 421875000,
      tck: 84375000,
      tenhoatchat: "Vitamin C",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Vitamin C",
      tdk: 675000000,
      tck: 135000000,
      tenhoatchat: "Vitamin C",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Vitamin C",
      tdk: 843750000,
      tck: 168750000,
      tenhoatchat: "Vitamin C",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Vitamin C",
      tdk: 1096875000,
      tck: 219375000,
      tenhoatchat: "Vitamin C",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Vitamin C",
      tdk: 2531250000,
      tck: 506250000,
      tenhoatchat: "Vitamin C",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Vitamin C",
      tdk: 3712499175,
      tck: 742499835,
      tenhoatchat: "Vitamin C",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Mediplex",
      tdk: 1260000000,
      tck: 252000000,
      tenhoatchat: "Aciclovir",
      nhom: "Thuốc chống Virus"
    },
    {
      tenthuoc: "Mediplex",
      tdk: 2520000000,
      tck: 504000000,
      tenhoatchat: "Aciclovir",
      nhom: "Thuốc chống Virus"
    },
    {
      tenthuoc: "Mediplex",
      tdk: 6287400000,
      tck: 1257480000,
      tenhoatchat: "Aciclovir",
      nhom: "Thuốc chống Virus"
    },
    {
      tenthuoc: "Cefdyvax-200",
      tdk: 2875000000,
      tck: 575000000,
      tenhoatchat: "Cefixim",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Cefdyvax-200",
      tdk: 7187500000,
      tck: 1437500000,
      tenhoatchat: "Cefixim",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Losec Mups Tab",
      tdk: 10017875009,
      tck: 2003575002,
      tenhoatchat: "Omeprazole",
      nhom: "Thuốc Dạ dày - Ruột"
    },
    {
      tenthuoc: "Hydrocortison",
      tdk: 1330533750,
      tck: 266106750,
      tenhoatchat: "Hydrocortison",
      nhom: "Thuốc chống Dị ứng"
    },
    {
      tenthuoc: "Hydrocortison",
      tdk: 8662500000,
      tck: 1732500000,
      tenhoatchat: "Lidocain (hydroclorid) + Hydrocortison",
      nhom: "Thuốc chống Dị ứng"
    },
    {
      tenthuoc: "Transamin 250mg/5ml",
      tdk: 9975000000,
      tck: 1995000000,
      tenhoatchat: "Tranexamic acid",
      nhom: "Thuốc ảnh hưởng đến Máu"
    },
    {
      tenthuoc: "Amlaxopin 10mg",
      tdk: 9965025000,
      tck: 1993005000,
      tenhoatchat: "Amlodipin",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Bestdocel 20",
      tdk: 9843750000,
      tck: 1968750000,
      tenhoatchat: "Docetaxel",
      nhom: "Thuốc Ung Thư - Miễn Dịch"
    },
    {
      tenthuoc: "Hydrocortion- Lidocain- Richter",
      tdk: 9843750000,
      tck: 1968750000,
      tenhoatchat: "Hydrocortison acetat + Lidocain (Hydroclorid)",
      nhom: "Thuốc Gây tê - Gây mê"
    },
    {
      tenthuoc: "Refresh Tears Lubricant eye Drops 15ml",
      tdk: 400637500,
      tck: 80127500,
      tenhoatchat: "Natri carboxymethylcellulose",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Refresh Tears Lubricant eye Drops 15ml",
      tdk: 2403825000,
      tck: 480765000,
      tenhoatchat: "Natricarboxy methylcellulose",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Refresh Tears Lubricant eye Drops 15ml",
      tdk: 6810837500,
      tck: 1362167500,
      tenhoatchat: "Natri carboxy methylcellulose",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Imidu 60mg",
      tdk: 9602696250,
      tck: 1920539250,
      tenhoatchat: "Isosorbid 5 - mononitrat",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Nexium 40 mg",
      tdk: 9597499438,
      tck: 1919499888,
      tenhoatchat: "Esomeprazole",
      nhom: "Thuốc Dạ dày - Ruột"
    },
    {
      tenthuoc: "Cerecaps(Hoạt huyết dưỡng não )",
      tdk: 9551250000,
      tck: 3183750000,
      tenhoatchat: "Cao khô hỗn hợp (tương ứng với :Hồng hoaĐương quyXuyên khungSinh địaCam thảoXích thượcSài hồChỉ xácNgưu tất) Bạch quả",
      nhom: "Thuốc Đông dược"
    },
    {
      tenthuoc: "Bổ gan P/H",
      tdk: 1112500000,
      tck: 222500000,
      tenhoatchat: "Cao đặc Diệp hạ châu; Cao đặc Bồ bồ; Cao đặc Chi tử.",
      nhom: "Thuốc Đông dược"
    },
    {
      tenthuoc: "Bổ gan P/H",
      tdk: 8343750000,
      tck: 1668750000,
      tenhoatchat: "Cao đặc Diệp hạ châu; Cao đặc Bồ bồ; Cao đặc Chi tử.",
      nhom: "Thuốc Đông dược"
    },
    {
      tenthuoc: "Maxitrol",
      tdk: 498750000,
      tck: 99750000,
      tenhoatchat: "Dexamethasone Sulfate + Neomycin Sulfate + Polymycin B Sulfate",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Maxitrol",
      tdk: 748125000,
      tck: 149625000,
      tenhoatchat: "Dexamethason+ Neomycin+ Polymycin B",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Maxitrol",
      tdk: 3712425000,
      tck: 742485000,
      tenhoatchat: "Dexamethason+ Neomycin+ Polymycin B",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Maxitrol",
      tdk: 4488750000,
      tck: 897750000,
      tenhoatchat: "Neomycin + polymyxin B + dexamethason",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Rocephin 250mg I.V 250mg",
      tdk: 9436875000,
      tck: 1887375000,
      tenhoatchat: "Ceftriaxone",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Hyzaar Tab",
      tdk: 9417374938,
      tck: 1883474988,
      tenhoatchat: "Losartan + Hydrochlothiazide",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Paclispec (Paclitaxel) 30mg/5ml",
      tdk: 2034375000,
      tck: 406875000,
      tenhoatchat: "Paclitaxel",
      nhom: "Thuốc Ung Thư - Miễn Dịch"
    },
    {
      tenthuoc: "Paclispec (Paclitaxel) 30mg/5ml",
      tdk: 7374999975,
      tck: 1474999995,
      tenhoatchat: "Paclitaxel",
      nhom: "Thuốc Ung Thư - Miễn Dịch"
    },
    {
      tenthuoc: "Amohexine",
      tdk: 9375000000,
      tck: 1875000000,
      tenhoatchat: "Amoxicilin + Bromhexin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Lidocain",
      tdk: 180000000,
      tck: 36000000,
      tenhoatchat: "Lidocain (hydroclorid)",
      nhom: "Thuốc Gây tê - Gây mê"
    },
    {
      tenthuoc: "Lidocain",
      tdk: 270000000,
      tck: 54000000,
      tenhoatchat: "Lidocain (hydroclorid)",
      nhom: "Thuốc Gây tê - Gây mê"
    },
    {
      tenthuoc: "Lidocain",
      tdk: 324000000,
      tck: 64800000,
      tenhoatchat: "Lidocain (hydroclorid)",
      nhom: "Thuốc Gây tê - Gây mê"
    },
    {
      tenthuoc: "Lidocain",
      tdk: 360000000,
      tck: 72000000,
      tenhoatchat: "Lidocain",
      nhom: "Thuốc Gây tê - Gây mê"
    },
    {
      tenthuoc: "Lidocain",
      tdk: 385000000,
      tck: 77000000,
      tenhoatchat: "Lidocain (hydroclorid)",
      nhom: "Thuốc Gây tê - Gây mê"
    },
    {
      tenthuoc: "Lidocain",
      tdk: 540000000,
      tck: 108000000,
      tenhoatchat: "Lidocain ( hydroclorid)",
      nhom: "Thuốc Gây tê - Gây mê"
    },
    {
      tenthuoc: "Lidocain",
      tdk: 552000000,
      tck: 110400000,
      tenhoatchat: "Lidocain (hydroclorid)",
      nhom: "Thuốc Gây tê - Gây mê"
    },
    {
      tenthuoc: "Lidocain",
      tdk: 600000000,
      tck: 120000000,
      tenhoatchat: "Lidocain",
      nhom: "Thuốc Gây tê - Gây mê"
    },
    {
      tenthuoc: "Lidocain",
      tdk: 1080000000,
      tck: 120000000,
      tenhoatchat: "Lidocain (hydroclorid)",
      nhom: "Thuốc Gây tê - Gây mê"
    },
    {
      tenthuoc: "Lidocain",
      tdk: 1093800000,
      tck: 218760000,
      tenhoatchat: "Lidocain ( hydroclorid)",
      nhom: "Thuốc Gây tê - Gây mê"
    },
    {
      tenthuoc: "Lidocain",
      tdk: 1800000169,
      tck: 360000034,
      tenhoatchat: "Lidocaine",
      nhom: "Thuốc Gây tê - Gây mê"
    },
    {
      tenthuoc: "Lidocain",
      tdk: 2160000000,
      tck: 432000000,
      tenhoatchat: "Lidocain",
      nhom: "Thuốc Gây tê - Gây mê"
    },
    {
      tenthuoc: "Augmentin 250/31.25 Sac 250mg 12's",
      tdk: 9339501600,
      tck: 3849300000,
      tenhoatchat: "Amoxicillin  250mg, Acid Clavulanic  31.25mg",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Campto Inj 40mg 2ml",
      tdk: 9197562500,
      tck: 1839512500,
      tenhoatchat: "Irinotecan",
      nhom: "Thuốc Ung Thư - Miễn Dịch"
    },
    {
      tenthuoc: "Mitotax 30mg",
      tdk: 9185625000,
      tck: 1837125000,
      tenhoatchat: "Paclitaxel",
      nhom: "Thuốc Ung Thư - Miễn Dịch"
    },
    {
      tenthuoc: "Usalota",
      tdk: 18573750,
      tck: 3714750,
      tenhoatchat: "Loratadin",
      nhom: "Thuốc chống Dị ứng"
    },
    {
      tenthuoc: "Usalota",
      tdk: 79375000,
      tck: 15875000,
      tenhoatchat: "Loratadin",
      nhom: "Thuốc chống Dị ứng"
    },
    {
      tenthuoc: "Usalota",
      tdk: 158750000,
      tck: 31750000,
      tenhoatchat: "Loratadin",
      nhom: "Thuốc chống Dị ứng"
    },
    {
      tenthuoc: "Usalota",
      tdk: 190500000,
      tck: 38100000,
      tenhoatchat: "Loratadin",
      nhom: "Thuốc chống Dị ứng"
    },
    {
      tenthuoc: "Usalota",
      tdk: 333368438,
      tck: 66673688,
      tenhoatchat: "Loratadin",
      nhom: "Thuốc chống Dị ứng"
    },
    {
      tenthuoc: "Usalota",
      tdk: 349250000,
      tck: 69850000,
      tenhoatchat: "Loratadin",
      nhom: "Thuốc chống Dị ứng"
    },
    {
      tenthuoc: "Usalota",
      tdk: 539750000,
      tck: 107950000,
      tenhoatchat: "Loratadin",
      nhom: "Thuốc chống Dị ứng"
    },
    {
      tenthuoc: "Usalota",
      tdk: 793750000,
      tck: 158750000,
      tenhoatchat: "Loratadin",
      nhom: "Thuốc chống Dị ứng"
    },
    {
      tenthuoc: "Usalota",
      tdk: 1270000200,
      tck: 254000040,
      tenhoatchat: "Loratadin",
      nhom: "Thuốc chống Dị ứng"
    },
    {
      tenthuoc: "Usalota",
      tdk: 1587500000,
      tck: 317500000,
      tenhoatchat: "Loratadin",
      nhom: "Thuốc chống Dị ứng"
    },
    {
      tenthuoc: "Usalota",
      tdk: 3714750000,
      tck: 412750000,
      tenhoatchat: "Loratadin",
      nhom: "Thuốc chống Dị ứng"
    },
    {
      tenthuoc: "Danotan 100mg/ml",
      tdk: 8887500000,
      tck: 1777500000,
      tenhoatchat: "Phenobarbital",
      nhom: "Thuốc chống Co Giật"
    },
    {
      tenthuoc: "Đại tràng hoàn P/H",
      tdk: 8856250000,
      tck: 3006250000,
      tenhoatchat: "Bột Bạch truật; Bột Mộc hương; Bột Hoàng đằng; Bột Hoài sơn; Bột Trần bì; Bột Hoàng liên; Bột Bạch linh; Bột Sa nhân; Bột Bạch thược; Cao đặc Cam thảo; Cao đặc Đảng sâm.",
      nhom: "Thuốc Đông dược"
    },
    {
      tenthuoc: "Gardenal 100mg V10",
      tdk: 750000,
      tck: 150000,
      tenhoatchat: "Phenobacbital",
      nhom: "Thuốc chống Co Giật"
    },
    {
      tenthuoc: "Gardenal 100mg V10",
      tdk: 213750000,
      tck: 42750000,
      tenhoatchat: "Phenobarbital",
      nhom: "Thuốc chống Co Giật"
    },
    {
      tenthuoc: "Gardenal 100mg V10",
      tdk: 8571375000,
      tck: 1714275000,
      tenhoatchat: "Phenobarbital",
      nhom: "Thuốc chống Co Giật"
    },
    {
      tenthuoc: "Transamin Inj",
      tdk: 8749997813,
      tck: 1749999563,
      tenhoatchat: "Tranexamic acid",
      nhom: "Thuốc ảnh hưởng đến Máu"
    },
    {
      tenthuoc: "Rolnadez",
      tdk: 2493750020,
      tck: 498750004,
      tenhoatchat: "Tamoxifen",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Rolnadez",
      tdk: 6243750000,
      tck: 1248750000,
      tenhoatchat: "Tamoxifen",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Cozaar XQ (Amlodipin+Losartan) (5+100)mg",
      tdk: 8639999550,
      tck: 1727999910,
      tenhoatchat: "Amlodipin + Losartan",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Ebelgiumtac 12.5",
      tdk: 8625000000,
      tck: 1725000000,
      tenhoatchat: "Enalapril maleat +Hydroclorothiazid",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Azicine  250mg (Sacchet(s))",
      tdk: 8624137500,
      tck: 1724827500,
      tenhoatchat: "Azithromycin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Thấp khớp hoàn P/H",
      tdk: 4241250000,
      tck: 1631250000,
      tenhoatchat: "Cao Tần giao; Cao Đỗ trọng; Cao Ngưu tất; Cao Độc hoạt; Bột Phòng phong; Bột Phục linh; Bột Xuyên khung;  Bột Tục đoạn; Bột Hoàng kỳ; Bột Bạch thược; Bột Cam thảo; Bột Đương quy; Bột Thiên niên kiện.",
      nhom: "Thuốc Đông dược"
    },
    {
      tenthuoc: "Thấp khớp hoàn P/H",
      tdk: 4350000000,
      tck: 870000000,
      tenhoatchat: "Cao tần giaoCao đỗ trọngCao ngưu tấtCao độc hoạtBột phòng phongBột phục linhBột xuyên khungBột tục đoạnBột hoàng kỳBột bạch thượcBột cam thảoBột đương quyBột thiên niên kiện",
      nhom: "Thuốc Đông dược"
    },
    {
      tenthuoc: "Danotan",
      tdk: 6693750,
      tck: 1338750,
      tenhoatchat: "Phenobarbital",
      nhom: "Thuốc chống Co Giật"
    },
    {
      tenthuoc: "Danotan",
      tdk: 557812500,
      tck: 111562500,
      tenhoatchat: "Phenobarbital",
      nhom: "Thuốc chống Co Giật"
    },
    {
      tenthuoc: "Danotan",
      tdk: 7932093750,
      tck: 1586418750,
      tenhoatchat: "Phenobarbital",
      nhom: "Thuốc chống Co Giật"
    },
    {
      tenthuoc: "EDNYT 5MG",
      tdk: 183000000,
      tck: 36600000,
      tenhoatchat: "Enalapril",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "EDNYT 5MG",
      tdk: 183000000,
      tck: 36600000,
      tenhoatchat: "Enalapril",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "EDNYT 5MG",
      tdk: 303780000,
      tck: 60756000,
      tenhoatchat: "Enalapril",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "EDNYT 5MG",
      tdk: 303780000,
      tck: 60756000,
      tenhoatchat: "Enalapril",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "EDNYT 5MG",
      tdk: 1219024000,
      tck: 244000000,
      tenhoatchat: "Enalapril",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "EDNYT 5MG",
      tdk: 6284464000,
      tck: 3172000000,
      tenhoatchat: "Enalapril",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Crila Forte",
      tdk: 8437500000,
      tck: 1687500000,
      tenhoatchat: "Cao khô Trinh nữ hoàng cung",
      nhom: "Thuốc Đông dược"
    },
    {
      tenthuoc: "Caldihasan",
      tdk: 522900000,
      tck: 104580000,
      tenhoatchat: "Calci carbonat+ Vitamin D3 1250mg+125UI",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Caldihasan",
      tdk: 535500000,
      tck: 107100000,
      tenhoatchat: "Calci carbonat + Vitamin D3",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Caldihasan",
      tdk: 730800000,
      tck: 146160000,
      tenhoatchat: "Calci carbonat + Vitamin D3",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Caldihasan",
      tdk: 1575000000,
      tck: 315000000,
      tenhoatchat: "Calci cacbonat+ Vitamin D3",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Caldihasan",
      tdk: 2097900000,
      tck: 419580000,
      tenhoatchat: "Calci carbonat + Vitamin D3",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Caldihasan",
      tdk: 2938950000,
      tck: 587790000,
      tenhoatchat: "Calci carbonat + Vitamin D3",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Liposic",
      tdk: 8400000000,
      tck: 1680000000,
      tenhoatchat: "Carbomer",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Noradrenalin 1mg/1ml",
      tdk: 8367187500,
      tck: 1673437500,
      tenhoatchat: "Nor epinephrin (Nor adrenalin)",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Propofol 200 mg/ống 20mL",
      tdk: 8268750000,
      tck: 1653750000,
      tenhoatchat: "PROPOFOL 1% KABI",
      nhom: "Thuốc Gây tê - Gây mê"
    },
    {
      tenthuoc: "Amogentine",
      tdk: 8250000000,
      tck: 1650000000,
      tenhoatchat: "Amoxicilin+Acid Clavulanic",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Profol 1%",
      tdk: 1375000000,
      tck: 275000000,
      tenhoatchat: "Propofol",
      nhom: "Thuốc Gây tê - Gây mê"
    },
    {
      tenthuoc: "Profol 1%",
      tdk: 6875000000,
      tck: 1375000000,
      tenhoatchat: "Propofol",
      nhom: "Thuốc Gây tê - Gây mê"
    },
    {
      tenthuoc: "CORDARONE 200mg B/   2bls x 15 Tabs",
      tdk: 50625000,
      tck: 50625000,
      tenhoatchat: "Amiodarone hydrochloride",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "CORDARONE 200mg B/   2bls x 15 Tabs",
      tdk: 151875000,
      tck: 30375000,
      tenhoatchat: "Amiodarone hydrochloride",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "CORDARONE 200mg B/   2bls x 15 Tabs",
      tdk: 3999375000,
      tck: 799875000,
      tenhoatchat: "Amiodaron (hydrochlorid)",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "CORDARONE 200mg B/   2bls x 15 Tabs",
      tdk: 3999375000,
      tck: 799875000,
      tenhoatchat: "Amiodaron (hydrochlorid)",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Humulin R 100IU 10ml",
      tdk: 128625000,
      tck: 25725000,
      tenhoatchat: "Insulin tác dụng nhUK, ngắn",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Humulin R 100IU 10ml",
      tdk: 182312500,
      tck: 36462500,
      tenhoatchat: "Insulin tác dụng nhanh, ngắn",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Humulin R 100IU 10ml",
      tdk: 543750000,
      tck: 108750000,
      tenhoatchat: "Insulin tác dụng nhanh, ngắn (Fast-acting, Short-acting)",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Humulin R 100IU 10ml",
      tdk: 546937500,
      tck: 109387500,
      tenhoatchat: "Insulin tác dụng nhanh, ngắn",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Humulin R 100IU 10ml",
      tdk: 1102500000,
      tck: 220500000,
      tenhoatchat: "Insulin người, rADN",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Humulin R 100IU 10ml",
      tdk: 5696250000,
      tck: 1139250000,
      tenhoatchat: "Insulin tác dụng nhanh, ngắn",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Boliveric",
      tdk: 8125000000,
      tck: 1625000000,
      tenhoatchat: "Cao Actiso,Cao biển xúc,cao bìm bìm biếc",
      nhom: "Thuốc Đông dược"
    },
    {
      tenthuoc: "Mezathion (spironolacton) 25mg",
      tdk: 2097900000,
      tck: 419580000,
      tenhoatchat: "Spironolacton",
      nhom: "Thuốc lợi tiểu"
    },
    {
      tenthuoc: "Mezathion (spironolacton) 25mg",
      tdk: 6018232500,
      tck: 1203646500,
      tenhoatchat: "Spironolacton",
      nhom: "Thuốc lợi tiểu"
    },
    {
      tenthuoc: "Scilin M30 (30/70) 1000IU/10ml",
      tdk: 8100000169,
      tck: 1620000034,
      tenhoatchat: "Insulin trộn, hỗn hợp",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Insulin H Mix 100 IU",
      tdk: 148500000,
      tck: 29700000,
      tenhoatchat: "Insulin trộn, hỗn hợp (Mixtard-acting, Dual-acting)",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Insulin H Mix 100 IU",
      tdk: 7938000000,
      tck: 1587600000,
      tenhoatchat: "Insulin trộn, hỗn hợp",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Dưỡng cốt  5g",
      tdk: 3674996325,
      tck: 734999265,
      tenhoatchat: "Cao xương hỗn hợp/Cao quy bản, Hoàng bá, Tri mẫu, Trần bì, Bạch thược, Can khương, Thục địa.",
      nhom: "Thuốc Đông dược"
    },
    {
      tenthuoc: "Dưỡng cốt  5g",
      tdk: 4374999956,
      tck: 874999991,
      tenhoatchat: "Cao xương hỗn hợp/Cao quy bản, Hoàng bá, Tri mẫu, Trần bì, Bạch thược, Can khương, Thục địa.",
      nhom: "Thuốc Đông dược"
    },
    {
      tenthuoc: "Cozaar 100mg",
      tdk: 8009551148,
      tck: 1601910230,
      tenhoatchat: "Losartan",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Otrivin Dro",
      tdk: 3799875004,
      tck: 759975001,
      tenhoatchat: "Xylometazoline",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Otrivin Dro",
      tdk: 4129899995,
      tck: 825979999,
      tenhoatchat: "Xylometazoline",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Amlodipin Stada 5mg",
      tdk: 918740813,
      tck: 183748163,
      tenhoatchat: "Amlodipin",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Amlodipin Stada 5mg",
      tdk: 1050000000,
      tck: 210000000,
      tenhoatchat: "Amlodipin",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Amlodipin Stada 5mg",
      tdk: 1399125000,
      tck: 279825000,
      tenhoatchat: "Amlodipin",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Amlodipin Stada 5mg",
      tdk: 1903105969,
      tck: 380621194,
      tenhoatchat: "Amlodipine",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Amlodipin Stada 5mg",
      tdk: 2625000000,
      tck: 525000000,
      tenhoatchat: "Amlodipin",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Cornergel",
      tdk: 7890625000,
      tck: 1578125000,
      tenhoatchat: "Dexpanthenol ( Vitamin B5 )",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Hydrocortison-Lidocain-Richter125mg/5ml",
      tdk: 7875000000,
      tck: 1575000000,
      tenhoatchat: "Hydrocortison acetat + Lidocain (Hydroclorid)",
      nhom: "Thuốc chống Dị ứng"
    },
    {
      tenthuoc: "Hydrocortison +Lidocain",
      tdk: 7875000000,
      tck: 1575000000,
      tenhoatchat: "Hydrocortison natri succinat + Lidocain (hydroclorid)",
      nhom: "Thuốc chống Dị ứng"
    },
    {
      tenthuoc: "Levonor 1 mg/ml",
      tdk: 7875000000,
      tck: 1575000000,
      tenhoatchat: "Nor epinephrin (Nor adrenalin)",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Paringold",
      tdk: 7875000000,
      tck: 1575000000,
      tenhoatchat: "Heparin (Natri)",
      nhom: "Thuốc ảnh hưởng đến Máu"
    },
    {
      tenthuoc: "Examin INJ 250mg/5ml",
      tdk: 523750000,
      tck: 104750000,
      tenhoatchat: "Acid Tranexamic",
      nhom: "Thuốc ảnh hưởng đến Máu"
    },
    {
      tenthuoc: "Examin INJ 250mg/5ml",
      tdk: 523750000,
      tck: 104750000,
      tenhoatchat: "Tranexamic acid",
      nhom: "Thuốc ảnh hưởng đến Máu"
    },
    {
      tenthuoc: "Examin INJ 250mg/5ml",
      tdk: 1571250000,
      tck: 314250000,
      tenhoatchat: "Acid Tranexamic",
      nhom: "Thuốc ảnh hưởng đến Máu"
    },
    {
      tenthuoc: "Examin INJ 250mg/5ml",
      tdk: 1571250000,
      tck: 314250000,
      tenhoatchat: "Tranexamic acid",
      nhom: "Thuốc ảnh hưởng đến Máu"
    },
    {
      tenthuoc: "Examin INJ 250mg/5ml",
      tdk: 1571250000,
      tck: 314250000,
      tenhoatchat: "Tranexamic acid",
      nhom: "Thuốc ảnh hưởng đến Máu"
    },
    {
      tenthuoc: "Examin INJ 250mg/5ml",
      tdk: 2095000000,
      tck: 419000000,
      tenhoatchat: "Acid Tranexamic",
      nhom: "Thuốc ảnh hưởng đến Máu"
    },
    {
      tenthuoc: "Aleradin 5mg",
      tdk: 7854637500,
      tck: 1570927500,
      tenhoatchat: "Desloratadin 5mg",
      nhom: "Thuốc chống Dị ứng"
    },
    {
      tenthuoc: "Dorokit",
      tdk: 519750000,
      tck: 103950000,
      tenhoatchat: "Clarithromycin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Dorokit",
      tdk: 7311564000,
      tck: 1462500000,
      tenhoatchat: "Clarithromycin 250mg; Tinidazol 500mg; Omeprazol 20mg",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Duoplavin (75+100)mg",
      tdk: 7810499813,
      tck: 1562099963,
      tenhoatchat: "Clopidogrel + Acetylsalicylic acid",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Augclamox 250mg",
      tdk: 7787500000,
      tck: 1557500000,
      tenhoatchat: "Amoxicilin+ Acid clavulanic",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Klacid",
      tdk: 1289250000,
      tck: 257850000,
      tenhoatchat: "Clarithromycin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Klacid",
      tdk: 2578499999,
      tck: 515700000,
      tenhoatchat: "Clarithromycin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Klacid",
      tdk: 3867750000,
      tck: 773550000,
      tenhoatchat: "Clarithromycin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "BFS-Noradrenalin 1mg",
      tdk: 272999993,
      tck: 54599999,
      tenhoatchat: "Nor epinephrin (Nor adrenalin)",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "BFS-Noradrenalin 1mg",
      tdk: 1050000131,
      tck: 210000026,
      tenhoatchat: "Nor epinephrin (Nor adrenalin)",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "BFS-Noradrenalin 1mg",
      tdk: 6377000797,
      tck: 1275400159,
      tenhoatchat: "Nor epinephrin (Nor adrenalin)",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Adrenalin 1mg/ml VP",
      tdk: 7687496250,
      tck: 1537499250,
      tenhoatchat: "Epinephrin",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Cefixim",
      tdk: 3255000000,
      tck: 651000000,
      tenhoatchat: "Cefixim",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Cefixim",
      tdk: 4262500000,
      tck: 852500000,
      tenhoatchat: "Cefixim",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Bisoprolol 5mg Stada",
      tdk: 7486867513,
      tck: 1497373503,
      tenhoatchat: "Bisoprolol",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Lovenox 4000IU",
      tdk: 7470837216,
      tck: 1494167443,
      tenhoatchat: "Natri Enoxaparin",
      nhom: "Thuốc ảnh hưởng đến Máu"
    },
    {
      tenthuoc: "Adalat LA Tab 20 mg",
      tdk: 7430062500,
      tck: 1486012500,
      tenhoatchat: "Nifedipin",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Vigamox 0.5% 5ml",
      tdk: 1349985000,
      tck: 269997000,
      tenhoatchat: "Moxifloxacin",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Vigamox 0.5% 5ml",
      tdk: 6074932500,
      tck: 674992500,
      tenhoatchat: "Moxifloxacin",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Celetran 1g",
      tdk: 7400000000,
      tck: 1480000000,
      tenhoatchat: "Ceftriaxon*",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Oraptic 40mg",
      tdk: 7397775000,
      tck: 1479555000,
      tenhoatchat: "Omeprazole 40 mg",
      nhom: "Thuốc Dạ dày - Ruột"
    },
    {
      tenthuoc: "Diprivan 20ml",
      tdk: 7385500000,
      tck: 1477100000,
      tenhoatchat: "Propofol (EDTA)",
      nhom: "Thuốc Gây tê - Gây mê"
    },
    {
      tenthuoc: "PMS-Claminat250mg/ 31,25mg",
      tdk: 3687500000,
      tck: 737500000,
      tenhoatchat: "Amoxicilin + Acid clavulanic",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "PMS-Claminat250mg/ 31,25mg",
      tdk: 3687500000,
      tck: 737500000,
      tenhoatchat: "Amoxicilin + Acidclavulanic",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Aspirin 100mg",
      tdk: 7310250000,
      tck: 1462050000,
      tenhoatchat: "Aspirin",
      nhom: "Thuốc Hạ nhiệt - Giảm đau"
    },
    {
      tenthuoc: "Ravonol",
      tdk: 298125000,
      tck: 59625000,
      tenhoatchat: "Paracetamol + Loratadin + Dextromethophan",
      nhom: "Thuốc chống Dị ứng"
    },
    {
      tenthuoc: "Ravonol",
      tdk: 7005937500,
      tck: 2235937500,
      tenhoatchat: "Paracetamol Loratadin Dextromethophan HBr",
      nhom: "Thuốc Hạ nhiệt - Giảm đau"
    },
    {
      tenthuoc: "Propofol KABI 1% 20ml",
      tdk: 7284375000,
      tck: 1456875000,
      tenhoatchat: "Propofol lipuro",
      nhom: "Thuốc Gây tê - Gây mê"
    },
    {
      tenthuoc: "Vitamin A-D 5400UI",
      tdk: 7272445950,
      tck: 1454489190,
      tenhoatchat: "Vitamin A+Vitamin D",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Bổ gan Thephaco",
      tdk: 7250000000,
      tck: 1450000000,
      tenhoatchat: "Cao Actiso, Cao rau đắng đất, cao diệp hạ châu, Powder mịn bìm bìm",
      nhom: "Thuốc Đông dược"
    },
    {
      tenthuoc: "Tobrex",
      tdk: 6250000,
      tck: 1250000,
      tenhoatchat: "Tobramycin",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Tobrex",
      tdk: 499987500,
      tck: 99997500,
      tenhoatchat: "Tobramycin",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Tobrex",
      tdk: 749981250,
      tck: 149996250,
      tenhoatchat: "Tobramycin 0.3% 5ml",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Tobrex",
      tdk: 999975000,
      tck: 199995000,
      tenhoatchat: "Tobramycin 0,3%/5ml",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Tobrex",
      tdk: 2499937500,
      tck: 499987500,
      tenhoatchat: "Tobramycin",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Tobrex",
      tdk: 2499937500,
      tck: 499987500,
      tenhoatchat: "Tobramycin",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Bupivacaine for spinal anaesthesia Aguettant 5mg/ml - 4ml",
      tdk: 918750000,
      tck: 183750000,
      tenhoatchat: "Bupivacain",
      nhom: "Thuốc Gây tê - Gây mê"
    },
    {
      tenthuoc: "Bupivacaine for spinal anaesthesia Aguettant 5mg/ml - 4ml",
      tdk: 918750000,
      tck: 183750000,
      tenhoatchat: "Bupivacain",
      nhom: "Thuốc Gây tê - Gây mê"
    },
    {
      tenthuoc: "Bupivacaine for spinal anaesthesia Aguettant 5mg/ml - 4ml",
      tdk: 1148437500,
      tck: 229687500,
      tenhoatchat: "Bupivacain",
      nhom: "Thuốc Gây tê - Gây mê"
    },
    {
      tenthuoc: "Bupivacaine for spinal anaesthesia Aguettant 5mg/ml - 4ml",
      tdk: 1878843750,
      tck: 375768750,
      tenhoatchat: "Bupivacain",
      nhom: "Thuốc Gây tê - Gây mê"
    },
    {
      tenthuoc: "Bupivacaine for spinal anaesthesia Aguettant 5mg/ml - 4ml",
      tdk: 2296875000,
      tck: 459375000,
      tenhoatchat: "Bupivacain",
      nhom: "Thuốc Gây tê - Gây mê"
    },
    {
      tenthuoc: "Bloza 50mg",
      tdk: 1424902500,
      tck: 284980500,
      tenhoatchat: "Losartan",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Bloza 50mg",
      tdk: 5716777500,
      tck: 1143355500,
      tenhoatchat: "Losartan",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Bupivacaine 0.5%",
      tdk: 7099312500,
      tck: 1419862500,
      tenhoatchat: "Bupivacaine HCl",
      nhom: "Thuốc Gây tê - Gây mê"
    },
    {
      tenthuoc: "Renapril (Enalapril) 5mg",
      tdk: 7062112150,
      tck: 1412422430,
      tenhoatchat: "Enalapril",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Briozcal",
      tdk: 6987285000,
      tck: 1397457000,
      tenhoatchat: "Calci cacbonat+ Vitamin D3",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Cordaron 200mg",
      tdk: 6986248448,
      tck: 1397249690,
      tenhoatchat: "Amiodaron (hydroclorid)",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Vaginapoly",
      tdk: 3093750000,
      tck: 618750000,
      tenhoatchat: "Nystatin + Neomycin + Polymyxin B",
      nhom: "Thuốc chống Nấm"
    },
    {
      tenthuoc: "Vaginapoly",
      tdk: 3881250000,
      tck: 776250000,
      tenhoatchat: "Nystatin+Neomycin+Polymyxin B",
      nhom: "Thuốc chống Nấm"
    },
    {
      tenthuoc: "Trimoxtal 250/250",
      tdk: 6970320000,
      tck: 1394064000,
      tenhoatchat: "Amoxicilin + Sulbactam",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Lantus (Insulin 300UI/3ml)",
      tdk: 6949975000,
      tck: 1389995000,
      tenhoatchat: "Insulin",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Clarithromycin Stada 500 mg",
      tdk: 6866750000,
      tck: 1373350000,
      tenhoatchat: "Clarithromycin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Scaneuron",
      tdk: 6862500000,
      tck: 1372500000,
      tenhoatchat: "Vitamin B1+Vitamin B6+Vitamin B12.",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Tobrex 5ml",
      tdk: 749975625,
      tck: 149995125,
      tenhoatchat: "Tobramycin",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Tobrex 5ml",
      tdk: 999967500,
      tck: 199993500,
      tenhoatchat: "Tobramycin",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Tobrex 5ml",
      tdk: 5049835875,
      tck: 1009967175,
      tenhoatchat: "Tobramycin",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Hemblood",
      tdk: 6790000000,
      tck: 1358000000,
      tenhoatchat: "Vitamin B1+vitamin B6+ vitamin B12",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Novomix 30 Flexpen",
      tdk: 142406250,
      tck: 28481250,
      tenhoatchat: "Insulin aspart Biphasic (DNA tái tổ hợp)",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Novomix 30 Flexpen",
      tdk: 786082500,
      tck: 157216500,
      tenhoatchat: "Insulin",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Adalat LA Tab 20mg 30's",
      tdk: 26507250,
      tck: 2945250,
      tenhoatchat: "Nifedipine 20 mg",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Adalat LA Tab 20mg 30's",
      tdk: 50426250,
      tck: 10085250,
      tenhoatchat: "Nifedipine",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Adalat LA Tab 20mg 30's",
      tdk: 59351250,
      tck: 11870250,
      tenhoatchat: "Nifedipine",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Adalat LA Tab 20mg 30's",
      tdk: 74077500,
      tck: 14815500,
      tenhoatchat: "Nifedipine 20 mg",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Adalat LA Tab 20mg 30's",
      tdk: 74315500,
      tck: 14875000,
      tenhoatchat: "Nifedipine 20 mg",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Adalat LA Tab 20mg 30's",
      tdk: 96613125,
      tck: 19322625,
      tenhoatchat: "Nifedipin",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Adalat LA Tab 20mg 30's",
      tdk: 111562500,
      tck: 22312500,
      tenhoatchat: "Nifedipin",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Adalat LA Tab 20mg 30's",
      tdk: 118940500,
      tck: 59500000,
      tenhoatchat: "Nifedipine",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Adalat LA Tab 20mg 30's",
      tdk: 185863125,
      tck: 37172625,
      tenhoatchat: "Nifedipin",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Adalat LA Tab 20mg 30's",
      tdk: 260163750,
      tck: 52032750,
      tenhoatchat: "Nifedipin",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "DBL Irinotecan Injection 100mg/5ml",
      tdk: 945000000,
      tck: 189000000,
      tenhoatchat: "Irinotecan",
      nhom: "Thuốc Ung Thư - Miễn Dịch"
    },
    {
      tenthuoc: "LOVENOX 40mg Inj B/ 2 syringes x0,4ml",
      tdk: 53363125,
      tck: 10672625,
      tenhoatchat: "Enoxaparin natri",
      nhom: "Thuốc ảnh hưởng đến Máu"
    },
    {
      tenthuoc: "LOVENOX 40mg Inj B/ 2 syringes x0,4ml",
      tdk: 10728875,
      tck: 2145775,
      tenhoatchat: "Natri  Enoxaparin",
      nhom: "Thuốc ảnh hưởng đến Máu"
    },
    {
      tenthuoc: "LOVENOX 40mg Inj B/ 2 syringes x0,4ml",
      tdk: 488806225,
      tck: 97761245,
      tenhoatchat: "Enoxaparin",
      nhom: "Thuốc ảnh hưởng đến Máu"
    },
    {
      tenthuoc: "LOVENOX 40mg Inj B/ 2 syringes x0,4ml",
      tdk: 488806225,
      tck: 97761245,
      tenhoatchat: "Enoxaparin",
      nhom: "Thuốc ảnh hưởng đến Máu"
    },
    {
      tenthuoc: "Scilin R 400IU",
      tdk: 154800,
      tck: 30960,
      tenhoatchat: "Insulin tác dụng ngắn (S) 400IU/10ml",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Scilin R 400IU",
      tdk: 30895200,
      tck: 6179040,
      tenhoatchat: "Insulin tác dụng ngắn (S) 400IU/10ml",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Scilin R 400IU",
      tdk: 309000006,
      tck: 618000012,
      tenhoatchat: "Insulin tác dụng ngắn (S) 400IU/10ml",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Scilin R 400IU",
      tdk: 618000012,
      tck: 1236000024,
      tenhoatchat: "Insulin tác dụng ngắn (S) 400IU/10ml",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Scilin R 400IU",
      tdk: 618000012,
      tck: 1236000024,
      tenhoatchat: "Insulin tác dụng ngắn (S) 400IU/10ml",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Scilin R 400IU",
      tdk: 61803000,
      tck: 12360600,
      tenhoatchat: "Insulin tác dụng ngắn (S) 400IU/10ml",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Scilin R 400IU",
      tdk: 622799856,
      tck: 1245599712,
      tenhoatchat: "Insulin tác dụng ngắn (S) 400IU/10ml",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Scilin R 400IU",
      tdk: 72103500,
      tck: 14420700,
      tenhoatchat: "Insulin tác dụng ngắn (S) 400IU/10ml",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Scilin R 400IU",
      tdk: 92685600,
      tck: 18537120,
      tenhoatchat: "Insulin tác dụng ngắn (S) 400IU/10ml",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Scilin R 400IU",
      tdk: 92699964,
      tck: 185399928,
      tenhoatchat: "Insulin tác dụng ngắn (S) 400IU/10ml",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Scilin R 400IU",
      tdk: 123631200,
      tck: 24726240,
      tenhoatchat: "Insulin tác dụng ngắn (S) 400IU/10ml",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Scilin R 400IU",
      tdk: 190543710,
      tck: 38108742,
      tenhoatchat: "Insulin tác dụng ngắn (S) 400IU/10ml",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "3Bpluzs",
      tdk: 29250000,
      tck: 5850000,
      tenhoatchat: "Vitamin Bl+Vitamin B6+Vitamin B12.",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "3Bpluzs",
      tdk: 48750000,
      tck: 9750000,
      tenhoatchat: "Vitamin B1 + Vitamin B6 + Vitamin B12",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "3Bpluzs",
      tdk: 58500000,
      tck: 11700000,
      tenhoatchat: "Vitamin B1 + B6 + B12",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "3Bpluzs",
      tdk: 58500000,
      tck: 11700000,
      tenhoatchat: "Vitamin B1+Vitamin B6+Vitamin B12.",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "3Bpluzs",
      tdk: 78000000,
      tck: 15600000,
      tenhoatchat: "Vitamin Bl+Vitamin B6+Vitamin B12.",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "3Bpluzs",
      tdk: 97500000,
      tck: 19500000,
      tenhoatchat: "Vitamin B1+Vitamin B6+Vitamin B12.",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "3Bpluzs",
      tdk: 97500000,
      tck: 19500000,
      tenhoatchat: "Vitamin B1+Vitamin B6+Vitamin B12.",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "3Bpluzs",
      tdk: 118950000,
      tck: 23790000,
      tenhoatchat: "Vitamin B1 + B6 + B12",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "3Bpluzs",
      tdk: 126750000,
      tck: 25350000,
      tenhoatchat: "Vitamin B1+ vitamin B6+ vitamin B12",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "3Bpluzs",
      tdk: 165325000,
      tck: 53125000,
      tenhoatchat: "Vitamin B1 + Vitamin B6 + Vitamin B12",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "I.V.-Globulin SN inj.",
      tdk: 72750000,
      tck: 14550000,
      tenhoatchat: "Immunoglobulin G",
      nhom: "Chế phẩm từ Máu"
    },
    {
      tenthuoc: "I.V.-Globulin SN inj.",
      tdk: 800250000,
      tck: 160050000,
      tenhoatchat: "Immunoglobulin G",
      nhom: "Chế phẩm từ Máu"
    },
    {
      tenthuoc: "Meronem",
      tdk: 870699375,
      tck: 174139875,
      tenhoatchat: "Meropenem",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Tartriakson  1g",
      tdk: 72375000,
      tck: 14475000,
      tenhoatchat: "Ceftriaxon",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Tartriakson  1g",
      tdk: 120625000,
      tck: 24125000,
      tenhoatchat: "Ceftriaxon",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Tartriakson  1g",
      tdk: 168875000,
      tck: 33775000,
      tenhoatchat: "Ceftriaxon",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Tartriakson  1g",
      tdk: 506625000,
      tck: 101325000,
      tenhoatchat: "Ceftriaxon",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Cozaar 50mg",
      tdk: 103591125,
      tck: 20718225,
      tenhoatchat: "Losartan",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Cozaar 50mg",
      tdk: 857922760,
      tck: 171584552,
      tenhoatchat: "Losartan",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Azipowder",
      tdk: 35500000,
      tck: 35500000,
      tenhoatchat: "Azithromycin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Azipowder",
      tdk: 355000000,
      tck: 71000000,
      tenhoatchat: "Azithromycin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Azipowder",
      tdk: 443750000,
      tck: 88750000,
      tenhoatchat: "Azithromycin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Tiepanem",
      tdk: 8150000001,
      tck: 163000000,
      tenhoatchat: "Meropenem",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Sastan-H",
      tdk: 174930000,
      tck: 34986000,
      tenhoatchat: "Losartan+hydroclorothiazid",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Sastan-H",
      tdk: 281587320,
      tck: 31287480,
      tenhoatchat: "Losartan+hydroclorothiazid",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Sastan-H",
      tdk: 349965000,
      tck: 69993000,
      tenhoatchat: "Losartan+hydroclorothiazid",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Moxilen (amoxicillin) 500mg",
      tdk: 7814033483,
      tck: 1562806697,
      tenhoatchat: "Amoxicilin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Moxacin",
      tdk: 37250000,
      tck: 7450000,
      tenhoatchat: "Amoxicilin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Moxacin",
      tdk: 72450000,
      tck: 14490000,
      tenhoatchat: "Amoxicilin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Moxacin",
      tdk: 102437500,
      tck: 20487500,
      tenhoatchat: "Amoxicilin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Moxacin",
      tdk: 130375000,
      tck: 26075000,
      tenhoatchat: "Amoxicilin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Moxacin",
      tdk: 188112500,
      tck: 37622500,
      tenhoatchat: "Amoxicilin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Moxacin",
      tdk: 235462500,
      tck: 47092500,
      tenhoatchat: "Amoxicilin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Ceftriaxone Panpharma 1g",
      tdk: 7590375844,
      tck: 1518075169,
      tenhoatchat: "Ceftriaxon",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Amoxicilin 500mg",
      tdk: 3215625,
      tck: 643125,
      tenhoatchat: "Amoxicilin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Amoxicilin 500mg",
      tdk: 4064000,
      tck: 812800,
      tenhoatchat: "Amoxicilin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Amoxicilin 500mg",
      tdk: 9646875,
      tck: 1929375,
      tenhoatchat: "Amoxicilin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Amoxicilin 500mg",
      tdk: 16078125,
      tck: 3215625,
      tenhoatchat: "Amoxicilin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Amoxicilin 500mg",
      tdk: 23000000,
      tck: 4600000,
      tenhoatchat: "Amoxicilin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Amoxicilin 500mg",
      tdk: 2874965625,
      tck: 574993125,
      tenhoatchat: "Amoxicilin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Amoxicilin 500mg",
      tdk: 287496875,
      tck: 57499375,
      tenhoatchat: "Amoxicilin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Amoxicilin 500mg",
      tdk: 28750000,
      tck: 5750000,
      tenhoatchat: "Amoxicilin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Amoxicilin 500mg",
      tdk: 28750000,
      tck: 5750000,
      tenhoatchat: "Amoxicilin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Amoxicilin 500mg",
      tdk: 35371875,
      tck: 7074375,
      tenhoatchat: "Amoxicilin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Amoxicilin 500mg",
      tdk: 36658125,
      tck: 7331625,
      tenhoatchat: "Amoxicillin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Amoxicilin 500mg",
      tdk: 67528125,
      tck: 13505625,
      tenhoatchat: "Amoxicilin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Amoxicilin 500mg",
      tdk: 96468750,
      tck: 19293750,
      tenhoatchat: "Amoxicilin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Amoxicilin 500mg",
      tdk: 115762500,
      tck: 23152500,
      tenhoatchat: "Amoxicilin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Amoxicilin 500mg",
      tdk: 231525000,
      tck: 25725000,
      tenhoatchat: "Amoxicilin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Vismed",
      tdk: 752686200,
      tck: 150537240,
      tenhoatchat: "Natri hyaluronat",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Tercef 1g",
      tdk: 139125000,
      tck: 27825000,
      tenhoatchat: "Ceftriaxon",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Tercef 1g",
      tdk: 500850000,
      tck: 55650000,
      tenhoatchat: "Ceftriaxon",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Medphadion Drops 100mg/5ml",
      tdk: 750000000,
      tck: 150000000,
      tenhoatchat: "Phytomenadione (Vitamin K1)",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Coveram 5-10 Tab 5mg-10mg 30's",
      tdk: 8236250,
      tck: 1647250,
      tenhoatchat: "Perindopril + Amlodipine",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Coveram 5-10 Tab 5mg-10mg 30's",
      tdk: 32862637,
      tck: 65725274,
      tenhoatchat: "Perindopril  +  Amlodipin",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Coveram 5-10 Tab 5mg-10mg 30's",
      tdk: 328626375,
      tck: 65725275,
      tenhoatchat: "Perindopril  +  Amlodipin",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Coveram 5-10 Tab 5mg-10mg 30's",
      tdk: 329120550,
      tck: 65824110,
      tenhoatchat: "Perindopril Arginine 5mg, Amlodipine besylate 10mg",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Coveram 5-10 Tab 5mg-10mg 30's",
      tdk: 345922500,
      tck: 69184500,
      tenhoatchat: "Amlodipin +Perindopril",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Insulin H  Mix 100UI",
      tdk: 103950000,
      tck: 20790000,
      tenhoatchat: "Insulin trộn, hỗn hợp",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Insulin H  Mix 100UI",
      tdk: 1913999913,
      tck: 3827999826,
      tenhoatchat: "Insulin trộn, hỗn hợp",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Insulin H  Mix 100UI",
      tdk: 452100000,
      tck: 90420000,
      tenhoatchat: "Insulin trộn, hỗn hợp",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Prospan cough syrup",
      tdk: 43509375,
      tck: 8701875,
      tenhoatchat: "Cao khô lá thường xuân",
      nhom: "Thuốc Đông dược"
    },
    {
      tenthuoc: "Prospan cough syrup",
      tdk: 702843750,
      tck: 140568750,
      tenhoatchat: "Cao khô lá thường xuân",
      nhom: "Thuốc Đông dược"
    },
    {
      tenthuoc: "Sanlein",
      tdk: 7769749995,
      tck: 1553949999,
      tenhoatchat: "Natri hyaluronate",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Sanlein",
      tdk: 236250000,
      tck: 47250000,
      tenhoatchat: "Natri hyaluronat",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Sanlein",
      tdk: 427336250,
      tck: 85467250,
      tenhoatchat: "Natri hyaluronat",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Diprivan Inj 20ml 5's 10mg/ml (1%) - 20ml",
      tdk: 723779000,
      tck: 144755800,
      tenhoatchat: "Propofol",
      nhom: "Thuốc Gây tê - Gây mê"
    },
    {
      tenthuoc: "DBL Irinotecan Injection 40mg/2ml",
      tdk: 708750000,
      tck: 141750000,
      tenhoatchat: "Irinotecan",
      nhom: "Thuốc Ung Thư - Miễn Dịch"
    },
    {
      tenthuoc: "Proxacin 200mg",
      tdk: 7087499969,
      tck: 1417499994,
      tenhoatchat: "Ciprofloxacin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Moxilen 500mg",
      tdk: 59500000,
      tck: 11900000,
      tenhoatchat: "Amoxicilin 500mg",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Moxilen 500mg",
      tdk: 89250000,
      tck: 17850000,
      tenhoatchat: "Amoxicilin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Moxilen 500mg",
      tdk: 121562500,
      tck: 24312500,
      tenhoatchat: "Amoxicilin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Moxilen 500mg",
      tdk: 121562500,
      tck: 24312500,
      tenhoatchat: "Amoxicilin 500mg",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Moxilen 500mg",
      tdk: 153168750,
      tck: 30633750,
      tenhoatchat: "Amoxicilin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Moxilen 500mg",
      tdk: 160650000,
      tck: 89250000,
      tenhoatchat: "Amoxicilin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Taxotere (Docetaxel) 80mg/4ml",
      tdk: 1401623749,
      tck: 2803247499,
      tenhoatchat: "Docetaxel",
      nhom: "Thuốc Ung Thư - Miễn Dịch"
    },
    {
      tenthuoc: "Taxotere (Docetaxel) 80mg/4ml",
      tdk: 560649495,
      tck: 112129899,
      tenhoatchat: "Docetaxel",
      nhom: "Thuốc Ung Thư - Miễn Dịch"
    },
    {
      tenthuoc: "Vismed 0.18",
      tdk: 688432500,
      tck: 137686500,
      tenhoatchat: "Natri hyaluronat",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Isotic Moxisone",
      tdk: 9375000,
      tck: 1875000,
      tenhoatchat: "Moxifloxacin + Dexamethason",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Isotic Moxisone",
      tdk: 19531250,
      tck: 3906250,
      tenhoatchat: "Moxifloxacin + Dexamethason",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Isotic Moxisone",
      tdk: 28125000,
      tck: 5625000,
      tenhoatchat: "Moxifloxacin + Dexamethason",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Isotic Moxisone",
      tdk: 62500000,
      tck: 12500000,
      tenhoatchat: "Moxifloxacin + Dexamethason",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Isotic Moxisone",
      tdk: 125000000,
      tck: 25000000,
      tenhoatchat: "Moxifloxacin + Dexamethason",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Isotic Moxisone",
      tdk: 221875000,
      tck: 44375000,
      tenhoatchat: "Moxifloxacin + Dexamethason",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Isotic Moxisone",
      tdk: 221875000,
      tck: 44375000,
      tenhoatchat: "Moxifloxacin + Dexamethason",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Tercef",
      tdk: 44520000,
      tck: 8904000,
      tenhoatchat: "Ceftriaxon",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Tercef",
      tdk: 222600000,
      tck: 44520000,
      tenhoatchat: "Ceftriaxon*",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Tercef",
      tdk: 417375000,
      tck: 83475000,
      tenhoatchat: "Ceftriaxon",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Bupivacaine Aguettant 5mg/ml x 20ml 0,5%/20ml",
      tdk: 6646236188,
      tck: 1329247238,
      tenhoatchat: "Bupivacain (hydroclorid)",
      nhom: "Thuốc Gây tê - Gây mê"
    },
    {
      tenthuoc: "Cravit 5mg/ ml",
      tdk: 23833485,
      tck: 2648165,
      tenhoatchat: "Levofloxacin",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Cravit 5mg/ ml",
      tdk: 285187000,
      tck: 57037400,
      tenhoatchat: "Levofloxacin",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Cravit 5mg/ ml",
      tdk: 331020625,
      tck: 66204125,
      tenhoatchat: "Levofloxacin",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Exforge 5/80mg",
      tdk: 2621587894,
      tck: 5243175788,
      tenhoatchat: "Amlodipin + Valsartan",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Exforge 5/80mg",
      tdk: 3743626725,
      tck: 748725345,
      tenhoatchat: "Amlodipin + Valsartan",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Fabapoxim",
      tdk: 5985000,
      tck: 1197000,
      tenhoatchat: "Cefpodoxim",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Fabapoxim",
      tdk: 17955000,
      tck: 3591000,
      tenhoatchat: "Cefpodoxim",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Fabapoxim",
      tdk: 120000000,
      tck: 24000000,
      tenhoatchat: "Cefpodoxim",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Fabapoxim",
      tdk: 225400000,
      tck: 45080000,
      tenhoatchat: "Cefpodoxim",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Fabapoxim",
      tdk: 250000000,
      tck: 50000000,
      tenhoatchat: "Cefpodoxim",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Unasyl",
      tdk: 618750000,
      tck: 123750000,
      tenhoatchat: "Ampicilin + sulbactam",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Paclitaxel Ebewe Inj 100MG/16.7ML 1's",
      tdk: 211994750,
      tck: 42398950,
      tenhoatchat: "Paclitaxel,6mg/ml",
      nhom: "Thuốc Ung Thư - Miễn Dịch"
    },
    {
      tenthuoc: "Paclitaxel Ebewe Inj 100MG/16.7ML 1's",
      tdk: 393933000,
      tck: 78786600,
      tenhoatchat: "Paclitacel",
      nhom: "Thuốc Ung Thư - Miễn Dịch"
    },
    {
      tenthuoc: "Levonor 4mg/4ml",
      tdk: 2551500,
      tck: 510300,
      tenhoatchat: "Nor epinephrin (Nor adrenalin)",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Levonor 4mg/4ml",
      tdk: 212625000,
      tck: 42525000,
      tenhoatchat: "Nor epinephrin (Nor adrenalin)",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Levonor 4mg/4ml",
      tdk: 387187500,
      tck: 77437500,
      tenhoatchat: "Nor epinephrin (Nor adrenalin)",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Aldacton tab",
      tdk: 592500000,
      tck: 118500000,
      tenhoatchat: "Spironolactone",
      nhom: "Thuốc lợi tiểu"
    },
    {
      tenthuoc: "Adalat LA 30mg",
      tdk: 5906386812,
      tck: 1181277363,
      tenhoatchat: "Nifedipin",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Progut",
      tdk: 590625000,
      tck: 118125000,
      tenhoatchat: "Esomeprazole",
      nhom: "Thuốc Dạ dày - Ruột"
    },
    {
      tenthuoc: "Adalat LA Tab 20mg",
      tdk: 37038750,
      tck: 7407750,
      tenhoatchat: "Nifedipin",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Adalat LA Tab 20mg",
      tdk: 550226250,
      tck: 110045250,
      tenhoatchat: "Nifedipin",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Exforge 5mg+80mg",
      tdk: 5865359814,
      tck: 1173071963,
      tenhoatchat: "Amlodipin + Valsartan",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Ebitac (Enalapril 10mg+Hydroclorothiazid 25mg)",
      tdk: 124200000,
      tck: 24840000,
      tenhoatchat: "Enalapril 10mg + Hydroclorothiazid 25mg",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Ebitac (Enalapril 10mg+Hydroclorothiazid 25mg)",
      tdk: 4602285326,
      tck: 9894568452,
      tenhoatchat: "Enalapril maleate 10mg + Hydrochlorothiazide 25mg",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Boganic",
      tdk: 15500000,
      tck: 3100000,
      tenhoatchat: "Cao đặc Actiso + Cao đặc Rau đắng đất + Cao đặc Bìm bìm",
      nhom: "Thuốc Đông dược"
    },
    {
      tenthuoc: "Boganic",
      tdk: 31000000,
      tck: 6200000,
      tenhoatchat: "Cao đặc Actiso + Cao đặc Rau đắng đất + Cao đặc Bìm bìm",
      nhom: "Thuốc Đông dược"
    },
    {
      tenthuoc: "Boganic",
      tdk: 77500000,
      tck: 15500000,
      tenhoatchat: "Cao Actiso + Cao đặc rau đắng đất + Cao đặc bìm bìm",
      nhom: "Thuốc Đông dược"
    },
    {
      tenthuoc: "Boganic",
      tdk: 457250000,
      tck: 91450000,
      tenhoatchat: "Cao khô Artiso+Cao khô rau đắng đất+Cao khô bìm bìm",
      nhom: "Thuốc Đông dược"
    },
    {
      tenthuoc: "Mecefix-B.E",
      tdk: 150000000,
      tck: 30000000,
      tenhoatchat: "Cefixim",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Mecefix-B.E",
      tdk: 150000000,
      tck: 30000000,
      tenhoatchat: "Cefixim",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Mecefix-B.E",
      tdk: 281250000,
      tck: 56250000,
      tenhoatchat: "Cefixim",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Meronem 500mg",
      tdk: 580466250,
      tck: 116093250,
      tenhoatchat: "Meropenem",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Nexium",
      tdk: 5758499644,
      tck: 1151699929,
      tenhoatchat: "Esomeprazole",
      nhom: "Thuốc Dạ dày - Ruột"
    },
    {
      tenthuoc: "Franilax",
      tdk: 2908125,
      tck: 581625,
      tenhoatchat: "Furosemide + Spironolacton",
      nhom: "Thuốc lợi tiểu"
    },
    {
      tenthuoc: "Franilax",
      tdk: 8250000,
      tck: 1650000,
      tenhoatchat: "Furosemide + Spironolacton",
      nhom: "Thuốc lợi tiểu"
    },
    {
      tenthuoc: "Franilax",
      tdk: 156187475,
      tck: 31237495,
      tenhoatchat: "Spironolacton + Furosemid",
      nhom: "Thuốc lợi tiểu"
    },
    {
      tenthuoc: "Franilax",
      tdk: 407673000,
      tck: 81534600,
      tenhoatchat: "Furosemid + Spironolacton",
      nhom: "Thuốc lợi tiểu"
    },
    {
      tenthuoc: "Gardenal 100mg",
      tdk: 89775,
      tck: 17955,
      tenhoatchat: "Phenobarbital",
      nhom: "Thuốc chống Co Giật"
    },
    {
      tenthuoc: "Gardenal 100mg",
      tdk: 178125,
      tck: 35625,
      tenhoatchat: "Phenobarbital",
      nhom: "Thuốc chống Co Giật"
    },
    {
      tenthuoc: "Gardenal 100mg",
      tdk: 29925,
      tck: 5985,
      tenhoatchat: "Phenobarbital",
      nhom: "Thuốc chống Co Giật"
    },
    {
      tenthuoc: "Gardenal 100mg",
      tdk: 53865,
      tck: 5985,
      tenhoatchat: "Phenobarbital",
      nhom: "Thuốc chống Co Giật"
    },
    {
      tenthuoc: "Gardenal 100mg",
      tdk: 59850,
      tck: 11970,
      tenhoatchat: "Phenobarbital",
      nhom: "Thuốc chống Co Giật"
    },
    {
      tenthuoc: "Gardenal 100mg",
      tdk: 59850,
      tck: 11970,
      tenhoatchat: "Phenobarbital",
      nhom: "Thuốc chống Co Giật"
    },
    {
      tenthuoc: "Gardenal 100mg",
      tdk: 89775,
      tck: 17955,
      tenhoatchat: "Phenobarbital",
      nhom: "Thuốc chống Co Giật"
    },
    {
      tenthuoc: "Gardenal 100mg",
      tdk: 89775,
      tck: 17955,
      tenhoatchat: "Phenobarbital",
      nhom: "Thuốc chống Co Giật"
    },
    {
      tenthuoc: "Gardenal 100mg",
      tdk: 149625,
      tck: 29925,
      tenhoatchat: "Phenobarbital",
      nhom: "Thuốc chống Co Giật"
    },
    {
      tenthuoc: "Gardenal 100mg",
      tdk: 1693125,
      tck: 338625,
      tenhoatchat: "Phenobarbital",
      nhom: "Thuốc chống Co Giật"
    },
    {
      tenthuoc: "Gardenal 100mg",
      tdk: 178125,
      tck: 35625,
      tenhoatchat: "Phenobarbital",
      nhom: "Thuốc chống Co Giật"
    },
    {
      tenthuoc: "Gardenal 100mg",
      tdk: 239400,
      tck: 47880,
      tenhoatchat: "Phenobarbital",
      nhom: "Thuốc chống Co Giật"
    },
    {
      tenthuoc: "Gardenal 100mg",
      tdk: 299250,
      tck: 59850,
      tenhoatchat: "Phenobarbital",
      nhom: "Thuốc chống Co Giật"
    },
    {
      tenthuoc: "Gardenal 100mg",
      tdk: 598500,
      tck: 119700,
      tenhoatchat: "Phenobacbital",
      nhom: "Thuốc chống Co Giật"
    },
    {
      tenthuoc: "Gardenal 100mg",
      tdk: 5985000,
      tck: 1197000,
      tenhoatchat: "Phenobarbital",
      nhom: "Thuốc chống Co Giật"
    },
    {
      tenthuoc: "Gardenal 100mg",
      tdk: 25650000,
      tck: 5130000,
      tenhoatchat: "Phenobarbital",
      nhom: "Thuốc chống Co Giật"
    },
    {
      tenthuoc: "Gardenal 100mg",
      tdk: 538650000,
      tck: 107730000,
      tenhoatchat: "Phenobarbital",
      nhom: "Thuốc chống Co Giật"
    },
    {
      tenthuoc: "Coveram Tab 5mg/5mg 30's",
      tdk: 1564063851,
      tck: 3128127703,
      tenhoatchat: "Perindopril Arginine 5mg, Amlodipine besylate 5mg",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Coveram Tab 5mg/5mg 30's",
      tdk: 164560275,
      tck: 32912055,
      tenhoatchat: "Perindopril  +  Amlodipin",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Coveram Tab 5mg/5mg 30's",
      tdk: 247087500,
      tck: 49417500,
      tenhoatchat: "Perindopril  +  Amlodipin",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Xalvobin (capecitabin) 500mg",
      tdk: 559125000,
      tck: 111825000,
      tenhoatchat: "Capecitabin",
      nhom: "Thuốc Ung Thư - Miễn Dịch"
    },
    {
      tenthuoc: "Levonor 1mg/ml",
      tdk: 393750,
      tck: 78750,
      tenhoatchat: "Nor epinephrin (Nor adrenalin)",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Levonor 1mg/ml",
      tdk: 1575000,
      tck: 315000,
      tenhoatchat: "Nor epinephrin (Nor adrenalin)",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Levonor 1mg/ml",
      tdk: 47250000,
      tck: 9450000,
      tenhoatchat: "Nor epinephrin (Nor adrenalin)",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Levonor 1mg/ml",
      tdk: 78750000,
      tck: 15750000,
      tenhoatchat: "Nor epinephrin (Nor adrenalin)",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Levonor 1mg/ml",
      tdk: 94500000,
      tck: 18900000,
      tenhoatchat: "Nor epinephrin (Nor adrenalin)",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Levonor 1mg/ml",
      tdk: 118125000,
      tck: 23625000,
      tenhoatchat: "Nor epinephrin (Nor adrenalin)",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Levonor 1mg/ml",
      tdk: 212625000,
      tck: 23625000,
      tenhoatchat: "Nor epinephrin (Nor adrenalin)",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Sanlein 0,1% x 5ml",
      tdk: 3076400,
      tck: 615280,
      tenhoatchat: "Natri hyaluronate 1mg/ml",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Sanlein 0,1% x 5ml",
      tdk: 538370000,
      tck: 107674000,
      tenhoatchat: "Natri hyaluronat",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Concor",
      tdk: 540704250,
      tck: 108140850,
      tenhoatchat: "Bisoprolol",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Infartan (Clopidogrel) 75mg",
      tdk: 5399999325,
      tck: 1079999865,
      tenhoatchat: "Clopidogrel",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Nifedipin Hasan 20 Retard",
      tdk: 252000,
      tck: 252000,
      tenhoatchat: "Nifedipin",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Nifedipin Hasan 20 Retard",
      tdk: 124160663,
      tck: 24832133,
      tenhoatchat: "Nifedipin",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Nifedipin Hasan 20 Retard",
      tdk: 14190000,
      tck: 2838000,
      tenhoatchat: "Nifedipin",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Nifedipin Hasan 20 Retard",
      tdk: 17737500,
      tck: 3547500,
      tenhoatchat: "Nifedipine",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Nifedipin Hasan 20 Retard",
      tdk: 70950000,
      tck: 14190000,
      tenhoatchat: "Nifedipin",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Nifedipin Hasan 20 Retard",
      tdk: 1478104688,
      tck: 2956209375,
      tenhoatchat: "Nifedipine",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Nifedipin Hasan 20 Retard",
      tdk: 283500000,
      tck: 56700000,
      tenhoatchat: "Nifedipin",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Aristin-C (Ciprofloxacin 200mg/100ml)",
      tdk: 5264999513,
      tck: 1052999903,
      tenhoatchat: "Ciprofloxacin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Resines (Amlodipin) 5mg",
      tdk: 524979000,
      tck: 104995800,
      tenhoatchat: "Amlodipin",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Kedrigamma",
      tdk: 520162500,
      tck: 104032500,
      tenhoatchat: "Immune Globulin",
      nhom: "Chế phẩm từ Máu"
    },
    {
      tenthuoc: "Tobradex 5ml",
      tdk: 16912125,
      tck: 3382425,
      tenhoatchat: "Tobramycin+Dexamethason",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Tobradex 5ml",
      tdk: 10147275,
      tck: 1127475,
      tenhoatchat: "Tobramycin0,3% Dexamethason 0,1%",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Tobradex 5ml",
      tdk: 11274750,
      tck: 2254950,
      tenhoatchat: "Tobramycin + Dexamethason",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Tobradex 5ml",
      tdk: 16912125,
      tck: 3382425,
      tenhoatchat: "Tobramycin + Dexamethasone",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Tobradex 5ml",
      tdk: 16912125,
      tck: 3382425,
      tenhoatchat: "Tobramycin + Dexamethasone",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Tobradex 5ml",
      tdk: 22549500,
      tck: 4509900,
      tenhoatchat: "Tobramycin + Dexamethason",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Tobradex 5ml",
      tdk: 2536793381,
      tck: 507358676,
      tenhoatchat: "Tobramycin + Dexamethasone",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Tobradex 5ml",
      tdk: 56373750,
      tck: 11274750,
      tenhoatchat: "Tobramycin + Dexamethason",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Tobradex 5ml",
      tdk: 355154625,
      tck: 71030925,
      tenhoatchat: "Tobramycin + Dexamethason",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Prospan 70ml",
      tdk: 13387500,
      tck: 2677500,
      tenhoatchat: "Cao lá thường xuân",
      nhom: "Thuốc Đông dược"
    },
    {
      tenthuoc: "Prospan 70ml",
      tdk: 502031250,
      tck: 100406250,
      tenhoatchat: "Cao khô lá thường xuân",
      nhom: "Thuốc Đông dược"
    },
    {
      tenthuoc: "Ceftriaxon",
      tdk: 506025000,
      tck: 101205000,
      tenhoatchat: "Ceftriaxon",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Amlor Tab 5mg 30's",
      tdk: 25626375,
      tck: 5125275,
      tenhoatchat: "Amlodipin",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Amlor Tab 5mg 30's",
      tdk: 56947500,
      tck: 11389500,
      tenhoatchat: "Amlodipin",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Amlor Tab 5mg 30's",
      tdk: 85079565,
      tck: 9453285,
      tenhoatchat: "Amlodipine besilate (amlodipine 5mg)",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Amlor Tab 5mg 30's",
      tdk: 948175875,
      tck: 189635175,
      tenhoatchat: "Amlodipin",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Amlor Tab 5mg 30's",
      tdk: 2363321259,
      tck: 4726642519,
      tenhoatchat: "Amlodipine",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Novomix 30/70 Flexpen 100UI 3ml",
      tdk: 498421875,
      tck: 99684375,
      tenhoatchat: "Insulin trộn (M) 30:70",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Lotemax",
      tdk: 16462500,
      tck: 3292500,
      tenhoatchat: "Lotepredol etabonat",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Lotemax",
      tdk: 192062500,
      tck: 38412500,
      tenhoatchat: "Loteprednol etabonat",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Lotemax",
      tdk: 288093750,
      tck: 57618750,
      tenhoatchat: "Loteprednol Etabonate",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Concor 2,5mg",
      tdk: 6176250203,
      tck: 1235250041,
      tenhoatchat: "Bisoprolol",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Concor 2,5mg",
      tdk: 431699940,
      tck: 86339988,
      tenhoatchat: "Bisoprolol",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Ebitac 12,5",
      tdk: 51750000,
      tck: 10350000,
      tenhoatchat: "Enalapril maleate + Hydrochlorothiazide",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Ebitac 12,5",
      tdk: 215625000,
      tck: 43125000,
      tenhoatchat: "Enalapril maleat +Hydroclorothiazid",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Ebitac 12,5",
      tdk: 218750000,
      tck: 43750000,
      tenhoatchat: "Enalapril maleat + Hydroclorothiazid",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Remeclar 500mg",
      tdk: 485301250,
      tck: 97060250,
      tenhoatchat: "Clarithromycin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Amlor Cap 5mg 30's",
      tdk: 18281250,
      tck: 3656250,
      tenhoatchat: "Amlodipin",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Amlor Cap 5mg 30's",
      tdk: 28640625,
      tck: 5728125,
      tenhoatchat: "Amlodipin",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Amlor Cap 5mg 30's",
      tdk: 60937500,
      tck: 12187500,
      tenhoatchat: "Amlodipin",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Amlor Cap 5mg 30's",
      tdk: 121875000,
      tck: 24375000,
      tenhoatchat: "Amlodipin",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Amlor Cap 5mg 30's",
      tdk: 2538046875,
      tck: 507609375,
      tenhoatchat: "Amlodipin",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Fudcime (Cefixim) 200mg",
      tdk: 1632918784,
      tck: 3265837568,
      tenhoatchat: "Cefixim",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Fudcime (Cefixim) 200mg",
      tdk: 3193749563,
      tck: 6387499125,
      tenhoatchat: "Cefixim",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Bisoprolol 2,5mg",
      tdk: 480138750,
      tck: 96027750,
      tenhoatchat: "Bisoprolol",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Paclispec 30",
      tdk: 479375000,
      tck: 95875000,
      tenhoatchat: "Paclitaxel",
      nhom: "Thuốc Ung Thư - Miễn Dịch"
    },
    {
      tenthuoc: "Augmentin BD Tab 625mg 14's",
      tdk: 32824000,
      tck: 32824000,
      tenhoatchat: "Amoxicillin trihydrate 500mg + Acid Clavulanic 125mg",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Augmentin BD Tab 625mg 14's",
      tdk: 116346160,
      tck: 23269232,
      tenhoatchat: "Amoxicilin + Acid clavulanic",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Augmentin BD Tab 625mg 14's",
      tdk: 149140320,
      tck: 29828064,
      tenhoatchat: "Amoxicilin + Acid clavulanic",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Augmentin BD Tab 625mg 14's",
      tdk: 179010160,
      tck: 35802032,
      tenhoatchat: "Amoxicillin + Acid Clavulanic",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Medopiren 500mg",
      tdk: 43687500,
      tck: 8737500,
      tenhoatchat: "Ciprofloxacin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Medopiren 500mg",
      tdk: 128150000,
      tck: 25630000,
      tenhoatchat: "Ciprofloxacin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Medopiren 500mg",
      tdk: 145562500,
      tck: 29112500,
      tenhoatchat: "Ciprofloxacin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Medopiren 500mg",
      tdk: 159546750,
      tck: 31909350,
      tenhoatchat: "Ciprofloxacin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Azicine",
      tdk: 9115875,
      tck: 1823175,
      tenhoatchat: "Azithromycin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Azicine",
      tdk: 13500000,
      tck: 2700000,
      tenhoatchat: "Azithromycin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Azicine",
      tdk: 13500000,
      tck: 2700000,
      tenhoatchat: "Azithromycin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Azicine",
      tdk: 13687500,
      tck: 2737500,
      tenhoatchat: "Azithromycin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Azicine",
      tdk: 15741000,
      tck: 3148200,
      tenhoatchat: "Azithromycin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Azicine",
      tdk: 15959625,
      tck: 3191925,
      tenhoatchat: "Azithromycin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Azicine",
      tdk: 41062500,
      tck: 8212500,
      tenhoatchat: "Azithromycin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Azicine",
      tdk: 59294250,
      tck: 11858850,
      tenhoatchat: "Azithromycin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Azicine",
      tdk: 1285499998,
      tck: 2570999996,
      tenhoatchat: "Azithromycin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Azicine",
      tdk: 164797500,
      tck: 32959500,
      tenhoatchat: "Azithromycin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Hyđan",
      tdk: 462475000,
      tck: 92495000,
      tenhoatchat: "Bột mã tiền chế, hy thiêm, ngũ gia bì",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Ucyrin 75mg",
      tdk: 4476150,
      tck: 895230,
      tenhoatchat: "Clopidogrel",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Ucyrin 75mg",
      tdk: 7566825,
      tck: 1513365,
      tenhoatchat: "Clopidogrel 75mg",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Ucyrin 75mg",
      tdk: 11403525,
      tck: 2280705,
      tenhoatchat: "Clopidogrel",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Ucyrin 75mg",
      tdk: 44505720,
      tck: 4945080,
      tenhoatchat: "Clopidogrel",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Ucyrin 75mg",
      tdk: 393901200,
      tck: 78780240,
      tenhoatchat: "Clopidogrel",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Campto (Irinotecan) 100mg",
      tdk: 4599049875,
      tck: 919809975,
      tenhoatchat: "Irinotecan",
      nhom: "Thuốc Ung Thư - Miễn Dịch"
    },
    {
      tenthuoc: "Campto (Irinotecan) 100mg",
      tdk: 183961995,
      tck: 36792399,
      tenhoatchat: "Irinotecan",
      nhom: "Thuốc Ung Thư - Miễn Dịch"
    },
    {
      tenthuoc: "Campto (Irinotecan) 100mg",
      tdk: 2299524997,
      tck: 4599049993,
      tenhoatchat: "Irinotecan",
      nhom: "Thuốc Ung Thư - Miễn Dịch"
    },
    {
      tenthuoc: "Scilin R",
      tdk: 2575000,
      tck: 515000,
      tenhoatchat: "Insulin tác dụng nhanh ngắn",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Scilin R",
      tdk: 12480000,
      tck: 2496000,
      tenhoatchat: "Insulin tác dụng nhanh (fast-acting, short-acting)",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Scilin R",
      tdk: 13475000,
      tck: 2695000,
      tenhoatchat: "Insulin tác dụng nhanh, ngắn",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Scilin R",
      tdk: 19312500,
      tck: 3862500,
      tenhoatchat: "Insulin tác dụng nhanh, ngắn (Fast-acting, Short-acting)",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Scilin R",
      tdk: 25750000,
      tck: 5150000,
      tenhoatchat: "Insulin tác dụng nhanh, ngắn (Fast-acting, Short-acting)",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Scilin R",
      tdk: 128750000,
      tck: 25750000,
      tenhoatchat: "Insulin ( tác dụng nhanh)",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Scilin R",
      tdk: 257500000,
      tck: 51500000,
      tenhoatchat: "Insulin tác dụng nhanh, ngắn",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Resines",
      tdk: 488775,
      tck: 97755,
      tenhoatchat: "Amlodipin",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Resines",
      tdk: 52479000,
      tck: 10495800,
      tenhoatchat: "Amlodipin",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Resines",
      tdk: 60350850,
      tck: 12070170,
      tenhoatchat: "Amlodipine",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Resines",
      tdk: 104958000,
      tck: 20991600,
      tenhoatchat: "Amlodipin",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Resines",
      tdk: 2399039963,
      tck: 4798079925,
      tenhoatchat: "Amlodipine",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Nexium mups",
      tdk: 4519270007,
      tck: 9038540014,
      tenhoatchat: "Esomeprazole",
      nhom: "Thuốc Dạ dày - Ruột"
    },
    {
      tenthuoc: "Chirocain 5mg/ml Ampoule 10x10ml 5mg/ml",
      tdk: 4499999963,
      tck: 8999999925,
      tenhoatchat: "Levobupivacaine hydrochloride,  levobupivacaine 5mg/ml",
      nhom: "Thuốc Gây tê - Gây mê"
    },
    {
      tenthuoc: "Scilin N 400IU",
      tdk: 30901500,
      tck: 6180300,
      tenhoatchat: "Insulin tác dụng trung Bottle(s) (insulin NPH) 400IU/10ml",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Scilin N 400IU",
      tdk: 61803000,
      tck: 12360600,
      tenhoatchat: "Insulin tác dụng trung Bottle(s) (insulin NPH) 400IU/10ml",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Scilin N 400IU",
      tdk: 772500015,
      tck: 154500003,
      tenhoatchat: "Insulin tác dụng trung Bottle(s) (insulin NPH) 400IU/10ml",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Scilin N 400IU",
      tdk: 92699964,
      tck: 185399928,
      tenhoatchat: "Insulin tác dụng trung Bottle(s) (insulin NPH) 400IU/10ml",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Scilin N 400IU",
      tdk: 927000018,
      tck: 1854000036,
      tenhoatchat: "Insulin tác dụng trung Bottle(s) (insulin NPH) 400IU/10ml",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Scilin N 400IU",
      tdk: 92700720,
      tck: 18540144,
      tenhoatchat: "Insulin tác dụng trung Bottle(s) (insulin NPH) 400IU/10ml",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Levemir Flexpen",
      tdk: 446875000,
      tck: 8937499999,
      tenhoatchat: "Insulin determir",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Coveram (5+10)mg",
      tdk: 4447574933,
      tck: 8895149865,
      tenhoatchat: "Perindopril + amlodipin",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Cardilopin",
      tdk: 442800000,
      tck: 88560000,
      tenhoatchat: "Amlodipin",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Humulin N 100IU 10ml",
      tdk: 441000000,
      tck: 88200000,
      tenhoatchat: "Insulin tác dụng trung bình, trung gian",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Ebitac 25mg",
      tdk: 175000000,
      tck: 35000000,
      tenhoatchat: "Enalapril maleate 10mg+hydroclorothiazid 25mg",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Ebitac 25mg",
      tdk: 258750000,
      tck: 51750000,
      tenhoatchat: "Enalapril maleate 10mg+hydroclorothiazid 25mg",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Ebitac12.5",
      tdk: 431250000,
      tck: 86250000,
      tenhoatchat: "Enalapril",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Scanneuron 300,2mg",
      tdk: 54899775,
      tck: 10979955,
      tenhoatchat: "Vitamin B1+Vitamin B6+Vitamin B12.",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Scanneuron 300,2mg",
      tdk: 1619989875,
      tck: 323997975,
      tenhoatchat: "Vitamin B1+Vitamin B6+Vitamin B12.",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Scanneuron 300,2mg",
      tdk: 2062501875,
      tck: 412500375,
      tenhoatchat: "Vitamin B1+Vitamin B6+Vitamin B12.",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Coveram",
      tdk: 86974800,
      tck: 17394960,
      tenhoatchat: "Perindopril + amlodipin",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Coveram",
      tdk: 3293676375,
      tck: 658735275,
      tenhoatchat: "Perindopril + amlodipin",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Propofol 1 % Kabi",
      tdk: 415209375,
      tck: 83041875,
      tenhoatchat: "Propofol",
      nhom: "Thuốc Gây tê - Gây mê"
    },
    {
      tenthuoc: "Oflovid ophthalmic ointment",
      tdk: 4658125,
      tck: 931625,
      tenhoatchat: "Ofloxacin",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Oflovid ophthalmic ointment",
      tdk: 408653625,
      tck: 81730725,
      tenhoatchat: "Ofloxacin",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Mixtard 30",
      tdk: 1625000,
      tck: 325000,
      tenhoatchat: "Insulin trộn, hỗn hợp (Mixtard-acting, Dual-acting)",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Mixtard 30",
      tdk: 69500000,
      tck: 13900000,
      tenhoatchat: "Insulin người, rADN",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Mixtard 30",
      tdk: 81250000,
      tck: 16250000,
      tenhoatchat: "Insulin trộn, hỗn hợp",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Mixtard 30",
      tdk: 100200000,
      tck: 20040000,
      tenhoatchat: "Insulin người, rADN",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Mixtard 30",
      tdk: 130000000,
      tck: 26000000,
      tenhoatchat: "Insulin người, rADN",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Xeloda (Capecitabin) 500mg",
      tdk: 3817802772,
      tck: 7635605544,
      tenhoatchat: "Capecitabin",
      nhom: "Thuốc Ung Thư - Miễn Dịch"
    },
    {
      tenthuoc: "Resines 5mg",
      tdk: 23601375,
      tck: 4720275,
      tenhoatchat: "Amlodipin 5mg",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Resines 5mg",
      tdk: 45927000,
      tck: 9185400,
      tenhoatchat: "Amlodipine",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Resines 5mg",
      tdk: 48258000,
      tck: 9651600,
      tenhoatchat: "Amlodipin 5mg",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Resines 5mg",
      tdk: 262458000,
      tck: 52491600,
      tenhoatchat: "Amlodipin",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Emas",
      tdk: 375000000,
      tck: 75000000,
      tenhoatchat: "Glycerin",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Scilin M30",
      tdk: 103000000,
      tck: 20600000,
      tenhoatchat: "Insulin trộn, hỗn hợp",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Scilin M30",
      tdk: 108000000,
      tck: 21600000,
      tenhoatchat: "Insulin trộn, hỗn hợp (Mixtard-acting, Dual-acting)",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Scilin M30",
      tdk: 162000000,
      tck: 3239999999,
      tenhoatchat: "Insulin (30/70)",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Unasyn 1500mg",
      tdk: 371250000,
      tck: 74250000,
      tenhoatchat: "Amoxicilin + Sulbactam",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Ofmantine-Domesco 625mg",
      tdk: 18749080,
      tck: 3749816,
      tenhoatchat: "Amoxicilin + Acid clavulanic",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Ofmantine-Domesco 625mg",
      tdk: 25764690,
      tck: 5152938,
      tenhoatchat: "Amoxicilin + Acid clavulanic",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Ofmantine-Domesco 625mg",
      tdk: 57701910,
      tck: 11540382,
      tenhoatchat: "Amoxicilin + Acid clavulanic",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Ofmantine-Domesco 625mg",
      tdk: 267534190,
      tck: 53506838,
      tenhoatchat: "Amoxicilin + Acid clavulanic",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Cozaar Tab 50mg 30's",
      tdk: 31391250,
      tck: 6278250,
      tenhoatchat: "Losartan",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Cozaar Tab 50mg 30's",
      tdk: 52109475,
      tck: 10421895,
      tenhoatchat: "Losartan Kali",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Cozaar Tab 50mg 30's",
      tdk: 64875250,
      tck: 12975050,
      tenhoatchat: "Losartan",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Cozaar Tab 50mg 30's",
      tdk: 1045328625,
      tck: 209065725,
      tenhoatchat: "Losartan Kali",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Cozaar Tab 50mg 30's",
      tdk: 116775450,
      tck: 23355090,
      tenhoatchat: "Losartan K 50mg",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Diprivan 20ml/200mg",
      tdk: 3692749922,
      tck: 7385499844,
      tenhoatchat: "Propofol",
      nhom: "Thuốc Gây tê - Gây mê"
    },
    {
      tenthuoc: "Parocontin",
      tdk: 352506000,
      tck: 70501200,
      tenhoatchat: "Methocarbamol + Paracetamol",
      nhom: "Thuốc Hạ nhiệt - Giảm đau"
    },
    {
      tenthuoc: "Medsamic",
      tdk: 1178750,
      tck: 235750,
      tenhoatchat: "Acid Tranexamic",
      nhom: "Thuốc ảnh hưởng đến Máu"
    },
    {
      tenthuoc: "Medsamic",
      tdk: 2357500,
      tck: 471500,
      tenhoatchat: "Tranexamic acid",
      nhom: "Thuốc ảnh hưởng đến Máu"
    },
    {
      tenthuoc: "Medsamic",
      tdk: 20038750,
      tck: 4007750,
      tenhoatchat: "Tranexamic acid",
      nhom: "Thuốc ảnh hưởng đến Máu"
    },
    {
      tenthuoc: "Medsamic",
      tdk: 8722699125,
      tck: 1744539825,
      tenhoatchat: "Tranexamic acid",
      nhom: "Thuốc ảnh hưởng đến Máu"
    },
    {
      tenthuoc: "Medsamic",
      tdk: 235750000,
      tck: 47150000,
      tenhoatchat: "Acid Tranexamic",
      nhom: "Thuốc ảnh hưởng đến Máu"
    },
    {
      tenthuoc: "Claritek 125mg/5mlx50mL",
      tdk: 3750019875,
      tck: 750003975,
      tenhoatchat: "Clarithromycin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Claritek 125mg/5mlx50mL",
      tdk: 599999904,
      tck: 1199999808,
      tenhoatchat: "Clarithromycin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Claritek 125mg/5mlx50mL",
      tdk: 60000192,
      tck: 120000384,
      tenhoatchat: "Clarithromycin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Claritek 125mg/5mlx50mL",
      tdk: 187374970,
      tck: 37474994,
      tenhoatchat: "Clarithromycin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Twynsta 40/5mg",
      tdk: 3425058193,
      tck: 6850116386,
      tenhoatchat: "Telmisartan + Amlodipin",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Paclitaxel Ebewe Inj 30MG/5ML 1's",
      tdk: 113429625,
      tck: 22685925,
      tenhoatchat: "Paclitacel",
      nhom: "Thuốc Ung Thư - Miễn Dịch"
    },
    {
      tenthuoc: "Paclitaxel Ebewe Inj 30MG/5ML 1's",
      tdk: 225550000,
      tck: 45110000,
      tenhoatchat: "Paclitacel",
      nhom: "Thuốc Ung Thư - Miễn Dịch"
    },
    {
      tenthuoc: "Exforge HCT 5/160/12.5mg",
      tdk: 3358848129,
      tck: 6717696258,
      tenhoatchat: "Amlodipin+Valsartan+Hydrochlorothiazide",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Diamisu-N 10ml",
      tdk: 333500000,
      tck: 66700000,
      tenhoatchat: "Insulin chậm, kéo dài",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Heparin (Paringold) 25000IU/5ml",
      tdk: 151250001,
      tck: 302500002,
      tenhoatchat: "Heparin (Natri)",
      nhom: "Thuốc ảnh hưởng đến Máu"
    },
    {
      tenthuoc: "Heparin (Paringold) 25000IU/5ml",
      tdk: 1809374995,
      tck: 3618749991,
      tenhoatchat: "Heparin (Natri)",
      nhom: "Thuốc ảnh hưởng đến Máu"
    },
    {
      tenthuoc: "Reditux 100mg/10ml",
      tdk: 330000000,
      tck: 66000000,
      tenhoatchat: "Rituximab",
      nhom: "Thuốc Ung Thư - Miễn Dịch"
    },
    {
      tenthuoc: "Unasyn 1,5g",
      tdk: 8249999625,
      tck: 1649999925,
      tenhoatchat: "Ampicilin + sulbactam",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Unasyn 1,5g",
      tdk: 247500000,
      tck: 49500000,
      tenhoatchat: "Ampicillin+Sulbactam",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Dicortineff 5ml",
      tdk: 19421250,
      tck: 3884250,
      tenhoatchat: "Neomycin; gramicidin; Fludrocortisone acetate",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Dicortineff 5ml",
      tdk: 4531625044,
      tck: 906325009,
      tenhoatchat: "Neomycin sulfat + gramicidin + 9-alpha fluohydrocortison acetat",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Dicortineff 5ml",
      tdk: 5749999999,
      tck: 11500000,
      tenhoatchat: "Neomycin sulfat + gramicidin + 9-alpha fluohydrocortison acetat",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Dicortineff 5ml",
      tdk: 5999999906,
      tck: 1199999981,
      tenhoatchat: "Neomycin sulfat + gramicidin + 9-alpha fluohydrocortison acetat",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Dicortineff 5ml",
      tdk: 5999999998,
      tck: 12000000,
      tenhoatchat: "Neomycin sulfat + gramicidin + 9-alpha fluohydrocortison acetat",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Dicortineff 5ml",
      tdk: 8625000038,
      tck: 1725000008,
      tenhoatchat: "Neomycin sulfat + gramicidin + 9-alpha fluohydrocortison acetat",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Diprivan Pre-Filled Syring 1% 10mg/ml (1%) - 50ml",
      tdk: 328125000,
      tck: 65625000,
      tenhoatchat: "Propofol",
      nhom: "Thuốc Gây tê - Gây mê"
    },
    {
      tenthuoc: "Mixtard 30 Flexpen 100ui/mlx3ml",
      tdk: 327247875,
      tck: 65449575,
      tenhoatchat: "Insulin người (rDNA)",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Exforge tab 5mg/ 80mg 2x14's",
      tdk: 327174120,
      tck: 65434824,
      tenhoatchat: "Amlodipine + Valsartan",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Vitamin B1 + B6 + B12",
      tdk: 46035000,
      tck: 9207000,
      tenhoatchat: "Vitamin B1+Vitamin B6+Vitamin B12.",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Vitamin B1 + B6 + B12",
      tdk: 55374000,
      tck: 11074800,
      tenhoatchat: "Vitamin B1 + B6 + B12",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Vitamin B1 + B6 + B12",
      tdk: 61710000,
      tck: 12342000,
      tenhoatchat: "Vitamin B1+Vitamin B6+Vitamin B12.",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Vitamin B1 + B6 + B12",
      tdk: 66000000,
      tck: 13200000,
      tenhoatchat: "Vitamin B1 + Vitamin B6 + Vitamin B12",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Vitamin B1 + B6 + B12",
      tdk: 96000000,
      tck: 24000000,
      tenhoatchat: "Vitamin B1+Vitamin B6+Vitamin B12.",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Medoclav 1g",
      tdk: 75798100,
      tck: 15159620,
      tenhoatchat: "Amoxicilin + Acid clavulanic",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Medoclav 1g",
      tdk: 85141875,
      tck: 17028375,
      tenhoatchat: "Amoxicilin + Acid clavulanic",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Medoclav 1g",
      tdk: 162684900,
      tck: 32536980,
      tenhoatchat: "Amoxicillin + Acid Clavulanic",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Tamifine",
      tdk: 53750025,
      tck: 10750005,
      tenhoatchat: "Tamoxifen",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Tamifine",
      tdk: 268750000,
      tck: 53750000,
      tenhoatchat: "Tamoxifen",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Savi Losartan plus HCT 50/12.5",
      tdk: 317724375,
      tck: 63544875,
      tenhoatchat: "Losartan + Hydrochlorothiazid",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Panadol Caplet (Paracetamol) 500mg",
      tdk: 6689911883,
      tck: 1337982377,
      tenhoatchat: "Paracetamol",
      nhom: "Thuốc Hạ nhiệt - Giảm đau"
    },
    {
      tenthuoc: "Panadol Caplet (Paracetamol) 500mg",
      tdk: 82619838,
      tck: 165239676,
      tenhoatchat: "Paracetamol",
      nhom: "Thuốc Hạ nhiệt - Giảm đau"
    },
    {
      tenthuoc: "Panadol Caplet (Paracetamol) 500mg",
      tdk: 1659270359,
      tck: 3318540719,
      tenhoatchat: "Paracetamol",
      nhom: "Thuốc Hạ nhiệt - Giảm đau"
    },
    {
      tenthuoc: "Heparin-Belmed",
      tdk: 311393125,
      tck: 62278625,
      tenhoatchat: "Heparin (Natri)",
      nhom: "Thuốc ảnh hưởng đến Máu"
    },
    {
      tenthuoc: "Cravit",
      tdk: 305557500,
      tck: 61111500,
      tenhoatchat: "Levofloxacin",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Tobrin",
      tdk: 14625000,
      tck: 2925000,
      tenhoatchat: "Tobramycin",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Tobrin",
      tdk: 24375000,
      tck: 24375000,
      tenhoatchat: "Tobramycin",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Tobrin",
      tdk: 29250000,
      tck: 5850000,
      tenhoatchat: "Tobramycin",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Tobrin",
      tdk: 48750000,
      tck: 9750000,
      tenhoatchat: "Tobramycin",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Tobrin",
      tdk: 51187500,
      tck: 10237500,
      tenhoatchat: "Tobramycin 0,3%/5ml",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Tobrin",
      tdk: 68250000,
      tck: 13650000,
      tenhoatchat: "Tobramycin",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Tobrin",
      tdk: 68250000,
      tck: 13650000,
      tenhoatchat: "Tobramycin",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Losartan Stada 50mg",
      tdk: 302445000,
      tck: 60489000,
      tenhoatchat: "Losartan",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Resines 5mg(amlodipin)",
      tdk: 301826700,
      tck: 60365340,
      tenhoatchat: "Amlodipin",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Hoạt huyết thông mạch P/H",
      tdk: 54750000,
      tck: 18750000,
      tenhoatchat: "Bột Đương quy; Cao đặc các dược liệu (tương đương với Đương quy; Sinh địa; Xuyên khung; Ngưu tất; Ích mẫu; Đan sâm).",
      nhom: "Thuốc Đông dược"
    },
    {
      tenthuoc: "Hoạt huyết thông mạch P/H",
      tdk: 94800000,
      tck: 18960000,
      tenhoatchat: "Bột Đương quy; Cao đặc các dược liệu (tương đương với Đương quy; Sinh địa; Xuyên khung; Ngưu tất; Ích mẫu; Đan sâm).",
      nhom: "Thuốc Đông dược"
    },
    {
      tenthuoc: "Hoạt huyết thông mạch P/H",
      tdk: 152000000,
      tck: 76000000,
      tenhoatchat: "Mỗi 200ml cao lỏng chứa: Đương quy; Bạch thược; Ngưu tất; Thục địa; Xuyên khung; Cao đặc ích mẫu (10:1)",
      nhom: "Thuốc Đông dược"
    },
    {
      tenthuoc: "Meronem 1g (Meropenem)",
      tdk: 301396125,
      tck: 60279225,
      tenhoatchat: "Meropenem",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Aldacton 25mg",
      tdk: 49374990,
      tck: 9874998,
      tenhoatchat: "Spironolacton",
      nhom: "Thuốc lợi tiểu"
    },
    {
      tenthuoc: "Aldacton 25mg",
      tdk: 2481090609,
      tck: 4962181219,
      tenhoatchat: "Spironolacton",
      nhom: "Thuốc lợi tiểu"
    },
    {
      tenthuoc: "Bimoxine",
      tdk: 296450000,
      tck: 59290000,
      tenhoatchat: "Amoxicilin + Cloxacilin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Plavix",
      tdk: 39000430,
      tck: 7800086,
      tenhoatchat: "Clopidogrel",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Plavix",
      tdk: 2569654503,
      tck: 5139309006,
      tenhoatchat: "Clopidogrel",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Novorapid",
      tdk: 295312500,
      tck: 59062500,
      tenhoatchat: "Insulin ( tác dụng nhanh)",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Bactirid 100mg/5ml",
      tdk: 295000000,
      tck: 59000000,
      tenhoatchat: "Cefixim",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Paringold Injection 25000Ui/5ml",
      tdk: 2949372563,
      tck: 5898745125,
      tenhoatchat: "Heparin (Natri)",
      nhom: "Thuốc ảnh hưởng đến Máu"
    },
    {
      tenthuoc: "Xalvobin",
      tdk: 24916500,
      tck: 4983300,
      tenhoatchat: "Capecitabin",
      nhom: "Thuốc Ung Thư - Miễn Dịch"
    },
    {
      tenthuoc: "Xalvobin",
      tdk: 267750000,
      tck: 53550000,
      tenhoatchat: "Capecitabin",
      nhom: "Thuốc Ung Thư - Miễn Dịch"
    },
    {
      tenthuoc: "Aleradin",
      tdk: 7875000,
      tck: 1575000,
      tenhoatchat: "Desloratadine",
      nhom: "Thuốc chống Dị ứng"
    },
    {
      tenthuoc: "Aleradin",
      tdk: 3160725005,
      tck: 632145001,
      tenhoatchat: "Desloratadine",
      nhom: "Thuốc chống Dị ứng"
    },
    {
      tenthuoc: "Aleradin",
      tdk: 47175000,
      tck: 9435000,
      tenhoatchat: "Desloratadine",
      nhom: "Thuốc chống Dị ứng"
    },
    {
      tenthuoc: "Aleradin",
      tdk: 94350000,
      tck: 18870000,
      tenhoatchat: "Desloratadine",
      nhom: "Thuốc chống Dị ứng"
    },
    {
      tenthuoc: "Aleradin",
      tdk: 106143750,
      tck: 21228750,
      tenhoatchat: "Desloratadin",
      nhom: "Thuốc chống Dị ứng"
    },
    {
      tenthuoc: "Paracetamol 500mg( Panadol Caplet)",
      tdk: 286875000,
      tck: 57375000,
      tenhoatchat: "Paracetamol",
      nhom: "Thuốc Hạ nhiệt - Giảm đau"
    },
    {
      tenthuoc: "Clocardigel (Clopidogrel) 75mg",
      tdk: 2846025004,
      tck: 5692050009,
      tenhoatchat: "Clopidogrel",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Cozaar XQ 5mg+100mg",
      tdk: 2842557779,
      tck: 5685115559,
      tenhoatchat: "Amlodipin + Losartan",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "LOVENOX 60mg Inj B/ 2 syringes x0,6ml",
      tdk: 495088125,
      tck: 99017625,
      tenhoatchat: "Enoxaparin (natri)",
      nhom: "Thuốc ảnh hưởng đến Máu"
    },
    {
      tenthuoc: "LOVENOX 60mg Inj B/ 2 syringes x0,6ml",
      tdk: 229155075,
      tck: 45831015,
      tenhoatchat: "Enoxaparin",
      nhom: "Thuốc ảnh hưởng đến Máu"
    },
    {
      tenthuoc: "Lantus Solostar",
      tdk: 138999500,
      tck: 27799900,
      tenhoatchat: "Insulin tác dụng chậm, kéo dài (Slow-acting, Long-acting)",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Lantus Solostar",
      tdk: 138999500,
      tck: 27799900,
      tenhoatchat: "Insulin tác dụng chậm, kéo dài (Slow-acting, Long-acting)",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Nifedipin T20 Stada retard",
      tdk: 130499775,
      tck: 26099955,
      tenhoatchat: "Nifedipin",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Nifedipin T20 Stada retard",
      tdk: 21750000,
      tck: 4350000,
      tenhoatchat: "Nifedipin",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Nifedipin T20 Stada retard",
      tdk: 253750000,
      tck: 50750000,
      tenhoatchat: "Nifedipin",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Enamigal 5mg",
      tdk: 2354625,
      tck: 470925,
      tenhoatchat: "Enalapril",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Enamigal 5mg",
      tdk: 486975038,
      tck: 97395008,
      tenhoatchat: "Enalapril",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Enamigal 5mg",
      tdk: 9740250,
      tck: 1948050,
      tenhoatchat: "Enalapril",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Enamigal 5mg",
      tdk: 9740250,
      tck: 1948050,
      tenhoatchat: "Enalapril",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Enamigal 5mg",
      tdk: 19480500,
      tck: 3896100,
      tenhoatchat: "Enalapril",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Enamigal 5mg",
      tdk: 29250000,
      tck: 5850000,
      tenhoatchat: "Enalapril 5mg",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Enamigal 5mg",
      tdk: 48730500,
      tck: 9746100,
      tenhoatchat: "Enalapril",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Enamigal 5mg",
      tdk: 146250000,
      tck: 29250000,
      tenhoatchat: "Enalapril",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Tobrex Eye Ointment",
      tdk: 37125000,
      tck: 7425000,
      tenhoatchat: "Tobramycin",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Tobrex Eye Ointment",
      tdk: 6187375005,
      tck: 1237475001,
      tenhoatchat: "Tobramycin",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Tobrex Eye Ointment",
      tdk: 154684375,
      tck: 30936875,
      tenhoatchat: "Tobramycin",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Noradrenalin Base Aguettant  4mg/4ml",
      tdk: 2531249438,
      tck: 5062498875,
      tenhoatchat: "Nor epinephrin (Nor adrenalin)",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Hoạt huyết dưỡng não",
      tdk: 9750000,
      tck: 1950000,
      tenhoatchat: "Cao đinh lăng, cao bạch quả",
      nhom: "Thuốc Đông dược"
    },
    {
      tenthuoc: "Hoạt huyết dưỡng não",
      tdk: 21319672,
      tck: 42639344,
      tenhoatchat: "Ginkgo biloba + Cao đan sâm",
      nhom: "Thuốc Đông dược"
    },
    {
      tenthuoc: "Hoạt huyết dưỡng não",
      tdk: 220500000,
      tck: 44100000,
      tenhoatchat: "Cao đinh lăng +Cao bạch quả",
      nhom: "Thuốc Đông dược"
    },
    {
      tenthuoc: "Sotstop",
      tdk: 250250000,
      tck: 50050000,
      tenhoatchat: "Ibuprofen",
      nhom: "Thuốc Hạ nhiệt - Giảm đau"
    },
    {
      tenthuoc: "Panadol caplet",
      tdk: 16248600000,
      tck: 3249720000,
      tenhoatchat: "Paracetamol   500mg",
      nhom: "Thuốc Hạ nhiệt - Giảm đau"
    },
    {
      tenthuoc: "Actrapid",
      tdk: 1624993750,
      tck: 324998750,
      tenhoatchat: "Insulin người,rADN",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Actrapid",
      tdk: 4875000000,
      tck: 975000000,
      tenhoatchat: "Insulin người, rADN",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Actrapid",
      tdk: 9668750000,
      tck: 1933750000,
      tenhoatchat: "Insulin trộn, hỗn hợp",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Kalecin 250",
      tdk: 1874250000,
      tck: 374850000,
      tenhoatchat: "Clarithromycin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Kalecin 250",
      tdk: 4499250000,
      tck: 899850000,
      tenhoatchat: "Clarithromycin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Kalecin 250",
      tdk: 4685625000,
      tck: 937125000,
      tenhoatchat: "Clarithromycin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Kalecin 250",
      tdk: 4685625000,
      tck: 937125000,
      tenhoatchat: "Clarithromycin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Denxif",
      tdk: 15625000000,
      tck: 3125000000,
      tenhoatchat: "Digoxin WZF 0.25mg",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Ceftriaxone - Panpharma",
      tdk: 15461875000,
      tck: 3092375000,
      tenhoatchat: "Ceftriaxone",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "VEROSPIRON 25MG",
      tdk: 2786391090,
      tck: 557278218,
      tenhoatchat: "Spironolacton",
      nhom: "Thuốc lợi tiểu"
    },
    {
      tenthuoc: "VEROSPIRON 25MG",
      tdk: 4574062500,
      tck: 914812500,
      tenhoatchat: "Spironolacton",
      nhom: "Thuốc lợi tiểu"
    },
    {
      tenthuoc: "VEROSPIRON 25MG",
      tdk: 8032500000,
      tck: 1606500000,
      tenhoatchat: "Spironolacton",
      nhom: "Thuốc lợi tiểu"
    },
    {
      tenthuoc: "Hadokit",
      tdk: 2875000000,
      tck: 575000000,
      tenhoatchat: "Omeprazol + Tinidazol + Clarithromycin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Hadokit",
      tdk: 12506250000,
      tck: 2501250000,
      tenhoatchat: "Omeprazol+Tinidazol+Clarithromycin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Paringold Injection",
      tdk: 1512500000,
      tck: 302500000,
      tenhoatchat: "Heparin (Natri)",
      nhom: "Thuốc ảnh hưởng đến Máu"
    },
    {
      tenthuoc: "Paringold Injection",
      tdk: 2268750000,
      tck: 453750000,
      tenhoatchat: "Heparin (Natri)",
      nhom: "Thuốc ảnh hưởng đến Máu"
    },
    {
      tenthuoc: "Paringold Injection",
      tdk: 11343750000,
      tck: 2268750000,
      tenhoatchat: "Heparin (Natri)",
      nhom: "Thuốc ảnh hưởng đến Máu"
    },
    {
      tenthuoc: "Erolin",
      tdk: 337500000,
      tck: 337500000,
      tenhoatchat: "Loratadin",
      nhom: "Thuốc chống Dị ứng"
    },
    {
      tenthuoc: "Erolin",
      tdk: 668250000,
      tck: 133650000,
      tenhoatchat: "Loratadin",
      nhom: "Thuốc chống Dị ứng"
    },
    {
      tenthuoc: "Erolin",
      tdk: 1680750000,
      tck: 336150000,
      tenhoatchat: "Loratadin",
      nhom: "Thuốc chống Dị ứng"
    },
    {
      tenthuoc: "Erolin",
      tdk: 4125000000,
      tck: 825000000,
      tenhoatchat: "Loratadin",
      nhom: "Thuốc chống Dị ứng"
    },
    {
      tenthuoc: "Erolin",
      tdk: 8239687500,
      tck: 1647937500,
      tenhoatchat: "Loratadin",
      nhom: "Thuốc chống Dị ứng"
    },
    {
      tenthuoc: "Clarithromycin Stada 250mg",
      tdk: 412500000,
      tck: 82500000,
      tenhoatchat: "Clarithromycin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Clarithromycin Stada 250mg",
      tdk: 2062500000,
      tck: 412500000,
      tenhoatchat: "Clarithromycin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Clarithromycin Stada 250mg",
      tdk: 2062500000,
      tck: 412500000,
      tenhoatchat: "Clarithromycin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Clarithromycin Stada 250mg",
      tdk: 10312500000,
      tck: 2062500000,
      tenhoatchat: "Clarithromycin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Utrogestan 200mg",
      tdk: 866250000,
      tck: 173250000,
      tenhoatchat: "Progesteron",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Utrogestan 200mg",
      tdk: 1732500000,
      tck: 346500000,
      tenhoatchat: "Progesteron",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Utrogestan 200mg",
      tdk: 1732500000,
      tck: 346500000,
      tenhoatchat: "Progesteron",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Utrogestan 200mg",
      tdk: 3491250000,
      tck: 698250000,
      tenhoatchat: "Progesteron",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Utrogestan 200mg",
      tdk: 7008750000,
      tck: 1401750000,
      tenhoatchat: "Progesteron",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Exforge HCT 10/160/12.5mg",
      tdk: 14766256869,
      tck: 2953251374,
      tenhoatchat: "Amlodipin+Valsartan+Hydrochlorothiazide",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Ebitac Forte",
      tdk: 1462500000,
      tck: 292500000,
      tenhoatchat: "Enalapril+ Hydrochlorothiazid",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Ebitac Forte",
      tdk: 3800000000,
      tck: 760000000,
      tenhoatchat: "Enalapril + Hydroclorothiazid",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Ebitac Forte",
      tdk: 9500000000,
      tck: 1900000000,
      tenhoatchat: "Enalapril+ Hydrochlorothiazid",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Clarithromycin Stada 500mg",
      tdk: 673750000,
      tck: 134750000,
      tenhoatchat: "Clarithromycin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Clarithromycin Stada 500mg",
      tdk: 6872250000,
      tck: 1374450000,
      tenhoatchat: "Clarithromycin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Clarithromycin Stada 500mg",
      tdk: 7216000000,
      tck: 1443200000,
      tenhoatchat: "Clarithromycin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Enaminal (elalapril 10mg)",
      tdk: 14698530000,
      tck: 2939706000,
      tenhoatchat: "Enalapril",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Enamigal (enalapril 5mg)",
      tdk: 14623875000,
      tck: 2924775000,
      tenhoatchat: "Enalapril",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Medopiren",
      tdk: 2912500000,
      tck: 582500000,
      tenhoatchat: "Ciprofloxacin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Medopiren",
      tdk: 11650000000,
      tck: 2330000000,
      tenhoatchat: "Ciprofloxacin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Osaki",
      tdk: 14499999938,
      tck: 2899999988,
      tenhoatchat: "Lysin+ Vitamin+ Khoáng chất",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Acuvail",
      tdk: 1526250000,
      tck: 305250000,
      tenhoatchat: "Ketorolac tromethamine",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Acuvail",
      tdk: 12959250000,
      tck: 2591850000,
      tenhoatchat: "Ketorolac tromethamin",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Tartriakson",
      tdk: 2412500000,
      tck: 482500000,
      tenhoatchat: "Ceftriaxone",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Tartriakson",
      tdk: 12062500013,
      tck: 2412500003,
      tenhoatchat: "Ceftriaxone",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Bupivacaine 20mg 0.5% 5 mg/ml",
      tdk: 946575000,
      tck: 189315000,
      tenhoatchat: "Bupivacain (hydroclorid)",
      nhom: "Thuốc Gây tê - Gây mê"
    },
    {
      tenthuoc: "Bupivacaine 20mg 0.5% 5 mg/ml",
      tdk: 1893150000,
      tck: 378630000,
      tenhoatchat: "Bupivacain (hydroclorid)",
      nhom: "Thuốc Gây tê - Gây mê"
    },
    {
      tenthuoc: "Bupivacaine 20mg 0.5% 5 mg/ml",
      tdk: 2319108750,
      tck: 463821750,
      tenhoatchat: "Bupivacain",
      nhom: "Thuốc Gây tê - Gây mê"
    },
    {
      tenthuoc: "Bupivacaine 20mg 0.5% 5 mg/ml",
      tdk: 4496231250,
      tck: 899246250,
      tenhoatchat: "Bupivacain (hydroclorid)",
      nhom: "Thuốc Gây tê - Gây mê"
    },
    {
      tenthuoc: "Bupivacaine 20mg 0.5% 5 mg/ml",
      tdk: 4732875000,
      tck: 946575000,
      tenhoatchat: "Bupivacain (hydroclorid)",
      nhom: "Thuốc Gây tê - Gây mê"
    },
    {
      tenthuoc: "Cefixime 100mg",
      tdk: 400000000,
      tck: 400000000,
      tenhoatchat: "Cefixim",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Cefixime 100mg",
      tdk: 1920000000,
      tck: 384000000,
      tenhoatchat: "Cefixim",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Cefixime 100mg",
      tdk: 11875000000,
      tck: 2375000000,
      tenhoatchat: "Cefixim",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Neo- Tergynan",
      tdk: 1782000000,
      tck: 356400000,
      tenhoatchat: "Metronidazol + Neomycin+Nystatin",
      nhom: "Thuốc chống Nấm"
    },
    {
      tenthuoc: "Neo- Tergynan",
      tdk: 12375000000,
      tck: 2475000000,
      tenhoatchat: "Nystatin + metronidazol + neomycin",
      nhom: "Thuốc chống Nấm"
    },
    {
      tenthuoc: "Sastan-H (Losartan+Hydroclorothiazid) (25+12,5)mg",
      tdk: 3696000462,
      tck: 739200092,
      tenhoatchat: "Losartan+hydroclorothiazid",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Sastan-H (Losartan+Hydroclorothiazid) (25+12,5)mg",
      tdk: 10300497425,
      tck: 2060099485,
      tenhoatchat: "Losartan+hydroclorothiazid",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Vitamin A-D",
      tdk: 26906250,
      tck: 5381250,
      tenhoatchat: "Vitamin A+Vitamin D",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Vitamin A-D",
      tdk: 76875000,
      tck: 15375000,
      tenhoatchat: "Vitamin A+ Vitamin D",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Vitamin A-D",
      tdk: 100000000,
      tck: 20000000,
      tenhoatchat: "Vitamin A+Vitamin D",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Vitamin A-D",
      tdk: 484317225,
      tck: 96863445,
      tenhoatchat: "Vitamin A+Vitamin D",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Vitamin A-D",
      tdk: 3075000000,
      tck: 615000000,
      tenhoatchat: "Vitamin A + D",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Vitamin A-D",
      tdk: 3839906250,
      tck: 767981250,
      tenhoatchat: "Vitamin A + Vitamin D",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Vitamin A-D",
      tdk: 6250000000,
      tck: 1250000000,
      tenhoatchat: "Vitamin A + D",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "CORDARONE 150mg/3ml Inj B/ 6 Amps x 3ml",
      tdk: 22536000,
      tck: 22536000,
      tenhoatchat: "Amiodarone HCL 150mg",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "CORDARONE 150mg/3ml Inj B/ 6 Amps x 3ml",
      tdk: 37560000,
      tck: 7512000,
      tenhoatchat: "Amiodarone HCl",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "CORDARONE 150mg/3ml Inj B/ 6 Amps x 3ml",
      tdk: 45072000,
      tck: 9014400,
      tenhoatchat: "Amiodarone HCL 150mg/ 3ml",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "CORDARONE 150mg/3ml Inj B/ 6 Amps x 3ml",
      tdk: 67608000,
      tck: 13521600,
      tenhoatchat: "Amiodarone HCl",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "CORDARONE 150mg/3ml Inj B/ 6 Amps x 3ml",
      tdk: 81129600,
      tck: 9014400,
      tenhoatchat: "Amiodarone HCL 150mg/ 3ml",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "CORDARONE 150mg/3ml Inj B/ 6 Amps x 3ml",
      tdk: 90144000,
      tck: 18028800,
      tenhoatchat: "Amiodarone HCl",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "CORDARONE 150mg/3ml Inj B/ 6 Amps x 3ml",
      tdk: 180288000,
      tck: 36057600,
      tenhoatchat: "Amiodarone HCl",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "CORDARONE 150mg/3ml Inj B/ 6 Amps x 3ml",
      tdk: 247896000,
      tck: 49579200,
      tenhoatchat: "Amiodarone HCl",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "CORDARONE 150mg/3ml Inj B/ 6 Amps x 3ml",
      tdk: 338040000,
      tck: 67608000,
      tenhoatchat: "Amiodaron (hydrochlorid)",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "CORDARONE 150mg/3ml Inj B/ 6 Amps x 3ml",
      tdk: 405648000,
      tck: 81129600,
      tenhoatchat: "Amiodarone HCL 150mg/ 3ml",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "CORDARONE 150mg/3ml Inj B/ 6 Amps x 3ml",
      tdk: 12244560000,
      tck: 2448912000,
      tenhoatchat: "Amiodaron",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Diprivan Inj 20ml 5's",
      tdk: 2954200000,
      tck: 590840000,
      tenhoatchat: "Propofol",
      nhom: "Thuốc Gây tê - Gây mê"
    },
    {
      tenthuoc: "Diprivan Inj 20ml 5's",
      tdk: 3249620000,
      tck: 649924000,
      tenhoatchat: "Propofol 10mg/ml (1%)",
      nhom: "Thuốc Gây tê - Gây mê"
    },
    {
      tenthuoc: "Diprivan Inj 20ml 5's",
      tdk: 7385500000,
      tck: 1477100000,
      tenhoatchat: "Propofol",
      nhom: "Thuốc Gây tê - Gây mê"
    },
    {
      tenthuoc: "Nexium 40mg",
      tdk: 13436499169,
      tck: 2687299834,
      tenhoatchat: "Esomeprazole",
      nhom: "Thuốc Dạ dày - Ruột"
    },
    {
      tenthuoc: "Maxitrol 5ml",
      tdk: 199500000,
      tck: 39900000,
      tenhoatchat: "Neomycin + Polymicin + Dexamethason",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Maxitrol 5ml",
      tdk: 249375000,
      tck: 49875000,
      tenhoatchat: "Dexamethasone+neomycin+polymycin B 0,1%+3500IU/ml+6000IU/g",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Maxitrol 5ml",
      tdk: 498750000,
      tck: 99750000,
      tenhoatchat: "Dexamethason+ Neomycin+ Polymycin B",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Maxitrol 5ml",
      tdk: 498750000,
      tck: 99750000,
      tenhoatchat: "Dexamethason+ Neomycin+ Polymycin B",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Maxitrol 5ml",
      tdk: 997500000,
      tck: 199500000,
      tenhoatchat: "Dexamethason+ Neomycin+ Polymycin B",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Maxitrol 5ml",
      tdk: 1496250000,
      tck: 299250000,
      tenhoatchat: "Dexamethason+ Neomycin+ Polymycin B",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Maxitrol 5ml",
      tdk: 1496250000,
      tck: 299250000,
      tenhoatchat: "Dexamethason+ Neomycin+ Polymycin B",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Maxitrol 5ml",
      tdk: 2493750000,
      tck: 498750000,
      tenhoatchat: "Dexamethason+ Neomycin+ Polymycin B",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Maxitrol 5ml",
      tdk: 5486250000,
      tck: 1097250000,
      tenhoatchat: "Dexamethason+ Neomycin+ Polymycin B",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Osmofundin",
      tdk: 13387500000,
      tck: 2677500000,
      tenhoatchat: "Mannitol",
      nhom: "Thuốc lợi tiểu"
    },
    {
      tenthuoc: "Omeprem",
      tdk: 13324500000,
      tck: 2664900000,
      tenhoatchat: "Omeprazole",
      nhom: "Thuốc Dạ dày - Ruột"
    },
    {
      tenthuoc: "3B-Medi",
      tdk: 13125000000,
      tck: 2625000000,
      tenhoatchat: "Vitamin B1 + Vitamin B6 + Vitamin B12",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Medoclav 625mg",
      tdk: 6192290000,
      tck: 1238458000,
      tenhoatchat: "Amoxicilin + Acid clavulanic",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Medoclav 625mg",
      tdk: 6900790000,
      tck: 1380158000,
      tenhoatchat: "Amoxicilin+Acid Clavulanic",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Fabafixim",
      tdk: 13085625000,
      tck: 2617125000,
      tenhoatchat: "Cefixim",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Hyzaar Tab 50/12.5 30's",
      tdk: 2605473750,
      tck: 521094750,
      tenhoatchat: "Losartan + hydroclorothiazid",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Hyzaar Tab 50/12.5 30's",
      tdk: 5210947500,
      tck: 1042189500,
      tenhoatchat: "Losartan potassium; Hydrochlorothiazide",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Hyzaar Tab 50/12.5 30's",
      tdk: 5210947500,
      tck: 1042189500,
      tenhoatchat: "Losartan potassium; Hydrochlorothiazide",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Adrenalin",
      tdk: 25625000,
      tck: 5125000,
      tenhoatchat: "Epinephrin (adrenalin)",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Adrenalin",
      tdk: 25625000,
      tck: 5125000,
      tenhoatchat: "Epinephrin (adrenalin)",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Adrenalin",
      tdk: 51250000,
      tck: 10250000,
      tenhoatchat: "Epinephrin (adrenalin)",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Adrenalin",
      tdk: 51250000,
      tck: 10250000,
      tenhoatchat: "Epinephrin (adrenalin)",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Adrenalin",
      tdk: 76875000,
      tck: 15375000,
      tenhoatchat: "Epinephrin (adrenalin)",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Adrenalin",
      tdk: 78750000,
      tck: 15750000,
      tenhoatchat: "Epinephrin (adrenalin)",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Adrenalin",
      tdk: 110625000,
      tck: 22125000,
      tenhoatchat: "Epinephrin (adrenalin)",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Adrenalin",
      tdk: 128125000,
      tck: 25625000,
      tenhoatchat: "Epinephrin (adrenalin)",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Adrenalin",
      tdk: 156312500,
      tck: 31262500,
      tenhoatchat: "Epinephrin (adrenalin)",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Adrenalin",
      tdk: 205000000,
      tck: 41000000,
      tenhoatchat: "Epinephrin (adrenalin)",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Adrenalin",
      tdk: 218750000,
      tck: 43750000,
      tenhoatchat: "Epinephrin (adrenalin)",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Adrenalin",
      tdk: 250000000,
      tck: 50000000,
      tenhoatchat: "Epinephrin (adrenalin)",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Adrenalin",
      tdk: 256250000,
      tck: 51250000,
      tenhoatchat: "Epinephrin (adrenalin)",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Adrenalin",
      tdk: 256250000,
      tck: 51250000,
      tenhoatchat: "Epinephrin (adrenalin)",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Adrenalin",
      tdk: 262500000,
      tck: 52500000,
      tenhoatchat: "Adrenalin(Epinephrin)",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Adrenalin",
      tdk: 262500000,
      tck: 52500000,
      tenhoatchat: "Epinephrin (adrenalin)",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Adrenalin",
      tdk: 294687500,
      tck: 58937500,
      tenhoatchat: "Epinephrin (adrenalin)",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Adrenalin",
      tdk: 328125000,
      tck: 65625000,
      tenhoatchat: "Epinephrin (adrenalin)",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Adrenalin",
      tdk: 350000000,
      tck: 70000000,
      tenhoatchat: "Epinephrin (adrenalin)",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Adrenalin",
      tdk: 359775000,
      tck: 39975000,
      tenhoatchat: "Epinephrin (adrenalin)",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Adrenalin",
      tdk: 393750000,
      tck: 78750000,
      tenhoatchat: "Epinephrin(Adrenalin)",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Adrenalin",
      tdk: 452812500,
      tck: 90562500,
      tenhoatchat: "Epinephrin (adrenalin)",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Adrenalin",
      tdk: 512500000,
      tck: 102500000,
      tenhoatchat: "Epinephrin (adrenalin)",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Adrenalin",
      tdk: 656250000,
      tck: 131250000,
      tenhoatchat: "Epinephrin (adrenalin)",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Adrenalin",
      tdk: 1793750044,
      tck: 358750009,
      tenhoatchat: "Epinephrine (Adrenaline)",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Adrenalin",
      tdk: 2690625000,
      tck: 538125000,
      tenhoatchat: "Epinephrin(Adrenalin)",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Adrenalin",
      tdk: 2756250000,
      tck: 551250000,
      tenhoatchat: "Epinephrin(Adrenalin)",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Cosyndo B",
      tdk: 12993750000,
      tck: 2598750000,
      tenhoatchat: "Vitamin B1 + B6 + B12",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Dicortinef",
      tdk: 12947500000,
      tck: 2589500000,
      tenhoatchat: "neomycin sulfat + gramicidin+ 9-alpha fluohydrocortison",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Ambelin",
      tdk: 12600000000,
      tck: 2520000000,
      tenhoatchat: "Amlodipin",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Bupivacaine Aguettant 5mg/ml",
      tdk: 12600000000,
      tck: 2520000000,
      tenhoatchat: "Bupivacaine hydrochloride",
      nhom: "Thuốc Gây tê - Gây mê"
    },
    {
      tenthuoc: "Exforge HCT 10/160/12,5 mg",
      tdk: 6781071537,
      tck: 1356214307,
      tenhoatchat: "Amlodipin + Valsartan",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Vitamin 3B",
      tdk: 420000000,
      tck: 84000000,
      tenhoatchat: "Vitamin B1 + B6 +B12",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Vitamin 3B",
      tdk: 6349999688,
      tck: 1269999938,
      tenhoatchat: "Vitamin B1 + B6 + B12",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Ostocare",
      tdk: 6750000000,
      tck: 1350000000,
      tenhoatchat: "Calci Gluconate, Vitamin D3",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Cammic",
      tdk: 314250000,
      tck: 62850000,
      tenhoatchat: "Tranexamic acid",
      nhom: "Thuốc ảnh hưởng đến Máu"
    },
    {
      tenthuoc: "Cammic",
      tdk: 472500000,
      tck: 94500000,
      tenhoatchat: "Acid Tranexamic",
      nhom: "Thuốc ảnh hưởng đến Máu"
    },
    {
      tenthuoc: "Cammic",
      tdk: 472500000,
      tck: 94500000,
      tenhoatchat: "Acid Tranexamic",
      nhom: "Thuốc ảnh hưởng đến Máu"
    },
    {
      tenthuoc: "Cammic",
      tdk: 850500000,
      tck: 170100000,
      tenhoatchat: "Acid Tranexamic",
      nhom: "Thuốc ảnh hưởng đến Máu"
    },
    {
      tenthuoc: "Cammic",
      tdk: 1417500000,
      tck: 283500000,
      tenhoatchat: "Tranexamic acid",
      nhom: "Thuốc ảnh hưởng đến Máu"
    },
    {
      tenthuoc: "Cammic",
      tdk: 1562500000,
      tck: 312500000,
      tenhoatchat: "Acid Tranexamic",
      nhom: "Thuốc ảnh hưởng đến Máu"
    },
    {
      tenthuoc: "Cammic",
      tdk: 1653750000,
      tck: 330750000,
      tenhoatchat: "Acid Tranexamic",
      nhom: "Thuốc ảnh hưởng đến Máu"
    },
    {
      tenthuoc: "Ambelin (Amlodipin) 5mg",
      tdk: 6720000000,
      tck: 1344000000,
      tenhoatchat: "Amlodipin",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Cordaflex",
      tdk: 1998000000,
      tck: 399600000,
      tenhoatchat: "Nifedipin",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Cordaflex",
      tdk: 4591912500,
      tck: 918382500,
      tenhoatchat: "Nifedipin",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Isotic Moxicin",
      tdk: 806250000,
      tck: 161250000,
      tenhoatchat: "Moxifloxacin",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Isotic Moxicin",
      tdk: 5750000000,
      tck: 1150000000,
      tenhoatchat: "Moxifloxacin",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Dalacin C Inj 600mg 4ml",
      tdk: 1310012500,
      tck: 262002500,
      tenhoatchat: "Clindamycin  phosphate",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Dalacin C Inj 600mg 4ml",
      tdk: 5240050000,
      tck: 1048010000,
      tenhoatchat: "Clindamycin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Dropstar",
      tdk: 595000000,
      tck: 119000000,
      tenhoatchat: "Levofloxacin",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Dropstar",
      tdk: 5950000000,
      tck: 1190000000,
      tenhoatchat: "Levofloxacin",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Hydrocortison-Lidocain-Richter 125mg/5ml",
      tdk: 39375000,
      tck: 7875000,
      tenhoatchat: "Hydrocortison",
      nhom: "Thuốc chống Dị ứng"
    },
    {
      tenthuoc: "Hydrocortison-Lidocain-Richter 125mg/5ml",
      tdk: 590625000,
      tck: 118125000,
      tenhoatchat: "Hydrocortison + Lidocain",
      nhom: "Thuốc chống Dị ứng"
    },
    {
      tenthuoc: "Hydrocortison-Lidocain-Richter 125mg/5ml",
      tdk: 5906250000,
      tck: 1181250000,
      tenhoatchat: "Hydrocortison",
      nhom: "Thuốc chống Dị ứng"
    },
    {
      tenthuoc: "Diệp hạ châu TP",
      tdk: 6412500000,
      tck: 1282500000,
      tenhoatchat: "Cao khô diệp hạ châu,lá khô diệp hạ châu",
      nhom: "Thuốc Đông dược"
    },
    {
      tenthuoc: "Albis (ranitidin+tripotassium)",
      tdk: 6374994000,
      tck: 1274998800,
      tenhoatchat: "Ranitidine  + Tripotassium Bismuth Dicitrate + Sucralfate",
      nhom: "Thuốc Dạ dày - Ruột"
    },
    {
      tenthuoc: "Clamoxyl",
      tdk: 6359955000,
      tck: 1271991000,
      tenhoatchat: "Amoxicilin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Tioga",
      tdk: 675000000,
      tck: 135000000,
      tenhoatchat: "Cao actiso, Sài đất, Thương nhĩ tử, Kim ngân hoa, Hạ khô thảo, tá dược vừa đủ",
      nhom: "Thuốc Đông dược"
    },
    {
      tenthuoc: "Tioga",
      tdk: 5625000000,
      tck: 1125000000,
      tenhoatchat: "Cao Actiso, Sài đất, Thương nhĩ tử, Kim ngân hoa, Hạ khô thảo",
      nhom: "Thuốc Đông dược"
    },
    {
      tenthuoc: "Ucyrin",
      tdk: 6298582500,
      tck: 1259716500,
      tenhoatchat: "Clopidogrel",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Hoạt huyết dưỡng não TP",
      tdk: 6250000000,
      tck: 1250000000,
      tenhoatchat: "Cao đinh lăng,cao bạch quả",
      nhom: "Thuốc Đông dược"
    },
    {
      tenthuoc: "Polyclox 750mg (amoxicilin+cloxacilin)",
      tdk: 6250000000,
      tck: 1250000000,
      tenhoatchat: "amoxicilin+cloxacilin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Levofloxacin Stada 500mg",
      tdk: 1181250000,
      tck: 236250000,
      tenhoatchat: "Levofloxacin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Levofloxacin Stada 500mg",
      tdk: 1686825000,
      tck: 337365000,
      tenhoatchat: "Levofloxacin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Levofloxacin Stada 500mg",
      tdk: 3373650000,
      tck: 674730000,
      tenhoatchat: "Levofloxacin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Clarithromycin",
      tdk: 6212500000,
      tck: 1242500000,
      tenhoatchat: "Clarithromycin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Dicortineff",
      tdk: 1299375000,
      tck: 259875000,
      tenhoatchat: "Neomycin sulfat + gramicidin + 9-alpha fluohydrocortison acetat",
      nhom: "Thuốc chống Dị ứng"
    },
    {
      tenthuoc: "Dicortineff",
      tdk: 1437187500,
      tck: 287437500,
      tenhoatchat: "Neomycin sulfat + gramicidin + 9-alpha fluohydrocortison acetat",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Dicortineff",
      tdk: 3450000000,
      tck: 690000000,
      tenhoatchat: "Neomycin+Gramicidin+Fludrococortisone",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Mediclovir 5g",
      tdk: 6150000000,
      tck: 1230000000,
      tenhoatchat: "Aciclovir",
      nhom: "Thuốc chống Virus"
    },
    {
      tenthuoc: "Cefix",
      tdk: 6088875000,
      tck: 1217775000,
      tenhoatchat: "Cefixime",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Milflox",
      tdk: 6075000000,
      tck: 1215000000,
      tenhoatchat: "Moxifloxacin",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Tetracycllin 1% 5g",
      tdk: 6023915625,
      tck: 1204783125,
      tenhoatchat: "Tetracycllin",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Vitamin B1+B6+B13",
      tdk: 6000000000,
      tck: 1200000000,
      tenhoatchat: "Vitamin B1+ vitamin B6+ vitamin B12",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Augmentin BD 625mg",
      tdk: 5956064000,
      tck: 1193600000,
      tenhoatchat: "Amoxicillin trihydrate, Acid Clavulanic",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Venrutin (rutin 500mg + vitamin c 100mg)",
      tdk: 1406248875,
      tck: 281249775,
      tenhoatchat: "rutin  + vitamin c",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Venrutin (rutin 500mg + vitamin c 100mg)",
      tdk: 4528121378,
      tck: 905624276,
      tenhoatchat: "rutin  + vitamin c",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Ednyt (Enalapril) 5mg",
      tdk: 5929175700,
      tck: 1185835140,
      tenhoatchat: "Enalapril",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Praverix 500 mg",
      tdk: 5912500000,
      tck: 1182500000,
      tenhoatchat: "Amoxicilin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Heparin 25000 UI",
      tdk: 5906250000,
      tck: 1181250000,
      tenhoatchat: "Heparin (Natri)",
      nhom: "Thuốc ảnh hưởng đến Máu"
    },
    {
      tenthuoc: "Amlaxopin 5mg",
      tdk: 2444156250,
      tck: 488831250,
      tenhoatchat: "Amlodipin",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Amlaxopin 5mg",
      tdk: 3371248125,
      tck: 674249625,
      tenhoatchat: "Amlodipine",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Meronem 500mg (Meropenem)",
      tdk: 5804662500,
      tck: 1160932500,
      tenhoatchat: "Meropenem",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Novorapid Flexpen",
      tdk: 157646250,
      tck: 31529250,
      tenhoatchat: "Insulin tác dụng nhUK, ngắn (Fast-acting, Short- acting)",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Novorapid Flexpen",
      tdk: 5625000000,
      tck: 1125000000,
      tenhoatchat: "Insulin tác dụng nhUK, ngắn (Fast-acting, Short- acting)",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Scanax 500",
      tdk: 525000000,
      tck: 105000000,
      tenhoatchat: "Ciprofloxacin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Scanax 500",
      tdk: 525000000,
      tck: 105000000,
      tenhoatchat: "Ciprofloxacin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Scanax 500",
      tdk: 1050000000,
      tck: 210000000,
      tenhoatchat: "Ciprofloxacin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Scanax 500",
      tdk: 1575000000,
      tck: 315000000,
      tenhoatchat: "Ciprofloxacin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Scanax 500",
      tdk: 2100000000,
      tck: 420000000,
      tenhoatchat: "Ciprofloxacin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Mixtad",
      tdk: 5774962500,
      tck: 1154992500,
      tenhoatchat: "Insulin người,rADN",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Bidilucil 250",
      tdk: 5670000000,
      tck: 1134000000,
      tenhoatchat: "Meclophenoxat",
      nhom: "Thuốc ảnh hưởng đến Máu"
    },
    {
      tenthuoc: "Panalganeffer 150mg",
      tdk: 2443359000,
      tck: 488671800,
      tenhoatchat: "Paracetamol (acetaminophen)",
      nhom: "Thuốc Hạ nhiệt - Giảm đau"
    },
    {
      tenthuoc: "Panalganeffer 150mg",
      tdk: 3218235000,
      tck: 643647000,
      tenhoatchat: "Paracetamol (acetaminophen)",
      nhom: "Thuốc Hạ nhiệt - Giảm đau"
    },
    {
      tenthuoc: "Claritek granules",
      tdk: 5625000000,
      tck: 1125000000,
      tenhoatchat: "Clarithromycin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Crila forte 500mg",
      tdk: 5625000000,
      tck: 1125000000,
      tenhoatchat: "Cao khô Trinh nữ hoàng cung",
      nhom: "Thuốc Đông dược"
    },
    {
      tenthuoc: "Dex-Tobrin 5ml",
      tdk: 5625000000,
      tck: 1125000000,
      tenhoatchat: "Tobramycin + Dexamethason",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Visulin 0,75g",
      tdk: 5625000000,
      tck: 1125000000,
      tenhoatchat: "Ampicilin +Sulbactam",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Morphin 10mg/1ml",
      tdk: 189000000,
      tck: 37800000,
      tenhoatchat: "Morphin (hydroclorid)",
      nhom: "Thuốc Gây tê - Gây mê"
    },
    {
      tenthuoc: "Morphin 10mg/1ml",
      tdk: 1811250000,
      tck: 362250000,
      tenhoatchat: "Morphin hydroclorid",
      nhom: "Thuốc Gây tê - Gây mê"
    },
    {
      tenthuoc: "Morphin 10mg/1ml",
      tdk: 3543750000,
      tck: 708750000,
      tenhoatchat: "Morphin",
      nhom: "Thuốc Gây tê - Gây mê"
    },
    {
      tenthuoc: "Levonor 1mg",
      tdk: 5512500000,
      tck: 1102500000,
      tenhoatchat: "Nor epinephrin (Nor adrenalin)",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Diệp hạ châu - V",
      tdk: 5510000000,
      tck: 1102000000,
      tenhoatchat: "Lá diệp hạ châu, cao khô diệp hạ châu toàn cây",
      nhom: "Thuốc Đông dược"
    },
    {
      tenthuoc: "Augmentin 250/31.25",
      tdk: 5494600800,
      tck: 1099800000,
      tenhoatchat: "Amoxicillin, Acid Clavulanic",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Vipocef 200",
      tdk: 5450000000,
      tck: 1090000000,
      tenhoatchat: "Cefpodoxim",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Tranexamic acid 250mg/5ml",
      tdk: 196250000,
      tck: 39250000,
      tenhoatchat: "Tranexamic acid 250mg/5ml",
      nhom: "Thuốc ảnh hưởng đến Máu"
    },
    {
      tenthuoc: "Tranexamic acid 250mg/5ml",
      tdk: 5250000000,
      tck: 1050000000,
      tenhoatchat: "Tranexamic acid 250mg/5ml",
      nhom: "Thuốc ảnh hưởng đến Máu"
    },
    {
      tenthuoc: "Nifedipin Hasan 20 Retart",
      tdk: 5439500000,
      tck: 1087900000,
      tenhoatchat: "Nifedipin",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Concor cor 2.5mg",
      tdk: 5385452822,
      tck: 1077090564,
      tenhoatchat: "Bisoprolol",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Seduxen",
      tdk: 287778750,
      tck: 57555750,
      tenhoatchat: "Diazepam",
      nhom: "Thuốc chống Co Giật"
    },
    {
      tenthuoc: "Seduxen",
      tdk: 360937500,
      tck: 72187500,
      tenhoatchat: "Diazepam",
      nhom: "Thuốc chống Co Giật"
    },
    {
      tenthuoc: "Seduxen",
      tdk: 459375000,
      tck: 91875000,
      tenhoatchat: "Diazepam",
      nhom: "Thuốc chống Co Giật"
    },
    {
      tenthuoc: "Seduxen",
      tdk: 472500000,
      tck: 94500000,
      tenhoatchat: "Diazepam",
      nhom: "Thuốc chống Co Giật"
    },
    {
      tenthuoc: "Seduxen",
      tdk: 590625000,
      tck: 118125000,
      tenhoatchat: "Diazepam",
      nhom: "Thuốc chống Co Giật"
    },
    {
      tenthuoc: "Seduxen",
      tdk: 885937500,
      tck: 177187500,
      tenhoatchat: "Diazepam",
      nhom: "Thuốc chống Co Giật"
    },
    {
      tenthuoc: "Seduxen",
      tdk: 2322000000,
      tck: 464400000,
      tenhoatchat: "Diazepam",
      nhom: "Thuốc chống Co Giật"
    },
    {
      tenthuoc: "Morphin",
      tdk: 3937500,
      tck: 787500,
      tenhoatchat: "Morphin (hydroclorid)",
      nhom: "Thuốc Gây tê - Gây mê"
    },
    {
      tenthuoc: "Morphin",
      tdk: 27562500,
      tck: 5512500,
      tenhoatchat: "Morphin (hydroclorid)",
      nhom: "Thuốc Gây tê - Gây mê"
    },
    {
      tenthuoc: "Morphin",
      tdk: 39375000,
      tck: 7875000,
      tenhoatchat: "Morphin (hydroclorid)",
      nhom: "Thuốc Gây tê - Gây mê"
    },
    {
      tenthuoc: "Morphin",
      tdk: 210000000,
      tck: 42000000,
      tenhoatchat: "Morphin (hydroclorid)",
      nhom: "Thuốc Gây tê - Gây mê"
    },
    {
      tenthuoc: "Morphin",
      tdk: 504000000,
      tck: 100800000,
      tenhoatchat: "Morphin",
      nhom: "Thuốc Gây tê - Gây mê"
    },
    {
      tenthuoc: "Morphin",
      tdk: 1968750000,
      tck: 393750000,
      tenhoatchat: "Morphin",
      nhom: "Thuốc Gây tê - Gây mê"
    },
    {
      tenthuoc: "Morphin",
      tdk: 2625000000,
      tck: 525000000,
      tenhoatchat: "Morphin",
      nhom: "Thuốc Gây tê - Gây mê"
    },
    {
      tenthuoc: "Partamol Tab 500mg",
      tdk: 2250000000,
      tck: 450000000,
      tenhoatchat: "Paracetamol",
      nhom: "Thuốc Hạ nhiệt - Giảm đau"
    },
    {
      tenthuoc: "Partamol Tab 500mg",
      tdk: 3125000000,
      tck: 625000000,
      tenhoatchat: "Paracetamol",
      nhom: "Thuốc Hạ nhiệt - Giảm đau"
    },
    {
      tenthuoc: "Diazepam Injection BP 10mg 2ml H10",
      tdk: 257512500,
      tck: 51502500,
      tenhoatchat: "Diazepam",
      nhom: "Thuốc chống Co Giật"
    },
    {
      tenthuoc: "Diazepam Injection BP 10mg 2ml H10",
      tdk: 300431250,
      tck: 60086250,
      tenhoatchat: "Diazepam",
      nhom: "Thuốc chống Co Giật"
    },
    {
      tenthuoc: "Diazepam Injection BP 10mg 2ml H10",
      tdk: 4806900000,
      tck: 961380000,
      tenhoatchat: "Diazepam",
      nhom: "Thuốc chống Co Giật"
    },
    {
      tenthuoc: "Lovenox 40mg/0,4ml",
      tdk: 5336312250,
      tck: 1067262450,
      tenhoatchat: "Natri  Enoxaparin  40mg/ 0,4ml",
      nhom: "Thuốc ảnh hưởng đến Máu"
    },
    {
      tenthuoc: "Pataday",
      tdk: 409683750,
      tck: 81936750,
      tenhoatchat: "Pataday",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Pataday",
      tdk: 4916212500,
      tck: 983242500,
      tenhoatchat: "Olopatadine hydrochloride",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Cefpodoxime 100mg",
      tdk: 1543750000,
      tck: 308750000,
      tenhoatchat: "Cefpodoxim",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Cefpodoxime 100mg",
      tdk: 3740000000,
      tck: 748000000,
      tenhoatchat: "Cefpodoxim",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Amlodipin 5 mg",
      tdk: 5250000000,
      tck: 1050000000,
      tenhoatchat: "Amlodipine",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Eyewise (Moxifloxacin) 0,5%/3ml",
      tdk: 5250000000,
      tck: 1050000000,
      tenhoatchat: "Moxifloxacin",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Ebitac (Enalapril + Hydroclorothiazide) (10+25)",
      tdk: 5249994750,
      tck: 1049998950,
      tenhoatchat: "Enalapril + Hydroclorothiazide",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Lisonorm",
      tdk: 5244750000,
      tck: 1048950000,
      tenhoatchat: "Lisinopril + Amlodipin",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Amlodipin (Domesco) 5mg",
      tdk: 5223493800,
      tck: 1044698760,
      tenhoatchat: "Amlodipin",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "LANTUS Solostar 100IU/ml B/ 5 pens x 3ml",
      tdk: 5212481250,
      tck: 1042496250,
      tenhoatchat: "Insulin tác dụng chậm, kéo dài",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Lantus Solostar 100IU/ml B/5 pensx3ml",
      tdk: 5212481250,
      tck: 1042496250,
      tenhoatchat: "Insulin glargine",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Hyzaar 50mg+12.5mg",
      tdk: 5210946878,
      tck: 1042189376,
      tenhoatchat: "Losartan + hydroclorothiazid",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Bisoprolol Fumarate 2.5mg",
      tdk: 5192775000,
      tck: 1038555000,
      tenhoatchat: "Bisoprolol",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Bipisyn",
      tdk: 5177088000,
      tck: 1035417600,
      tenhoatchat: "Ampicilin +Sulbactam",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Savi 3B",
      tdk: 5175000000,
      tck: 2875000000,
      tenhoatchat: "Vitamin B1      Vitamin B6      Vitamin B12",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Azithromycin(Azicine) 250mg",
      tdk: 5174993250,
      tck: 1034998650,
      tenhoatchat: "Azithromycin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Cefixim 100",
      tdk: 5060475000,
      tck: 1012095000,
      tenhoatchat: "Cefixim",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Acular 5ml",
      tdk: 5043375000,
      tck: 1008675000,
      tenhoatchat: "Ketorolac",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Vitamin AD",
      tdk: 500000000,
      tck: 100000000,
      tenhoatchat: "Vitamin A + Vitamin D",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Vitamin AD",
      tdk: 1000000000,
      tck: 200000000,
      tenhoatchat: "Vitamin A Vitamin D",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Vitamin AD",
      tdk: 1000000000,
      tck: 200000000,
      tenhoatchat: "Vitamin A Vitamin D",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Vitamin AD",
      tdk: 2500000125,
      tck: 500000025,
      tenhoatchat: "Vitamin A + D",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "syrup  Bổ tỳ P/H",
      tdk: 5000000000,
      tck: 2500000000,
      tenhoatchat: "Mỗi 100ml syrup  chứa cao lỏng dược liệu chiết từ: Đẳng sâm; Bạch linh; Bạch truật; Cát cánh; Mạch nha; Cam thảo; Long nhãn; Trần bì; Liên nhục; Sa nhân; Sử quân tử; Bán hạ.",
      nhom: "Thuốc Đông dược"
    },
    {
      tenthuoc: "Cordaflex 20mg",
      tdk: 4998000000,
      tck: 999600000,
      tenhoatchat: "Nifedipin",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Coveram 5mg/5mg",
      tdk: 4941750000,
      tck: 988350000,
      tenhoatchat: "Perindopril Arginine5mg+ Amlodipin 5mg",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Pamilonor",
      tdk: 4935525000,
      tck: 987105000,
      tenhoatchat: "Amlodipine",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Eyewise",
      tdk: 328125000,
      tck: 328125000,
      tenhoatchat: "Moxifloxacin",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Eyewise",
      tdk: 656250000,
      tck: 131250000,
      tenhoatchat: "Moxifloxacin",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Eyewise",
      tdk: 1312500000,
      tck: 262500000,
      tenhoatchat: "Moxifloxacin",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Eyewise",
      tdk: 2625000000,
      tck: 525000000,
      tenhoatchat: "Moxifloxacin",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Pataday 0,2% 2.5ml",
      tdk: 4916212500,
      tck: 983242500,
      tenhoatchat: "Olopatadine hydrochloride",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Refresh Liquigel 15ml",
      tdk: 4908750000,
      tck: 981750000,
      tenhoatchat: "Natri carboxy methylcellulose",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Bisoprolol fumarate 2,5mg",
      tdk: 1633905000,
      tck: 326781000,
      tenhoatchat: "Bisoprolol",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Bisoprolol fumarate 2,5mg",
      tdk: 1633905000,
      tck: 326781000,
      tenhoatchat: "Bisoprolol",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Bisoprolol fumarate 2,5mg",
      tdk: 1633905000,
      tck: 326781000,
      tenhoatchat: "Bisoprolol",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Rutin C 100mg",
      tdk: 2127494250,
      tck: 425498850,
      tenhoatchat: "Vitamin C+Rutine",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Rutin C 100mg",
      tdk: 2729994750,
      tck: 545998950,
      tenhoatchat: "Vitamin C+Rutine",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Lovenox",
      tdk: 4802681250,
      tck: 960536250,
      tenhoatchat: "Enoxaparin",
      nhom: "Thuốc ảnh hưởng đến Máu"
    },
    {
      tenthuoc: "Hoạt huyết dưỡng não (ACP)",
      tdk: 4725000000,
      tck: 945000000,
      tenhoatchat: "Cao đinh lăng,cao bạch quả",
      nhom: "Thuốc Đông dược"
    },
    {
      tenthuoc: "Prospan Cough Liquid",
      tdk: 4725000000,
      tck: 945000000,
      tenhoatchat: "Cao khô lá thường xuân",
      nhom: "Thuốc Đông dược"
    },
    {
      tenthuoc: "Aldan tablets 5mg",
      tdk: 425790000,
      tck: 85158000,
      tenhoatchat: "Amlodipin",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Aldan tablets 5mg",
      tdk: 4273290000,
      tck: 854658000,
      tenhoatchat: "Amlodipin",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Cordaflex 20mg (Nifedipin)",
      tdk: 4685625000,
      tck: 937125000,
      tenhoatchat: "Nifedipin",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Troysar (Losartan) 50mg",
      tdk: 4685623594,
      tck: 937124719,
      tenhoatchat: "Losartan",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Azithromycin 250mg",
      tdk: 653625000,
      tck: 130725000,
      tenhoatchat: "Azithromycin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Azithromycin 250mg",
      tdk: 1174530000,
      tck: 234906000,
      tenhoatchat: "Azithromycin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Azithromycin 250mg",
      tdk: 1307250000,
      tck: 261450000,
      tenhoatchat: "Azithromycin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Azithromycin 250mg",
      tdk: 1525125000,
      tck: 305025000,
      tenhoatchat: "Azithromycin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Tobradex 3,5g",
      tdk: 623726250,
      tck: 124745250,
      tenhoatchat: "Tobramycin + Dexamethason",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Tobradex 3,5g",
      tdk: 1871218125,
      tck: 374243625,
      tenhoatchat: "Tobramycin",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Tobradex 3,5g",
      tdk: 2164375675,
      tck: 432875135,
      tenhoatchat: "Tobramycin + Dexamethasone",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Kavasdin 5",
      tdk: 428749847,
      tck: 85749969,
      tenhoatchat: "Amlodipin",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Kavasdin 5",
      tdk: 980000000,
      tck: 196000000,
      tenhoatchat: "Amlodipin",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Kavasdin 5",
      tdk: 1225000000,
      tck: 245000000,
      tenhoatchat: "Amlodipin",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Kavasdin 5",
      tdk: 1960000000,
      tck: 392000000,
      tenhoatchat: "Amlodipin",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Rutin - Vitamin C",
      tdk: 74000000,
      tck: 14800000,
      tenhoatchat: "Vitamin C+Rutin",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Rutin - Vitamin C",
      tdk: 254374313,
      tck: 50874863,
      tenhoatchat: "Vitamin C+Rutin",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Rutin - Vitamin C",
      tdk: 416250000,
      tck: 83250000,
      tenhoatchat: "Vitamin C + rutine",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Rutin - Vitamin C",
      tdk: 423280000,
      tck: 84656000,
      tenhoatchat: "Vitamin C+Rutin",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Rutin - Vitamin C",
      tdk: 693750000,
      tck: 138750000,
      tenhoatchat: "Vitamin C+Rutin",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Rutin - Vitamin C",
      tdk: 723750000,
      tck: 144750000,
      tenhoatchat: "Vitamin C + rutine",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Rutin - Vitamin C",
      tdk: 1988750000,
      tck: 397750000,
      tenhoatchat: "Vitamin C + rutine",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Levonor 1mg/1ml",
      tdk: 590625000,
      tck: 118125000,
      tenhoatchat: "Nor-epinephrin (Nor- adrenalin)",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Levonor 1mg/1ml",
      tdk: 3937500000,
      tck: 787500000,
      tenhoatchat: "Nor epinephrin (Nor adrenalin)",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Cesyrup (Vitamin C) 1200mg/60ml",
      tdk: 4501909361,
      tck: 900381872,
      tenhoatchat: "Vitamin C",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Natriclorid 0,9%",
      tdk: 4500000000,
      tck: 900000000,
      tenhoatchat: "Natriclorid",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Dalacin C Inj 300mg 2ml",
      tdk: 1228500000,
      tck: 245700000,
      tenhoatchat: "Clindamycin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Dalacin C Inj 300mg 2ml",
      tdk: 3071250000,
      tck: 614250000,
      tenhoatchat: "Clindamycin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Azicin 250 mg",
      tdk: 4283286000,
      tck: 856657200,
      tenhoatchat: "Azithromycin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Aldan tablets",
      tdk: 4273290000,
      tck: 854658000,
      tenhoatchat: "Amlodipin",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Polyclox 1000 (amoxicilin 500mg+cloxacilin 500mg)",
      tdk: 4265625000,
      tck: 853125000,
      tenhoatchat: "amoxicilin+cloxacilin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Lovenox 6000IU/0,6ml",
      tdk: 4243612472,
      tck: 848722494,
      tenhoatchat: "Enoxaparin (natri)",
      nhom: "Thuốc ảnh hưởng đến Máu"
    },
    {
      tenthuoc: "Azopt",
      tdk: 37500,
      tck: 7500,
      tenhoatchat: "Brinzolamid",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Azopt",
      tdk: 729368750,
      tck: 145873750,
      tenhoatchat: "Brinzolamide",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Azopt",
      tdk: 729368750,
      tck: 145873750,
      tenhoatchat: "Brinzolamide",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Azopt",
      tdk: 2771601250,
      tck: 554320250,
      tenhoatchat: "Brinzolamid",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Azopt 1% 5ml",
      tdk: 145875000,
      tck: 29175000,
      tenhoatchat: "Brinzolamid",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Azopt 1% 5ml",
      tdk: 1896358750,
      tck: 379271750,
      tenhoatchat: "Brinzolamid",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Azopt 1% 5ml",
      tdk: 2188106250,
      tck: 437621250,
      tenhoatchat: "Brinzolamid",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Augclmox 250",
      tdk: 4227500000,
      tck: 845500000,
      tenhoatchat: "Amoxicilin + Acid clavulanic",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Enalapril 5mg",
      tdk: 333749981,
      tck: 66749996,
      tenhoatchat: "Enalapril",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Enalapril 5mg",
      tdk: 3893750000,
      tck: 778750000,
      tenhoatchat: "Enalapril",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Ambelin 5",
      tdk: 4200000000,
      tck: 840000000,
      tenhoatchat: "Amlodipin",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Scanax",
      tdk: 1050000000,
      tck: 210000000,
      tenhoatchat: "Ciprofloxacin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Scanax",
      tdk: 3150000000,
      tck: 630000000,
      tenhoatchat: "Ciprofloxacin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "VITAMIN A & D",
      tdk: 126843750,
      tck: 25368750,
      tenhoatchat: "Vitamin A+Vitamin D",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "VITAMIN A & D",
      tdk: 530250000,
      tck: 106050000,
      tenhoatchat: "Vitamin A + D",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "VITAMIN A & D",
      tdk: 1050000000,
      tck: 210000000,
      tenhoatchat: "Vitamin A + D",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "VITAMIN A & D",
      tdk: 2475000000,
      tck: 495000000,
      tenhoatchat: "Vitamin A Vitamin D",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Clopistad",
      tdk: 4143750000,
      tck: 828750000,
      tenhoatchat: "Clopidogrel",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Clarithromycin  stada",
      tdk: 4125000000,
      tck: 825000000,
      tenhoatchat: "Clarithromycin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Corneregel",
      tdk: 4103125000,
      tck: 820625000,
      tenhoatchat: "Dexpanthenol (panthenol)",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Vitamin B1 25mg",
      tdk: 63000000,
      tck: 12600000,
      tenhoatchat: "vitamin B1",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Vitamin B1 25mg",
      tdk: 630000000,
      tck: 126000000,
      tenhoatchat: "vitamin B1",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Vitamin B1 25mg",
      tdk: 1530900000,
      tck: 306180000,
      tenhoatchat: "vitamin B1",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Vitamin B1 25mg",
      tdk: 1858500000,
      tck: 371700000,
      tenhoatchat: "vitamin B1",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Mezathion",
      tdk: 37012500,
      tck: 7402500,
      tenhoatchat: "Spironolactone",
      nhom: "Thuốc lợi tiểu"
    },
    {
      tenthuoc: "Mezathion",
      tdk: 148050000,
      tck: 29610000,
      tenhoatchat: "Spironolacton",
      nhom: "Thuốc lợi tiểu"
    },
    {
      tenthuoc: "Mezathion",
      tdk: 207900000,
      tck: 41580000,
      tenhoatchat: "Spironolacton",
      nhom: "Thuốc lợi tiểu"
    },
    {
      tenthuoc: "Mezathion",
      tdk: 244282500,
      tck: 48856500,
      tenhoatchat: "Spironolacton",
      nhom: "Thuốc lợi tiểu"
    },
    {
      tenthuoc: "Mezathion",
      tdk: 630000000,
      tck: 126000000,
      tenhoatchat: "Spironolacton",
      nhom: "Thuốc lợi tiểu"
    },
    {
      tenthuoc: "Mezathion",
      tdk: 1228815000,
      tck: 245763000,
      tenhoatchat: "Spironolacton",
      nhom: "Thuốc lợi tiểu"
    },
    {
      tenthuoc: "Mezathion",
      tdk: 1575000000,
      tck: 315000000,
      tenhoatchat: "Spironolacton",
      nhom: "Thuốc lợi tiểu"
    },
    {
      tenthuoc: "Levemir Flexpen 300IU/3ml",
      tdk: 4062499875,
      tck: 812499975,
      tenhoatchat: "Insulin tác dụng chậm, kéo dài (Slow-acting, Long-acting)",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Panalganeffer Codein",
      tdk: 4040000000,
      tck: 808000000,
      tenhoatchat: "Paracetamol + codein phosphat",
      nhom: "Thuốc Hạ nhiệt - Giảm đau"
    },
    {
      tenthuoc: "Maxitrol drop",
      tdk: 3990000000,
      tck: 798000000,
      tenhoatchat: "Dexamethason+ Neomycin+ Polymycin B",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Midagentin 250/31,25",
      tdk: 3990000000,
      tck: 798000000,
      tenhoatchat: "Amoxicilin + Acid clavulanic",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Renapril 10mg",
      tdk: 784686000,
      tck: 156937200,
      tenhoatchat: "Enalapril",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Renapril 10mg",
      tdk: 1569372000,
      tck: 313874400,
      tenhoatchat: "Enalapril 10mg",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Renapril 10mg",
      tdk: 1591352000,
      tck: 318270400,
      tenhoatchat: "Enalapril",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Vitamin K1 1mg/1ml",
      tdk: 118125000,
      tck: 23625000,
      tenhoatchat: "Phytomenadion (Vitamin K1)",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Vitamin K1 1mg/1ml",
      tdk: 236250000,
      tck: 47250000,
      tenhoatchat: "Phytomenadion (Vitamin K1)",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Vitamin K1 1mg/1ml",
      tdk: 236250000,
      tck: 47250000,
      tenhoatchat: "Phytomenadion (Vitamin K1)",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Vitamin K1 1mg/1ml",
      tdk: 236250000,
      tck: 47250000,
      tenhoatchat: "Phytomenadion (Vitamin K1)",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Vitamin K1 1mg/1ml",
      tdk: 401625000,
      tck: 80325000,
      tenhoatchat: "Phytomenadion (Vitamin K1)",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Vitamin K1 1mg/1ml",
      tdk: 637875000,
      tck: 70875000,
      tenhoatchat: "Phytomenadion (Vitamin K1)",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Vitamin K1 1mg/1ml",
      tdk: 945000000,
      tck: 189000000,
      tenhoatchat: "Phytomenadion (Vitamin K1)",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Vitamin K1 1mg/1ml",
      tdk: 1082025000,
      tck: 216405000,
      tenhoatchat: "Phytomenadion (Vitamin K1)",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Vitamin B1 100mg",
      tdk: 63000000,
      tck: 12600000,
      tenhoatchat: "vitamin B1",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Vitamin B1 100mg",
      tdk: 63000000,
      tck: 12600000,
      tenhoatchat: "vitamin B1",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Vitamin B1 100mg",
      tdk: 75600000,
      tck: 15120000,
      tenhoatchat: "vitamin B1",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Vitamin B1 100mg",
      tdk: 151200000,
      tck: 30240000,
      tenhoatchat: "vitamin B1",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Vitamin B1 100mg",
      tdk: 163800000,
      tck: 32760000,
      tenhoatchat: "vitamin B1",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Vitamin B1 100mg",
      tdk: 819000000,
      tck: 163800000,
      tenhoatchat: "vitamin B1",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Vitamin B1 100mg",
      tdk: 1134000000,
      tck: 126000000,
      tenhoatchat: "vitamin B1",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Vitamin B1 100mg",
      tdk: 1417500000,
      tck: 283500000,
      tenhoatchat: "vitamin B1",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Verospiron",
      tdk: 3870000000,
      tck: 774000000,
      tenhoatchat: "Spironolacton",
      nhom: "Thuốc lợi tiểu"
    },
    {
      tenthuoc: "SCILIN R 40IU/ml",
      tdk: 1287500000,
      tck: 257500000,
      tenhoatchat: "Insulin tác dụng nhanh, ngắn (Fast-acting, Short-acting)",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "SCILIN R 40IU/ml",
      tdk: 2575000000,
      tck: 515000000,
      tenhoatchat: "Insulin tác dụng nhanh, ngắn (Fast-acting, Short-acting)",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Biloxcin Eye",
      tdk: 164587500,
      tck: 32917500,
      tenhoatchat: "Ofloxacin",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Biloxcin Eye",
      tdk: 210000000,
      tck: 42000000,
      tenhoatchat: "Ofloxacin",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Biloxcin Eye",
      tdk: 216706875,
      tck: 43341375,
      tenhoatchat: "Ofloxacin",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Biloxcin Eye",
      tdk: 255937500,
      tck: 51187500,
      tenhoatchat: "Ofloxacin 0.3%/5ml",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Biloxcin Eye",
      tdk: 274312500,
      tck: 54862500,
      tenhoatchat: "Ofloxacin",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Biloxcin Eye",
      tdk: 329175000,
      tck: 65835000,
      tenhoatchat: "Ofloxacin 0.3%/5ml",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Biloxcin Eye",
      tdk: 1075305000,
      tck: 215061000,
      tenhoatchat: "Ofloxacin",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Biloxcin Eye",
      tdk: 1310400000,
      tck: 262080000,
      tenhoatchat: "Omeprazole",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "KALBENOX",
      tdk: 3812500000,
      tck: 762500000,
      tenhoatchat: "Enoxaparin",
      nhom: "Thuốc ảnh hưởng đến Máu"
    },
    {
      tenthuoc: "Cefpodoxim 100mg",
      tdk: 1749995625,
      tck: 349999125,
      tenhoatchat: "Cefpodoxim",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Cefpodoxim 100mg",
      tdk: 2048749973,
      tck: 409749995,
      tenhoatchat: "Cefpodoxim",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Alcaine",
      tdk: 49225000,
      tck: 9845000,
      tenhoatchat: "Proparacain",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Alcaine",
      tdk: 246125000,
      tck: 49225000,
      tenhoatchat: "Proparacain hydrochlorid",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Alcaine",
      tdk: 894975000,
      tck: 178995000,
      tenhoatchat: "Proparacaine Hydrochloride (Proxymetacaine hydrochloride)",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Aerius Tab 5mg 10's",
      tdk: 1190000000,
      tck: 238000000,
      tenhoatchat: "Desloratadin",
      nhom: "Thuốc chống Dị ứng"
    },
    {
      tenthuoc: "Bupivacain 0.5%/4ml",
      tdk: 1183218750,
      tck: 236643750,
      tenhoatchat: "Bupivacain",
      nhom: "Thuốc Gây tê - Gây mê"
    },
    {
      tenthuoc: "Morphin HCl",
      tdk: 1181250000,
      tck: 236250000,
      tenhoatchat: "Morphin (clohydrat)",
      nhom: "Thuốc Gây tê - Gây mê"
    },
    {
      tenthuoc: "Azithromycin 250mg",
      tdk: 1176525000,
      tck: 235305000,
      tenhoatchat: "Azithromycin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Propofol-Lipuro 1% (10mg/ ml)20ml 5's",
      tdk: 1175625000,
      tck: 653125000,
      tenhoatchat: "Propofol - Lipuro 10mg/ml MCT& LCT, 20ml",
      nhom: "Thuốc Gây tê - Gây mê"
    },
    {
      tenthuoc: "Cefpodoxime 100",
      tdk: 238875000,
      tck: 238875000,
      tenhoatchat: "Cefpodoxim",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Cefpodoxime 100",
      tdk: 931250000,
      tck: 186250000,
      tenhoatchat: "Cefpodoxim",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Tobramycin",
      tdk: 26250000,
      tck: 5250000,
      tenhoatchat: "Tobramycin",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Tobramycin",
      tdk: 150000000,
      tck: 30000000,
      tenhoatchat: "Tobramycin",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Tobramycin",
      tdk: 237500000,
      tck: 47500000,
      tenhoatchat: "Tobramycin",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Tobramycin",
      tdk: 250000000,
      tck: 50000000,
      tenhoatchat: "Tobramycin",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Tobramycin",
      tdk: 500000000,
      tck: 100000000,
      tenhoatchat: "Tobramycin",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Gardenal 10mg V10",
      tdk: 85625000,
      tck: 17125000,
      tenhoatchat: "Phenobarbital",
      nhom: "Thuốc chống Co Giật"
    },
    {
      tenthuoc: "Gardenal 10mg V10",
      tdk: 174000000,
      tck: 34800000,
      tenhoatchat: "Phenobarbital",
      nhom: "Thuốc chống Co Giật"
    },
    {
      tenthuoc: "Gardenal 10mg V10",
      tdk: 345000000,
      tck: 69000000,
      tenhoatchat: "Phenobarbital",
      nhom: "Thuốc chống Co Giật"
    },
    {
      tenthuoc: "Gardenal 10mg V10",
      tdk: 552000000,
      tck: 110400000,
      tenhoatchat: "Phenobarbital",
      nhom: "Thuốc chống Co Giật"
    },
    {
      tenthuoc: "Agicipro",
      tdk: 512000000,
      tck: 102400000,
      tenhoatchat: "Ciprofloxacin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Agicipro",
      tdk: 640000000,
      tck: 128000000,
      tenhoatchat: "Ciprofloxacin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Vitamin K",
      tdk: 1146600000,
      tck: 229320000,
      tenhoatchat: "Vitamin K",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Adrenalin Aguettant 5ml/5mg",
      tdk: 1125000844,
      tck: 225000169,
      tenhoatchat: "Epinephrin",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Vigamox 0.5% 5m",
      tdk: 1124987500,
      tck: 224997500,
      tenhoatchat: "Moxifloxacin",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Vitamin B6 100mg",
      tdk: 32812500,
      tck: 6562500,
      tenhoatchat: "Vitamin B6",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Vitamin B6 100mg",
      tdk: 34374375,
      tck: 6874875,
      tenhoatchat: "Vitamin B6",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Vitamin B6 100mg",
      tdk: 39375000,
      tck: 7875000,
      tenhoatchat: "Vitamin B6",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Vitamin B6 100mg",
      tdk: 229687500,
      tck: 45937500,
      tenhoatchat: "Vitamin B6",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Vitamin B6 100mg",
      tdk: 787500000,
      tck: 157500000,
      tenhoatchat: "Vitamin B6",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Neo-codion",
      tdk: 1120312500,
      tck: 224062500,
      tenhoatchat: "Codein camphosulphonat + Sulfoguaiacol + Cao mềm Grindelia",
      nhom: "Thuốc Đông dược"
    },
    {
      tenthuoc: "Pharmaton",
      tdk: 1116134618,
      tck: 223226924,
      tenhoatchat: "Ginseng G115, vitamins",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Bestdocel (Docetaxel) 20mg",
      tdk: 1115625000,
      tck: 223125000,
      tenhoatchat: "Docetaxel",
      nhom: "Thuốc Ung Thư - Miễn Dịch"
    },
    {
      tenthuoc: "Furosemide Salf",
      tdk: 11000000,
      tck: 11000000,
      tenhoatchat: "Furosemide",
      nhom: "Thuốc lợi tiểu"
    },
    {
      tenthuoc: "Furosemide Salf",
      tdk: 1100000000,
      tck: 220000000,
      tenhoatchat: "Furosemide",
      nhom: "Thuốc lợi tiểu"
    },
    {
      tenthuoc: "Azicin  250 mg",
      tdk: 1104150000,
      tck: 220830000,
      tenhoatchat: "Azithromycin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Transamin 250mg",
      tdk: 1100001000,
      tck: 220000200,
      tenhoatchat: "Tranexamic acid",
      nhom: "Thuốc ảnh hưởng đến Máu"
    },
    {
      tenthuoc: "Thập toàn đại bổ P/H",
      tdk: 1065750000,
      tck: 543750000,
      tenhoatchat: "Bột Bạch thược; Bột Bạch truật; Bột Cam thảo; Bột Đương Quy; Cao Đảng sâm; Bột Phục linh; Bột Quế; Bột Thục địa; Bột Xuyên Khung; Cao Hoàng kỳ.",
      nhom: "Thuốc Đông dược"
    },
    {
      tenthuoc: "Levobact 0,5%5ml (levofloxacin)",
      tdk: 1050000000,
      tck: 210000000,
      tenhoatchat: "Levofloxacin",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Gentamicin",
      tdk: 231625000,
      tck: 46325000,
      tenhoatchat: "Gentamicin",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Gentamicin",
      tdk: 801000000,
      tck: 160200000,
      tenhoatchat: "Gentamicin",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Otrivin 0.1% Nasal Spray 10ml",
      tdk: 1032465000,
      tck: 206493000,
      tenhoatchat: "Xylometazoline hydrochloride",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Biloxcin (ofloxacin) 0,3%/5ml",
      tdk: 478800000,
      tck: 95760000,
      tenhoatchat: "Ofloxacin",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Biloxcin (ofloxacin) 0,3%/5ml",
      tdk: 548625000,
      tck: 109725000,
      tenhoatchat: "Ofloxacin",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Azicine 250 mg",
      tdk: 1020000000,
      tck: 204000000,
      tenhoatchat: "Azithromycin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Cammic 250mg/5ml",
      tdk: 438000000,
      tck: 87600000,
      tenhoatchat: "Tranexamic acid",
      nhom: "Thuốc ảnh hưởng đến Máu"
    },
    {
      tenthuoc: "Cammic 250mg/5ml",
      tdk: 558750000,
      tck: 111750000,
      tenhoatchat: "Tranexamic acid",
      nhom: "Thuốc ảnh hưởng đến Máu"
    },
    {
      tenthuoc: "Aerius 60ml",
      tdk: 986249913,
      tck: 197249983,
      tenhoatchat: "Desloratadin",
      nhom: "Thuốc chống Dị ứng"
    },
    {
      tenthuoc: "Omeprem 20",
      tdk: 3354750000,
      tck: 670950000,
      tenhoatchat: "Omeprazole",
      nhom: "Thuốc Dạ dày - Ruột"
    },
    {
      tenthuoc: "Insunova 30/70",
      tdk: 3350000000,
      tck: 670000000,
      tenhoatchat: "Insulin hỗn hợp 30/70 100IU/ml 10ml",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Thuốc ho K/H",
      tdk: 3350000000,
      tck: 670000000,
      tenhoatchat: "Ma hoàng, hạnh nhân/khô hạnh nhân, quế chi/thạch cao, cam thảo.",
      nhom: "Thuốc Đông dược"
    },
    {
      tenthuoc: "VIDLOX 200 (Cefpodoxim 200mg)",
      tdk: 3349500000,
      tck: 669900000,
      tenhoatchat: "Cefpodoxim",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Vitamin B12",
      tdk: 31250000,
      tck: 6250000,
      tenhoatchat: "Vitamin B12",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Vitamin B12",
      tdk: 125000000,
      tck: 25000000,
      tenhoatchat: "Vitamin B12",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Vitamin B12",
      tdk: 144500000,
      tck: 28900000,
      tenhoatchat: "Vitamin B12 (Cyanocobalamin, Hydroxocobalamin)",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Vitamin B12",
      tdk: 156249844,
      tck: 31249969,
      tenhoatchat: "Vitamin B12",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Vitamin B12",
      tdk: 156250000,
      tck: 31250000,
      tenhoatchat: "Vitamin B12",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Vitamin B12",
      tdk: 250000000,
      tck: 50000000,
      tenhoatchat: "Vitamin B12",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Vitamin B12",
      tdk: 437500000,
      tck: 87500000,
      tenhoatchat: "Vitamin B12(Cyanocobalamin, Hydroxocobalamin)",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Vitamin B12",
      tdk: 2025000000,
      tck: 225000000,
      tenhoatchat: "Vitamin B12",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Ucyrin 75mg (Clopidogrel)",
      tdk: 3279937500,
      tck: 655987500,
      tenhoatchat: "Clopidogrel",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Propofol Kabelgium",
      tdk: 3277968750,
      tck: 655593750,
      tenhoatchat: "Propofol",
      nhom: "Thuốc Gây tê - Gây mê"
    },
    {
      tenthuoc: "Adrenalin 1mg/ml",
      tdk: 1575000000,
      tck: 315000000,
      tenhoatchat: "Epinephrin (adrenalin)",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Adrenalin 1mg/ml",
      tdk: 1665625041,
      tck: 333125008,
      tenhoatchat: "Epinephrin (adrenalin)",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Propofol 1% Kabelgium",
      tdk: 3215625000,
      tck: 643125000,
      tenhoatchat: "Propofol",
      nhom: "Thuốc Gây tê - Gây mê"
    },
    {
      tenthuoc: "Loratadin Stada 10mg",
      tdk: 3187500000,
      tck: 637500000,
      tenhoatchat: "Losartan",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Exforge HCT Tab",
      tdk: 3168724999,
      tck: 633745000,
      tenhoatchat: "Amlodipine + Hydrochlorothiazide + Valsartan",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Sastan - H",
      tdk: 3126870000,
      tck: 625374000,
      tenhoatchat: "Losartan+hydroclorothiazid",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Digoxin Richter 0.25mg",
      tdk: 3123750000,
      tck: 624750000,
      tenhoatchat: "Digoxin",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Venrutine (100mg+500mg)",
      tdk: 3121871878,
      tck: 624374376,
      tenhoatchat: "Rutin+vitamin C",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Tobradex Oint 0,3% +0,1%/3,5g",
      tdk: 3118687500,
      tck: 623737500,
      tenhoatchat: "Tobramycin + Dexamethason",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Tobradex onit",
      tdk: 3118687500,
      tck: 623737500,
      tenhoatchat: "Tobramycin + Dexamethason",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Panadol caplet",
      tdk: 16248600000,
      tck: 3249720000,
      tenhoatchat: "Paracetamol   500mg",
      nhom: "Thuốc Hạ nhiệt - Giảm đau"
    },
    {
      tenthuoc: "Actrapid",
      tdk: 1624993750,
      tck: 324998750,
      tenhoatchat: "Insulin người,rADN",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Actrapid",
      tdk: 4875000000,
      tck: 975000000,
      tenhoatchat: "Insulin người, rADN",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Actrapid",
      tdk: 9668750000,
      tck: 1933750000,
      tenhoatchat: "Insulin trộn, hỗn hợp",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Kalecin 250",
      tdk: 1874250000,
      tck: 374850000,
      tenhoatchat: "Clarithromycin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Kalecin 250",
      tdk: 4499250000,
      tck: 899850000,
      tenhoatchat: "Clarithromycin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Kalecin 250",
      tdk: 4685625000,
      tck: 937125000,
      tenhoatchat: "Clarithromycin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Kalecin 250",
      tdk: 4685625000,
      tck: 937125000,
      tenhoatchat: "Clarithromycin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Denxif",
      tdk: 15625000000,
      tck: 3125000000,
      tenhoatchat: "Digoxin WZF 0.25mg",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Ceftriaxone - Panpharma",
      tdk: 15461875000,
      tck: 3092375000,
      tenhoatchat: "Ceftriaxone",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "VEROSPIRON 25MG",
      tdk: 2786391090,
      tck: 557278218,
      tenhoatchat: "Spironolacton",
      nhom: "Thuốc lợi tiểu"
    },
    {
      tenthuoc: "VEROSPIRON 25MG",
      tdk: 4574062500,
      tck: 914812500,
      tenhoatchat: "Spironolacton",
      nhom: "Thuốc lợi tiểu"
    },
    {
      tenthuoc: "VEROSPIRON 25MG",
      tdk: 8032500000,
      tck: 1606500000,
      tenhoatchat: "Spironolacton",
      nhom: "Thuốc lợi tiểu"
    },
    {
      tenthuoc: "Hadokit",
      tdk: 2875000000,
      tck: 575000000,
      tenhoatchat: "Omeprazol + Tinidazol + Clarithromycin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Hadokit",
      tdk: 12506250000,
      tck: 2501250000,
      tenhoatchat: "Omeprazol+Tinidazol+Clarithromycin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Paringold Injection",
      tdk: 1512500000,
      tck: 302500000,
      tenhoatchat: "Heparin (Natri)",
      nhom: "Thuốc ảnh hưởng đến Máu"
    },
    {
      tenthuoc: "Paringold Injection",
      tdk: 2268750000,
      tck: 453750000,
      tenhoatchat: "Heparin (Natri)",
      nhom: "Thuốc ảnh hưởng đến Máu"
    },
    {
      tenthuoc: "Paringold Injection",
      tdk: 11343750000,
      tck: 2268750000,
      tenhoatchat: "Heparin (Natri)",
      nhom: "Thuốc ảnh hưởng đến Máu"
    },
    {
      tenthuoc: "Erolin",
      tdk: 337500000,
      tck: 337500000,
      tenhoatchat: "Loratadin",
      nhom: "Thuốc chống Dị ứng"
    },
    {
      tenthuoc: "Erolin",
      tdk: 668250000,
      tck: 133650000,
      tenhoatchat: "Loratadin",
      nhom: "Thuốc chống Dị ứng"
    },
    {
      tenthuoc: "Erolin",
      tdk: 1680750000,
      tck: 336150000,
      tenhoatchat: "Loratadin",
      nhom: "Thuốc chống Dị ứng"
    },
    {
      tenthuoc: "Erolin",
      tdk: 4125000000,
      tck: 825000000,
      tenhoatchat: "Loratadin",
      nhom: "Thuốc chống Dị ứng"
    },
    {
      tenthuoc: "Erolin",
      tdk: 8239687500,
      tck: 1647937500,
      tenhoatchat: "Loratadin",
      nhom: "Thuốc chống Dị ứng"
    },
    {
      tenthuoc: "Clarithromycin Stada 250mg",
      tdk: 412500000,
      tck: 82500000,
      tenhoatchat: "Clarithromycin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Clarithromycin Stada 250mg",
      tdk: 2062500000,
      tck: 412500000,
      tenhoatchat: "Clarithromycin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Clarithromycin Stada 250mg",
      tdk: 2062500000,
      tck: 412500000,
      tenhoatchat: "Clarithromycin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Clarithromycin Stada 250mg",
      tdk: 10312500000,
      tck: 2062500000,
      tenhoatchat: "Clarithromycin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Utrogestan 200mg",
      tdk: 866250000,
      tck: 173250000,
      tenhoatchat: "Progesteron",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Utrogestan 200mg",
      tdk: 1732500000,
      tck: 346500000,
      tenhoatchat: "Progesteron",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Utrogestan 200mg",
      tdk: 1732500000,
      tck: 346500000,
      tenhoatchat: "Progesteron",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Utrogestan 200mg",
      tdk: 3491250000,
      tck: 698250000,
      tenhoatchat: "Progesteron",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Utrogestan 200mg",
      tdk: 7008750000,
      tck: 1401750000,
      tenhoatchat: "Progesteron",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Exforge HCT 10/160/12.5mg",
      tdk: 14766256869,
      tck: 2953251374,
      tenhoatchat: "Amlodipin+Valsartan+Hydrochlorothiazide",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Ebitac Forte",
      tdk: 1462500000,
      tck: 292500000,
      tenhoatchat: "Enalapril+ Hydrochlorothiazid",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Ebitac Forte",
      tdk: 3800000000,
      tck: 760000000,
      tenhoatchat: "Enalapril + Hydroclorothiazid",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Ebitac Forte",
      tdk: 9500000000,
      tck: 1900000000,
      tenhoatchat: "Enalapril+ Hydrochlorothiazid",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Clarithromycin Stada 500mg",
      tdk: 673750000,
      tck: 134750000,
      tenhoatchat: "Clarithromycin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Clarithromycin Stada 500mg",
      tdk: 6872250000,
      tck: 1374450000,
      tenhoatchat: "Clarithromycin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Clarithromycin Stada 500mg",
      tdk: 7216000000,
      tck: 1443200000,
      tenhoatchat: "Clarithromycin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Enaminal (elalapril 10mg)",
      tdk: 14698530000,
      tck: 2939706000,
      tenhoatchat: "Enalapril",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Enamigal (enalapril 5mg)",
      tdk: 14623875000,
      tck: 2924775000,
      tenhoatchat: "Enalapril",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Medopiren",
      tdk: 2912500000,
      tck: 582500000,
      tenhoatchat: "Ciprofloxacin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Medopiren",
      tdk: 11650000000,
      tck: 2330000000,
      tenhoatchat: "Ciprofloxacin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Osaki",
      tdk: 14499999938,
      tck: 2899999988,
      tenhoatchat: "Lysin+ Vitamin+ Khoáng chất",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Acuvail",
      tdk: 1526250000,
      tck: 305250000,
      tenhoatchat: "Ketorolac tromethamine",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Acuvail",
      tdk: 12959250000,
      tck: 2591850000,
      tenhoatchat: "Ketorolac tromethamin",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Tartriakson",
      tdk: 2412500000,
      tck: 482500000,
      tenhoatchat: "Ceftriaxone",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Tartriakson",
      tdk: 12062500013,
      tck: 2412500003,
      tenhoatchat: "Ceftriaxone",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Bupivacaine 20mg 0.5% 5 mg/ml",
      tdk: 946575000,
      tck: 189315000,
      tenhoatchat: "Bupivacain (hydroclorid)",
      nhom: "Thuốc Gây tê - Gây mê"
    },
    {
      tenthuoc: "Bupivacaine 20mg 0.5% 5 mg/ml",
      tdk: 1893150000,
      tck: 378630000,
      tenhoatchat: "Bupivacain (hydroclorid)",
      nhom: "Thuốc Gây tê - Gây mê"
    },
    {
      tenthuoc: "Bupivacaine 20mg 0.5% 5 mg/ml",
      tdk: 2319108750,
      tck: 463821750,
      tenhoatchat: "Bupivacain",
      nhom: "Thuốc Gây tê - Gây mê"
    },
    {
      tenthuoc: "Bupivacaine 20mg 0.5% 5 mg/ml",
      tdk: 4496231250,
      tck: 899246250,
      tenhoatchat: "Bupivacain (hydroclorid)",
      nhom: "Thuốc Gây tê - Gây mê"
    },
    {
      tenthuoc: "Bupivacaine 20mg 0.5% 5 mg/ml",
      tdk: 4732875000,
      tck: 946575000,
      tenhoatchat: "Bupivacain (hydroclorid)",
      nhom: "Thuốc Gây tê - Gây mê"
    },
    {
      tenthuoc: "Cefixime 100mg",
      tdk: 400000000,
      tck: 400000000,
      tenhoatchat: "Cefixim",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Cefixime 100mg",
      tdk: 1920000000,
      tck: 384000000,
      tenhoatchat: "Cefixim",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Cefixime 100mg",
      tdk: 11875000000,
      tck: 2375000000,
      tenhoatchat: "Cefixim",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Neo- Tergynan",
      tdk: 1782000000,
      tck: 356400000,
      tenhoatchat: "Metronidazol + Neomycin+Nystatin",
      nhom: "Thuốc chống Nấm"
    },
    {
      tenthuoc: "Neo- Tergynan",
      tdk: 12375000000,
      tck: 2475000000,
      tenhoatchat: "Nystatin + metronidazol + neomycin",
      nhom: "Thuốc chống Nấm"
    },
    {
      tenthuoc: "Sastan-H (Losartan+Hydroclorothiazid) (25+12,5)mg",
      tdk: 3696000462,
      tck: 739200092,
      tenhoatchat: "Losartan+hydroclorothiazid",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Sastan-H (Losartan+Hydroclorothiazid) (25+12,5)mg",
      tdk: 10300497425,
      tck: 2060099485,
      tenhoatchat: "Losartan+hydroclorothiazid",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Vitamin A-D",
      tdk: 26906250,
      tck: 5381250,
      tenhoatchat: "Vitamin A+Vitamin D",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Vitamin A-D",
      tdk: 76875000,
      tck: 15375000,
      tenhoatchat: "Vitamin A+ Vitamin D",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Vitamin A-D",
      tdk: 100000000,
      tck: 20000000,
      tenhoatchat: "Vitamin A+Vitamin D",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Vitamin A-D",
      tdk: 484317225,
      tck: 96863445,
      tenhoatchat: "Vitamin A+Vitamin D",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Vitamin A-D",
      tdk: 3075000000,
      tck: 615000000,
      tenhoatchat: "Vitamin A + D",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Vitamin A-D",
      tdk: 3839906250,
      tck: 767981250,
      tenhoatchat: "Vitamin A + Vitamin D",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Vitamin A-D",
      tdk: 6250000000,
      tck: 1250000000,
      tenhoatchat: "Vitamin A + D",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "CORDARONE 150mg/3ml Inj B/ 6 Amps x 3ml",
      tdk: 22536000,
      tck: 22536000,
      tenhoatchat: "Amiodarone HCL 150mg",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "CORDARONE 150mg/3ml Inj B/ 6 Amps x 3ml",
      tdk: 37560000,
      tck: 7512000,
      tenhoatchat: "Amiodarone HCl",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "CORDARONE 150mg/3ml Inj B/ 6 Amps x 3ml",
      tdk: 45072000,
      tck: 9014400,
      tenhoatchat: "Amiodarone HCL 150mg/ 3ml",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "CORDARONE 150mg/3ml Inj B/ 6 Amps x 3ml",
      tdk: 67608000,
      tck: 13521600,
      tenhoatchat: "Amiodarone HCl",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "CORDARONE 150mg/3ml Inj B/ 6 Amps x 3ml",
      tdk: 81129600,
      tck: 9014400,
      tenhoatchat: "Amiodarone HCL 150mg/ 3ml",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "CORDARONE 150mg/3ml Inj B/ 6 Amps x 3ml",
      tdk: 90144000,
      tck: 18028800,
      tenhoatchat: "Amiodarone HCl",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "CORDARONE 150mg/3ml Inj B/ 6 Amps x 3ml",
      tdk: 180288000,
      tck: 36057600,
      tenhoatchat: "Amiodarone HCl",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "CORDARONE 150mg/3ml Inj B/ 6 Amps x 3ml",
      tdk: 247896000,
      tck: 49579200,
      tenhoatchat: "Amiodarone HCl",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "CORDARONE 150mg/3ml Inj B/ 6 Amps x 3ml",
      tdk: 338040000,
      tck: 67608000,
      tenhoatchat: "Amiodaron (hydrochlorid)",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "CORDARONE 150mg/3ml Inj B/ 6 Amps x 3ml",
      tdk: 405648000,
      tck: 81129600,
      tenhoatchat: "Amiodarone HCL 150mg/ 3ml",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "CORDARONE 150mg/3ml Inj B/ 6 Amps x 3ml",
      tdk: 12244560000,
      tck: 2448912000,
      tenhoatchat: "Amiodaron",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Diprivan Inj 20ml 5's",
      tdk: 2954200000,
      tck: 590840000,
      tenhoatchat: "Propofol",
      nhom: "Thuốc Gây tê - Gây mê"
    },
    {
      tenthuoc: "Diprivan Inj 20ml 5's",
      tdk: 3249620000,
      tck: 649924000,
      tenhoatchat: "Propofol 10mg/ml (1%)",
      nhom: "Thuốc Gây tê - Gây mê"
    },
    {
      tenthuoc: "Diprivan Inj 20ml 5's",
      tdk: 7385500000,
      tck: 1477100000,
      tenhoatchat: "Propofol",
      nhom: "Thuốc Gây tê - Gây mê"
    },
    {
      tenthuoc: "Nexium 40mg",
      tdk: 13436499169,
      tck: 2687299834,
      tenhoatchat: "Esomeprazole",
      nhom: "Thuốc Dạ dày - Ruột"
    },
    {
      tenthuoc: "Maxitrol 5ml",
      tdk: 199500000,
      tck: 39900000,
      tenhoatchat: "Neomycin + Polymicin + Dexamethason",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Maxitrol 5ml",
      tdk: 249375000,
      tck: 49875000,
      tenhoatchat: "Dexamethasone+neomycin+polymycin B 0,1%+3500IU/ml+6000IU/g",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Maxitrol 5ml",
      tdk: 498750000,
      tck: 99750000,
      tenhoatchat: "Dexamethason+ Neomycin+ Polymycin B",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Maxitrol 5ml",
      tdk: 498750000,
      tck: 99750000,
      tenhoatchat: "Dexamethason+ Neomycin+ Polymycin B",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Maxitrol 5ml",
      tdk: 997500000,
      tck: 199500000,
      tenhoatchat: "Dexamethason+ Neomycin+ Polymycin B",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Maxitrol 5ml",
      tdk: 1496250000,
      tck: 299250000,
      tenhoatchat: "Dexamethason+ Neomycin+ Polymycin B",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Maxitrol 5ml",
      tdk: 1496250000,
      tck: 299250000,
      tenhoatchat: "Dexamethason+ Neomycin+ Polymycin B",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Maxitrol 5ml",
      tdk: 2493750000,
      tck: 498750000,
      tenhoatchat: "Dexamethason+ Neomycin+ Polymycin B",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Maxitrol 5ml",
      tdk: 5486250000,
      tck: 1097250000,
      tenhoatchat: "Dexamethason+ Neomycin+ Polymycin B",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Osmofundin",
      tdk: 13387500000,
      tck: 2677500000,
      tenhoatchat: "Mannitol",
      nhom: "Thuốc lợi tiểu"
    },
    {
      tenthuoc: "Omeprem",
      tdk: 13324500000,
      tck: 2664900000,
      tenhoatchat: "Omeprazole",
      nhom: "Thuốc Dạ dày - Ruột"
    },
    {
      tenthuoc: "3B-Medi",
      tdk: 13125000000,
      tck: 2625000000,
      tenhoatchat: "Vitamin B1 + Vitamin B6 + Vitamin B12",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Medoclav 625mg",
      tdk: 6192290000,
      tck: 1238458000,
      tenhoatchat: "Amoxicilin + Acid clavulanic",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Medoclav 625mg",
      tdk: 6900790000,
      tck: 1380158000,
      tenhoatchat: "Amoxicilin+Acid Clavulanic",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Fabafixim",
      tdk: 13085625000,
      tck: 2617125000,
      tenhoatchat: "Cefixim",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Hyzaar Tab 50/12.5 30's",
      tdk: 2605473750,
      tck: 521094750,
      tenhoatchat: "Losartan + hydroclorothiazid",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Hyzaar Tab 50/12.5 30's",
      tdk: 5210947500,
      tck: 1042189500,
      tenhoatchat: "Losartan potassium; Hydrochlorothiazide",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Hyzaar Tab 50/12.5 30's",
      tdk: 5210947500,
      tck: 1042189500,
      tenhoatchat: "Losartan potassium; Hydrochlorothiazide",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Adrenalin",
      tdk: 25625000,
      tck: 5125000,
      tenhoatchat: "Epinephrin (adrenalin)",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Adrenalin",
      tdk: 25625000,
      tck: 5125000,
      tenhoatchat: "Epinephrin (adrenalin)",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Adrenalin",
      tdk: 51250000,
      tck: 10250000,
      tenhoatchat: "Epinephrin (adrenalin)",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Adrenalin",
      tdk: 51250000,
      tck: 10250000,
      tenhoatchat: "Epinephrin (adrenalin)",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Adrenalin",
      tdk: 76875000,
      tck: 15375000,
      tenhoatchat: "Epinephrin (adrenalin)",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Adrenalin",
      tdk: 78750000,
      tck: 15750000,
      tenhoatchat: "Epinephrin (adrenalin)",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Adrenalin",
      tdk: 110625000,
      tck: 22125000,
      tenhoatchat: "Epinephrin (adrenalin)",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Adrenalin",
      tdk: 128125000,
      tck: 25625000,
      tenhoatchat: "Epinephrin (adrenalin)",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Adrenalin",
      tdk: 156312500,
      tck: 31262500,
      tenhoatchat: "Epinephrin (adrenalin)",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Adrenalin",
      tdk: 205000000,
      tck: 41000000,
      tenhoatchat: "Epinephrin (adrenalin)",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Adrenalin",
      tdk: 218750000,
      tck: 43750000,
      tenhoatchat: "Epinephrin (adrenalin)",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Adrenalin",
      tdk: 250000000,
      tck: 50000000,
      tenhoatchat: "Epinephrin (adrenalin)",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Adrenalin",
      tdk: 256250000,
      tck: 51250000,
      tenhoatchat: "Epinephrin (adrenalin)",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Adrenalin",
      tdk: 256250000,
      tck: 51250000,
      tenhoatchat: "Epinephrin (adrenalin)",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Adrenalin",
      tdk: 262500000,
      tck: 52500000,
      tenhoatchat: "Adrenalin(Epinephrin)",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Adrenalin",
      tdk: 262500000,
      tck: 52500000,
      tenhoatchat: "Epinephrin (adrenalin)",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Adrenalin",
      tdk: 294687500,
      tck: 58937500,
      tenhoatchat: "Epinephrin (adrenalin)",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Adrenalin",
      tdk: 328125000,
      tck: 65625000,
      tenhoatchat: "Epinephrin (adrenalin)",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Adrenalin",
      tdk: 350000000,
      tck: 70000000,
      tenhoatchat: "Epinephrin (adrenalin)",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Adrenalin",
      tdk: 359775000,
      tck: 39975000,
      tenhoatchat: "Epinephrin (adrenalin)",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Adrenalin",
      tdk: 393750000,
      tck: 78750000,
      tenhoatchat: "Epinephrin(Adrenalin)",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Adrenalin",
      tdk: 452812500,
      tck: 90562500,
      tenhoatchat: "Epinephrin (adrenalin)",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Adrenalin",
      tdk: 512500000,
      tck: 102500000,
      tenhoatchat: "Epinephrin (adrenalin)",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Adrenalin",
      tdk: 656250000,
      tck: 131250000,
      tenhoatchat: "Epinephrin (adrenalin)",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Adrenalin",
      tdk: 1793750044,
      tck: 358750009,
      tenhoatchat: "Epinephrine (Adrenaline)",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Adrenalin",
      tdk: 2690625000,
      tck: 538125000,
      tenhoatchat: "Epinephrin(Adrenalin)",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Adrenalin",
      tdk: 2756250000,
      tck: 551250000,
      tenhoatchat: "Epinephrin(Adrenalin)",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Cosyndo B",
      tdk: 12993750000,
      tck: 2598750000,
      tenhoatchat: "Vitamin B1 + B6 + B12",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Dicortinef",
      tdk: 12947500000,
      tck: 2589500000,
      tenhoatchat: "neomycin sulfat + gramicidin+ 9-alpha fluohydrocortison",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Ambelin",
      tdk: 12600000000,
      tck: 2520000000,
      tenhoatchat: "Amlodipin",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Bupivacaine Aguettant 5mg/ml",
      tdk: 12600000000,
      tck: 2520000000,
      tenhoatchat: "Bupivacaine hydrochloride",
      nhom: "Thuốc Gây tê - Gây mê"
    },
    {
      tenthuoc: "Exforge HCT 10/160/12,5 mg",
      tdk: 6781071537,
      tck: 1356214307,
      tenhoatchat: "Amlodipin + Valsartan",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Vitamin 3B",
      tdk: 420000000,
      tck: 84000000,
      tenhoatchat: "Vitamin B1 + B6 +B12",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Vitamin 3B",
      tdk: 6349999688,
      tck: 1269999938,
      tenhoatchat: "Vitamin B1 + B6 + B12",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Ostocare",
      tdk: 6750000000,
      tck: 1350000000,
      tenhoatchat: "Calci Gluconate, Vitamin D3",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Cammic",
      tdk: 314250000,
      tck: 62850000,
      tenhoatchat: "Tranexamic acid",
      nhom: "Thuốc ảnh hưởng đến Máu"
    },
    {
      tenthuoc: "Cammic",
      tdk: 472500000,
      tck: 94500000,
      tenhoatchat: "Acid Tranexamic",
      nhom: "Thuốc ảnh hưởng đến Máu"
    },
    {
      tenthuoc: "Cammic",
      tdk: 472500000,
      tck: 94500000,
      tenhoatchat: "Acid Tranexamic",
      nhom: "Thuốc ảnh hưởng đến Máu"
    },
    {
      tenthuoc: "Cammic",
      tdk: 850500000,
      tck: 170100000,
      tenhoatchat: "Acid Tranexamic",
      nhom: "Thuốc ảnh hưởng đến Máu"
    },
    {
      tenthuoc: "Cammic",
      tdk: 1417500000,
      tck: 283500000,
      tenhoatchat: "Tranexamic acid",
      nhom: "Thuốc ảnh hưởng đến Máu"
    },
    {
      tenthuoc: "Cammic",
      tdk: 1562500000,
      tck: 312500000,
      tenhoatchat: "Acid Tranexamic",
      nhom: "Thuốc ảnh hưởng đến Máu"
    },
    {
      tenthuoc: "Cammic",
      tdk: 1653750000,
      tck: 330750000,
      tenhoatchat: "Acid Tranexamic",
      nhom: "Thuốc ảnh hưởng đến Máu"
    },
    {
      tenthuoc: "Ambelin (Amlodipin) 5mg",
      tdk: 6720000000,
      tck: 1344000000,
      tenhoatchat: "Amlodipin",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Cordaflex",
      tdk: 1998000000,
      tck: 399600000,
      tenhoatchat: "Nifedipin",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Cordaflex",
      tdk: 4591912500,
      tck: 918382500,
      tenhoatchat: "Nifedipin",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Isotic Moxicin",
      tdk: 806250000,
      tck: 161250000,
      tenhoatchat: "Moxifloxacin",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Isotic Moxicin",
      tdk: 5750000000,
      tck: 1150000000,
      tenhoatchat: "Moxifloxacin",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Dalacin C Inj 600mg 4ml",
      tdk: 1310012500,
      tck: 262002500,
      tenhoatchat: "Clindamycin  phosphate",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Dalacin C Inj 600mg 4ml",
      tdk: 5240050000,
      tck: 1048010000,
      tenhoatchat: "Clindamycin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Dropstar",
      tdk: 595000000,
      tck: 119000000,
      tenhoatchat: "Levofloxacin",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Dropstar",
      tdk: 5950000000,
      tck: 1190000000,
      tenhoatchat: "Levofloxacin",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Hydrocortison-Lidocain-Richter 125mg/5ml",
      tdk: 39375000,
      tck: 7875000,
      tenhoatchat: "Hydrocortison",
      nhom: "Thuốc chống Dị ứng"
    },
    {
      tenthuoc: "Hydrocortison-Lidocain-Richter 125mg/5ml",
      tdk: 590625000,
      tck: 118125000,
      tenhoatchat: "Hydrocortison + Lidocain",
      nhom: "Thuốc chống Dị ứng"
    },
    {
      tenthuoc: "Hydrocortison-Lidocain-Richter 125mg/5ml",
      tdk: 5906250000,
      tck: 1181250000,
      tenhoatchat: "Hydrocortison",
      nhom: "Thuốc chống Dị ứng"
    },
    {
      tenthuoc: "Diệp hạ châu TP",
      tdk: 6412500000,
      tck: 1282500000,
      tenhoatchat: "Cao khô diệp hạ châu,lá khô diệp hạ châu",
      nhom: "Thuốc Đông dược"
    },
    {
      tenthuoc: "Albis (ranitidin+tripotassium)",
      tdk: 6374994000,
      tck: 1274998800,
      tenhoatchat: "Ranitidine  + Tripotassium Bismuth Dicitrate + Sucralfate",
      nhom: "Thuốc Dạ dày - Ruột"
    },
    {
      tenthuoc: "Clamoxyl",
      tdk: 6359955000,
      tck: 1271991000,
      tenhoatchat: "Amoxicilin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Tioga",
      tdk: 675000000,
      tck: 135000000,
      tenhoatchat: "Cao actiso, Sài đất, Thương nhĩ tử, Kim ngân hoa, Hạ khô thảo, tá dược vừa đủ",
      nhom: "Thuốc Đông dược"
    },
    {
      tenthuoc: "Tioga",
      tdk: 5625000000,
      tck: 1125000000,
      tenhoatchat: "Cao Actiso, Sài đất, Thương nhĩ tử, Kim ngân hoa, Hạ khô thảo",
      nhom: "Thuốc Đông dược"
    },
    {
      tenthuoc: "Ucyrin",
      tdk: 6298582500,
      tck: 1259716500,
      tenhoatchat: "Clopidogrel",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Hoạt huyết dưỡng não TP",
      tdk: 6250000000,
      tck: 1250000000,
      tenhoatchat: "Cao đinh lăng,cao bạch quả",
      nhom: "Thuốc Đông dược"
    },
    {
      tenthuoc: "Polyclox 750mg (amoxicilin+cloxacilin)",
      tdk: 6250000000,
      tck: 1250000000,
      tenhoatchat: "amoxicilin+cloxacilin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Levofloxacin Stada 500mg",
      tdk: 1181250000,
      tck: 236250000,
      tenhoatchat: "Levofloxacin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Levofloxacin Stada 500mg",
      tdk: 1686825000,
      tck: 337365000,
      tenhoatchat: "Levofloxacin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Levofloxacin Stada 500mg",
      tdk: 3373650000,
      tck: 674730000,
      tenhoatchat: "Levofloxacin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Clarithromycin",
      tdk: 6212500000,
      tck: 1242500000,
      tenhoatchat: "Clarithromycin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Dicortineff",
      tdk: 1299375000,
      tck: 259875000,
      tenhoatchat: "Neomycin sulfat + gramicidin + 9-alpha fluohydrocortison acetat",
      nhom: "Thuốc chống Dị ứng"
    },
    {
      tenthuoc: "Dicortineff",
      tdk: 1437187500,
      tck: 287437500,
      tenhoatchat: "Neomycin sulfat + gramicidin + 9-alpha fluohydrocortison acetat",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Dicortineff",
      tdk: 3450000000,
      tck: 690000000,
      tenhoatchat: "Neomycin+Gramicidin+Fludrococortisone",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Mediclovir 5g",
      tdk: 6150000000,
      tck: 1230000000,
      tenhoatchat: "Aciclovir",
      nhom: "Thuốc chống Virus"
    },
    {
      tenthuoc: "Cefix",
      tdk: 6088875000,
      tck: 1217775000,
      tenhoatchat: "Cefixime",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Milflox",
      tdk: 6075000000,
      tck: 1215000000,
      tenhoatchat: "Moxifloxacin",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Tetracycllin 1% 5g",
      tdk: 6023915625,
      tck: 1204783125,
      tenhoatchat: "Tetracycllin",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Vitamin B1+B6+B13",
      tdk: 6000000000,
      tck: 1200000000,
      tenhoatchat: "Vitamin B1+ vitamin B6+ vitamin B12",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Augmentin BD 625mg",
      tdk: 5956064000,
      tck: 1193600000,
      tenhoatchat: "Amoxicillin trihydrate, Acid Clavulanic",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Venrutin (rutin 500mg + vitamin c 100mg)",
      tdk: 1406248875,
      tck: 281249775,
      tenhoatchat: "rutin  + vitamin c",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Venrutin (rutin 500mg + vitamin c 100mg)",
      tdk: 4528121378,
      tck: 905624276,
      tenhoatchat: "rutin  + vitamin c",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Ednyt (Enalapril) 5mg",
      tdk: 5929175700,
      tck: 1185835140,
      tenhoatchat: "Enalapril",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Praverix 500 mg",
      tdk: 5912500000,
      tck: 1182500000,
      tenhoatchat: "Amoxicilin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Heparin 25000 UI",
      tdk: 5906250000,
      tck: 1181250000,
      tenhoatchat: "Heparin (Natri)",
      nhom: "Thuốc ảnh hưởng đến Máu"
    },
    {
      tenthuoc: "Amlaxopin 5mg",
      tdk: 2444156250,
      tck: 488831250,
      tenhoatchat: "Amlodipin",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Amlaxopin 5mg",
      tdk: 3371248125,
      tck: 674249625,
      tenhoatchat: "Amlodipine",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Meronem 500mg (Meropenem)",
      tdk: 5804662500,
      tck: 1160932500,
      tenhoatchat: "Meropenem",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Novorapid Flexpen",
      tdk: 157646250,
      tck: 31529250,
      tenhoatchat: "Insulin tác dụng nhUK, ngắn (Fast-acting, Short- acting)",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Novorapid Flexpen",
      tdk: 5625000000,
      tck: 1125000000,
      tenhoatchat: "Insulin tác dụng nhUK, ngắn (Fast-acting, Short- acting)",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Scanax 500",
      tdk: 525000000,
      tck: 105000000,
      tenhoatchat: "Ciprofloxacin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Scanax 500",
      tdk: 525000000,
      tck: 105000000,
      tenhoatchat: "Ciprofloxacin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Scanax 500",
      tdk: 1050000000,
      tck: 210000000,
      tenhoatchat: "Ciprofloxacin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Scanax 500",
      tdk: 1575000000,
      tck: 315000000,
      tenhoatchat: "Ciprofloxacin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Scanax 500",
      tdk: 2100000000,
      tck: 420000000,
      tenhoatchat: "Ciprofloxacin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Mixtad",
      tdk: 5774962500,
      tck: 1154992500,
      tenhoatchat: "Insulin người,rADN",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Bidilucil 250",
      tdk: 5670000000,
      tck: 1134000000,
      tenhoatchat: "Meclophenoxat",
      nhom: "Thuốc ảnh hưởng đến Máu"
    },
    {
      tenthuoc: "Panalganeffer 150mg",
      tdk: 2443359000,
      tck: 488671800,
      tenhoatchat: "Paracetamol (acetaminophen)",
      nhom: "Thuốc Hạ nhiệt - Giảm đau"
    },
    {
      tenthuoc: "Panalganeffer 150mg",
      tdk: 3218235000,
      tck: 643647000,
      tenhoatchat: "Paracetamol (acetaminophen)",
      nhom: "Thuốc Hạ nhiệt - Giảm đau"
    },
    {
      tenthuoc: "Claritek granules",
      tdk: 5625000000,
      tck: 1125000000,
      tenhoatchat: "Clarithromycin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Crila forte 500mg",
      tdk: 5625000000,
      tck: 1125000000,
      tenhoatchat: "Cao khô Trinh nữ hoàng cung",
      nhom: "Thuốc Đông dược"
    },
    {
      tenthuoc: "Dex-Tobrin 5ml",
      tdk: 5625000000,
      tck: 1125000000,
      tenhoatchat: "Tobramycin + Dexamethason",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Visulin 0,75g",
      tdk: 5625000000,
      tck: 1125000000,
      tenhoatchat: "Ampicilin +Sulbactam",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Morphin 10mg/1ml",
      tdk: 189000000,
      tck: 37800000,
      tenhoatchat: "Morphin (hydroclorid)",
      nhom: "Thuốc Gây tê - Gây mê"
    },
    {
      tenthuoc: "Morphin 10mg/1ml",
      tdk: 1811250000,
      tck: 362250000,
      tenhoatchat: "Morphin hydroclorid",
      nhom: "Thuốc Gây tê - Gây mê"
    },
    {
      tenthuoc: "Morphin 10mg/1ml",
      tdk: 3543750000,
      tck: 708750000,
      tenhoatchat: "Morphin",
      nhom: "Thuốc Gây tê - Gây mê"
    },
    {
      tenthuoc: "Levonor 1mg",
      tdk: 5512500000,
      tck: 1102500000,
      tenhoatchat: "Nor epinephrin (Nor adrenalin)",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Diệp hạ châu - V",
      tdk: 5510000000,
      tck: 1102000000,
      tenhoatchat: "Lá diệp hạ châu, cao khô diệp hạ châu toàn cây",
      nhom: "Thuốc Đông dược"
    },
    {
      tenthuoc: "Augmentin 250/31.25",
      tdk: 5494600800,
      tck: 1099800000,
      tenhoatchat: "Amoxicillin, Acid Clavulanic",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Vipocef 200",
      tdk: 5450000000,
      tck: 1090000000,
      tenhoatchat: "Cefpodoxim",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Tranexamic acid 250mg/5ml",
      tdk: 196250000,
      tck: 39250000,
      tenhoatchat: "Tranexamic acid 250mg/5ml",
      nhom: "Thuốc ảnh hưởng đến Máu"
    },
    {
      tenthuoc: "Tranexamic acid 250mg/5ml",
      tdk: 5250000000,
      tck: 1050000000,
      tenhoatchat: "Tranexamic acid 250mg/5ml",
      nhom: "Thuốc ảnh hưởng đến Máu"
    },
    {
      tenthuoc: "Nifedipin Hasan 20 Retart",
      tdk: 5439500000,
      tck: 1087900000,
      tenhoatchat: "Nifedipin",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Concor cor 2.5mg",
      tdk: 5385452822,
      tck: 1077090564,
      tenhoatchat: "Bisoprolol",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Seduxen",
      tdk: 287778750,
      tck: 57555750,
      tenhoatchat: "Diazepam",
      nhom: "Thuốc chống Co Giật"
    },
    {
      tenthuoc: "Seduxen",
      tdk: 360937500,
      tck: 72187500,
      tenhoatchat: "Diazepam",
      nhom: "Thuốc chống Co Giật"
    },
    {
      tenthuoc: "Seduxen",
      tdk: 459375000,
      tck: 91875000,
      tenhoatchat: "Diazepam",
      nhom: "Thuốc chống Co Giật"
    },
    {
      tenthuoc: "Seduxen",
      tdk: 472500000,
      tck: 94500000,
      tenhoatchat: "Diazepam",
      nhom: "Thuốc chống Co Giật"
    },
    {
      tenthuoc: "Seduxen",
      tdk: 590625000,
      tck: 118125000,
      tenhoatchat: "Diazepam",
      nhom: "Thuốc chống Co Giật"
    },
    {
      tenthuoc: "Seduxen",
      tdk: 885937500,
      tck: 177187500,
      tenhoatchat: "Diazepam",
      nhom: "Thuốc chống Co Giật"
    },
    {
      tenthuoc: "Seduxen",
      tdk: 2322000000,
      tck: 464400000,
      tenhoatchat: "Diazepam",
      nhom: "Thuốc chống Co Giật"
    },
    {
      tenthuoc: "Morphin",
      tdk: 3937500,
      tck: 787500,
      tenhoatchat: "Morphin (hydroclorid)",
      nhom: "Thuốc Gây tê - Gây mê"
    },
    {
      tenthuoc: "Morphin",
      tdk: 27562500,
      tck: 5512500,
      tenhoatchat: "Morphin (hydroclorid)",
      nhom: "Thuốc Gây tê - Gây mê"
    },
    {
      tenthuoc: "Morphin",
      tdk: 39375000,
      tck: 7875000,
      tenhoatchat: "Morphin (hydroclorid)",
      nhom: "Thuốc Gây tê - Gây mê"
    },
    {
      tenthuoc: "Morphin",
      tdk: 210000000,
      tck: 42000000,
      tenhoatchat: "Morphin (hydroclorid)",
      nhom: "Thuốc Gây tê - Gây mê"
    },
    {
      tenthuoc: "Morphin",
      tdk: 504000000,
      tck: 100800000,
      tenhoatchat: "Morphin",
      nhom: "Thuốc Gây tê - Gây mê"
    },
    {
      tenthuoc: "Morphin",
      tdk: 1968750000,
      tck: 393750000,
      tenhoatchat: "Morphin",
      nhom: "Thuốc Gây tê - Gây mê"
    },
    {
      tenthuoc: "Morphin",
      tdk: 2625000000,
      tck: 525000000,
      tenhoatchat: "Morphin",
      nhom: "Thuốc Gây tê - Gây mê"
    },
    {
      tenthuoc: "Partamol Tab 500mg",
      tdk: 2250000000,
      tck: 450000000,
      tenhoatchat: "Paracetamol",
      nhom: "Thuốc Hạ nhiệt - Giảm đau"
    },
    {
      tenthuoc: "Partamol Tab 500mg",
      tdk: 3125000000,
      tck: 625000000,
      tenhoatchat: "Paracetamol",
      nhom: "Thuốc Hạ nhiệt - Giảm đau"
    },
    {
      tenthuoc: "Diazepam Injection BP 10mg 2ml H10",
      tdk: 257512500,
      tck: 51502500,
      tenhoatchat: "Diazepam",
      nhom: "Thuốc chống Co Giật"
    },
    {
      tenthuoc: "Diazepam Injection BP 10mg 2ml H10",
      tdk: 300431250,
      tck: 60086250,
      tenhoatchat: "Diazepam",
      nhom: "Thuốc chống Co Giật"
    },
    {
      tenthuoc: "Diazepam Injection BP 10mg 2ml H10",
      tdk: 4806900000,
      tck: 961380000,
      tenhoatchat: "Diazepam",
      nhom: "Thuốc chống Co Giật"
    },
    {
      tenthuoc: "Lovenox 40mg/0,4ml",
      tdk: 5336312250,
      tck: 1067262450,
      tenhoatchat: "Natri  Enoxaparin  40mg/ 0,4ml",
      nhom: "Thuốc ảnh hưởng đến Máu"
    },
    {
      tenthuoc: "Pataday",
      tdk: 409683750,
      tck: 81936750,
      tenhoatchat: "Pataday",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Pataday",
      tdk: 4916212500,
      tck: 983242500,
      tenhoatchat: "Olopatadine hydrochloride",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Cefpodoxime 100mg",
      tdk: 1543750000,
      tck: 308750000,
      tenhoatchat: "Cefpodoxim",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Cefpodoxime 100mg",
      tdk: 3740000000,
      tck: 748000000,
      tenhoatchat: "Cefpodoxim",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Amlodipin 5 mg",
      tdk: 5250000000,
      tck: 1050000000,
      tenhoatchat: "Amlodipine",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Eyewise (Moxifloxacin) 0,5%/3ml",
      tdk: 5250000000,
      tck: 1050000000,
      tenhoatchat: "Moxifloxacin",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Ebitac (Enalapril + Hydroclorothiazide) (10+25)",
      tdk: 5249994750,
      tck: 1049998950,
      tenhoatchat: "Enalapril + Hydroclorothiazide",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Lisonorm",
      tdk: 5244750000,
      tck: 1048950000,
      tenhoatchat: "Lisinopril + Amlodipin",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Amlodipin (Domesco) 5mg",
      tdk: 5223493800,
      tck: 1044698760,
      tenhoatchat: "Amlodipin",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "LANTUS Solostar 100IU/ml B/ 5 pens x 3ml",
      tdk: 5212481250,
      tck: 1042496250,
      tenhoatchat: "Insulin tác dụng chậm, kéo dài",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Lantus Solostar 100IU/ml B/5 pensx3ml",
      tdk: 5212481250,
      tck: 1042496250,
      tenhoatchat: "Insulin glargine",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Hyzaar 50mg+12.5mg",
      tdk: 5210946878,
      tck: 1042189376,
      tenhoatchat: "Losartan + hydroclorothiazid",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Bisoprolol Fumarate 2.5mg",
      tdk: 5192775000,
      tck: 1038555000,
      tenhoatchat: "Bisoprolol",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Bipisyn",
      tdk: 5177088000,
      tck: 1035417600,
      tenhoatchat: "Ampicilin +Sulbactam",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Savi 3B",
      tdk: 5175000000,
      tck: 2875000000,
      tenhoatchat: "Vitamin B1      Vitamin B6      Vitamin B12",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Azithromycin(Azicine) 250mg",
      tdk: 5174993250,
      tck: 1034998650,
      tenhoatchat: "Azithromycin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Cefixim 100",
      tdk: 5060475000,
      tck: 1012095000,
      tenhoatchat: "Cefixim",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Acular 5ml",
      tdk: 5043375000,
      tck: 1008675000,
      tenhoatchat: "Ketorolac",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Vitamin AD",
      tdk: 500000000,
      tck: 100000000,
      tenhoatchat: "Vitamin A + Vitamin D",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Vitamin AD",
      tdk: 1000000000,
      tck: 200000000,
      tenhoatchat: "Vitamin A Vitamin D",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Vitamin AD",
      tdk: 1000000000,
      tck: 200000000,
      tenhoatchat: "Vitamin A Vitamin D",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Vitamin AD",
      tdk: 2500000125,
      tck: 500000025,
      tenhoatchat: "Vitamin A + D",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "syrup  Bổ tỳ P/H",
      tdk: 5000000000,
      tck: 2500000000,
      tenhoatchat: "Mỗi 100ml syrup  chứa cao lỏng dược liệu chiết từ: Đẳng sâm; Bạch linh; Bạch truật; Cát cánh; Mạch nha; Cam thảo; Long nhãn; Trần bì; Liên nhục; Sa nhân; Sử quân tử; Bán hạ.",
      nhom: "Thuốc Đông dược"
    },
    {
      tenthuoc: "Cordaflex 20mg",
      tdk: 4998000000,
      tck: 999600000,
      tenhoatchat: "Nifedipin",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Coveram 5mg/5mg",
      tdk: 4941750000,
      tck: 988350000,
      tenhoatchat: "Perindopril Arginine5mg+ Amlodipin 5mg",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Pamilonor",
      tdk: 4935525000,
      tck: 987105000,
      tenhoatchat: "Amlodipine",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Eyewise",
      tdk: 328125000,
      tck: 328125000,
      tenhoatchat: "Moxifloxacin",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Eyewise",
      tdk: 656250000,
      tck: 131250000,
      tenhoatchat: "Moxifloxacin",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Eyewise",
      tdk: 1312500000,
      tck: 262500000,
      tenhoatchat: "Moxifloxacin",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Eyewise",
      tdk: 2625000000,
      tck: 525000000,
      tenhoatchat: "Moxifloxacin",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Pataday 0,2% 2.5ml",
      tdk: 4916212500,
      tck: 983242500,
      tenhoatchat: "Olopatadine hydrochloride",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Refresh Liquigel 15ml",
      tdk: 4908750000,
      tck: 981750000,
      tenhoatchat: "Natri carboxy methylcellulose",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Bisoprolol fumarate 2,5mg",
      tdk: 1633905000,
      tck: 326781000,
      tenhoatchat: "Bisoprolol",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Bisoprolol fumarate 2,5mg",
      tdk: 1633905000,
      tck: 326781000,
      tenhoatchat: "Bisoprolol",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Bisoprolol fumarate 2,5mg",
      tdk: 1633905000,
      tck: 326781000,
      tenhoatchat: "Bisoprolol",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Rutin C 100mg",
      tdk: 2127494250,
      tck: 425498850,
      tenhoatchat: "Vitamin C+Rutine",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Rutin C 100mg",
      tdk: 2729994750,
      tck: 545998950,
      tenhoatchat: "Vitamin C+Rutine",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Lovenox",
      tdk: 4802681250,
      tck: 960536250,
      tenhoatchat: "Enoxaparin",
      nhom: "Thuốc ảnh hưởng đến Máu"
    },
    {
      tenthuoc: "Hoạt huyết dưỡng não (ACP)",
      tdk: 4725000000,
      tck: 945000000,
      tenhoatchat: "Cao đinh lăng,cao bạch quả",
      nhom: "Thuốc Đông dược"
    },
    {
      tenthuoc: "Prospan Cough Liquid",
      tdk: 4725000000,
      tck: 945000000,
      tenhoatchat: "Cao khô lá thường xuân",
      nhom: "Thuốc Đông dược"
    },
    {
      tenthuoc: "Aldan tablets 5mg",
      tdk: 425790000,
      tck: 85158000,
      tenhoatchat: "Amlodipin",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Aldan tablets 5mg",
      tdk: 4273290000,
      tck: 854658000,
      tenhoatchat: "Amlodipin",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Cordaflex 20mg (Nifedipin)",
      tdk: 4685625000,
      tck: 937125000,
      tenhoatchat: "Nifedipin",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Troysar (Losartan) 50mg",
      tdk: 4685623594,
      tck: 937124719,
      tenhoatchat: "Losartan",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Azithromycin 250mg",
      tdk: 653625000,
      tck: 130725000,
      tenhoatchat: "Azithromycin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Azithromycin 250mg",
      tdk: 1174530000,
      tck: 234906000,
      tenhoatchat: "Azithromycin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Azithromycin 250mg",
      tdk: 1307250000,
      tck: 261450000,
      tenhoatchat: "Azithromycin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Azithromycin 250mg",
      tdk: 1525125000,
      tck: 305025000,
      tenhoatchat: "Azithromycin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Tobradex 3,5g",
      tdk: 623726250,
      tck: 124745250,
      tenhoatchat: "Tobramycin + Dexamethason",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Tobradex 3,5g",
      tdk: 1871218125,
      tck: 374243625,
      tenhoatchat: "Tobramycin",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Tobradex 3,5g",
      tdk: 2164375675,
      tck: 432875135,
      tenhoatchat: "Tobramycin + Dexamethasone",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Kavasdin 5",
      tdk: 428749847,
      tck: 85749969,
      tenhoatchat: "Amlodipin",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Kavasdin 5",
      tdk: 980000000,
      tck: 196000000,
      tenhoatchat: "Amlodipin",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Kavasdin 5",
      tdk: 1225000000,
      tck: 245000000,
      tenhoatchat: "Amlodipin",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Kavasdin 5",
      tdk: 1960000000,
      tck: 392000000,
      tenhoatchat: "Amlodipin",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Rutin - Vitamin C",
      tdk: 74000000,
      tck: 14800000,
      tenhoatchat: "Vitamin C+Rutin",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Rutin - Vitamin C",
      tdk: 254374313,
      tck: 50874863,
      tenhoatchat: "Vitamin C+Rutin",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Rutin - Vitamin C",
      tdk: 416250000,
      tck: 83250000,
      tenhoatchat: "Vitamin C + rutine",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Rutin - Vitamin C",
      tdk: 423280000,
      tck: 84656000,
      tenhoatchat: "Vitamin C+Rutin",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Rutin - Vitamin C",
      tdk: 693750000,
      tck: 138750000,
      tenhoatchat: "Vitamin C+Rutin",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Rutin - Vitamin C",
      tdk: 723750000,
      tck: 144750000,
      tenhoatchat: "Vitamin C + rutine",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Rutin - Vitamin C",
      tdk: 1988750000,
      tck: 397750000,
      tenhoatchat: "Vitamin C + rutine",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Levonor 1mg/1ml",
      tdk: 590625000,
      tck: 118125000,
      tenhoatchat: "Nor-epinephrin (Nor- adrenalin)",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Levonor 1mg/1ml",
      tdk: 3937500000,
      tck: 787500000,
      tenhoatchat: "Nor epinephrin (Nor adrenalin)",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Cesyrup (Vitamin C) 1200mg/60ml",
      tdk: 4501909361,
      tck: 900381872,
      tenhoatchat: "Vitamin C",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Natriclorid 0,9%",
      tdk: 4500000000,
      tck: 900000000,
      tenhoatchat: "Natriclorid",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Dalacin C Inj 300mg 2ml",
      tdk: 1228500000,
      tck: 245700000,
      tenhoatchat: "Clindamycin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Dalacin C Inj 300mg 2ml",
      tdk: 3071250000,
      tck: 614250000,
      tenhoatchat: "Clindamycin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Azicin 250 mg",
      tdk: 4283286000,
      tck: 856657200,
      tenhoatchat: "Azithromycin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Aldan tablets",
      tdk: 4273290000,
      tck: 854658000,
      tenhoatchat: "Amlodipin",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Polyclox 1000 (amoxicilin 500mg+cloxacilin 500mg)",
      tdk: 4265625000,
      tck: 853125000,
      tenhoatchat: "amoxicilin+cloxacilin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Lovenox 6000IU/0,6ml",
      tdk: 4243612472,
      tck: 848722494,
      tenhoatchat: "Enoxaparin (natri)",
      nhom: "Thuốc ảnh hưởng đến Máu"
    },
    {
      tenthuoc: "Azopt",
      tdk: 37500,
      tck: 7500,
      tenhoatchat: "Brinzolamid",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Azopt",
      tdk: 729368750,
      tck: 145873750,
      tenhoatchat: "Brinzolamide",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Azopt",
      tdk: 729368750,
      tck: 145873750,
      tenhoatchat: "Brinzolamide",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Azopt",
      tdk: 2771601250,
      tck: 554320250,
      tenhoatchat: "Brinzolamid",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Azopt 1% 5ml",
      tdk: 145875000,
      tck: 29175000,
      tenhoatchat: "Brinzolamid",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Azopt 1% 5ml",
      tdk: 1896358750,
      tck: 379271750,
      tenhoatchat: "Brinzolamid",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Azopt 1% 5ml",
      tdk: 2188106250,
      tck: 437621250,
      tenhoatchat: "Brinzolamid",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Augclmox 250",
      tdk: 4227500000,
      tck: 845500000,
      tenhoatchat: "Amoxicilin + Acid clavulanic",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Enalapril 5mg",
      tdk: 333749981,
      tck: 66749996,
      tenhoatchat: "Enalapril",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Enalapril 5mg",
      tdk: 3893750000,
      tck: 778750000,
      tenhoatchat: "Enalapril",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Ambelin 5",
      tdk: 4200000000,
      tck: 840000000,
      tenhoatchat: "Amlodipin",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Scanax",
      tdk: 1050000000,
      tck: 210000000,
      tenhoatchat: "Ciprofloxacin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Scanax",
      tdk: 3150000000,
      tck: 630000000,
      tenhoatchat: "Ciprofloxacin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "VITAMIN A & D",
      tdk: 126843750,
      tck: 25368750,
      tenhoatchat: "Vitamin A+Vitamin D",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "VITAMIN A & D",
      tdk: 530250000,
      tck: 106050000,
      tenhoatchat: "Vitamin A + D",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "VITAMIN A & D",
      tdk: 1050000000,
      tck: 210000000,
      tenhoatchat: "Vitamin A + D",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "VITAMIN A & D",
      tdk: 2475000000,
      tck: 495000000,
      tenhoatchat: "Vitamin A Vitamin D",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Clopistad",
      tdk: 4143750000,
      tck: 828750000,
      tenhoatchat: "Clopidogrel",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Clarithromycin  stada",
      tdk: 4125000000,
      tck: 825000000,
      tenhoatchat: "Clarithromycin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Corneregel",
      tdk: 4103125000,
      tck: 820625000,
      tenhoatchat: "Dexpanthenol (panthenol)",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Vitamin B1 25mg",
      tdk: 63000000,
      tck: 12600000,
      tenhoatchat: "vitamin B1",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Vitamin B1 25mg",
      tdk: 630000000,
      tck: 126000000,
      tenhoatchat: "vitamin B1",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Vitamin B1 25mg",
      tdk: 1530900000,
      tck: 306180000,
      tenhoatchat: "vitamin B1",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Vitamin B1 25mg",
      tdk: 1858500000,
      tck: 371700000,
      tenhoatchat: "vitamin B1",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Mezathion",
      tdk: 37012500,
      tck: 7402500,
      tenhoatchat: "Spironolactone",
      nhom: "Thuốc lợi tiểu"
    },
    {
      tenthuoc: "Mezathion",
      tdk: 148050000,
      tck: 29610000,
      tenhoatchat: "Spironolacton",
      nhom: "Thuốc lợi tiểu"
    },
    {
      tenthuoc: "Mezathion",
      tdk: 207900000,
      tck: 41580000,
      tenhoatchat: "Spironolacton",
      nhom: "Thuốc lợi tiểu"
    },
    {
      tenthuoc: "Mezathion",
      tdk: 244282500,
      tck: 48856500,
      tenhoatchat: "Spironolacton",
      nhom: "Thuốc lợi tiểu"
    },
    {
      tenthuoc: "Mezathion",
      tdk: 630000000,
      tck: 126000000,
      tenhoatchat: "Spironolacton",
      nhom: "Thuốc lợi tiểu"
    },
    {
      tenthuoc: "Mezathion",
      tdk: 1228815000,
      tck: 245763000,
      tenhoatchat: "Spironolacton",
      nhom: "Thuốc lợi tiểu"
    },
    {
      tenthuoc: "Mezathion",
      tdk: 1575000000,
      tck: 315000000,
      tenhoatchat: "Spironolacton",
      nhom: "Thuốc lợi tiểu"
    },
    {
      tenthuoc: "Levemir Flexpen 300IU/3ml",
      tdk: 4062499875,
      tck: 812499975,
      tenhoatchat: "Insulin tác dụng chậm, kéo dài (Slow-acting, Long-acting)",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Panalganeffer Codein",
      tdk: 4040000000,
      tck: 808000000,
      tenhoatchat: "Paracetamol + codein phosphat",
      nhom: "Thuốc Hạ nhiệt - Giảm đau"
    },
    {
      tenthuoc: "Maxitrol drop",
      tdk: 3990000000,
      tck: 798000000,
      tenhoatchat: "Dexamethason+ Neomycin+ Polymycin B",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Midagentin 250/31,25",
      tdk: 3990000000,
      tck: 798000000,
      tenhoatchat: "Amoxicilin + Acid clavulanic",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Renapril 10mg",
      tdk: 784686000,
      tck: 156937200,
      tenhoatchat: "Enalapril",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Renapril 10mg",
      tdk: 1569372000,
      tck: 313874400,
      tenhoatchat: "Enalapril 10mg",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Renapril 10mg",
      tdk: 1591352000,
      tck: 318270400,
      tenhoatchat: "Enalapril",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Vitamin K1 1mg/1ml",
      tdk: 118125000,
      tck: 23625000,
      tenhoatchat: "Phytomenadion (Vitamin K1)",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Vitamin K1 1mg/1ml",
      tdk: 236250000,
      tck: 47250000,
      tenhoatchat: "Phytomenadion (Vitamin K1)",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Vitamin K1 1mg/1ml",
      tdk: 236250000,
      tck: 47250000,
      tenhoatchat: "Phytomenadion (Vitamin K1)",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Vitamin K1 1mg/1ml",
      tdk: 236250000,
      tck: 47250000,
      tenhoatchat: "Phytomenadion (Vitamin K1)",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Vitamin K1 1mg/1ml",
      tdk: 401625000,
      tck: 80325000,
      tenhoatchat: "Phytomenadion (Vitamin K1)",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Vitamin K1 1mg/1ml",
      tdk: 637875000,
      tck: 70875000,
      tenhoatchat: "Phytomenadion (Vitamin K1)",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Vitamin K1 1mg/1ml",
      tdk: 945000000,
      tck: 189000000,
      tenhoatchat: "Phytomenadion (Vitamin K1)",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Vitamin K1 1mg/1ml",
      tdk: 1082025000,
      tck: 216405000,
      tenhoatchat: "Phytomenadion (Vitamin K1)",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Vitamin B1 100mg",
      tdk: 63000000,
      tck: 12600000,
      tenhoatchat: "vitamin B1",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Vitamin B1 100mg",
      tdk: 63000000,
      tck: 12600000,
      tenhoatchat: "vitamin B1",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Vitamin B1 100mg",
      tdk: 75600000,
      tck: 15120000,
      tenhoatchat: "vitamin B1",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Vitamin B1 100mg",
      tdk: 151200000,
      tck: 30240000,
      tenhoatchat: "vitamin B1",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Vitamin B1 100mg",
      tdk: 163800000,
      tck: 32760000,
      tenhoatchat: "vitamin B1",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Vitamin B1 100mg",
      tdk: 819000000,
      tck: 163800000,
      tenhoatchat: "vitamin B1",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Vitamin B1 100mg",
      tdk: 1134000000,
      tck: 126000000,
      tenhoatchat: "vitamin B1",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Vitamin B1 100mg",
      tdk: 1417500000,
      tck: 283500000,
      tenhoatchat: "vitamin B1",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Verospiron",
      tdk: 3870000000,
      tck: 774000000,
      tenhoatchat: "Spironolacton",
      nhom: "Thuốc lợi tiểu"
    },
    {
      tenthuoc: "SCILIN R 40IU/ml",
      tdk: 1287500000,
      tck: 257500000,
      tenhoatchat: "Insulin tác dụng nhanh, ngắn (Fast-acting, Short-acting)",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "SCILIN R 40IU/ml",
      tdk: 2575000000,
      tck: 515000000,
      tenhoatchat: "Insulin tác dụng nhanh, ngắn (Fast-acting, Short-acting)",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Biloxcin Eye",
      tdk: 164587500,
      tck: 32917500,
      tenhoatchat: "Ofloxacin",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Biloxcin Eye",
      tdk: 210000000,
      tck: 42000000,
      tenhoatchat: "Ofloxacin",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Biloxcin Eye",
      tdk: 216706875,
      tck: 43341375,
      tenhoatchat: "Ofloxacin",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Biloxcin Eye",
      tdk: 255937500,
      tck: 51187500,
      tenhoatchat: "Ofloxacin 0.3%/5ml",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Biloxcin Eye",
      tdk: 274312500,
      tck: 54862500,
      tenhoatchat: "Ofloxacin",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Biloxcin Eye",
      tdk: 329175000,
      tck: 65835000,
      tenhoatchat: "Ofloxacin 0.3%/5ml",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Biloxcin Eye",
      tdk: 1075305000,
      tck: 215061000,
      tenhoatchat: "Ofloxacin",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Biloxcin Eye",
      tdk: 1310400000,
      tck: 262080000,
      tenhoatchat: "Omeprazole",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "KALBENOX",
      tdk: 3812500000,
      tck: 762500000,
      tenhoatchat: "Enoxaparin",
      nhom: "Thuốc ảnh hưởng đến Máu"
    },
    {
      tenthuoc: "Cefpodoxim 100mg",
      tdk: 1749995625,
      tck: 349999125,
      tenhoatchat: "Cefpodoxim",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Cefpodoxim 100mg",
      tdk: 2048749973,
      tck: 409749995,
      tenhoatchat: "Cefpodoxim",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Lidocain 2%",
      tdk: 132000000,
      tck: 26400000,
      tenhoatchat: "Lidocain",
      nhom: "Thuốc Gây tê - Gây mê"
    },
    {
      tenthuoc: "Lidocain 2%",
      tdk: 132000000,
      tck: 26400000,
      tenhoatchat: "Lidocain",
      nhom: "Thuốc Gây tê - Gây mê"
    },
    {
      tenthuoc: "Lidocain 2%",
      tdk: 132000000,
      tck: 26400000,
      tenhoatchat: "Lidocain",
      nhom: "Thuốc Gây tê - Gây mê"
    },
    {
      tenthuoc: "Lidocain 2%",
      tdk: 165000000,
      tck: 33000000,
      tenhoatchat: "Lidocain",
      nhom: "Thuốc Gây tê - Gây mê"
    },
    {
      tenthuoc: "Lidocain 2%",
      tdk: 171600000,
      tck: 66000000,
      tenhoatchat: "Lidocain",
      nhom: "Thuốc Gây tê - Gây mê"
    },
    {
      tenthuoc: "Lidocain 2%",
      tdk: 240000000,
      tck: 48000000,
      tenhoatchat: "Lidocain",
      nhom: "Thuốc Gây tê - Gây mê"
    },
    {
      tenthuoc: "Lidocain 2%",
      tdk: 275000000,
      tck: 55000000,
      tenhoatchat: "Lidocain",
      nhom: "Thuốc Gây tê - Gây mê"
    },
    {
      tenthuoc: "Lidocain 2%",
      tdk: 302494500,
      tck: 82498500,
      tenhoatchat: "Lidocaine (hydrocloride)",
      nhom: "Thuốc Gây tê - Gây mê"
    },
    {
      tenthuoc: "Lidocain 2%",
      tdk: 528000000,
      tck: 105600000,
      tenhoatchat: "Lidocain",
      nhom: "Thuốc Gây tê - Gây mê"
    },
    {
      tenthuoc: "Lidocain 2%",
      tdk: 792000000,
      tck: 158400000,
      tenhoatchat: "Lidocain",
      nhom: "Thuốc Gây tê - Gây mê"
    },
    {
      tenthuoc: "Lidocain 2%",
      tdk: 924000000,
      tck: 184800000,
      tenhoatchat: "Lidocain",
      nhom: "Thuốc Gây tê - Gây mê"
    },
    {
      tenthuoc: "Midagentin 250/62,5",
      tdk: 1261250000,
      tck: 252250000,
      tenhoatchat: "Amoxicilin + Acid clavulanic",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Midagentin 250/62,5",
      tdk: 2522500000,
      tck: 504500000,
      tenhoatchat: "Amoxicilin+ Acid clavulanic",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Neostimine-hameln",
      tdk: 3741250000,
      tck: 748250000,
      tenhoatchat: "Neostigmin Bromid",
      nhom: "Thuốc giãn cơ"
    },
    {
      tenthuoc: "Paracetamol 500mg",
      tdk: 1805895000,
      tck: 361179000,
      tenhoatchat: "Paracetamol",
      nhom: "Thuốc Hạ nhiệt - Giảm đau"
    },
    {
      tenthuoc: "Paracetamol 500mg",
      tdk: 1935000000,
      tck: 387000000,
      tenhoatchat: "Paracetamol",
      nhom: "Thuốc Hạ nhiệt - Giảm đau"
    },
    {
      tenthuoc: "Vitfermin",
      tdk: 153000000,
      tck: 30600000,
      tenhoatchat: "Sắt fumarat + acid folic + vitamin B12",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Vitfermin",
      tdk: 218750000,
      tck: 43750000,
      tenhoatchat: "Sắt fumarat + acid folic + vitamin B12",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Vitfermin",
      tdk: 450000000,
      tck: 90000000,
      tenhoatchat: "Sắt fumarat + acid folic + vitamin B12",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Vitfermin",
      tdk: 810000000,
      tck: 162000000,
      tenhoatchat: "Sắt fumarat + acid folic + vitamin B12",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Vitfermin",
      tdk: 2070000000,
      tck: 414000000,
      tenhoatchat: "Sắt fumarat + acid folic + vitamin B12",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "PMS-Claminat 250mg/31,25mg",
      tdk: 3687500000,
      tck: 737500000,
      tenhoatchat: "Amoxicilin + Acidclavulanic",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Valygyno",
      tdk: 525000000,
      tck: 105000000,
      tenhoatchat: "Nystatin + Neomycin + Polymyxin B",
      nhom: "Thuốc chống Nấm"
    },
    {
      tenthuoc: "Valygyno",
      tdk: 525000000,
      tck: 105000000,
      tenhoatchat: "Nystatin + Neomycin+ Polymicin B",
      nhom: "Thuốc chống Nấm"
    },
    {
      tenthuoc: "Valygyno",
      tdk: 1050000000,
      tck: 210000000,
      tenhoatchat: "Nystatin + neomycin + polymyxin B",
      nhom: "Thuốc chống Nấm"
    },
    {
      tenthuoc: "Valygyno",
      tdk: 1575000000,
      tck: 315000000,
      tenhoatchat: "Nystatin + Neomycin + Polymyxin B",
      nhom: "Thuốc chống Nấm"
    },
    {
      tenthuoc: "Propofol 1% Kabi 10mg/ml 20ml",
      tdk: 3642187500,
      tck: 728437500,
      tenhoatchat: "Propofol",
      nhom: "Thuốc Gây tê - Gây mê"
    },
    {
      tenthuoc: "Mitafix",
      tdk: 371562500,
      tck: 74312500,
      tenhoatchat: "Cefixim",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Mitafix",
      tdk: 1485774400,
      tck: 297250000,
      tenhoatchat: "Cefixim",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Mitafix",
      tdk: 1783500000,
      tck: 356700000,
      tenhoatchat: "Cefixim",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Travatan 2.5ml",
      tdk: 3626798125,
      tck: 725359625,
      tenhoatchat: "Travoprost",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Babi B.O.N",
      tdk: 3600000000,
      tck: 720000000,
      tenhoatchat: "Vitamin D3",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Scilin N 100IU/ml 10ml",
      tdk: 3575000100,
      tck: 715000020,
      tenhoatchat: "Insulin chậm (Insulin isophane)",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Boligenax Soft capsules",
      tdk: 3562500000,
      tck: 712500000,
      tenhoatchat: "Nystatin + neomycin + polymycin B",
      nhom: "Thuốc chống Nấm"
    },
    {
      tenthuoc: "CORDARONE TAB 200mg B/ 2bls  x 15 Tabs",
      tdk: 25312500,
      tck: 5062500,
      tenhoatchat: "Amiodarone HCl",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "CORDARONE TAB 200mg B/ 2bls  x 15 Tabs",
      tdk: 582187500,
      tck: 116437500,
      tenhoatchat: "Amiodarone HCl",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "CORDARONE TAB 200mg B/ 2bls  x 15 Tabs",
      tdk: 1265625000,
      tck: 253125000,
      tenhoatchat: "Amiodarone hydrochloride  200 mg",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "CORDARONE TAB 200mg B/ 2bls  x 15 Tabs",
      tdk: 1670625000,
      tck: 334125000,
      tenhoatchat: "Amiodarone HCl",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Hydrocortison + Lidocain",
      tdk: 3543750000,
      tck: 708750000,
      tenhoatchat: "Hydrocortison + Lidocain",
      nhom: "Thuốc chống Dị ứng"
    },
    {
      tenthuoc: "Kim tiền thảo",
      tdk: 262500000,
      tck: 52500000,
      tenhoatchat: "Cao khô kim tiền thảo 120mg",
      nhom: "Thuốc Đông dược"
    },
    {
      tenthuoc: "Kim tiền thảo",
      tdk: 1200000000,
      tck: 240000000,
      tenhoatchat: "Cao khô Kim tiền thảo",
      nhom: "Thuốc Đông dược"
    },
    {
      tenthuoc: "Kim tiền thảo",
      tdk: 2070000000,
      tck: 414000000,
      tenhoatchat: "Cao khô Kim tiền thảo",
      nhom: "Thuốc Đông dược"
    },
    {
      tenthuoc: "Lorista H 50mg+12.5mg",
      tdk: 3532410000,
      tck: 706482000,
      tenhoatchat: "Losartan+hydroclorothiazid",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Clarithromycin tablets 250mg",
      tdk: 3528000000,
      tck: 705600000,
      tenhoatchat: "Clarithromycin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Lostad HCT 50/12,5 mg",
      tdk: 3510000000,
      tck: 702000000,
      tenhoatchat: "Losartan+hydroclorothiazid",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Amdirel",
      tdk: 3500000000,
      tck: 700000000,
      tenhoatchat: "Amlodipin",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Tranecid (acid tranexamic) 250mg/5ml",
      tdk: 3499999781,
      tck: 699999956,
      tenhoatchat: "Acid Tranexamic",
      nhom: "Thuốc ảnh hưởng đến Máu"
    },
    {
      tenthuoc: "Concor Cor Tab 2.5mg 3x10's",
      tdk: 539625000,
      tck: 107925000,
      tenhoatchat: "Bisoprolol fumarate",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Concor Cor Tab 2.5mg 3x10's",
      tdk: 1435402500,
      tck: 287080500,
      tenhoatchat: "Bisoprolol fumarate",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Concor Cor Tab 2.5mg 3x10's",
      tdk: 1510950000,
      tck: 302190000,
      tenhoatchat: "Bisoprolol",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Adalat retard 20mg",
      tdk: 679623706,
      tck: 135924741,
      tenhoatchat: "Nifedipin",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Adalat retard 20mg",
      tdk: 2805623533,
      tck: 561124707,
      tenhoatchat: "Nifedipin",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Nolvadex (Tamoxifen) 10mg",
      tdk: 3472875000,
      tck: 694575000,
      tenhoatchat: "Tamoxifen",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Panalganeffer 500",
      tdk: 3443750000,
      tck: 688750000,
      tenhoatchat: "Paracetamol",
      nhom: "Thuốc Hạ nhiệt - Giảm đau"
    },
    {
      tenthuoc: "Combigan",
      tdk: 917570000,
      tck: 183514000,
      tenhoatchat: "Brimonidine tartrate + Timolol maleat",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Combigan",
      tdk: 1146962500,
      tck: 229392500,
      tenhoatchat: "Brimonidin tartrat + Timolol",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Combigan",
      tdk: 1376355000,
      tck: 275271000,
      tenhoatchat: "Brimonidin tartra + Timonol maleate",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "INSUNOVA -30/70 (BIPHASIC)",
      tdk: 3425000000,
      tck: 685000000,
      tenhoatchat: "Insulin trộn, hỗn hợp (Mixtard-acting, Dual-acting)",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Scilin M30       ( 30/70)",
      tdk: 3425000000,
      tck: 685000000,
      tenhoatchat: "Insulin trộn, hỗn hợp",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Nifedipin T20 Standa retard",
      tdk: 3375000000,
      tck: 675000000,
      tenhoatchat: "Nifedipin",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Aldactone Tab 25mg 100's",
      tdk: 29625000,
      tck: 29625000,
      tenhoatchat: "Spironolactone",
      nhom: "Thuốc lợi tiểu"
    },
    {
      tenthuoc: "Aldactone Tab 25mg 100's",
      tdk: 74062500,
      tck: 14812500,
      tenhoatchat: "Spironolactone",
      nhom: "Thuốc lợi tiểu"
    },
    {
      tenthuoc: "Aldactone Tab 25mg 100's",
      tdk: 98750000,
      tck: 19750000,
      tenhoatchat: "Spironolactone 25mg",
      nhom: "Thuốc lợi tiểu"
    },
    {
      tenthuoc: "Aldactone Tab 25mg 100's",
      tdk: 246875000,
      tck: 49375000,
      tenhoatchat: "Spironolactone",
      nhom: "Thuốc lợi tiểu"
    },
    {
      tenthuoc: "Aldactone Tab 25mg 100's",
      tdk: 246875000,
      tck: 49375000,
      tenhoatchat: "Spironolactone",
      nhom: "Thuốc lợi tiểu"
    },
    {
      tenthuoc: "Aldactone Tab 25mg 100's",
      tdk: 2666250000,
      tck: 296250000,
      tenhoatchat: "Spironolactone 25mg",
      nhom: "Thuốc lợi tiểu"
    },
    {
      tenthuoc: "Omeprem 20",
      tdk: 3354750000,
      tck: 670950000,
      tenhoatchat: "Omeprazole",
      nhom: "Thuốc Dạ dày - Ruột"
    },
    {
      tenthuoc: "Morphin HCl 0.01g H 25",
      tdk: 84000000,
      tck: 16800000,
      tenhoatchat: "Morphin (hydroclorid)",
      nhom: "Thuốc Gây tê - Gây mê"
    },
    {
      tenthuoc: "Morphin HCl 0.01g H 25",
      tdk: 118125000,
      tck: 23625000,
      tenhoatchat: "Morphin",
      nhom: "Thuốc Gây tê - Gây mê"
    },
    {
      tenthuoc: "Morphin HCl 0.01g H 25",
      tdk: 360000000,
      tck: 72000000,
      tenhoatchat: "Morphin",
      nhom: "Thuốc Gây tê - Gây mê"
    },
    {
      tenthuoc: "Morphin HCl 0.01g H 25",
      tdk: 420000000,
      tck: 84000000,
      tenhoatchat: "Morphin (hydroclorid)",
      nhom: "Thuốc Gây tê - Gây mê"
    },
    {
      tenthuoc: "Mibeviru 400mg",
      tdk: 107625000,
      tck: 21525000,
      tenhoatchat: "Aciclovir",
      nhom: "Thuốc chống Virus"
    },
    {
      tenthuoc: "Mibeviru 400mg",
      tdk: 871762500,
      tck: 96862500,
      tenhoatchat: "Aciclovir",
      nhom: "Thuốc chống Virus"
    },
    {
      tenthuoc: "Novomix 30 Flexpen",
      tdk: 142406250,
      tck: 28481250,
      tenhoatchat: "Insulin aspart Biphasic (DNA tái tổ hợp)",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Novomix 30 Flexpen",
      tdk: 786082500,
      tck: 157216500,
      tenhoatchat: "Insulin",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Adalat LA Tab 20mg 30's",
      tdk: 26507250,
      tck: 2945250,
      tenhoatchat: "Nifedipine 20 mg",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Adalat LA Tab 20mg 30's",
      tdk: 50426250,
      tck: 10085250,
      tenhoatchat: "Nifedipine",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Adalat LA Tab 20mg 30's",
      tdk: 59351250,
      tck: 11870250,
      tenhoatchat: "Nifedipine",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Adalat LA Tab 20mg 30's",
      tdk: 74077500,
      tck: 14815500,
      tenhoatchat: "Nifedipine 20 mg",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Adalat LA Tab 20mg 30's",
      tdk: 74315500,
      tck: 14875000,
      tenhoatchat: "Nifedipine 20 mg",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Adalat LA Tab 20mg 30's",
      tdk: 96613125,
      tck: 19322625,
      tenhoatchat: "Nifedipin",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Adalat LA Tab 20mg 30's",
      tdk: 111562500,
      tck: 22312500,
      tenhoatchat: "Nifedipin",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Adalat LA Tab 20mg 30's",
      tdk: 118940500,
      tck: 59500000,
      tenhoatchat: "Nifedipine",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Adalat LA Tab 20mg 30's",
      tdk: 185863125,
      tck: 37172625,
      tenhoatchat: "Nifedipin",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Adalat LA Tab 20mg 30's",
      tdk: 260163750,
      tck: 52032750,
      tenhoatchat: "Nifedipin",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "DBL Irinotecan Injection 100mg/5ml",
      tdk: 945000000,
      tck: 189000000,
      tenhoatchat: "Irinotecan",
      nhom: "Thuốc Ung Thư - Miễn Dịch"
    },
    {
      tenthuoc: "LOVENOX 40mg Inj B/ 2 syringes x0,4ml",
      tdk: 53363125,
      tck: 10672625,
      tenhoatchat: "Enoxaparin natri",
      nhom: "Thuốc ảnh hưởng đến Máu"
    },
    {
      tenthuoc: "LOVENOX 40mg Inj B/ 2 syringes x0,4ml",
      tdk: 10728875,
      tck: 2145775,
      tenhoatchat: "Natri  Enoxaparin",
      nhom: "Thuốc ảnh hưởng đến Máu"
    },
    {
      tenthuoc: "LOVENOX 40mg Inj B/ 2 syringes x0,4ml",
      tdk: 488806225,
      tck: 97761245,
      tenhoatchat: "Enoxaparin",
      nhom: "Thuốc ảnh hưởng đến Máu"
    },
    {
      tenthuoc: "LOVENOX 40mg Inj B/ 2 syringes x0,4ml",
      tdk: 488806225,
      tck: 97761245,
      tenhoatchat: "Enoxaparin",
      nhom: "Thuốc ảnh hưởng đến Máu"
    },
    {
      tenthuoc: "Scilin R 400IU",
      tdk: 154800,
      tck: 30960,
      tenhoatchat: "Insulin tác dụng ngắn (S) 400IU/10ml",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Scilin R 400IU",
      tdk: 30895200,
      tck: 6179040,
      tenhoatchat: "Insulin tác dụng ngắn (S) 400IU/10ml",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Scilin R 400IU",
      tdk: 309000006,
      tck: 618000012,
      tenhoatchat: "Insulin tác dụng ngắn (S) 400IU/10ml",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Scilin R 400IU",
      tdk: 618000012,
      tck: 1236000024,
      tenhoatchat: "Insulin tác dụng ngắn (S) 400IU/10ml",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Scilin R 400IU",
      tdk: 618000012,
      tck: 1236000024,
      tenhoatchat: "Insulin tác dụng ngắn (S) 400IU/10ml",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Scilin R 400IU",
      tdk: 61803000,
      tck: 12360600,
      tenhoatchat: "Insulin tác dụng ngắn (S) 400IU/10ml",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Scilin R 400IU",
      tdk: 622799856,
      tck: 1245599712,
      tenhoatchat: "Insulin tác dụng ngắn (S) 400IU/10ml",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Scilin R 400IU",
      tdk: 72103500,
      tck: 14420700,
      tenhoatchat: "Insulin tác dụng ngắn (S) 400IU/10ml",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Scilin R 400IU",
      tdk: 92685600,
      tck: 18537120,
      tenhoatchat: "Insulin tác dụng ngắn (S) 400IU/10ml",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Scilin R 400IU",
      tdk: 92699964,
      tck: 185399928,
      tenhoatchat: "Insulin tác dụng ngắn (S) 400IU/10ml",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Scilin R 400IU",
      tdk: 123631200,
      tck: 24726240,
      tenhoatchat: "Insulin tác dụng ngắn (S) 400IU/10ml",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Scilin R 400IU",
      tdk: 190543710,
      tck: 38108742,
      tenhoatchat: "Insulin tác dụng ngắn (S) 400IU/10ml",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "3Bpluzs",
      tdk: 29250000,
      tck: 5850000,
      tenhoatchat: "Vitamin Bl+Vitamin B6+Vitamin B12.",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "3Bpluzs",
      tdk: 48750000,
      tck: 9750000,
      tenhoatchat: "Vitamin B1 + Vitamin B6 + Vitamin B12",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "3Bpluzs",
      tdk: 58500000,
      tck: 11700000,
      tenhoatchat: "Vitamin B1 + B6 + B12",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "3Bpluzs",
      tdk: 58500000,
      tck: 11700000,
      tenhoatchat: "Vitamin B1+Vitamin B6+Vitamin B12.",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "3Bpluzs",
      tdk: 78000000,
      tck: 15600000,
      tenhoatchat: "Vitamin Bl+Vitamin B6+Vitamin B12.",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "3Bpluzs",
      tdk: 97500000,
      tck: 19500000,
      tenhoatchat: "Vitamin B1+Vitamin B6+Vitamin B12.",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "3Bpluzs",
      tdk: 97500000,
      tck: 19500000,
      tenhoatchat: "Vitamin B1+Vitamin B6+Vitamin B12.",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "3Bpluzs",
      tdk: 118950000,
      tck: 23790000,
      tenhoatchat: "Vitamin B1 + B6 + B12",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "3Bpluzs",
      tdk: 126750000,
      tck: 25350000,
      tenhoatchat: "Vitamin B1+ vitamin B6+ vitamin B12",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "3Bpluzs",
      tdk: 165325000,
      tck: 53125000,
      tenhoatchat: "Vitamin B1 + Vitamin B6 + Vitamin B12",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "I.V.-Globulin SN inj.",
      tdk: 72750000,
      tck: 14550000,
      tenhoatchat: "Immunoglobulin G",
      nhom: "Chế phẩm từ Máu"
    },
    {
      tenthuoc: "I.V.-Globulin SN inj.",
      tdk: 800250000,
      tck: 160050000,
      tenhoatchat: "Immunoglobulin G",
      nhom: "Chế phẩm từ Máu"
    },
    {
      tenthuoc: "Meronem",
      tdk: 870699375,
      tck: 174139875,
      tenhoatchat: "Meropenem",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Tartriakson  1g",
      tdk: 72375000,
      tck: 14475000,
      tenhoatchat: "Ceftriaxon",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Tartriakson  1g",
      tdk: 120625000,
      tck: 24125000,
      tenhoatchat: "Ceftriaxon",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Tartriakson  1g",
      tdk: 168875000,
      tck: 33775000,
      tenhoatchat: "Ceftriaxon",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Tartriakson  1g",
      tdk: 506625000,
      tck: 101325000,
      tenhoatchat: "Ceftriaxon",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Cozaar 50mg",
      tdk: 103591125,
      tck: 20718225,
      tenhoatchat: "Losartan",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Cozaar 50mg",
      tdk: 857922760,
      tck: 171584552,
      tenhoatchat: "Losartan",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Azipowder",
      tdk: 35500000,
      tck: 35500000,
      tenhoatchat: "Azithromycin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Azipowder",
      tdk: 355000000,
      tck: 71000000,
      tenhoatchat: "Azithromycin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Azipowder",
      tdk: 443750000,
      tck: 88750000,
      tenhoatchat: "Azithromycin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Tiepanem",
      tdk: 8150000001,
      tck: 163000000,
      tenhoatchat: "Meropenem",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Sastan-H",
      tdk: 174930000,
      tck: 34986000,
      tenhoatchat: "Losartan+hydroclorothiazid",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Sastan-H",
      tdk: 281587320,
      tck: 31287480,
      tenhoatchat: "Losartan+hydroclorothiazid",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Sastan-H",
      tdk: 349965000,
      tck: 69993000,
      tenhoatchat: "Losartan+hydroclorothiazid",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Moxilen (amoxicillin) 500mg",
      tdk: 7814033483,
      tck: 1562806697,
      tenhoatchat: "Amoxicilin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Moxacin",
      tdk: 37250000,
      tck: 7450000,
      tenhoatchat: "Amoxicilin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Moxacin",
      tdk: 72450000,
      tck: 14490000,
      tenhoatchat: "Amoxicilin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Moxacin",
      tdk: 102437500,
      tck: 20487500,
      tenhoatchat: "Amoxicilin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Moxacin",
      tdk: 130375000,
      tck: 26075000,
      tenhoatchat: "Amoxicilin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Moxacin",
      tdk: 188112500,
      tck: 37622500,
      tenhoatchat: "Amoxicilin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Moxacin",
      tdk: 235462500,
      tck: 47092500,
      tenhoatchat: "Amoxicilin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Ceftriaxone Panpharma 1g",
      tdk: 7590375844,
      tck: 1518075169,
      tenhoatchat: "Ceftriaxon",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Amoxicilin 500mg",
      tdk: 3215625,
      tck: 643125,
      tenhoatchat: "Amoxicilin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Amoxicilin 500mg",
      tdk: 4064000,
      tck: 812800,
      tenhoatchat: "Amoxicilin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Amoxicilin 500mg",
      tdk: 9646875,
      tck: 1929375,
      tenhoatchat: "Amoxicilin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Amoxicilin 500mg",
      tdk: 16078125,
      tck: 3215625,
      tenhoatchat: "Amoxicilin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Amoxicilin 500mg",
      tdk: 23000000,
      tck: 4600000,
      tenhoatchat: "Amoxicilin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Amoxicilin 500mg",
      tdk: 2874965625,
      tck: 574993125,
      tenhoatchat: "Amoxicilin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Amoxicilin 500mg",
      tdk: 287496875,
      tck: 57499375,
      tenhoatchat: "Amoxicilin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Amoxicilin 500mg",
      tdk: 28750000,
      tck: 5750000,
      tenhoatchat: "Amoxicilin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Amoxicilin 500mg",
      tdk: 28750000,
      tck: 5750000,
      tenhoatchat: "Amoxicilin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Amoxicilin 500mg",
      tdk: 35371875,
      tck: 7074375,
      tenhoatchat: "Amoxicilin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Amoxicilin 500mg",
      tdk: 36658125,
      tck: 7331625,
      tenhoatchat: "Amoxicillin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Amoxicilin 500mg",
      tdk: 67528125,
      tck: 13505625,
      tenhoatchat: "Amoxicilin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Amoxicilin 500mg",
      tdk: 96468750,
      tck: 19293750,
      tenhoatchat: "Amoxicilin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Amoxicilin 500mg",
      tdk: 115762500,
      tck: 23152500,
      tenhoatchat: "Amoxicilin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Amoxicilin 500mg",
      tdk: 231525000,
      tck: 25725000,
      tenhoatchat: "Amoxicilin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Vismed",
      tdk: 752686200,
      tck: 150537240,
      tenhoatchat: "Natri hyaluronat",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Tercef 1g",
      tdk: 139125000,
      tck: 27825000,
      tenhoatchat: "Ceftriaxon",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Tercef 1g",
      tdk: 500850000,
      tck: 55650000,
      tenhoatchat: "Ceftriaxon",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Medphadion Drops 100mg/5ml",
      tdk: 750000000,
      tck: 150000000,
      tenhoatchat: "Phytomenadione (Vitamin K1)",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Coveram 5-10 Tab 5mg-10mg 30's",
      tdk: 8236250,
      tck: 1647250,
      tenhoatchat: "Perindopril + Amlodipine",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Coveram 5-10 Tab 5mg-10mg 30's",
      tdk: 32862637,
      tck: 65725274,
      tenhoatchat: "Perindopril  +  Amlodipin",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Coveram 5-10 Tab 5mg-10mg 30's",
      tdk: 328626375,
      tck: 65725275,
      tenhoatchat: "Perindopril  +  Amlodipin",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Coveram 5-10 Tab 5mg-10mg 30's",
      tdk: 329120550,
      tck: 65824110,
      tenhoatchat: "Perindopril Arginine 5mg, Amlodipine besylate 10mg",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Coveram 5-10 Tab 5mg-10mg 30's",
      tdk: 345922500,
      tck: 69184500,
      tenhoatchat: "Amlodipin +Perindopril",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Insulin H  Mix 100UI",
      tdk: 103950000,
      tck: 20790000,
      tenhoatchat: "Insulin trộn, hỗn hợp",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Insulin H  Mix 100UI",
      tdk: 1913999913,
      tck: 3827999826,
      tenhoatchat: "Insulin trộn, hỗn hợp",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Insulin H  Mix 100UI",
      tdk: 452100000,
      tck: 90420000,
      tenhoatchat: "Insulin trộn, hỗn hợp",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Prospan cough syrup",
      tdk: 43509375,
      tck: 8701875,
      tenhoatchat: "Cao khô lá thường xuân",
      nhom: "Thuốc Đông dược"
    },
    {
      tenthuoc: "Prospan cough syrup",
      tdk: 702843750,
      tck: 140568750,
      tenhoatchat: "Cao khô lá thường xuân",
      nhom: "Thuốc Đông dược"
    },
    {
      tenthuoc: "Sanlein",
      tdk: 7769749995,
      tck: 1553949999,
      tenhoatchat: "Natri hyaluronate",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Sanlein",
      tdk: 236250000,
      tck: 47250000,
      tenhoatchat: "Natri hyaluronat",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Sanlein",
      tdk: 427336250,
      tck: 85467250,
      tenhoatchat: "Natri hyaluronat",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Diprivan Inj 20ml 5's 10mg/ml (1%) - 20ml",
      tdk: 723779000,
      tck: 144755800,
      tenhoatchat: "Propofol",
      nhom: "Thuốc Gây tê - Gây mê"
    },
    {
      tenthuoc: "DBL Irinotecan Injection 40mg/2ml",
      tdk: 708750000,
      tck: 141750000,
      tenhoatchat: "Irinotecan",
      nhom: "Thuốc Ung Thư - Miễn Dịch"
    },
    {
      tenthuoc: "Proxacin 200mg",
      tdk: 7087499969,
      tck: 1417499994,
      tenhoatchat: "Ciprofloxacin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Moxilen 500mg",
      tdk: 59500000,
      tck: 11900000,
      tenhoatchat: "Amoxicilin 500mg",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Moxilen 500mg",
      tdk: 89250000,
      tck: 17850000,
      tenhoatchat: "Amoxicilin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Moxilen 500mg",
      tdk: 121562500,
      tck: 24312500,
      tenhoatchat: "Amoxicilin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Moxilen 500mg",
      tdk: 121562500,
      tck: 24312500,
      tenhoatchat: "Amoxicilin 500mg",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Moxilen 500mg",
      tdk: 153168750,
      tck: 30633750,
      tenhoatchat: "Amoxicilin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Moxilen 500mg",
      tdk: 160650000,
      tck: 89250000,
      tenhoatchat: "Amoxicilin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Taxotere (Docetaxel) 80mg/4ml",
      tdk: 1401623749,
      tck: 2803247499,
      tenhoatchat: "Docetaxel",
      nhom: "Thuốc Ung Thư - Miễn Dịch"
    },
    {
      tenthuoc: "Taxotere (Docetaxel) 80mg/4ml",
      tdk: 560649495,
      tck: 112129899,
      tenhoatchat: "Docetaxel",
      nhom: "Thuốc Ung Thư - Miễn Dịch"
    },
    {
      tenthuoc: "Vismed 0.18",
      tdk: 688432500,
      tck: 137686500,
      tenhoatchat: "Natri hyaluronat",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Isotic Moxisone",
      tdk: 9375000,
      tck: 1875000,
      tenhoatchat: "Moxifloxacin + Dexamethason",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Isotic Moxisone",
      tdk: 19531250,
      tck: 3906250,
      tenhoatchat: "Moxifloxacin + Dexamethason",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Isotic Moxisone",
      tdk: 28125000,
      tck: 5625000,
      tenhoatchat: "Moxifloxacin + Dexamethason",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Isotic Moxisone",
      tdk: 62500000,
      tck: 12500000,
      tenhoatchat: "Moxifloxacin + Dexamethason",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Isotic Moxisone",
      tdk: 125000000,
      tck: 25000000,
      tenhoatchat: "Moxifloxacin + Dexamethason",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Isotic Moxisone",
      tdk: 221875000,
      tck: 44375000,
      tenhoatchat: "Moxifloxacin + Dexamethason",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Isotic Moxisone",
      tdk: 221875000,
      tck: 44375000,
      tenhoatchat: "Moxifloxacin + Dexamethason",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Tercef",
      tdk: 44520000,
      tck: 8904000,
      tenhoatchat: "Ceftriaxon",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Tercef",
      tdk: 222600000,
      tck: 44520000,
      tenhoatchat: "Ceftriaxon*",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Tercef",
      tdk: 417375000,
      tck: 83475000,
      tenhoatchat: "Ceftriaxon",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Bupivacaine Aguettant 5mg/ml x 20ml 0,5%/20ml",
      tdk: 6646236188,
      tck: 1329247238,
      tenhoatchat: "Bupivacain (hydroclorid)",
      nhom: "Thuốc Gây tê - Gây mê"
    },
    {
      tenthuoc: "Cravit 5mg/ ml",
      tdk: 23833485,
      tck: 2648165,
      tenhoatchat: "Levofloxacin",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Cravit 5mg/ ml",
      tdk: 285187000,
      tck: 57037400,
      tenhoatchat: "Levofloxacin",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Cravit 5mg/ ml",
      tdk: 331020625,
      tck: 66204125,
      tenhoatchat: "Levofloxacin",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Exforge 5/80mg",
      tdk: 2621587894,
      tck: 5243175788,
      tenhoatchat: "Amlodipin + Valsartan",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Exforge 5/80mg",
      tdk: 3743626725,
      tck: 748725345,
      tenhoatchat: "Amlodipin + Valsartan",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Fabapoxim",
      tdk: 5985000,
      tck: 1197000,
      tenhoatchat: "Cefpodoxim",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Fabapoxim",
      tdk: 17955000,
      tck: 3591000,
      tenhoatchat: "Cefpodoxim",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Fabapoxim",
      tdk: 120000000,
      tck: 24000000,
      tenhoatchat: "Cefpodoxim",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Fabapoxim",
      tdk: 225400000,
      tck: 45080000,
      tenhoatchat: "Cefpodoxim",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Fabapoxim",
      tdk: 250000000,
      tck: 50000000,
      tenhoatchat: "Cefpodoxim",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Unasyl",
      tdk: 618750000,
      tck: 123750000,
      tenhoatchat: "Ampicilin + sulbactam",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Paclitaxel Ebewe Inj 100MG/16.7ML 1's",
      tdk: 211994750,
      tck: 42398950,
      tenhoatchat: "Paclitaxel,6mg/ml",
      nhom: "Thuốc Ung Thư - Miễn Dịch"
    },
    {
      tenthuoc: "Paclitaxel Ebewe Inj 100MG/16.7ML 1's",
      tdk: 393933000,
      tck: 78786600,
      tenhoatchat: "Paclitacel",
      nhom: "Thuốc Ung Thư - Miễn Dịch"
    },
    {
      tenthuoc: "Levonor 4mg/4ml",
      tdk: 2551500,
      tck: 510300,
      tenhoatchat: "Nor epinephrin (Nor adrenalin)",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Levonor 4mg/4ml",
      tdk: 212625000,
      tck: 42525000,
      tenhoatchat: "Nor epinephrin (Nor adrenalin)",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Levonor 4mg/4ml",
      tdk: 387187500,
      tck: 77437500,
      tenhoatchat: "Nor epinephrin (Nor adrenalin)",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Aldacton tab",
      tdk: 592500000,
      tck: 118500000,
      tenhoatchat: "Spironolactone",
      nhom: "Thuốc lợi tiểu"
    },
    {
      tenthuoc: "Adalat LA 30mg",
      tdk: 5906386812,
      tck: 1181277363,
      tenhoatchat: "Nifedipin",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Progut",
      tdk: 590625000,
      tck: 118125000,
      tenhoatchat: "Esomeprazole",
      nhom: "Thuốc Dạ dày - Ruột"
    },
    {
      tenthuoc: "Adalat LA Tab 20mg",
      tdk: 37038750,
      tck: 7407750,
      tenhoatchat: "Nifedipin",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Adalat LA Tab 20mg",
      tdk: 550226250,
      tck: 110045250,
      tenhoatchat: "Nifedipin",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Exforge 5mg+80mg",
      tdk: 5865359814,
      tck: 1173071963,
      tenhoatchat: "Amlodipin + Valsartan",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Ebitac (Enalapril 10mg+Hydroclorothiazid 25mg)",
      tdk: 124200000,
      tck: 24840000,
      tenhoatchat: "Enalapril 10mg + Hydroclorothiazid 25mg",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Ebitac (Enalapril 10mg+Hydroclorothiazid 25mg)",
      tdk: 4602285326,
      tck: 9894568452,
      tenhoatchat: "Enalapril maleate 10mg + Hydrochlorothiazide 25mg",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Boganic",
      tdk: 15500000,
      tck: 3100000,
      tenhoatchat: "Cao đặc Actiso + Cao đặc Rau đắng đất + Cao đặc Bìm bìm",
      nhom: "Thuốc Đông dược"
    },
    {
      tenthuoc: "Boganic",
      tdk: 31000000,
      tck: 6200000,
      tenhoatchat: "Cao đặc Actiso + Cao đặc Rau đắng đất + Cao đặc Bìm bìm",
      nhom: "Thuốc Đông dược"
    },
    {
      tenthuoc: "Boganic",
      tdk: 77500000,
      tck: 15500000,
      tenhoatchat: "Cao Actiso + Cao đặc rau đắng đất + Cao đặc bìm bìm",
      nhom: "Thuốc Đông dược"
    },
    {
      tenthuoc: "Boganic",
      tdk: 457250000,
      tck: 91450000,
      tenhoatchat: "Cao khô Artiso+Cao khô rau đắng đất+Cao khô bìm bìm",
      nhom: "Thuốc Đông dược"
    },
    {
      tenthuoc: "Mecefix-B.E",
      tdk: 150000000,
      tck: 30000000,
      tenhoatchat: "Cefixim",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Mecefix-B.E",
      tdk: 150000000,
      tck: 30000000,
      tenhoatchat: "Cefixim",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Mecefix-B.E",
      tdk: 281250000,
      tck: 56250000,
      tenhoatchat: "Cefixim",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Meronem 500mg",
      tdk: 580466250,
      tck: 116093250,
      tenhoatchat: "Meropenem",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Nexium",
      tdk: 5758499644,
      tck: 1151699929,
      tenhoatchat: "Esomeprazole",
      nhom: "Thuốc Dạ dày - Ruột"
    },
    {
      tenthuoc: "Franilax",
      tdk: 2908125,
      tck: 581625,
      tenhoatchat: "Furosemide + Spironolacton",
      nhom: "Thuốc lợi tiểu"
    },
    {
      tenthuoc: "Franilax",
      tdk: 8250000,
      tck: 1650000,
      tenhoatchat: "Furosemide + Spironolacton",
      nhom: "Thuốc lợi tiểu"
    },
    {
      tenthuoc: "Franilax",
      tdk: 156187475,
      tck: 31237495,
      tenhoatchat: "Spironolacton + Furosemid",
      nhom: "Thuốc lợi tiểu"
    },
    {
      tenthuoc: "Franilax",
      tdk: 407673000,
      tck: 81534600,
      tenhoatchat: "Furosemid + Spironolacton",
      nhom: "Thuốc lợi tiểu"
    },
    {
      tenthuoc: "Gardenal 100mg",
      tdk: 89775,
      tck: 17955,
      tenhoatchat: "Phenobarbital",
      nhom: "Thuốc chống Co Giật"
    },
    {
      tenthuoc: "Gardenal 100mg",
      tdk: 178125,
      tck: 35625,
      tenhoatchat: "Phenobarbital",
      nhom: "Thuốc chống Co Giật"
    },
    {
      tenthuoc: "Gardenal 100mg",
      tdk: 29925,
      tck: 5985,
      tenhoatchat: "Phenobarbital",
      nhom: "Thuốc chống Co Giật"
    },
    {
      tenthuoc: "Gardenal 100mg",
      tdk: 53865,
      tck: 5985,
      tenhoatchat: "Phenobarbital",
      nhom: "Thuốc chống Co Giật"
    },
    {
      tenthuoc: "Gardenal 100mg",
      tdk: 59850,
      tck: 11970,
      tenhoatchat: "Phenobarbital",
      nhom: "Thuốc chống Co Giật"
    },
    {
      tenthuoc: "Gardenal 100mg",
      tdk: 59850,
      tck: 11970,
      tenhoatchat: "Phenobarbital",
      nhom: "Thuốc chống Co Giật"
    },
    {
      tenthuoc: "Gardenal 100mg",
      tdk: 89775,
      tck: 17955,
      tenhoatchat: "Phenobarbital",
      nhom: "Thuốc chống Co Giật"
    },
    {
      tenthuoc: "Gardenal 100mg",
      tdk: 89775,
      tck: 17955,
      tenhoatchat: "Phenobarbital",
      nhom: "Thuốc chống Co Giật"
    },
    {
      tenthuoc: "Gardenal 100mg",
      tdk: 149625,
      tck: 29925,
      tenhoatchat: "Phenobarbital",
      nhom: "Thuốc chống Co Giật"
    },
    {
      tenthuoc: "Gardenal 100mg",
      tdk: 1693125,
      tck: 338625,
      tenhoatchat: "Phenobarbital",
      nhom: "Thuốc chống Co Giật"
    },
    {
      tenthuoc: "Gardenal 100mg",
      tdk: 178125,
      tck: 35625,
      tenhoatchat: "Phenobarbital",
      nhom: "Thuốc chống Co Giật"
    },
    {
      tenthuoc: "Gardenal 100mg",
      tdk: 239400,
      tck: 47880,
      tenhoatchat: "Phenobarbital",
      nhom: "Thuốc chống Co Giật"
    },
    {
      tenthuoc: "Gardenal 100mg",
      tdk: 299250,
      tck: 59850,
      tenhoatchat: "Phenobarbital",
      nhom: "Thuốc chống Co Giật"
    },
    {
      tenthuoc: "Gardenal 100mg",
      tdk: 598500,
      tck: 119700,
      tenhoatchat: "Phenobacbital",
      nhom: "Thuốc chống Co Giật"
    },
    {
      tenthuoc: "Gardenal 100mg",
      tdk: 5985000,
      tck: 1197000,
      tenhoatchat: "Phenobarbital",
      nhom: "Thuốc chống Co Giật"
    },
    {
      tenthuoc: "Gardenal 100mg",
      tdk: 25650000,
      tck: 5130000,
      tenhoatchat: "Phenobarbital",
      nhom: "Thuốc chống Co Giật"
    },
    {
      tenthuoc: "Gardenal 100mg",
      tdk: 538650000,
      tck: 107730000,
      tenhoatchat: "Phenobarbital",
      nhom: "Thuốc chống Co Giật"
    },
    {
      tenthuoc: "Coveram Tab 5mg/5mg 30's",
      tdk: 1564063851,
      tck: 3128127703,
      tenhoatchat: "Perindopril Arginine 5mg, Amlodipine besylate 5mg",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Coveram Tab 5mg/5mg 30's",
      tdk: 164560275,
      tck: 32912055,
      tenhoatchat: "Perindopril  +  Amlodipin",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Coveram Tab 5mg/5mg 30's",
      tdk: 247087500,
      tck: 49417500,
      tenhoatchat: "Perindopril  +  Amlodipin",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Xalvobin (capecitabin) 500mg",
      tdk: 559125000,
      tck: 111825000,
      tenhoatchat: "Capecitabin",
      nhom: "Thuốc Ung Thư - Miễn Dịch"
    },
    {
      tenthuoc: "Levonor 1mg/ml",
      tdk: 393750,
      tck: 78750,
      tenhoatchat: "Nor epinephrin (Nor adrenalin)",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Levonor 1mg/ml",
      tdk: 1575000,
      tck: 315000,
      tenhoatchat: "Nor epinephrin (Nor adrenalin)",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Levonor 1mg/ml",
      tdk: 47250000,
      tck: 9450000,
      tenhoatchat: "Nor epinephrin (Nor adrenalin)",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Levonor 1mg/ml",
      tdk: 78750000,
      tck: 15750000,
      tenhoatchat: "Nor epinephrin (Nor adrenalin)",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Levonor 1mg/ml",
      tdk: 94500000,
      tck: 18900000,
      tenhoatchat: "Nor epinephrin (Nor adrenalin)",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Levonor 1mg/ml",
      tdk: 118125000,
      tck: 23625000,
      tenhoatchat: "Nor epinephrin (Nor adrenalin)",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Levonor 1mg/ml",
      tdk: 212625000,
      tck: 23625000,
      tenhoatchat: "Nor epinephrin (Nor adrenalin)",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Sanlein 0,1% x 5ml",
      tdk: 3076400,
      tck: 615280,
      tenhoatchat: "Natri hyaluronate 1mg/ml",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Sanlein 0,1% x 5ml",
      tdk: 538370000,
      tck: 107674000,
      tenhoatchat: "Natri hyaluronat",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Concor",
      tdk: 540704250,
      tck: 108140850,
      tenhoatchat: "Bisoprolol",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Infartan (Clopidogrel) 75mg",
      tdk: 5399999325,
      tck: 1079999865,
      tenhoatchat: "Clopidogrel",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Nifedipin Hasan 20 Retard",
      tdk: 252000,
      tck: 252000,
      tenhoatchat: "Nifedipin",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Nifedipin Hasan 20 Retard",
      tdk: 124160663,
      tck: 24832133,
      tenhoatchat: "Nifedipin",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Nifedipin Hasan 20 Retard",
      tdk: 14190000,
      tck: 2838000,
      tenhoatchat: "Nifedipin",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Nifedipin Hasan 20 Retard",
      tdk: 17737500,
      tck: 3547500,
      tenhoatchat: "Nifedipine",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Nifedipin Hasan 20 Retard",
      tdk: 70950000,
      tck: 14190000,
      tenhoatchat: "Nifedipin",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Nifedipin Hasan 20 Retard",
      tdk: 1478104688,
      tck: 2956209375,
      tenhoatchat: "Nifedipine",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Nifedipin Hasan 20 Retard",
      tdk: 283500000,
      tck: 56700000,
      tenhoatchat: "Nifedipin",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Aristin-C (Ciprofloxacin 200mg/100ml)",
      tdk: 5264999513,
      tck: 1052999903,
      tenhoatchat: "Ciprofloxacin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Resines (Amlodipin) 5mg",
      tdk: 524979000,
      tck: 104995800,
      tenhoatchat: "Amlodipin",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Kedrigamma",
      tdk: 520162500,
      tck: 104032500,
      tenhoatchat: "Immune Globulin",
      nhom: "Chế phẩm từ Máu"
    },
    {
      tenthuoc: "Tobradex 5ml",
      tdk: 16912125,
      tck: 3382425,
      tenhoatchat: "Tobramycin+Dexamethason",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Tobradex 5ml",
      tdk: 10147275,
      tck: 1127475,
      tenhoatchat: "Tobramycin0,3% Dexamethason 0,1%",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Tobradex 5ml",
      tdk: 11274750,
      tck: 2254950,
      tenhoatchat: "Tobramycin + Dexamethason",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Tobradex 5ml",
      tdk: 16912125,
      tck: 3382425,
      tenhoatchat: "Tobramycin + Dexamethasone",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Tobradex 5ml",
      tdk: 16912125,
      tck: 3382425,
      tenhoatchat: "Tobramycin + Dexamethasone",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Tobradex 5ml",
      tdk: 22549500,
      tck: 4509900,
      tenhoatchat: "Tobramycin + Dexamethason",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Tobradex 5ml",
      tdk: 2536793381,
      tck: 507358676,
      tenhoatchat: "Tobramycin + Dexamethasone",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Tobradex 5ml",
      tdk: 56373750,
      tck: 11274750,
      tenhoatchat: "Tobramycin + Dexamethason",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Tobradex 5ml",
      tdk: 355154625,
      tck: 71030925,
      tenhoatchat: "Tobramycin + Dexamethason",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Prospan 70ml",
      tdk: 13387500,
      tck: 2677500,
      tenhoatchat: "Cao lá thường xuân",
      nhom: "Thuốc Đông dược"
    },
    {
      tenthuoc: "Prospan 70ml",
      tdk: 502031250,
      tck: 100406250,
      tenhoatchat: "Cao khô lá thường xuân",
      nhom: "Thuốc Đông dược"
    },
    {
      tenthuoc: "Ceftriaxon",
      tdk: 506025000,
      tck: 101205000,
      tenhoatchat: "Ceftriaxon",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Amlor Tab 5mg 30's",
      tdk: 25626375,
      tck: 5125275,
      tenhoatchat: "Amlodipin",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Amlor Tab 5mg 30's",
      tdk: 56947500,
      tck: 11389500,
      tenhoatchat: "Amlodipin",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Amlor Tab 5mg 30's",
      tdk: 85079565,
      tck: 9453285,
      tenhoatchat: "Amlodipine besilate (amlodipine 5mg)",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Amlor Tab 5mg 30's",
      tdk: 948175875,
      tck: 189635175,
      tenhoatchat: "Amlodipin",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Amlor Tab 5mg 30's",
      tdk: 2363321259,
      tck: 4726642519,
      tenhoatchat: "Amlodipine",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Novomix 30/70 Flexpen 100UI 3ml",
      tdk: 498421875,
      tck: 99684375,
      tenhoatchat: "Insulin trộn (M) 30:70",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Lotemax",
      tdk: 16462500,
      tck: 3292500,
      tenhoatchat: "Lotepredol etabonat",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Lotemax",
      tdk: 192062500,
      tck: 38412500,
      tenhoatchat: "Loteprednol etabonat",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Lotemax",
      tdk: 288093750,
      tck: 57618750,
      tenhoatchat: "Loteprednol Etabonate",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Concor 2,5mg",
      tdk: 6176250203,
      tck: 1235250041,
      tenhoatchat: "Bisoprolol",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Concor 2,5mg",
      tdk: 431699940,
      tck: 86339988,
      tenhoatchat: "Bisoprolol",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Ebitac 12,5",
      tdk: 51750000,
      tck: 10350000,
      tenhoatchat: "Enalapril maleate + Hydrochlorothiazide",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Ebitac 12,5",
      tdk: 215625000,
      tck: 43125000,
      tenhoatchat: "Enalapril maleat +Hydroclorothiazid",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Ebitac 12,5",
      tdk: 218750000,
      tck: 43750000,
      tenhoatchat: "Enalapril maleat + Hydroclorothiazid",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Remeclar 500mg",
      tdk: 485301250,
      tck: 97060250,
      tenhoatchat: "Clarithromycin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Amlor Cap 5mg 30's",
      tdk: 18281250,
      tck: 3656250,
      tenhoatchat: "Amlodipin",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Amlor Cap 5mg 30's",
      tdk: 28640625,
      tck: 5728125,
      tenhoatchat: "Amlodipin",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Amlor Cap 5mg 30's",
      tdk: 60937500,
      tck: 12187500,
      tenhoatchat: "Amlodipin",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Amlor Cap 5mg 30's",
      tdk: 121875000,
      tck: 24375000,
      tenhoatchat: "Amlodipin",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Amlor Cap 5mg 30's",
      tdk: 2538046875,
      tck: 507609375,
      tenhoatchat: "Amlodipin",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Fudcime (Cefixim) 200mg",
      tdk: 1632918784,
      tck: 3265837568,
      tenhoatchat: "Cefixim",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Fudcime (Cefixim) 200mg",
      tdk: 3193749563,
      tck: 6387499125,
      tenhoatchat: "Cefixim",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Bisoprolol 2,5mg",
      tdk: 480138750,
      tck: 96027750,
      tenhoatchat: "Bisoprolol",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Paclispec 30",
      tdk: 479375000,
      tck: 95875000,
      tenhoatchat: "Paclitaxel",
      nhom: "Thuốc Ung Thư - Miễn Dịch"
    },
    {
      tenthuoc: "Augmentin BD Tab 625mg 14's",
      tdk: 32824000,
      tck: 32824000,
      tenhoatchat: "Amoxicillin trihydrate 500mg + Acid Clavulanic 125mg",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Augmentin BD Tab 625mg 14's",
      tdk: 116346160,
      tck: 23269232,
      tenhoatchat: "Amoxicilin + Acid clavulanic",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Augmentin BD Tab 625mg 14's",
      tdk: 149140320,
      tck: 29828064,
      tenhoatchat: "Amoxicilin + Acid clavulanic",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Augmentin BD Tab 625mg 14's",
      tdk: 179010160,
      tck: 35802032,
      tenhoatchat: "Amoxicillin + Acid Clavulanic",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Medopiren 500mg",
      tdk: 43687500,
      tck: 8737500,
      tenhoatchat: "Ciprofloxacin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Medopiren 500mg",
      tdk: 128150000,
      tck: 25630000,
      tenhoatchat: "Ciprofloxacin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Medopiren 500mg",
      tdk: 145562500,
      tck: 29112500,
      tenhoatchat: "Ciprofloxacin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Medopiren 500mg",
      tdk: 159546750,
      tck: 31909350,
      tenhoatchat: "Ciprofloxacin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Azicine",
      tdk: 9115875,
      tck: 1823175,
      tenhoatchat: "Azithromycin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Azicine",
      tdk: 13500000,
      tck: 2700000,
      tenhoatchat: "Azithromycin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Azicine",
      tdk: 13500000,
      tck: 2700000,
      tenhoatchat: "Azithromycin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Azicine",
      tdk: 13687500,
      tck: 2737500,
      tenhoatchat: "Azithromycin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Azicine",
      tdk: 15741000,
      tck: 3148200,
      tenhoatchat: "Azithromycin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Azicine",
      tdk: 15959625,
      tck: 3191925,
      tenhoatchat: "Azithromycin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Azicine",
      tdk: 41062500,
      tck: 8212500,
      tenhoatchat: "Azithromycin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Azicine",
      tdk: 59294250,
      tck: 11858850,
      tenhoatchat: "Azithromycin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Azicine",
      tdk: 1285499998,
      tck: 2570999996,
      tenhoatchat: "Azithromycin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Azicine",
      tdk: 164797500,
      tck: 32959500,
      tenhoatchat: "Azithromycin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Hyđan",
      tdk: 462475000,
      tck: 92495000,
      tenhoatchat: "Bột mã tiền chế, hy thiêm, ngũ gia bì",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Ucyrin 75mg",
      tdk: 4476150,
      tck: 895230,
      tenhoatchat: "Clopidogrel",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Ucyrin 75mg",
      tdk: 7566825,
      tck: 1513365,
      tenhoatchat: "Clopidogrel 75mg",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Ucyrin 75mg",
      tdk: 11403525,
      tck: 2280705,
      tenhoatchat: "Clopidogrel",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Ucyrin 75mg",
      tdk: 44505720,
      tck: 4945080,
      tenhoatchat: "Clopidogrel",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Ucyrin 75mg",
      tdk: 393901200,
      tck: 78780240,
      tenhoatchat: "Clopidogrel",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Campto (Irinotecan) 100mg",
      tdk: 4599049875,
      tck: 919809975,
      tenhoatchat: "Irinotecan",
      nhom: "Thuốc Ung Thư - Miễn Dịch"
    },
    {
      tenthuoc: "Campto (Irinotecan) 100mg",
      tdk: 183961995,
      tck: 36792399,
      tenhoatchat: "Irinotecan",
      nhom: "Thuốc Ung Thư - Miễn Dịch"
    },
    {
      tenthuoc: "Campto (Irinotecan) 100mg",
      tdk: 2299524997,
      tck: 4599049993,
      tenhoatchat: "Irinotecan",
      nhom: "Thuốc Ung Thư - Miễn Dịch"
    },
    {
      tenthuoc: "Scilin R",
      tdk: 2575000,
      tck: 515000,
      tenhoatchat: "Insulin tác dụng nhanh ngắn",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Scilin R",
      tdk: 12480000,
      tck: 2496000,
      tenhoatchat: "Insulin tác dụng nhanh (fast-acting, short-acting)",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Scilin R",
      tdk: 13475000,
      tck: 2695000,
      tenhoatchat: "Insulin tác dụng nhanh, ngắn",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Scilin R",
      tdk: 19312500,
      tck: 3862500,
      tenhoatchat: "Insulin tác dụng nhanh, ngắn (Fast-acting, Short-acting)",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Scilin R",
      tdk: 25750000,
      tck: 5150000,
      tenhoatchat: "Insulin tác dụng nhanh, ngắn (Fast-acting, Short-acting)",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Scilin R",
      tdk: 128750000,
      tck: 25750000,
      tenhoatchat: "Insulin ( tác dụng nhanh)",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Scilin R",
      tdk: 257500000,
      tck: 51500000,
      tenhoatchat: "Insulin tác dụng nhanh, ngắn",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Resines",
      tdk: 488775,
      tck: 97755,
      tenhoatchat: "Amlodipin",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Resines",
      tdk: 52479000,
      tck: 10495800,
      tenhoatchat: "Amlodipin",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Resines",
      tdk: 60350850,
      tck: 12070170,
      tenhoatchat: "Amlodipine",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Resines",
      tdk: 104958000,
      tck: 20991600,
      tenhoatchat: "Amlodipin",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Resines",
      tdk: 2399039963,
      tck: 4798079925,
      tenhoatchat: "Amlodipine",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Nexium mups",
      tdk: 4519270007,
      tck: 9038540014,
      tenhoatchat: "Esomeprazole",
      nhom: "Thuốc Dạ dày - Ruột"
    },
    {
      tenthuoc: "Chirocain 5mg/ml Ampoule 10x10ml 5mg/ml",
      tdk: 4499999963,
      tck: 8999999925,
      tenhoatchat: "Levobupivacaine hydrochloride,  levobupivacaine 5mg/ml",
      nhom: "Thuốc Gây tê - Gây mê"
    },
    {
      tenthuoc: "Scilin N 400IU",
      tdk: 30901500,
      tck: 6180300,
      tenhoatchat: "Insulin tác dụng trung Bottle(s) (insulin NPH) 400IU/10ml",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Scilin N 400IU",
      tdk: 61803000,
      tck: 12360600,
      tenhoatchat: "Insulin tác dụng trung Bottle(s) (insulin NPH) 400IU/10ml",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Scilin N 400IU",
      tdk: 772500015,
      tck: 154500003,
      tenhoatchat: "Insulin tác dụng trung Bottle(s) (insulin NPH) 400IU/10ml",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Scilin N 400IU",
      tdk: 92699964,
      tck: 185399928,
      tenhoatchat: "Insulin tác dụng trung Bottle(s) (insulin NPH) 400IU/10ml",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Scilin N 400IU",
      tdk: 927000018,
      tck: 1854000036,
      tenhoatchat: "Insulin tác dụng trung Bottle(s) (insulin NPH) 400IU/10ml",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Scilin N 400IU",
      tdk: 92700720,
      tck: 18540144,
      tenhoatchat: "Insulin tác dụng trung Bottle(s) (insulin NPH) 400IU/10ml",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Levemir Flexpen",
      tdk: 446875000,
      tck: 8937499999,
      tenhoatchat: "Insulin determir",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Coveram (5+10)mg",
      tdk: 4447574933,
      tck: 8895149865,
      tenhoatchat: "Perindopril + amlodipin",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Cardilopin",
      tdk: 442800000,
      tck: 88560000,
      tenhoatchat: "Amlodipin",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Humulin N 100IU 10ml",
      tdk: 441000000,
      tck: 88200000,
      tenhoatchat: "Insulin tác dụng trung bình, trung gian",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Ebitac 25mg",
      tdk: 175000000,
      tck: 35000000,
      tenhoatchat: "Enalapril maleate 10mg+hydroclorothiazid 25mg",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Ebitac 25mg",
      tdk: 258750000,
      tck: 51750000,
      tenhoatchat: "Enalapril maleate 10mg+hydroclorothiazid 25mg",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Ebitac12.5",
      tdk: 431250000,
      tck: 86250000,
      tenhoatchat: "Enalapril",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Scanneuron 300,2mg",
      tdk: 54899775,
      tck: 10979955,
      tenhoatchat: "Vitamin B1+Vitamin B6+Vitamin B12.",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Scanneuron 300,2mg",
      tdk: 1619989875,
      tck: 323997975,
      tenhoatchat: "Vitamin B1+Vitamin B6+Vitamin B12.",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Scanneuron 300,2mg",
      tdk: 2062501875,
      tck: 412500375,
      tenhoatchat: "Vitamin B1+Vitamin B6+Vitamin B12.",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Coveram",
      tdk: 86974800,
      tck: 17394960,
      tenhoatchat: "Perindopril + amlodipin",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Coveram",
      tdk: 3293676375,
      tck: 658735275,
      tenhoatchat: "Perindopril + amlodipin",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Propofol 1 % Kabi",
      tdk: 415209375,
      tck: 83041875,
      tenhoatchat: "Propofol",
      nhom: "Thuốc Gây tê - Gây mê"
    },
    {
      tenthuoc: "Oflovid ophthalmic ointment",
      tdk: 4658125,
      tck: 931625,
      tenhoatchat: "Ofloxacin",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Oflovid ophthalmic ointment",
      tdk: 408653625,
      tck: 81730725,
      tenhoatchat: "Ofloxacin",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Mixtard 30",
      tdk: 1625000,
      tck: 325000,
      tenhoatchat: "Insulin trộn, hỗn hợp (Mixtard-acting, Dual-acting)",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Mixtard 30",
      tdk: 69500000,
      tck: 13900000,
      tenhoatchat: "Insulin người, rADN",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Mixtard 30",
      tdk: 81250000,
      tck: 16250000,
      tenhoatchat: "Insulin trộn, hỗn hợp",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Mixtard 30",
      tdk: 100200000,
      tck: 20040000,
      tenhoatchat: "Insulin người, rADN",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Mixtard 30",
      tdk: 130000000,
      tck: 26000000,
      tenhoatchat: "Insulin người, rADN",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Xeloda (Capecitabin) 500mg",
      tdk: 3817802772,
      tck: 7635605544,
      tenhoatchat: "Capecitabin",
      nhom: "Thuốc Ung Thư - Miễn Dịch"
    },
    {
      tenthuoc: "Resines 5mg",
      tdk: 23601375,
      tck: 4720275,
      tenhoatchat: "Amlodipin 5mg",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Resines 5mg",
      tdk: 45927000,
      tck: 9185400,
      tenhoatchat: "Amlodipine",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Resines 5mg",
      tdk: 48258000,
      tck: 9651600,
      tenhoatchat: "Amlodipin 5mg",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Resines 5mg",
      tdk: 262458000,
      tck: 52491600,
      tenhoatchat: "Amlodipin",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Emas",
      tdk: 375000000,
      tck: 75000000,
      tenhoatchat: "Glycerin",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Scilin M30",
      tdk: 103000000,
      tck: 20600000,
      tenhoatchat: "Insulin trộn, hỗn hợp",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Scilin M30",
      tdk: 108000000,
      tck: 21600000,
      tenhoatchat: "Insulin trộn, hỗn hợp (Mixtard-acting, Dual-acting)",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Scilin M30",
      tdk: 162000000,
      tck: 3239999999,
      tenhoatchat: "Insulin (30/70)",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Unasyn 1500mg",
      tdk: 371250000,
      tck: 74250000,
      tenhoatchat: "Amoxicilin + Sulbactam",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Ofmantine-Domesco 625mg",
      tdk: 18749080,
      tck: 3749816,
      tenhoatchat: "Amoxicilin + Acid clavulanic",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Ofmantine-Domesco 625mg",
      tdk: 25764690,
      tck: 5152938,
      tenhoatchat: "Amoxicilin + Acid clavulanic",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Ofmantine-Domesco 625mg",
      tdk: 57701910,
      tck: 11540382,
      tenhoatchat: "Amoxicilin + Acid clavulanic",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Ofmantine-Domesco 625mg",
      tdk: 267534190,
      tck: 53506838,
      tenhoatchat: "Amoxicilin + Acid clavulanic",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Cozaar Tab 50mg 30's",
      tdk: 31391250,
      tck: 6278250,
      tenhoatchat: "Losartan",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Cozaar Tab 50mg 30's",
      tdk: 52109475,
      tck: 10421895,
      tenhoatchat: "Losartan Kali",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Cozaar Tab 50mg 30's",
      tdk: 64875250,
      tck: 12975050,
      tenhoatchat: "Losartan",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Cozaar Tab 50mg 30's",
      tdk: 1045328625,
      tck: 209065725,
      tenhoatchat: "Losartan Kali",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Cozaar Tab 50mg 30's",
      tdk: 116775450,
      tck: 23355090,
      tenhoatchat: "Losartan K 50mg",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Diprivan 20ml/200mg",
      tdk: 3692749922,
      tck: 7385499844,
      tenhoatchat: "Propofol",
      nhom: "Thuốc Gây tê - Gây mê"
    },
    {
      tenthuoc: "Parocontin",
      tdk: 352506000,
      tck: 70501200,
      tenhoatchat: "Methocarbamol + Paracetamol",
      nhom: "Thuốc Hạ nhiệt - Giảm đau"
    },
    {
      tenthuoc: "Medsamic",
      tdk: 1178750,
      tck: 235750,
      tenhoatchat: "Acid Tranexamic",
      nhom: "Thuốc ảnh hưởng đến Máu"
    },
    {
      tenthuoc: "Medsamic",
      tdk: 2357500,
      tck: 471500,
      tenhoatchat: "Tranexamic acid",
      nhom: "Thuốc ảnh hưởng đến Máu"
    },
    {
      tenthuoc: "Medsamic",
      tdk: 20038750,
      tck: 4007750,
      tenhoatchat: "Tranexamic acid",
      nhom: "Thuốc ảnh hưởng đến Máu"
    },
    {
      tenthuoc: "Medsamic",
      tdk: 8722699125,
      tck: 1744539825,
      tenhoatchat: "Tranexamic acid",
      nhom: "Thuốc ảnh hưởng đến Máu"
    },
    {
      tenthuoc: "Medsamic",
      tdk: 235750000,
      tck: 47150000,
      tenhoatchat: "Acid Tranexamic",
      nhom: "Thuốc ảnh hưởng đến Máu"
    },
    {
      tenthuoc: "Claritek 125mg/5mlx50mL",
      tdk: 3750019875,
      tck: 750003975,
      tenhoatchat: "Clarithromycin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Claritek 125mg/5mlx50mL",
      tdk: 599999904,
      tck: 1199999808,
      tenhoatchat: "Clarithromycin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Claritek 125mg/5mlx50mL",
      tdk: 60000192,
      tck: 120000384,
      tenhoatchat: "Clarithromycin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Claritek 125mg/5mlx50mL",
      tdk: 187374970,
      tck: 37474994,
      tenhoatchat: "Clarithromycin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Twynsta 40/5mg",
      tdk: 3425058193,
      tck: 6850116386,
      tenhoatchat: "Telmisartan + Amlodipin",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Paclitaxel Ebewe Inj 30MG/5ML 1's",
      tdk: 113429625,
      tck: 22685925,
      tenhoatchat: "Paclitacel",
      nhom: "Thuốc Ung Thư - Miễn Dịch"
    },
    {
      tenthuoc: "Paclitaxel Ebewe Inj 30MG/5ML 1's",
      tdk: 225550000,
      tck: 45110000,
      tenhoatchat: "Paclitacel",
      nhom: "Thuốc Ung Thư - Miễn Dịch"
    },
    {
      tenthuoc: "Exforge HCT 5/160/12.5mg",
      tdk: 3358848129,
      tck: 6717696258,
      tenhoatchat: "Amlodipin+Valsartan+Hydrochlorothiazide",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Diamisu-N 10ml",
      tdk: 333500000,
      tck: 66700000,
      tenhoatchat: "Insulin chậm, kéo dài",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Heparin (Paringold) 25000IU/5ml",
      tdk: 151250001,
      tck: 302500002,
      tenhoatchat: "Heparin (Natri)",
      nhom: "Thuốc ảnh hưởng đến Máu"
    },
    {
      tenthuoc: "Heparin (Paringold) 25000IU/5ml",
      tdk: 1809374995,
      tck: 3618749991,
      tenhoatchat: "Heparin (Natri)",
      nhom: "Thuốc ảnh hưởng đến Máu"
    },
    {
      tenthuoc: "Reditux 100mg/10ml",
      tdk: 330000000,
      tck: 66000000,
      tenhoatchat: "Rituximab",
      nhom: "Thuốc Ung Thư - Miễn Dịch"
    },
    {
      tenthuoc: "Unasyn 1,5g",
      tdk: 8249999625,
      tck: 1649999925,
      tenhoatchat: "Ampicilin + sulbactam",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Unasyn 1,5g",
      tdk: 247500000,
      tck: 49500000,
      tenhoatchat: "Ampicillin+Sulbactam",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Dicortineff 5ml",
      tdk: 19421250,
      tck: 3884250,
      tenhoatchat: "Neomycin; gramicidin; Fludrocortisone acetate",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Dicortineff 5ml",
      tdk: 4531625044,
      tck: 906325009,
      tenhoatchat: "Neomycin sulfat + gramicidin + 9-alpha fluohydrocortison acetat",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Dicortineff 5ml",
      tdk: 5749999999,
      tck: 11500000,
      tenhoatchat: "Neomycin sulfat + gramicidin + 9-alpha fluohydrocortison acetat",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Dicortineff 5ml",
      tdk: 5999999906,
      tck: 1199999981,
      tenhoatchat: "Neomycin sulfat + gramicidin + 9-alpha fluohydrocortison acetat",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Dicortineff 5ml",
      tdk: 5999999998,
      tck: 12000000,
      tenhoatchat: "Neomycin sulfat + gramicidin + 9-alpha fluohydrocortison acetat",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Dicortineff 5ml",
      tdk: 8625000038,
      tck: 1725000008,
      tenhoatchat: "Neomycin sulfat + gramicidin + 9-alpha fluohydrocortison acetat",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Diprivan Pre-Filled Syring 1% 10mg/ml (1%) - 50ml",
      tdk: 328125000,
      tck: 65625000,
      tenhoatchat: "Propofol",
      nhom: "Thuốc Gây tê - Gây mê"
    },
    {
      tenthuoc: "Mixtard 30 Flexpen 100ui/mlx3ml",
      tdk: 327247875,
      tck: 65449575,
      tenhoatchat: "Insulin người (rDNA)",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Exforge tab 5mg/ 80mg 2x14's",
      tdk: 327174120,
      tck: 65434824,
      tenhoatchat: "Amlodipine + Valsartan",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Vitamin B1 + B6 + B12",
      tdk: 46035000,
      tck: 9207000,
      tenhoatchat: "Vitamin B1+Vitamin B6+Vitamin B12.",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Vitamin B1 + B6 + B12",
      tdk: 55374000,
      tck: 11074800,
      tenhoatchat: "Vitamin B1 + B6 + B12",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Vitamin B1 + B6 + B12",
      tdk: 61710000,
      tck: 12342000,
      tenhoatchat: "Vitamin B1+Vitamin B6+Vitamin B12.",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Vitamin B1 + B6 + B12",
      tdk: 66000000,
      tck: 13200000,
      tenhoatchat: "Vitamin B1 + Vitamin B6 + Vitamin B12",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Vitamin B1 + B6 + B12",
      tdk: 96000000,
      tck: 24000000,
      tenhoatchat: "Vitamin B1+Vitamin B6+Vitamin B12.",
      nhom: "Vitamin & Khoáng chất"
    },
    {
      tenthuoc: "Medoclav 1g",
      tdk: 75798100,
      tck: 15159620,
      tenhoatchat: "Amoxicilin + Acid clavulanic",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Medoclav 1g",
      tdk: 85141875,
      tck: 17028375,
      tenhoatchat: "Amoxicilin + Acid clavulanic",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Medoclav 1g",
      tdk: 162684900,
      tck: 32536980,
      tenhoatchat: "Amoxicillin + Acid Clavulanic",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Tamifine",
      tdk: 53750025,
      tck: 10750005,
      tenhoatchat: "Tamoxifen",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Tamifine",
      tdk: 268750000,
      tck: 53750000,
      tenhoatchat: "Tamoxifen",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Savi Losartan plus HCT 50/12.5",
      tdk: 317724375,
      tck: 63544875,
      tenhoatchat: "Losartan + Hydrochlorothiazid",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Panadol Caplet (Paracetamol) 500mg",
      tdk: 6689911883,
      tck: 1337982377,
      tenhoatchat: "Paracetamol",
      nhom: "Thuốc Hạ nhiệt - Giảm đau"
    },
    {
      tenthuoc: "Panadol Caplet (Paracetamol) 500mg",
      tdk: 82619838,
      tck: 165239676,
      tenhoatchat: "Paracetamol",
      nhom: "Thuốc Hạ nhiệt - Giảm đau"
    },
    {
      tenthuoc: "Panadol Caplet (Paracetamol) 500mg",
      tdk: 1659270359,
      tck: 3318540719,
      tenhoatchat: "Paracetamol",
      nhom: "Thuốc Hạ nhiệt - Giảm đau"
    },
    {
      tenthuoc: "Heparin-Belmed",
      tdk: 311393125,
      tck: 62278625,
      tenhoatchat: "Heparin (Natri)",
      nhom: "Thuốc ảnh hưởng đến Máu"
    },
    {
      tenthuoc: "Cravit",
      tdk: 305557500,
      tck: 61111500,
      tenhoatchat: "Levofloxacin",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Tobrin",
      tdk: 14625000,
      tck: 2925000,
      tenhoatchat: "Tobramycin",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Tobrin",
      tdk: 24375000,
      tck: 24375000,
      tenhoatchat: "Tobramycin",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Tobrin",
      tdk: 29250000,
      tck: 5850000,
      tenhoatchat: "Tobramycin",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Tobrin",
      tdk: 48750000,
      tck: 9750000,
      tenhoatchat: "Tobramycin",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Tobrin",
      tdk: 51187500,
      tck: 10237500,
      tenhoatchat: "Tobramycin 0,3%/5ml",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Tobrin",
      tdk: 68250000,
      tck: 13650000,
      tenhoatchat: "Tobramycin",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Tobrin",
      tdk: 68250000,
      tck: 13650000,
      tenhoatchat: "Tobramycin",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Losartan Stada 50mg",
      tdk: 302445000,
      tck: 60489000,
      tenhoatchat: "Losartan",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Resines 5mg(amlodipin)",
      tdk: 301826700,
      tck: 60365340,
      tenhoatchat: "Amlodipin",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Hoạt huyết thông mạch P/H",
      tdk: 54750000,
      tck: 18750000,
      tenhoatchat: "Bột Đương quy; Cao đặc các dược liệu (tương đương với Đương quy; Sinh địa; Xuyên khung; Ngưu tất; Ích mẫu; Đan sâm).",
      nhom: "Thuốc Đông dược"
    },
    {
      tenthuoc: "Hoạt huyết thông mạch P/H",
      tdk: 94800000,
      tck: 18960000,
      tenhoatchat: "Bột Đương quy; Cao đặc các dược liệu (tương đương với Đương quy; Sinh địa; Xuyên khung; Ngưu tất; Ích mẫu; Đan sâm).",
      nhom: "Thuốc Đông dược"
    },
    {
      tenthuoc: "Hoạt huyết thông mạch P/H",
      tdk: 152000000,
      tck: 76000000,
      tenhoatchat: "Mỗi 200ml cao lỏng chứa: Đương quy; Bạch thược; Ngưu tất; Thục địa; Xuyên khung; Cao đặc ích mẫu (10:1)",
      nhom: "Thuốc Đông dược"
    },
    {
      tenthuoc: "Meronem 1g (Meropenem)",
      tdk: 301396125,
      tck: 60279225,
      tenhoatchat: "Meropenem",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Aldacton 25mg",
      tdk: 49374990,
      tck: 9874998,
      tenhoatchat: "Spironolacton",
      nhom: "Thuốc lợi tiểu"
    },
    {
      tenthuoc: "Aldacton 25mg",
      tdk: 2481090609,
      tck: 4962181219,
      tenhoatchat: "Spironolacton",
      nhom: "Thuốc lợi tiểu"
    },
    {
      tenthuoc: "Bimoxine",
      tdk: 296450000,
      tck: 59290000,
      tenhoatchat: "Amoxicilin + Cloxacilin",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Plavix",
      tdk: 39000430,
      tck: 7800086,
      tenhoatchat: "Clopidogrel",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Plavix",
      tdk: 2569654503,
      tck: 5139309006,
      tenhoatchat: "Clopidogrel",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Novorapid",
      tdk: 295312500,
      tck: 59062500,
      tenhoatchat: "Insulin ( tác dụng nhanh)",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Bactirid 100mg/5ml",
      tdk: 295000000,
      tck: 59000000,
      tenhoatchat: "Cefixim",
      nhom: "Kháng Sinh"
    },
    {
      tenthuoc: "Paringold Injection 25000Ui/5ml",
      tdk: 2949372563,
      tck: 5898745125,
      tenhoatchat: "Heparin (Natri)",
      nhom: "Thuốc ảnh hưởng đến Máu"
    },
    {
      tenthuoc: "Xalvobin",
      tdk: 24916500,
      tck: 4983300,
      tenhoatchat: "Capecitabin",
      nhom: "Thuốc Ung Thư - Miễn Dịch"
    },
    {
      tenthuoc: "Xalvobin",
      tdk: 267750000,
      tck: 53550000,
      tenhoatchat: "Capecitabin",
      nhom: "Thuốc Ung Thư - Miễn Dịch"
    },
    {
      tenthuoc: "Aleradin",
      tdk: 7875000,
      tck: 1575000,
      tenhoatchat: "Desloratadine",
      nhom: "Thuốc chống Dị ứng"
    },
    {
      tenthuoc: "Aleradin",
      tdk: 3160725005,
      tck: 632145001,
      tenhoatchat: "Desloratadine",
      nhom: "Thuốc chống Dị ứng"
    },
    {
      tenthuoc: "Aleradin",
      tdk: 47175000,
      tck: 9435000,
      tenhoatchat: "Desloratadine",
      nhom: "Thuốc chống Dị ứng"
    },
    {
      tenthuoc: "Aleradin",
      tdk: 94350000,
      tck: 18870000,
      tenhoatchat: "Desloratadine",
      nhom: "Thuốc chống Dị ứng"
    },
    {
      tenthuoc: "Aleradin",
      tdk: 106143750,
      tck: 21228750,
      tenhoatchat: "Desloratadin",
      nhom: "Thuốc chống Dị ứng"
    },
    {
      tenthuoc: "Paracetamol 500mg( Panadol Caplet)",
      tdk: 286875000,
      tck: 57375000,
      tenhoatchat: "Paracetamol",
      nhom: "Thuốc Hạ nhiệt - Giảm đau"
    },
    {
      tenthuoc: "Clocardigel (Clopidogrel) 75mg",
      tdk: 2846025004,
      tck: 5692050009,
      tenhoatchat: "Clopidogrel",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Cozaar XQ 5mg+100mg",
      tdk: 2842557779,
      tck: 5685115559,
      tenhoatchat: "Amlodipin + Losartan",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "LOVENOX 60mg Inj B/ 2 syringes x0,6ml",
      tdk: 495088125,
      tck: 99017625,
      tenhoatchat: "Enoxaparin (natri)",
      nhom: "Thuốc ảnh hưởng đến Máu"
    },
    {
      tenthuoc: "LOVENOX 60mg Inj B/ 2 syringes x0,6ml",
      tdk: 229155075,
      tck: 45831015,
      tenhoatchat: "Enoxaparin",
      nhom: "Thuốc ảnh hưởng đến Máu"
    },
    {
      tenthuoc: "Lantus Solostar",
      tdk: 138999500,
      tck: 27799900,
      tenhoatchat: "Insulin tác dụng chậm, kéo dài (Slow-acting, Long-acting)",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Lantus Solostar",
      tdk: 138999500,
      tck: 27799900,
      tenhoatchat: "Insulin tác dụng chậm, kéo dài (Slow-acting, Long-acting)",
      nhom: "Hoocmon - Nội tiết tố"
    },
    {
      tenthuoc: "Nifedipin T20 Stada retard",
      tdk: 130499775,
      tck: 26099955,
      tenhoatchat: "Nifedipin",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Nifedipin T20 Stada retard",
      tdk: 21750000,
      tck: 4350000,
      tenhoatchat: "Nifedipin",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Nifedipin T20 Stada retard",
      tdk: 253750000,
      tck: 50750000,
      tenhoatchat: "Nifedipin",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Enamigal 5mg",
      tdk: 2354625,
      tck: 470925,
      tenhoatchat: "Enalapril",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Enamigal 5mg",
      tdk: 486975038,
      tck: 97395008,
      tenhoatchat: "Enalapril",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Enamigal 5mg",
      tdk: 9740250,
      tck: 1948050,
      tenhoatchat: "Enalapril",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Enamigal 5mg",
      tdk: 9740250,
      tck: 1948050,
      tenhoatchat: "Enalapril",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Enamigal 5mg",
      tdk: 19480500,
      tck: 3896100,
      tenhoatchat: "Enalapril",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Enamigal 5mg",
      tdk: 29250000,
      tck: 5850000,
      tenhoatchat: "Enalapril 5mg",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Enamigal 5mg",
      tdk: 48730500,
      tck: 9746100,
      tenhoatchat: "Enalapril",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Enamigal 5mg",
      tdk: 146250000,
      tck: 29250000,
      tenhoatchat: "Enalapril",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Tobrex Eye Ointment",
      tdk: 37125000,
      tck: 7425000,
      tenhoatchat: "Tobramycin",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Tobrex Eye Ointment",
      tdk: 6187375005,
      tck: 1237475001,
      tenhoatchat: "Tobramycin",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Tobrex Eye Ointment",
      tdk: 154684375,
      tck: 30936875,
      tenhoatchat: "Tobramycin",
      nhom: "Thuốc Nhãn khoa"
    },
    {
      tenthuoc: "Noradrenalin Base Aguettant  4mg/4ml",
      tdk: 2531249438,
      tck: 5062498875,
      tenhoatchat: "Nor epinephrin (Nor adrenalin)",
      nhom: "Thuốc Tim mạch - Huyết áp"
    },
    {
      tenthuoc: "Hoạt huyết dưỡng não",
      tdk: 9750000,
      tck: 1950000,
      tenhoatchat: "Cao đinh lăng, cao bạch quả",
      nhom: "Thuốc Đông dược"
    },
    {
      tenthuoc: "Hoạt huyết dưỡng não",
      tdk: 21319672,
      tck: 42639344,
      tenhoatchat: "Ginkgo biloba + Cao đan sâm",
      nhom: "Thuốc Đông dược"
    },
    {
      tenthuoc: "Hoạt huyết dưỡng não",
      tdk: 220500000,
      tck: 44100000,
      tenhoatchat: "Cao đinh lăng +Cao bạch quả",
      nhom: "Thuốc Đông dược"
    },
    {
      tenthuoc: "Sotstop",
      tdk: 250250000,
      tck: 50050000,
      tenhoatchat: "Ibuprofen",
      nhom: "Thuốc Hạ nhiệt - Giảm đau"
    }
  ];

  
var lstTKDT = [{tenthuoc:"Aristin-C",donvi:"Chai/Lọ",tdk:32028750000,tdt:25623000000,tck:6405750000,ngaytk:"27/11/2018"},
{tenthuoc:"Tavanic",donvi:"Ống",tdk:17900000000,tdt:14320000000,tck:3580000000,ngaytk:"27/11/2018"},
{tenthuoc:"Xalvobin500mgfilmcoated-tablet",donvi:"Viên",tdk:7426125000,tdt:5940900000,tck:1485225000,ngaytk:"27/11/2018"},
{tenthuoc:"PaxusPM100mg",donvi:"Ống",tdk:4891800000,tdt:3913440000,tck:978360000,ngaytk:"27/12/2018"},
{tenthuoc:"Duoplavin75mg+100mg",donvi:"Viên",tdk:3644379213,tdt:2915503370,tck:728875843,ngaytk:"27/12/2018"},
{tenthuoc:"Taxotere80mg/1ml",donvi:"Ống",tdk:3504059375,tdt:2803247500,tck:700811875,ngaytk:"27/12/2018"},
{tenthuoc:"Vimotram",donvi:"Chai/Lọ",tdk:3285000338,tdt:2628000270,tck:657000068,ngaytk:"27/12/2018"},
{tenthuoc:"REDITUX",donvi:"Ống",tdk:3025000000,tdt:2420000000,tck:605000000,ngaytk:"27/12/2018"},
{tenthuoc:"Vimotram",donvi:"Chai/Lọ",tdk:2848750000,tdt:2279000000,tck:569750000,ngaytk:"27/12/2018"},
{tenthuoc:"Taxotere20mg/1ml",donvi:"Ống",tdk:2803248000,tdt:2242598400,tck:560649600,ngaytk:"27/12/2018"},
{tenthuoc:"Vimotram",donvi:"Chai/Lọ",tdk:2769250000,tdt:2215400000,tck:553850000,ngaytk:"27/12/2018"},
{tenthuoc:"PaclitaxelumActavis",donvi:"Ống",tdk:2709375000,tdt:2167500000,tck:541875000,ngaytk:"27/12/2018"},
{tenthuoc:"Concor5mg",donvi:"Viên",tdk:2696060573,tdt:2156848458,tck:539212115,ngaytk:"27/12/2018"},
{tenthuoc:"PAXUSPM30MG",donvi:"Ống",tdk:2343750000,tdt:1875000000,tck:468750000,ngaytk:"27/12/2018"},
{tenthuoc:"Heparin25000UI/5ml",donvi:"Ống",tdk:2279812500,tdt:1823850000,tck:455962500,ngaytk:"27/12/2018"},
{tenthuoc:"Meronem1g",donvi:"Phial",tdk:2009307484,tdt:1607445987,tck:401861497,ngaytk:"27/12/2018"},
{tenthuoc:"Rifaxon",donvi:"Chai/Lọ",tdk:1903812500,tdt:1523050000,tck:380762500,ngaytk:"27/12/2018"},
{tenthuoc:"Vimotram",donvi:"Ống",tdk:1881250000,tdt:1505000000,tck:376250000,ngaytk:"27/12/2018"},
{tenthuoc:"PaxusPM30mg",donvi:"Ống",tdk:1876162500,tdt:1500930000,tck:375232500,ngaytk:"27/12/2018"},
{tenthuoc:"MeronemInj1g10's",donvi:"Chai/Lọ",tdk:1808376750,tdt:1446701400,tck:361675350,ngaytk:"27/12/2018"},
{tenthuoc:"MeronemInj1g10's",donvi:"Chai/Lọ",tdk:1808376750,tdt:1446701400,tck:361675350,ngaytk:"27/12/2018"},
{tenthuoc:"UnasynINJ",donvi:"Ống",tdk:1732500000,tdt:1386000000,tck:346500000,ngaytk:"27/12/2018"},
{tenthuoc:"Rocephin",donvi:"Chai/Lọ",tdk:1655640000,tdt:1324512000,tck:331128000,ngaytk:"27/12/2018"},
{tenthuoc:"Rifaxon",donvi:"Chai/Lọ",tdk:1628562500,tdt:1302850000,tck:325712500,ngaytk:"27/12/2018"},
{tenthuoc:"Vimotram",donvi:"Chai/Lọ",tdk:1567500000,tdt:1254000000,tck:313500000,ngaytk:"27/12/2018"},
{tenthuoc:"ScilinM30(30/70)",donvi:"Chai/Lọ",tdk:1552500000,tdt:1242000000,tck:310500000,ngaytk:"27/12/2018"},
{tenthuoc:"Twynsta40+5mg",donvi:"Viên",tdk:1544802349,tdt:1235841879,tck:308960470,ngaytk:"27/12/2018"},
{tenthuoc:"Tiepanem1g",donvi:"Chai/Lọ",tdk:1543750000,tdt:1235000000,tck:308750000,ngaytk:"27/12/2018"},
{tenthuoc:"PaxusPM(Paclitaxel)100mg",donvi:"Chai/Lọ",tdk:1462499719,tdt:1169999775,tck:292499944,ngaytk:"27/12/2018"},
{tenthuoc:"Plavix75mg",donvi:"Viên",tdk:1457959965,tdt:1166367972,tck:291591993,ngaytk:"27/12/2018"},
{tenthuoc:"PaxusPM",donvi:"Chai/Lọ",tdk:1428375000,tdt:1142700000,tck:285675000,ngaytk:"27/12/2018"},
{tenthuoc:"Klamentin1g",donvi:"Viên",tdk:1421287875,tdt:1137030300,tck:284257575,ngaytk:"27/12/2018"},
{tenthuoc:"Ebitac25",donvi:"Viên",tdk:1401562500,tdt:1121250000,tck:280312500,ngaytk:"27/12/2018"},
{tenthuoc:"ScilinN",donvi:"Chai/Lọ",tdk:1389487500,tdt:1111590000,tck:277897500,ngaytk:"27/12/2018"},
{tenthuoc:"ScilinN",donvi:"Chai/Lọ",tdk:1382775000,tdt:1106220000,tck:276555000,ngaytk:"27/12/2018"},
{tenthuoc:"Levogol",donvi:"Chai/Lọ",tdk:1350000000,tdt:1080000000,tck:270000000,ngaytk:"27/12/2018"},
{tenthuoc:"Proxacin1%",donvi:"Chai/Lọ",tdk:1282500000,tdt:1026000000,tck:256500000,ngaytk:"27/12/2018"},
{tenthuoc:"Anzatax",donvi:"Ống",tdk:1260000000,tdt:1008000000,tck:252000000,ngaytk:"27/12/2018"},
{tenthuoc:"Lovenox0,6ml,6000UI",donvi:"Bơm/Búttiêm",tdk:1253280159,tdt:1002624127,tck:250656032,ngaytk:"27/12/2018"},
{tenthuoc:"Tadocel80mg/2ml",donvi:"Ống",tdk:1243125000,tdt:994500000,tck:248625000,ngaytk:"27/12/2018"},
{tenthuoc:"UnasynInj1500mg1'sAmpicillin1g,Sulba",donvi:"Ống",tdk:1237499963,tdt:989999970,tck:247499993,ngaytk:"27/12/2018"},
{tenthuoc:"Aristin-C",donvi:"Chai/Lọ",tdk:1215000000,tdt:972000000,tck:243000000,ngaytk:"27/12/2018"},
{tenthuoc:"CeftriaxonePanpharma[ChỉdànhchokhoNhi]",donvi:"Chai/Lọ",tdk:1135744999,tdt:908595999,tck:227149000,ngaytk:"27/12/2018"},
{tenthuoc:"Xeloda",donvi:"Viên",tdk:1135294050,tdt:908235240,tck:227058810,ngaytk:"27/12/2018"},
{tenthuoc:"Ceftriaxonepanpharma",donvi:"Chai/Lọ",tdk:1124218475,tdt:899374780,tck:224843695,ngaytk:"27/12/2018"},
{tenthuoc:"Cerecaps",donvi:"Viên",tdk:1119281267,tdt:895425013,tck:223856253,ngaytk:"27/12/2018"},
{tenthuoc:"Bupivacaine20mg0.5%5mg/ml0,5%/4ml",donvi:"Tuýp",tdk:1099920150,tdt:879936120,tck:219984030,ngaytk:"27/12/2018"},
{tenthuoc:"ScilinN400UI/10ml",donvi:"Ống",tdk:1081500000,tdt:865200000,tck:216300000,ngaytk:"27/12/2018"},
{tenthuoc:"Vimotram",donvi:"Chai/Lọ",tdk:1075000000,tdt:860000000,tck:215000000,ngaytk:"27/12/2018"},
{tenthuoc:"Heparin(natri)5ml.5000UI/ml",donvi:"Phial",tdk:1059843750,tdt:847875000,tck:211968750,ngaytk:"27/12/2018"},
{tenthuoc:"Infartan75",donvi:"Viên",tdk:1004400000,tdt:803520000,tck:200880000,ngaytk:"27/12/2018"},
{tenthuoc:"MeronemInj500mg10's",donvi:"Chai/Lọ",tdk:986792625,tdt:789434100,tck:197358525,ngaytk:"27/12/2018"},
{tenthuoc:"Anzatax",donvi:"Ống",tdk:981750000,tdt:785400000,tck:196350000,ngaytk:"27/12/2018"},
{tenthuoc:"MerugoldI.V1g",donvi:"Phial",tdk:975000009,tdt:780000008,tck:195000002,ngaytk:"27/12/2018"},
{tenthuoc:"Amlor5mg",donvi:"Viên",tdk:974999340,tdt:779999472,tck:194999868,ngaytk:"27/12/2018"},
{tenthuoc:"Meiunem0,5g",donvi:"Chai/Lọ",tdk:952750000,tdt:762200000,tck:190550000,ngaytk:"27/12/2018"},
{tenthuoc:"Tadocel20mg/0,5ml",donvi:"Ống",tdk:952000000,tdt:761600000,tck:190400000,ngaytk:"27/12/2018"},
{tenthuoc:"DBLIrinotecanInjection100mg/5ml",donvi:"Ống",tdk:945000000,tdt:756000000,tck:189000000,ngaytk:"27/12/2018"},
{tenthuoc:"Klamentin",donvi:"Viên",tdk:935131313,tdt:748105050,tck:187026263,ngaytk:"27/12/2018"},
{tenthuoc:"HumaGlobin2,5g/50ml",donvi:"Phial",tdk:925312500,tdt:740250000,tck:185062500,ngaytk:"27/12/2018"},
{tenthuoc:"Tadocel80mg/2ml",donvi:"Ống",tdk:924750000,tdt:739800000,tck:184950000,ngaytk:"27/12/2018"},
{tenthuoc:"Rocephin1g",donvi:"Phial",tdk:907200000,tdt:725760000,tck:181440000,ngaytk:"27/12/2018"},
{tenthuoc:"Fudcime200mg",donvi:"Viên",tdk:894750006,tdt:715800005,tck:178950001,ngaytk:"27/12/2018"},
{tenthuoc:"Tavanic",donvi:"Chai/Lọ",tdk:892762500,tdt:714210000,tck:178552500,ngaytk:"27/12/2018"},
{tenthuoc:"Coveram(5+5)mg",donvi:"Viên",tdk:889514987,tdt:711611989,tck:177902997,ngaytk:"27/12/2018"},
{tenthuoc:"Coveram5mg/5mg",donvi:"Viên",tdk:889513853,tdt:711611082,tck:177902771,ngaytk:"27/12/2018"},
{tenthuoc:"Meronem",donvi:"Chai/Lọ",tdk:870699375,tdt:696559500,tck:174139875,ngaytk:"27/12/2018"},
{tenthuoc:"Vigamox",donvi:"Ống",tdk:869390340,tdt:695512272,tck:173878068,ngaytk:"27/12/2018"},
{tenthuoc:"Cozaar50mg",donvi:"Viên",tdk:857922760,tdt:686338208,tck:171584552,ngaytk:"27/12/2018"},
{tenthuoc:"Tiepanem",donvi:"Chai/Lọ",tdk:815000000,tdt:652000000,tck:163000000,ngaytk:"27/12/2018"},
{tenthuoc:"Tiepanem1g",donvi:"Ống",tdk:812500000,tdt:650000000,tck:162500000,ngaytk:"27/12/2018"},
{tenthuoc:"Vimotram",donvi:"Chai/Lọ",tdk:806250000,tdt:645000000,tck:161250000,ngaytk:"27/12/2018"},
{tenthuoc:"I.V.-GlobulinSNinj.",donvi:"Chai/Lọ",tdk:800250000,tdt:640200000,tck:160050000,ngaytk:"27/12/2018"},
{tenthuoc:"Novomix30Flexpen",donvi:"Bơm/Búttiêm",tdk:786082500,tdt:628866000,tck:157216500,ngaytk:"27/12/2018"},
{tenthuoc:"Vimotram",donvi:"Chai/Lọ",tdk:783750000,tdt:627000000,tck:156750000,ngaytk:"27/12/2018"},
{tenthuoc:"Proxacin1%",donvi:"Ống",tdk:782000000,tdt:625600000,tck:156400000,ngaytk:"27/12/2018"},
{tenthuoc:"Moxilen(amoxicillin)500mg",donvi:"Viên",tdk:781403348,tdt:625122679,tck:156280670,ngaytk:"27/12/2018"},
{tenthuoc:"Ebitac12.5",donvi:"Viên",tdk:776250000,tdt:690000000,tck:86250000,ngaytk:"27/12/2018"},
{tenthuoc:"Ebitac25",donvi:"Viên",tdk:776250000,tdt:690000000,tck:86250000,ngaytk:"27/12/2018"},
{tenthuoc:"CeftriaxonePanpharma1g",donvi:"Chai/Lọ",tdk:759037584,tdt:607230068,tck:151807517,ngaytk:"27/12/2018"},
{tenthuoc:"PROPOFOL1%KABI",donvi:"Ống",tdk:758887500,tdt:607110000,tck:151777500,ngaytk:"27/12/2018"},
{tenthuoc:"Vismed",donvi:"Tuýp",tdk:752686200,tdt:602148960,tck:150537240,ngaytk:"27/12/2018"},
{tenthuoc:"MedphadionDrops100mg/5ml",donvi:"Ống",tdk:750000000,tdt:600000000,tck:150000000,ngaytk:"27/12/2018"},
{tenthuoc:"Coveram5-5Tab5mg/5mg30's",donvi:"Viên",tdk:740965995,tdt:658636440,tck:82329555,ngaytk:"27/12/2018"},
{tenthuoc:"Meiunem0,5g",donvi:"Chai/Lọ",tdk:740000000,tdt:592000000,tck:148000000,ngaytk:"27/12/2018"},
{tenthuoc:"Vimotram",donvi:"Chai/Lọ",tdk:731500000,tdt:585200000,tck:146300000,ngaytk:"27/12/2018"},
{tenthuoc:"DiprivanInj20ml5's10mg/ml(1%)-20ml",donvi:"Tuýp",tdk:723779000,tdt:579023200,tck:144755800,ngaytk:"27/12/2018"},
{tenthuoc:"DBLIrinotecanInjection40mg/2ml",donvi:"Ống",tdk:708750000,tdt:567000000,tck:141750000,ngaytk:"27/12/2018"},
{tenthuoc:"Proxacin200mg",donvi:"Chai/Lọ",tdk:708749997,tdt:566999997,tck:141749999,ngaytk:"27/12/2018"},
{tenthuoc:"Prospancoughsyrup",donvi:"Chai/Lọ",tdk:702843750,tdt:562275000,tck:140568750,ngaytk:"27/12/2018"},
{tenthuoc:"Ceftriaxonepanpharma",donvi:"Chai/Lọ",tdk:702812500,tdt:562250000,tck:140562500,ngaytk:"27/12/2018"},
{tenthuoc:"Vismed0.18",donvi:"Tuýp",tdk:688432500,tdt:550746000,tck:137686500,ngaytk:"27/12/2018"},
{tenthuoc:"Rocephin1gI.V.",donvi:"Chai/Lọ",tdk:680400000,tdt:544320000,tck:136080000,ngaytk:"27/12/2018"},
{tenthuoc:"BupivacaineAguettant5mg/mlx20ml0,5%/20ml",donvi:"Ống",tdk:664623619,tdt:531698895,tck:132924724,ngaytk:"27/12/2018"},
{tenthuoc:"Vimotram",donvi:"Chai/Lọ",tdk:645000000,tdt:516000000,tck:129000000,ngaytk:"27/12/2018"},
{tenthuoc:"Meiunem0,5g",donvi:"Phial",tdk:643750013,tdt:515000010,tck:128750003,ngaytk:"27/12/2018"},
{tenthuoc:"Polhuminmix-2",donvi:"Chai/Lọ",tdk:643125000,tdt:514500000,tck:128625000,ngaytk:"27/12/2018"},
{tenthuoc:"Levoflex",donvi:"Chai/Lọ",tdk:641700010,tdt:513360008,tck:128340002,ngaytk:"27/12/2018"},
{tenthuoc:"PaxusPM",donvi:"Chai/Lọ",tdk:632812502,tdt:506250002,tck:126562500,ngaytk:"27/12/2018"},
{tenthuoc:"Levoflex",donvi:"Chai/Lọ",tdk:627750000,tdt:558000000,tck:69750000,ngaytk:"27/12/2018"},
{tenthuoc:"Twynsta",donvi:"Viên",tdk:624099998,tdt:499279998,tck:124820000,ngaytk:"27/12/2018"},
{tenthuoc:"Unasyl",donvi:"Chai/Lọ",tdk:618750000,tdt:495000000,tck:123750000,ngaytk:"27/12/2018"},
{tenthuoc:"Aldactontab",donvi:"Viên",tdk:592500000,tdt:474000000,tck:118500000,ngaytk:"27/12/2018"},
{tenthuoc:"AdalatLA30mg",donvi:"Viên",tdk:590638681,tdt:472510945,tck:118127736,ngaytk:"27/12/2018"},
{tenthuoc:"Progut",donvi:"Chai/Lọ",tdk:590625000,tdt:472500000,tck:118125000,ngaytk:"27/12/2018"},
{tenthuoc:"Concor5mg",donvi:"Viên",tdk:590399933,tdt:472319946,tck:118079987,ngaytk:"27/12/2018"},
{tenthuoc:"Exforge5mg+80mg",donvi:"Viên",tdk:586535981,tdt:469228785,tck:117307196,ngaytk:"27/12/2018"},
{tenthuoc:"Meronem500mg",donvi:"Phial",tdk:580466250,tdt:464373000,tck:116093250,ngaytk:"27/12/2018"},
{tenthuoc:"Indocollyre",donvi:"Ống",tdk:577500000,tdt:462000000,tck:115500000,ngaytk:"27/12/2018"},
{tenthuoc:"Sanbeclaneksi(Amoxicillin+acidclavulanic)1.2g",donvi:"Chai/Lọ",tdk:577345934,tdt:461876748,tck:115469187,ngaytk:"27/12/2018"},
{tenthuoc:"Nexium",donvi:"Chai/Lọ",tdk:575849964,tdt:460679972,tck:115169993,ngaytk:"27/12/2018"},
{tenthuoc:"Rocephin1gI.V.",donvi:"Chai/Lọ",tdk:567000000,tdt:453600000,tck:113400000,ngaytk:"27/12/2018"},
{tenthuoc:"Taxotere(Docetaxel)80mg/4ml",donvi:"Chai/Lọ",tdk:560649495,tdt:448519596,tck:112129899,ngaytk:"27/12/2018"},
{tenthuoc:"Xalvobin(capecitabin)500mg",donvi:"Viên",tdk:559125000,tdt:447300000,tck:111825000,ngaytk:"27/12/2018"},
{tenthuoc:"AdalatLATab20mg",donvi:"Viên",tdk:550226250,tdt:440181000,tck:110045250,ngaytk:"27/12/2018"},
{tenthuoc:"Concor",donvi:"Viên",tdk:540704250,tdt:432563400,tck:108140850,ngaytk:"27/12/2018"},
{tenthuoc:"Infartan(Clopidogrel)75mg",donvi:"Viên",tdk:539999933,tdt:431999946,tck:107999987,ngaytk:"27/12/2018"},
{tenthuoc:"Gardenal100mg",donvi:"Viên",tdk:538650000,tdt:430920000,tck:107730000,ngaytk:"27/12/2018"},
{tenthuoc:"Sanlein0,1%x5ml",donvi:"Chai/Lọ",tdk:538370000,tdt:430696000,tck:107674000,ngaytk:"27/12/2018"},
{tenthuoc:"UnasynInj1500mg1's",donvi:"Chai/Lọ",tdk:528000000,tdt:422400000,tck:105600000,ngaytk:"27/12/2018"},
{tenthuoc:"Aristin-C(Ciprofloxacin200mg/100ml)",donvi:"Chai/Lọ",tdk:526499951,tdt:421199961,tck:105299990,ngaytk:"27/12/2018"},
{tenthuoc:"Resines(Amlodipin)5mg",donvi:"Viên",tdk:524979000,tdt:419983200,tck:104995800,ngaytk:"27/12/2018"},
{tenthuoc:"Kedrigamma",donvi:"Chai/Lọ",tdk:520162500,tdt:416130000,tck:104032500,ngaytk:"27/12/2018"},
{tenthuoc:"Ebitac25",donvi:"Viên",tdk:517500000,tdt:414000000,tck:103500000,ngaytk:"27/12/2018"},
{tenthuoc:"Tartriakson1g",donvi:"Chai/Lọ",tdk:506625000,tdt:405300000,tck:101325000,ngaytk:"27/12/2018"},
{tenthuoc:"Ceftriaxon",donvi:"Chai/Lọ",tdk:506025000,tdt:404820000,tck:101205000,ngaytk:"27/12/2018"},
{tenthuoc:"Prospan70ml",donvi:"Ống",tdk:502031250,tdt:401625000,tck:100406250,ngaytk:"27/12/2018"},
{tenthuoc:"Tercef1g",donvi:"Ống",tdk:500850000,tdt:445200000,tck:55650000,ngaytk:"27/12/2018"},
{tenthuoc:"Sanbeclaneksi(Amoxicillin+acidclavulanic)1.2g",donvi:"Chai/Lọ",tdk:500499431,tdt:400399545,tck:100099886,ngaytk:"27/12/2018"},
{tenthuoc:"Novomix30/70Flexpen100UI3ml",donvi:"Bơm/Búttiêm",tdk:498421875,tdt:398737500,tck:99684375,ngaytk:"27/12/2018"},
{tenthuoc:"Rocephin1gI.V.",donvi:"Chai/Lọ",tdk:495558000,tdt:396446400,tck:99111600,ngaytk:"27/12/2018"},
{tenthuoc:"Coveram5-5Tab5mg/5mg30's",donvi:"Viên",tdk:494175000,tdt:395340000,tck:98835000,ngaytk:"27/12/2018"},
{tenthuoc:"LOVENOX40mgInjB/2syringesx0,4ml",donvi:"Ống",tdk:488806225,tdt:391044980,tck:97761245,ngaytk:"27/12/2018"},
{tenthuoc:"LOVENOX40mgInjB/2syringesx0,4ml",donvi:"Ống",tdk:488806225,tdt:391044980,tck:97761245,ngaytk:"27/12/2018"},
{tenthuoc:"Remeclar500",donvi:"Viên",tdk:485301250,tdt:388241000,tck:97060250,ngaytk:"27/12/2018"},
{tenthuoc:"Remeclar500",donvi:"Viên",tdk:485301250,tdt:388241000,tck:97060250,ngaytk:"27/12/2018"},
{tenthuoc:"Remeclar500mg",donvi:"Viên",tdk:485301250,tdt:388241000,tck:97060250,ngaytk:"27/12/2018"},
{tenthuoc:"Mixtard30Flexpen100IU/mlx3ml",donvi:"Bơm/Búttiêm",tdk:481246875,tdt:384997500,tck:96249375,ngaytk:"27/12/2018"},
{tenthuoc:"Bisoprolol2,5mg",donvi:"Viên",tdk:480138750,tdt:384111000,tck:96027750,ngaytk:"27/12/2018"},
{tenthuoc:"Paclispec30",donvi:"Tuýp",tdk:479375000,tdt:383500000,tck:95875000,ngaytk:"27/12/2018"},
{tenthuoc:"Hyđan",donvi:"Viên",tdk:462475000,tdt:369980000,tck:92495000,ngaytk:"27/12/2018"},
{tenthuoc:"Ebitac(Enalapril10mg+Hydroclorothiazid25mg)",donvi:"Viên",tdk:460228533,tdt:361282848,tck:98945685,ngaytk:"27/12/2018"},
{tenthuoc:"Polhuminmix-2",donvi:"Ống",tdk:459375000,tdt:367500000,tck:91875000,ngaytk:"27/12/2018"},
{tenthuoc:"Boganic",donvi:"Viên",tdk:457250000,tdt:365800000,tck:91450000,ngaytk:"27/12/2018"},
{tenthuoc:"InsulinHMix100UI",donvi:"Chai/Lọ",tdk:452100000,tdt:361680000,tck:90420000,ngaytk:"27/12/2018"},
{tenthuoc:"Nexiummups",donvi:"Viên",tdk:451927001,tdt:361541601,tck:90385400,ngaytk:"27/12/2018"},
{tenthuoc:"Chirocain5mg/mlAmpoule10x10ml5mg/ml",donvi:"Tuýp",tdk:449999996,tdt:359999997,tck:89999999,ngaytk:"27/12/2018"},
{tenthuoc:"LevemirFlexpen",donvi:"Bơm/Búttiêm",tdk:446875000,tdt:357500000,tck:89375000,ngaytk:"27/12/2018"},
{tenthuoc:"Coveram(5+10)mg",donvi:"Viên",tdk:444757493,tdt:355805995,tck:88951499,ngaytk:"27/12/2018"},
{tenthuoc:"Azipowder",donvi:"Chai/Lọ",tdk:443750000,tdt:355000000,tck:88750000,ngaytk:"27/12/2018"},
{tenthuoc:"Cardilopin",donvi:"Viên",tdk:442800000,tdt:354240000,tck:88560000,ngaytk:"27/12/2018"},
{tenthuoc:"HumulinN100IU10ml",donvi:"Chai/Lọ",tdk:441000000,tdt:352800000,tck:88200000,ngaytk:"27/12/2018"},
{tenthuoc:"VitaminB1-B6-B12",donvi:"Viên",tdk:439229500,tdt:351383600,tck:87845900,ngaytk:"27/12/2018"},
{tenthuoc:"VitaminB1-B6-B12",donvi:"Viên",tdk:432000000,tdt:384000000,tck:48000000,ngaytk:"27/12/2018"},
{tenthuoc:"Concor2,5mg",donvi:"Viên",tdk:431699940,tdt:345359952,tck:86339988,ngaytk:"27/12/2018"},
{tenthuoc:"Ebitac12.5",donvi:"Viên",tdk:431250000,tdt:345000000,tck:86250000,ngaytk:"27/12/2018"},
{tenthuoc:"Sanlein",donvi:"Ống",tdk:427336250,tdt:341869000,tck:85467250,ngaytk:"27/12/2018"},
{tenthuoc:"Tercef",donvi:"Ống",tdk:417375000,tdt:333900000,tck:83475000,ngaytk:"27/12/2018"},
{tenthuoc:"Propofol1%Kabi",donvi:"Ống",tdk:415209375,tdt:332167500,tck:83041875,ngaytk:"27/12/2018"},
{tenthuoc:"UnasynInj1500mg1's",donvi:"Chai/Lọ",tdk:412500000,tdt:330000000,tck:82500000,ngaytk:"27/12/2018"},
{tenthuoc:"UnasynInj1500mg1's",donvi:"Chai/Lọ",tdk:412500000,tdt:330000000,tck:82500000,ngaytk:"27/12/2018"},
{tenthuoc:"InsulinHMix100IU",donvi:"Ống",tdk:412500000,tdt:330000000,tck:82500000,ngaytk:"27/12/2018"},
{tenthuoc:"Oflovidophthalmicointment",donvi:"Tuýp",tdk:408653625,tdt:326922900,tck:81730725,ngaytk:"27/12/2018"},
{tenthuoc:"Rocephin1gI.V.",donvi:"Ống",tdk:408240000,tdt:362880000,tck:45360000,ngaytk:"27/12/2018"},
{tenthuoc:"Franilax",donvi:"Viên",tdk:407673000,tdt:326138400,tck:81534600,ngaytk:"27/12/2018"},
{tenthuoc:"Fudcime",donvi:"Viên",tdk:402637500,tdt:322110000,tck:80527500,ngaytk:"27/12/2018"},
{tenthuoc:"Fudcime",donvi:"Chai/Lọ",tdk:395000000,tdt:316000000,tck:79000000,ngaytk:"27/12/2018"},
{tenthuoc:"Paclitaxel Ebewe Inj100MG/16.7ML1's",donvi:"Ống",tdk:393933000,tdt:315146400,tck:78786600,ngaytk:"27/12/2018"},
{tenthuoc:"Ucyrin75mg",donvi:"Viên",tdk:393901200,tdt:315120960,tck:78780240,ngaytk:"27/12/2018"},
{tenthuoc:"Vigamox",donvi:"Chai/Lọ",tdk:393750000,tdt:315000000,tck:78750000,ngaytk:"27/12/2018"},
{tenthuoc:"Levonor4mg/4ml",donvi:"Tuýp",tdk:387187500,tdt:309750000,tck:77437500,ngaytk:"27/12/2018"},
{tenthuoc:"ScilinM30(30/70)",donvi:"Chai/Lọ",tdk:386250000,tdt:309000000,tck:77250000,ngaytk:"27/12/2018"},
{tenthuoc:"Mixtard30FlexPen",donvi:"Bơm/Búttiêm",tdk:384997500,tdt:307998000,tck:76999500,ngaytk:"27/12/2018"},
{tenthuoc:"Xeloda(Capecitabin)500mg",donvi:"Viên",tdk:381780277,tdt:305424222,tck:76356055,ngaytk:"27/12/2018"},
{tenthuoc:"Emas",donvi:"Chai/Lọ",tdk:375000000,tdt:300000000,tck:75000000,ngaytk:"27/12/2018"},
{tenthuoc:"Exforge5/80mg",donvi:"Viên",tdk:374362673,tdt:299490138,tck:74872535,ngaytk:"27/12/2018"},
{tenthuoc:"Unasyn1500mg",donvi:"Chai/Lọ",tdk:371250000,tdt:297000000,tck:74250000,ngaytk:"27/12/2018"},
{tenthuoc:"UnasynInj1500mg1's",donvi:"Chai/Lọ",tdk:371250000,tdt:297000000,tck:74250000,ngaytk:"27/12/2018"},
{tenthuoc:"Diprivan20ml/200mg",donvi:"Tuýp",tdk:369274992,tdt:295419994,tck:73854998,ngaytk:"27/12/2018"},
{tenthuoc:"Tobradex5ml",donvi:"Chai/Lọ",tdk:355154625,tdt:284123700,tck:71030925,ngaytk:"27/12/2018"},
{tenthuoc:"Azipowder",donvi:"Chai/Lọ",tdk:355000000,tdt:284000000,tck:71000000,ngaytk:"27/12/2018"},
{tenthuoc:"Heparin25000UI/5ml",donvi:"Chai/Lọ",tdk:354375000,tdt:283500000,tck:70875000,ngaytk:"27/12/2018"},
{tenthuoc:"Parocontin",donvi:"Viên",tdk:352506000,tdt:282004800,tck:70501200,ngaytk:"27/12/2018"},
{tenthuoc:"Sastan-H",donvi:"Viên",tdk:349965000,tdt:279972000,tck:69993000,ngaytk:"27/12/2018"},
{tenthuoc:"Mixtard30Flexpen100IU/mlx3ml",donvi:"Bơm/Búttiêm",tdk:346497750,tdt:307998000,tck:38499750,ngaytk:"27/12/2018"},
{tenthuoc:"Coveram5-10Tab5mg-10mg30's",donvi:"Viên",tdk:345922500,tdt:276738000,tck:69184500,ngaytk:"27/12/2018"},
{tenthuoc:"Ebitac12.5",donvi:"Viên",tdk:345000000,tdt:276000000,tck:69000000,ngaytk:"27/12/2018"},
{tenthuoc:"Bloza",donvi:"Viên",tdk:343178325,tdt:274542660,tck:68635665,ngaytk:"27/12/2018"},
{tenthuoc:"Twynsta40/5mg",donvi:"Viên",tdk:342505819,tdt:274004655,tck:68501164,ngaytk:"27/12/2018"},
{tenthuoc:"Rocephin1gI.V.",donvi:"Chai/Lọ",tdk:340200000,tdt:272160000,tck:68040000,ngaytk:"27/12/2018"},
{tenthuoc:"ExforgeHCT5/160/12.5mg",donvi:"Viên",tdk:335884813,tdt:268707850,tck:67176963,ngaytk:"27/12/2018"},
{tenthuoc:"Diamisu-N10ml",donvi:"Ống",tdk:333500000,tdt:266800000,tck:66700000,ngaytk:"27/12/2018"},
{tenthuoc:"Cravit5mg/ml",donvi:"Chai/Lọ",tdk:331020625,tdt:264816500,tck:66204125,ngaytk:"27/12/2018"},
{tenthuoc:"Polhuminmix-2",donvi:"Tuýp",tdk:330750000,tdt:294000000,tck:36750000,ngaytk:"27/12/2018"},
{tenthuoc:"Bupivacaine20mg0.5%5mg/ml0,5%/4ml",donvi:"Tuýp",tdk:330750000,tdt:264600000,tck:66150000,ngaytk:"27/12/2018"},
{tenthuoc:"UnasynInj1500mg1's",donvi:"Ống",tdk:330000000,tdt:264000000,tck:66000000,ngaytk:"27/12/2018"},
{tenthuoc:"Reditux100mg/10ml",donvi:"Ống",tdk:330000000,tdt:264000000,tck:66000000,ngaytk:"27/12/2018"},
{tenthuoc:"Coveram",donvi:"Viên",tdk:329367638,tdt:263494110,tck:65873528,ngaytk:"27/12/2018"},
{tenthuoc:"Bibiso",donvi:"Viên",tdk:329175000,tdt:263340000,tck:65835000,ngaytk:"27/12/2018"},
{tenthuoc:"Coveram5-10Tab5mg-10mg30's",donvi:"Viên",tdk:329120550,tdt:263296440,tck:65824110,ngaytk:"27/12/2018"},
{tenthuoc:"InsulinHMix100IU",donvi:"Ống",tdk:328680000,tdt:262944000,tck:65736000,ngaytk:"27/12/2018"},
{tenthuoc:"DiprivanPre-FilledSyring1%10mg/ml(1%)-50ml",donvi:"Tuýp",tdk:328125000,tdt:262500000,tck:65625000,ngaytk:"27/12/2018"},
{tenthuoc:"Mixtard30Flexpen100ui/mlx3ml",donvi:"Bơm/Búttiêm",tdk:327247875,tdt:261798300,tck:65449575,ngaytk:"27/12/2018"},
{tenthuoc:"Exforgetab5mg/80mg2x14's",donvi:"Viên",tdk:327174120,tdt:261739296,tck:65434824,ngaytk:"27/12/2018"},
{tenthuoc:"Fudcime(Cefixim)200mg",donvi:"Viên",tdk:319374956,tdt:255499965,tck:63874991,ngaytk:"27/12/2018"},
{tenthuoc:"SaviLosartanplusHCT50/12.5",donvi:"Viên",tdk:317724375,tdt:254179500,tck:63544875,ngaytk:"27/12/2018"},
{tenthuoc:"Vigamox",donvi:"Chai/Lọ",tdk:314996500,tdt:251997200,tck:62999300,ngaytk:"27/12/2018"},
{tenthuoc:"Heparin-Belmed",donvi:"Ống",tdk:311393125,tdt:249114500,tck:62278625,ngaytk:"27/12/2018"},
{tenthuoc:"ScilinM30(30/70)",donvi:"Chai/Lọ",tdk:309000000,tdt:247200000,tck:61800000,ngaytk:"27/12/2018"},
{tenthuoc:"Cravit",donvi:"Ống",tdk:305557500,tdt:244446000,tck:61111500,ngaytk:"27/12/2018"},
{tenthuoc:"LosartanStada50mg",donvi:"Viên",tdk:302445000,tdt:241956000,tck:60489000,ngaytk:"27/12/2018"},
{tenthuoc:"Resines5mg(amlodipin)",donvi:"Viên",tdk:301826700,tdt:241461360,tck:60365340,ngaytk:"27/12/2018"},
{tenthuoc:"Meronem1g(Meropenem)",donvi:"Chai/Lọ",tdk:301396125,tdt:241116900,tck:60279225,ngaytk:"27/12/2018"},
{tenthuoc:"InsulinHMix100IU",donvi:"Ống",tdk:297000000,tdt:264000000,tck:33000000,ngaytk:"27/12/2018"},
{tenthuoc:"Bimoxine",donvi:"Gói",tdk:296450000,tdt:237160000,tck:59290000,ngaytk:"27/12/2018"},
{tenthuoc:"Fudcime200mg",donvi:"Viên",tdk:296250000,tdt:237000000,tck:59250000,ngaytk:"27/12/2018"},
{tenthuoc:"Novorapid",donvi:"Bơm/Búttiêm",tdk:295312500,tdt:236250000,tck:59062500,ngaytk:"27/12/2018"},
{tenthuoc:"Bactirid100mg/5ml",donvi:"Chai/Lọ",tdk:295000000,tdt:236000000,tck:59000000,ngaytk:"27/12/2018"},
{tenthuoc:"ParingoldInjection25000Ui/5ml",donvi:"Chai/Lọ",tdk:294937256,tdt:235949805,tck:58987451,ngaytk:"27/12/2018"},
{tenthuoc:"Mixtard30FlexPen",donvi:"Bơm/Búttiêm",tdk:288748125,tdt:230998500,tck:57749625,ngaytk:"27/12/2018"},
{tenthuoc:"Mixtard30Flexpen100IU/mlx3ml",donvi:"Bơm/Búttiêm",tdk:288747900,tdt:230998320,tck:57749580,ngaytk:"27/12/2018"},
{tenthuoc:"Lotemax",donvi:"Chai/Lọ",tdk:288093750,tdt:230475000,tck:57618750,ngaytk:"27/12/2018"},
{tenthuoc:"Paracetamol500mg(PanadolCaplet)",donvi:"Viên",tdk:286875000,tdt:229500000,tck:57375000,ngaytk:"27/12/2018"},
{tenthuoc:"Cravit5mg/ml",donvi:"Chai/Lọ",tdk:285187000,tdt:228149600,tck:57037400,ngaytk:"27/12/2018"},
{tenthuoc:"Clocardigel(Clopidogrel)75mg",donvi:"Viên",tdk:284602500,tdt:227682000,tck:56920500,ngaytk:"27/12/2018"},
{tenthuoc:"CozaarXQ5mg+100mg",donvi:"Viên",tdk:284255778,tdt:227404622,tck:56851156,ngaytk:"27/12/2018"},
{tenthuoc:"NifedipinHasan20Retard",donvi:"Viên",tdk:283500000,tdt:226800000,tck:56700000,ngaytk:"27/12/2018"},
{tenthuoc:"Venrutine",donvi:"Viên",tdk:281250000,tdt:225000000,tck:56250000,ngaytk:"27/12/2018"},
{tenthuoc:"Mecefix-B.E",donvi:"Viên",tdk:281250000,tdt:225000000,tck:56250000,ngaytk:"27/12/2018"},
{tenthuoc:"Twynsta",donvi:"Viên",tdk:277412450,tdt:221929960,tck:55482490,ngaytk:"27/12/2018"},
{tenthuoc:"Tamifine",donvi:"Viên",tdk:268750000,tdt:215000000,tck:53750000,ngaytk:"27/12/2018"},
{tenthuoc:"Heparin-Belmed25.000UI/5ml",donvi:"Chai/Lọ",tdk:268750000,tdt:215000000,tck:53750000,ngaytk:"27/12/2018"},
{tenthuoc:"Xalvobin",donvi:"Viên",tdk:267750000,tdt:214200000,tck:53550000,ngaytk:"27/12/2018"},
{tenthuoc:"Ofmantine-Domesco625mg",donvi:"Viên",tdk:267534190,tdt:214027352,tck:53506838,ngaytk:"27/12/2018"},
{tenthuoc:"NifedipineHasan20retard",donvi:"Viên",tdk:266062500,tdt:212850000,tck:53212500,ngaytk:"27/12/2018"},
{tenthuoc:"Resines5mg",donvi:"Viên",tdk:262458000,tdt:209966400,tck:52491600,ngaytk:"27/12/2018"},
{tenthuoc:"Exforge5/80mg",donvi:"Viên",tdk:262158789,tdt:209727032,tck:52431758,ngaytk:"27/12/2018"},
{tenthuoc:"AdalatLATab20mg30's",donvi:"Viên",tdk:260163750,tdt:208131000,tck:52032750,ngaytk:"27/12/2018"},
{tenthuoc:"Bibiso",donvi:"Viên",tdk:259875000,tdt:207900000,tck:51975000,ngaytk:"27/12/2018"},
{tenthuoc:"Ebitac25mg",donvi:"Viên",tdk:258750000,tdt:207000000,tck:51750000,ngaytk:"27/12/2018"},
{tenthuoc:"Diprivan200mg/10ml",donvi:"Tuýp",tdk:258492500,tdt:206794000,tck:51698500,ngaytk:"27/12/2018"},
{tenthuoc:"ScilinM30(30/70)",donvi:"Chai/Lọ",tdk:257500000,tdt:206000000,tck:51500000,ngaytk:"27/12/2018"},
{tenthuoc:"ScilinR",donvi:"Chai/Lọ",tdk:257500000,tdt:206000000,tck:51500000,ngaytk:"27/12/2018"},
{tenthuoc:"Plavix",donvi:"Viên",tdk:256965450,tdt:205572360,tck:51393090,ngaytk:"27/12/2018"},
{tenthuoc:"AmlorCap5mg30's",donvi:"Viên",tdk:253804688,tdt:203043750,tck:50760938,ngaytk:"27/12/2018"},
{tenthuoc:"NifedipinT20Stadaretard",donvi:"Viên",tdk:253750000,tdt:203000000,tck:50750000,ngaytk:"27/12/2018"},
{tenthuoc:"Loratadin5mg/5ml*30ml(Ganusa)",donvi:"Ống",tdk:253725000,tdt:202980000,tck:50745000,ngaytk:"27/12/2018"},
{tenthuoc:"NoradrenalinBaseAguettant4mg/4ml",donvi:"Tuýp",tdk:253124944,tdt:202499955,tck:50624989,ngaytk:"27/12/2018"},
{tenthuoc:"Tadocel20mg/0,5ml",donvi:"Ống",tdk:251562500,tdt:201250000,tck:50312500,ngaytk:"27/12/2018"},
{tenthuoc:"Sotstop",donvi:"Chai/Lọ",tdk:250250000,tdt:200200000,tck:50050000,ngaytk:"27/12/2018"},
{tenthuoc:"Fabapoxim",donvi:"Chai/Lọ",tdk:250000000,tdt:200000000,tck:50000000,ngaytk:"27/12/2018"},
{tenthuoc:"Klamentin1g",donvi:"Viên",tdk:249275250,tdt:199420200,tck:49855050,ngaytk:"27/12/2018"},
{tenthuoc:"Aldacton25mg",donvi:"Viên",tdk:248109061,tdt:198487249,tck:49621812,ngaytk:"27/12/2018"},
{tenthuoc:"Unasyn1,5g",donvi:"Chai/Lọ",tdk:247500000,tdt:198000000,tck:49500000,ngaytk:"27/12/2018"},
{tenthuoc:"Coveram5-5Tab5mg/5mg30's",donvi:"Viên",tdk:247087500,tdt:197670000,tck:49417500,ngaytk:"27/12/2018"},
{tenthuoc:"Coveram5-5Tab5mg/5mg30's",donvi:"Viên",tdk:247087500,tdt:197670000,tck:49417500,ngaytk:"27/12/2018"},
{tenthuoc:"CoveramTab5mg/5mg30's",donvi:"Viên",tdk:247087500,tdt:197670000,tck:49417500,ngaytk:"27/12/2018"},
{tenthuoc:"Amlorcap",donvi:"Viên",tdk:244765625,tdt:195812500,tck:48953125,ngaytk:"27/12/2018"},
{tenthuoc:"SystanUltra",donvi:"Ống",tdk:241898475,tdt:193518780,tck:48379695,ngaytk:"27/12/2018"},
{tenthuoc:"Tartriakson(Ceftriaxon)1g",donvi:"Ống",tdk:241249969,tdt:192999975,tck:48249994,ngaytk:"27/12/2018"},
{tenthuoc:"ToxaxineInj",donvi:"Tuýp",tdk:240625000,tdt:192500000,tck:48125000,ngaytk:"27/12/2018"},
{tenthuoc:"Cozaar",donvi:"Viên",tdk:240456976,tdt:192365581,tck:48091395,ngaytk:"27/12/2018"},
{tenthuoc:"Resines",donvi:"Viên",tdk:239903996,tdt:191923197,tck:47980799,ngaytk:"27/12/2018"},
{tenthuoc:"Moxilen500mg",donvi:"Viên",tdk:238000000,tdt:190400000,tck:47600000,ngaytk:"27/12/2018"},
{tenthuoc:"Hometex",donvi:"Viên",tdk:237500000,tdt:190000000,tck:47500000,ngaytk:"27/12/2018"},
{tenthuoc:"Ebitac12.5",donvi:"Viên",tdk:237187500,tdt:189750000,tck:47437500,ngaytk:"27/12/2018"},
{tenthuoc:"AmlorTab5mg30's",donvi:"Viên",tdk:236332126,tdt:189065701,tck:47266425,ngaytk:"27/12/2018"},
{tenthuoc:"Sanlein",donvi:"Ống",tdk:236250000,tdt:189000000,tck:47250000,ngaytk:"27/12/2018"},
{tenthuoc:"Medsamic",donvi:"Tuýp",tdk:235750000,tdt:188600000,tck:47150000,ngaytk:"27/12/2018"},
{tenthuoc:"Moxacin",donvi:"Viên",tdk:235462500,tdt:188370000,tck:47092500,ngaytk:"27/12/2018"},
{tenthuoc:"Thiênsứhộtâmđan",donvi:"Viên",tdk:235200022,tdt:188160018,tck:47040004,ngaytk:"27/12/2018"},
{tenthuoc:"Mecefix(cefixim150mg)",donvi:"Viên",tdk:234375000,tdt:187500000,tck:46875000,ngaytk:"27/12/2018"},
{tenthuoc:"ScilinM30400IU",donvi:"IU",tdk:231761250,tdt:185409000,tck:46352250,ngaytk:"27/12/2018"},
{tenthuoc:"ScilinM30(30/70)",donvi:"Chai/Lọ",tdk:231750000,tdt:185400000,tck:46350000,ngaytk:"27/12/2018"},
{tenthuoc:"CamptoInj100mg5ml",donvi:"Ống",tdk:229952500,tdt:183962000,tck:45990500,ngaytk:"27/12/2018"},
{tenthuoc:"Campto(Irinotecan)100mg",donvi:"Chai/Lọ",tdk:229952500,tdt:183962000,tck:45990500,ngaytk:"27/12/2018"},
{tenthuoc:"LOVENOX60mgInjB/2syringesx0,6ml",donvi:"Ống",tdk:229155075,tdt:183324060,tck:45831015,ngaytk:"27/12/2018"},
{tenthuoc:"LOVENOX60mgInjB/2syringesx0,6ml",donvi:"Ống",tdk:229155075,tdt:183324060,tck:45831015,ngaytk:"27/12/2018"},
{tenthuoc:"Rocephin1g",donvi:"Chai/Lọ",tdk:226800000,tdt:181440000,tck:45360000,ngaytk:"27/12/2018"},
{tenthuoc:"Rocephin1gI.V.",donvi:"Chai/Lọ",tdk:226800000,tdt:181440000,tck:45360000,ngaytk:"27/12/2018"},
{tenthuoc:"Paclitaxel Ebewe Inj30MG/5ML1's",donvi:"Ống",tdk:225550000,tdt:180440000,tck:45110000,ngaytk:"27/12/2018"},
{tenthuoc:"Lisonorm5mg+10mg",donvi:"Viên",tdk:225540000,tdt:180432000,tck:45108000,ngaytk:"27/12/2018"},
{tenthuoc:"Fabapoxim",donvi:"Chai/Lọ",tdk:225400000,tdt:180320000,tck:45080000,ngaytk:"27/12/2018"},
{tenthuoc:"EDNYT10MGTablet(s)nén",donvi:"Viên",tdk:224253738,tdt:179402990,tck:44850748,ngaytk:"27/12/2018"},
{tenthuoc:"AdalatLA20mg",donvi:"Viên",tdk:223125013,tdt:178500011,tck:44625003,ngaytk:"27/12/2018"},
{tenthuoc:"Tercef",donvi:"Chai/Lọ",tdk:222600000,tdt:178080000,tck:44520000,ngaytk:"27/12/2018"},
{tenthuoc:"IsoticMoxisone",donvi:"Chai/Lọ",tdk:221875000,tdt:177500000,tck:44375000,ngaytk:"27/12/2018"},
{tenthuoc:"IsoticMoxisone",donvi:"Chai/Lọ",tdk:221875000,tdt:177500000,tck:44375000,ngaytk:"27/12/2018"},
{tenthuoc:"Bactirid40ml",donvi:"Chai/Lọ",tdk:221250000,tdt:177000000,tck:44250000,ngaytk:"27/12/2018"},
{tenthuoc:"Hoạthuyếtdưỡngnão",donvi:"Viên",tdk:220500000,tdt:176400000,tck:44100000,ngaytk:"27/12/2018"},
{tenthuoc:"Bisoprololfumarat",donvi:"Viên",tdk:219459240,tdt:175567392,tck:43891848,ngaytk:"27/12/2018"},
{tenthuoc:"Ebitac12,5",donvi:"Viên",tdk:218750000,tdt:175000000,tck:43750000,ngaytk:"27/12/2018"},
{tenthuoc:"Venrutine",donvi:"Viên",tdk:218625000,tdt:174900000,tck:43725000,ngaytk:"27/12/2018"},
{tenthuoc:"Propofol1%/20mL.",donvi:"Ống",tdk:218531250,tdt:174825000,tck:43706250,ngaytk:"27/12/2018"},
{tenthuoc:"Zitromax600mg/15ml",donvi:"Chai/Lọ",tdk:217477496,tdt:173981997,tck:43495499,ngaytk:"27/12/2018"},
{tenthuoc:"Heparin25000UI/5ml",donvi:"Chai/Lọ",tdk:217350000,tdt:173880000,tck:43470000,ngaytk:"27/12/2018"},
{tenthuoc:"CIPROFLOXACINKABI",donvi:"Chai/Lọ",tdk:216562500,tdt:173250000,tck:43312500,ngaytk:"27/12/2018"},
{tenthuoc:"Oflovid3mg/mlx5ml",donvi:"Chai/Lọ",tdk:216504000,tdt:173203200,tck:43300800,ngaytk:"27/12/2018"},
{tenthuoc:"Ebitac12,5",donvi:"Viên",tdk:215625000,tdt:172500000,tck:43125000,ngaytk:"27/12/2018"},
{tenthuoc:"Bloza",donvi:"Viên",tdk:214593750,tdt:171675000,tck:42918750,ngaytk:"27/12/2018"},
{tenthuoc:"UnasynInj1500mg1's",donvi:"Ống",tdk:214500000,tdt:171600000,tck:42900000,ngaytk:"27/12/2018"},
{tenthuoc:"Levonor1mg/ml",donvi:"Tuýp",tdk:212625000,tdt:189000000,tck:23625000,ngaytk:"27/12/2018"},
{tenthuoc:"Levonor4mg/4ml",donvi:"Ống",tdk:212625000,tdt:170100000,tck:42525000,ngaytk:"27/12/2018"},
{tenthuoc:"Paclitaxel Ebewe Inj100MG/16.7ML1's",donvi:"Ống",tdk:211994750,tdt:169595800,tck:42398950,ngaytk:"27/12/2018"},
{tenthuoc:"Bidilucil500",donvi:"Ống",tdk:211125000,tdt:168900000,tck:42225000,ngaytk:"27/12/2018"},
{tenthuoc:"MixtardFlexpen30/70100UI/ml,3ml",donvi:"Bơm/Búttiêm",tdk:207898488,tdt:166318790,tck:41579698,ngaytk:"27/12/2018"},
{tenthuoc:"Scanneuron300,2mg",donvi:"Viên",tdk:206250188,tdt:165000150,tck:41250038,ngaytk:"27/12/2018"},
{tenthuoc:"Scanneuron",donvi:"Viên",tdk:205875000,tdt:164700000,tck:41175000,ngaytk:"27/12/2018"},
{tenthuoc:"Duotrav",donvi:"Ống",tdk:200000000,tdt:160000000,tck:40000000,ngaytk:"27/12/2018"},
{tenthuoc:"3bpluzs(vitaminb1+b6+b12)",donvi:"Viên",tdk:199875000,tdt:159900000,tck:39975000,ngaytk:"27/12/2018"},
{tenthuoc:"Paclitaxel100mg/16,7ml",donvi:"Chai/Lọ",tdk:196966500,tdt:157573200,tck:39393300,ngaytk:"27/12/2018"},
{tenthuoc:"MorphiniSulfasWzf0,1%2mg2mlSpinal2mg/2ml",donvi:"Tuýp",tdk:196875000,tdt:157500000,tck:39375000,ngaytk:"27/12/2018"},
{tenthuoc:"Curam625mg",donvi:"Viên",tdk:196664008,tdt:157331206,tck:39332802,ngaytk:"27/12/2018"},
{tenthuoc:"Mixtard30FlexPen",donvi:"Bơm/Búttiêm",tdk:192498750,tdt:153999000,tck:38499750,ngaytk:"27/12/2018"},
{tenthuoc:"Mixtard30flexpen(insulinhuman)",donvi:"Ống",tdk:192498600,tdt:153998880,tck:38499720,ngaytk:"27/12/2018"},
{tenthuoc:"Lovenox0,4ml,4000UI",donvi:"Bơm/Búttiêm",tdk:192107231,tdt:153685785,tck:38421446,ngaytk:"27/12/2018"},
{tenthuoc:"Lotemax",donvi:"Ống",tdk:192062500,tdt:153650000,tck:38412500,ngaytk:"27/12/2018"},
{tenthuoc:"InsulinHMix100UI",donvi:"Chai/Lọ",tdk:191399991,tdt:153119993,tck:38279998,ngaytk:"27/12/2018"},
{tenthuoc:"ScilinR400IU",donvi:"IU",tdk:190543710,tdt:152434968,tck:38108742,ngaytk:"27/12/2018"},
{tenthuoc:"Vigadexa5ml",donvi:"Chai/Lọ",tdk:189898200,tdt:151918560,tck:37979640,ngaytk:"27/12/2018"},
{tenthuoc:"Plofed",donvi:"Ống",tdk:189375000,tdt:151500000,tck:37875000,ngaytk:"27/12/2018"},
{tenthuoc:"Plofed(Propofol)1%/20ml",donvi:"Chai/Lọ",tdk:189375000,tdt:151500000,tck:37875000,ngaytk:"27/12/2018"},
{tenthuoc:"Bloza",donvi:"Viên",tdk:188811000,tdt:151048800,tck:37762200,ngaytk:"27/12/2018"},
{tenthuoc:"Moxacin",donvi:"Viên",tdk:188112500,tdt:150490000,tck:37622500,ngaytk:"27/12/2018"},
{tenthuoc:"Mecefix(Cefixim)150mg",donvi:"Viên",tdk:187499996,tdt:149999997,tck:37499999,ngaytk:"27/12/2018"},
{tenthuoc:"Claritek125mg/5mlx50mL",donvi:"Chai/Lọ",tdk:187374970,tdt:149899976,tck:37474994,ngaytk:"27/12/2018"},
{tenthuoc:"HeparinBelmed",donvi:"Chai/Lọ",tdk:187000000,tdt:149600000,tck:37400000,ngaytk:"27/12/2018"},
{tenthuoc:"Clocardigel",donvi:"Viên",tdk:186592000,tdt:149273600,tck:37318400,ngaytk:"27/12/2018"},
{tenthuoc:"AdalatLATab20mg30's",donvi:"Viên",tdk:185863125,tdt:148690500,tck:37172625,ngaytk:"27/12/2018"},
{tenthuoc:"SystaneUltra5ml",donvi:"Chai/Lọ",tdk:184053188,tdt:147242550,tck:36810638,ngaytk:"27/12/2018"},
{tenthuoc:"Campto(Irinotecan)100mg",donvi:"Chai/Lọ",tdk:183961995,tdt:147169596,tck:36792399,ngaytk:"27/12/2018"},
{tenthuoc:"Thiênsứhộtâmđan",donvi:"Viên",tdk:183750000,tdt:147000000,tck:36750000,ngaytk:"27/12/2018"},
{tenthuoc:"Polhuminmix-2",donvi:"Ống",tdk:183750000,tdt:147000000,tck:36750000,ngaytk:"27/12/2018"},
{tenthuoc:"Polhuminmix-2",donvi:"Tuýp",tdk:183750000,tdt:147000000,tck:36750000,ngaytk:"27/12/2018"},
{tenthuoc:"I.V-globulinS2500mg",donvi:"Chai/Lọ",tdk:181875000,tdt:145500000,tck:36375000,ngaytk:"27/12/2018"},
{tenthuoc:"Heparin(Paringold)25000IU/5ml",donvi:"Chai/Lọ",tdk:180937500,tdt:144750000,tck:36187500,ngaytk:"27/12/2018"},
{tenthuoc:"Asgizole(Esomeprazol)40mg",donvi:"Chai/Lọ",tdk:180000004,tdt:144000003,tck:36000001,ngaytk:"27/12/2018"},
{tenthuoc:"Scanneuron",donvi:"Viên",tdk:180000000,tdt:144000000,tck:36000000,ngaytk:"27/12/2018"},
{tenthuoc:"AugmentinBDTab625mg14's",donvi:"Viên",tdk:179010160,tdt:143208128,tck:35802032,ngaytk:"27/12/2018"},
{tenthuoc:"OflovidOint",donvi:"Tuýp",tdk:175765000,tdt:140612000,tck:35153000,ngaytk:"27/12/2018"},
{tenthuoc:"Bibiso",donvi:"Viên",tdk:175675500,tdt:140540400,tck:35135100,ngaytk:"27/12/2018"},
{tenthuoc:"Taxotere(Docetaxel)20mg/1ml",donvi:"Chai/Lọ",tdk:175203000,tdt:140162400,tck:35040600,ngaytk:"27/12/2018"},
{tenthuoc:"Ebitac25mg",donvi:"Viên",tdk:175000000,tdt:140000000,tck:35000000,ngaytk:"27/12/2018"},
{tenthuoc:"Sastan-H",donvi:"Viên",tdk:174930000,tdt:139944000,tck:34986000,ngaytk:"27/12/2018"},
{tenthuoc:"VitaminB1+B6+B12",donvi:"Viên",tdk:174834000,tdt:139867200,tck:34966800,ngaytk:"27/12/2018"},
{tenthuoc:"PROPOFOL1%KABI",donvi:"Ống",tdk:174825000,tdt:139860000,tck:34965000,ngaytk:"27/12/2018"},
{tenthuoc:"Campto(Irinotecan)40mg",donvi:"Chai/Lọ",tdk:174753687,tdt:139802950,tck:34950737,ngaytk:"27/12/2018"},
{tenthuoc:"Bibiso",donvi:"Viên",tdk:173250000,tdt:138600000,tck:34650000,ngaytk:"27/12/2018"},
{tenthuoc:"Coveram5-5Tab5mg/5mg30's",donvi:"Viên",tdk:172961250,tdt:138369000,tck:34592250,ngaytk:"27/12/2018"},
{tenthuoc:"Ebitac",donvi:"Viên",tdk:172500000,tdt:138000000,tck:34500000,ngaytk:"27/12/2018"},
{tenthuoc:"Ebitac12.5",donvi:"Viên",tdk:172500000,tdt:138000000,tck:34500000,ngaytk:"27/12/2018"},
{tenthuoc:"Medoclav1g875mg+125mg",donvi:"Viên",tdk:170625000,tdt:136500000,tck:34125000,ngaytk:"27/12/2018"},
{tenthuoc:"Tartriakson1g",donvi:"Chai/Lọ",tdk:168875000,tdt:135100000,tck:33775000,ngaytk:"27/12/2018"},
{tenthuoc:"Levonor1mg/mlH10",donvi:"Ống",tdk:168000000,tdt:134400000,tck:33600000,ngaytk:"27/12/2018"},
{tenthuoc:"ScilinM30(30/70)",donvi:"Chai/Lọ",tdk:167375000,tdt:133900000,tck:33475000,ngaytk:"27/12/2018"},
{tenthuoc:"Travatan",donvi:"Ống",tdk:167148088,tdt:133718470,tck:33429618,ngaytk:"27/12/2018"},
{tenthuoc:"Humulin70/30",donvi:"Chai/Lọ",tdk:166875000,tdt:133500000,tck:33375000,ngaytk:"27/12/2018"},
{tenthuoc:"Lidocain40mg/2ml",donvi:"Ống",tdk:166812500,tdt:133450000,tck:33362500,ngaytk:"27/12/2018"},
{tenthuoc:"PanadolCaplet(Paracetamol)500mg",donvi:"Viên",tdk:165927036,tdt:132741629,tck:33185407,ngaytk:"27/12/2018"},
{tenthuoc:"3Bpluzs",donvi:"Viên",tdk:165325000,tdt:112200000,tck:53125000,ngaytk:"27/12/2018"},
{tenthuoc:"Levobact0,5%eyedrops",donvi:"Chai/Lọ",tdk:165000055,tdt:132000044,tck:33000011,ngaytk:"27/12/2018"},
{tenthuoc:"UnasynInj1500mg1's",donvi:"Ống",tdk:165000000,tdt:132000000,tck:33000000,ngaytk:"27/12/2018"},
{tenthuoc:"UnasynInj1500mg1's",donvi:"Chai/Lọ",tdk:165000000,tdt:132000000,tck:33000000,ngaytk:"27/12/2018"},
{tenthuoc:"Azicine",donvi:"Viên",tdk:164797500,tdt:131838000,tck:32959500,ngaytk:"27/12/2018"},
{tenthuoc:"Coveram5-5Tab5mg/5mg30's",donvi:"Viên",tdk:164560275,tdt:131648220,tck:32912055,ngaytk:"27/12/2018"},
{tenthuoc:"CoveramTab5mg/5mg30's",donvi:"Viên",tdk:164560275,tdt:131648220,tck:32912055,ngaytk:"27/12/2018"},
{tenthuoc:"Fudcime(Cefixim)200mg",donvi:"Viên",tdk:163291878,tdt:130633503,tck:32658376,ngaytk:"27/12/2018"},
{tenthuoc:"Medoclav1g",donvi:"Viên",tdk:162684900,tdt:130147920,tck:32536980,ngaytk:"27/12/2018"},
{tenthuoc:"Panadolcaplet",donvi:"Viên",tdk:162486000,tdt:129988800,tck:32497200,ngaytk:"27/12/2018"},
{tenthuoc:"ScilinM30",donvi:"Chai/Lọ",tdk:162000000,tdt:129600000,tck:32400000,ngaytk:"27/12/2018"},
{tenthuoc:"Scanneuron300,2mg",donvi:"Viên",tdk:161998988,tdt:129599190,tck:32399798,ngaytk:"27/12/2018"},
{tenthuoc:"Moxilen500mg",donvi:"Viên",tdk:160650000,tdt:71400000,tck:89250000,ngaytk:"27/12/2018"},
{tenthuoc:"Medopiren500mg",donvi:"Viên",tdk:159546750,tdt:127637400,tck:31909350,ngaytk:"27/12/2018"},
{tenthuoc:"Bloza(Losartan)50mg",donvi:"Viên",tdk:157500000,tdt:126000000,tck:31500000,ngaytk:"27/12/2018"},
{tenthuoc:"Cloramphenicol",donvi:"Ống",tdk:157031250,tdt:125625000,tck:31406250,ngaytk:"27/12/2018"},
{tenthuoc:"CoveramTab5mg/5mg30's",donvi:"Viên",tdk:156406385,tdt:125125108,tck:31281277,ngaytk:"27/12/2018"},
{tenthuoc:"Denxif",donvi:"Viên",tdk:156250000,tdt:125000000,tck:31250000,ngaytk:"27/12/2018"},
{tenthuoc:"Franilax",donvi:"Viên",tdk:156187475,tdt:124949980,tck:31237495,ngaytk:"27/12/2018"},
{tenthuoc:"TobrexEyeOintment",donvi:"Tuýp",tdk:154684375,tdt:123747500,tck:30936875,ngaytk:"27/12/2018"},
{tenthuoc:"Ceftriaxone-Panpharma",donvi:"Ống",tdk:154618750,tdt:123695000,tck:30923750,ngaytk:"27/12/2018"},
{tenthuoc:"ScilinM30400IU",donvi:"IU",tdk:154500003,tdt:123600002,tck:30900001,ngaytk:"27/12/2018"},
{tenthuoc:"Mixtard30FlexPen",donvi:"Bơm/Búttiêm",tdk:153998000,tdt:123198400,tck:30799600,ngaytk:"27/12/2018"},
{tenthuoc:"Moxilen500mg",donvi:"Viên",tdk:153168750,tdt:122535000,tck:30633750,ngaytk:"27/12/2018"},
{tenthuoc:"HoạthuyếtthôngmạchP/H",donvi:"Ống",tdk:152000000,tdt:76000000,tck:76000000,ngaytk:"27/12/2018"},
{tenthuoc:"Heparin(Paringold)25000IU/5ml",donvi:"Chai/Lọ",tdk:151250001,tdt:121000001,tck:30250000,ngaytk:"27/12/2018"},
{tenthuoc:"Mecefix-B.E",donvi:"Viên",tdk:150000000,tdt:120000000,tck:30000000,ngaytk:"27/12/2018"},
{tenthuoc:"Mecefix-B.E",donvi:"Viên",tdk:150000000,tdt:120000000,tck:30000000,ngaytk:"27/12/2018"},
{tenthuoc:"AugmentinBDTab625mg14's",donvi:"Viên",tdk:149140320,tdt:119312256,tck:29828064,ngaytk:"27/12/2018"},
{tenthuoc:"NifedipinHasan20Retard",donvi:"Viên",tdk:147810469,tdt:118248375,tck:29562094,ngaytk:"27/12/2018"},
{tenthuoc:"ExforgeHCT10/160/12.5mg",donvi:"Viên",tdk:147662569,tdt:118130055,tck:29532514,ngaytk:"27/12/2018"},
{tenthuoc:"Enaminal(elalapril10mg)",donvi:"Viên",tdk:146985300,tdt:117588240,tck:29397060,ngaytk:"27/12/2018"},
{tenthuoc:"Enamigal5mg",donvi:"Viên",tdk:146250000,tdt:117000000,tck:29250000,ngaytk:"27/12/2018"},
{tenthuoc:"Tobrin0,3%/5ml",donvi:"Chai/Lọ",tdk:146250000,tdt:117000000,tck:29250000,ngaytk:"27/12/2018"},
{tenthuoc:"Enamigal(enalapril5mg)",donvi:"Viên",tdk:146238750,tdt:116991000,tck:29247750,ngaytk:"27/12/2018"},
{tenthuoc:"Enap(Enalapril)5mg",donvi:"Viên",tdk:145802580,tdt:116642064,tck:29160516,ngaytk:"27/12/2018"},
{tenthuoc:"Medopiren500mg",donvi:"Viên",tdk:145562500,tdt:116450000,tck:29112500,ngaytk:"27/12/2018"},
{tenthuoc:"Osaki",donvi:"Chai/Lọ",tdk:144999999,tdt:116000000,tck:29000000,ngaytk:"27/12/2018"},
{tenthuoc:"Bloza",donvi:"Viên",tdk:143005275,tdt:114404220,tck:28601055,ngaytk:"27/12/2018"},
{tenthuoc:"Bloza",donvi:"Viên",tdk:143005275,tdt:114404220,tck:28601055,ngaytk:"27/12/2018"},
{tenthuoc:"Novomix30Flexpen",donvi:"Bơm/Búttiêm",tdk:142406250,tdt:113925000,tck:28481250,ngaytk:"27/12/2018"}];

var tkTDT = [{tenthuoc: 'Aristin - C',tdt:25623000000},
{tenthuoc: 'Tavanic',tdt:15034210000},
{tenthuoc: 'Vimotram',tdt:13114600270},
{tenthuoc: 'Xalvobin 500mg film coated-tablet',tdt:5940900000},
{tenthuoc: 'Paxus PM 100 mg',tdt:3913440000},
{tenthuoc: 'Duoplavin 75mg+100mg',tdt:2915503370},
{tenthuoc: 'Meronem Inj 1g 10 s',tdt:2893652800},
{tenthuoc: 'Rifaxon',tdt:2825900000},
{tenthuoc: 'Taxotere 80mg/1ml',tdt:2803247500},
{tenthuoc: 'Concor 5mg',tdt:2629168404},
{tenthuoc: 'Scilin M30 (30/70)',tdt:2484867000},
{tenthuoc: 'REDITUX',tdt:2420000000},
{tenthuoc: 'Ebitac 25',tdt:2397750000},
{tenthuoc: 'Scilin N',tdt:2351810000},
{tenthuoc: 'Taxotere 20mg/1ml',tdt:2242598400},
{tenthuoc: 'Heparin 25000 UI/5ml',tdt:2214450000},
{tenthuoc: 'Rocephin 1g I.V.',tdt:2210846400},
{tenthuoc: 'Coveram 5-5 Tab 5mg/5mg 30 s',tdt:2195718360},
{tenthuoc: 'Unasyn Inj 1500mg 1 s',tdt:2182000000},
{tenthuoc: 'Paclitaxelum Actavis',tdt:2167500000}];

var xuatTrongKy = [{tenthuoc: "Aristin - C",t1:65338,t2:0,t3:0,t4:0,t5:0,t6:0,t7:0,t8:0,t9:0,t10:0,t11:0,t12:0},
{tenthuoc: "Tavanic",t1:26774,t2:345,t3:79884,t4:72242,t5:21049,t6:68726,t7:23861,t8:9000,t9:40161,t10:50374,t11:31770,t12:60585},
{tenthuoc: "Xalvobin 500mg film coated-tablet",t1:29357,t2:0,t3:5397,t4:0,t5:24278,t6:4702,t7:0,t8:52357,t9:31464,t10:0,t11:39346,t12:0},
{tenthuoc: "Paxus PM 100 mg",t1:55346,t2:10183,t3:50676,t4:0,t5:41289,t6:12018,t7:72882,t8:0,t9:12067,t10:71862,t11:33233,t12:49174},
{tenthuoc: "Duoplavin 75mg+100mg",t1:37456,t2:51737,t3:11375,t4:42466,t5:57971,t6:33813,t7:15079,t8:50703,t9:62814,t10:29327,t11:64111,t12:51307},
{tenthuoc: "Taxotere 80mg/1ml",t1:3551,t2:46727,t3:6102,t4:32146,t5:58601,t6:62098,t7:13426,t8:19024,t9:4041,t10:45861,t11:21642,t12:23691},
{tenthuoc: "Vimotram",t1:75429,t2:26017,t3:0,t4:66855,t5:30994,t6:27668,t7:61407,t8:76376,t9:67187,t10:33123,t11:0,t12:52170},
{tenthuoc: "REDITUX",t1:74776,t2:64221,t3:45652,t4:43459,t5:7270,t6:37411,t7:24100,t8:35738,t9:56758,t10:30026,t11:59643,t12:32559},
{tenthuoc: "Vimotram",t1:36491,t2:0,t3:26343,t4:30540,t5:68913,t6:0,t7:59939,t8:51478,t9:51991,t10:20973,t11:72972,t12:0},
{tenthuoc: "Taxotere 20mg/1ml",t1:1037,t2:43913,t3:0,t4:15884,t5:68779,t6:18731,t7:13145,t8:55901,t9:38644,t10:5321,t11:5044,t12:20735},
{tenthuoc: "Vimotram",t1:37144,t2:26506,t3:10060,t4:436,t5:76309,t6:56088,t7:73180,t8:73027,t9:73785,t10:1407,t11:29516,t12:70877},
{tenthuoc: "Paclitaxelum Actavis",t1:16767,t2:60957,t3:69793,t4:45264,t5:26976,t6:33257,t7:47445,t8:66757,t9:51602,t10:2183,t11:35744,t12:42414},
{tenthuoc: "Concor 5mg",t1:39309,t2:52414,t3:76436,t4:54786,t5:62362,t6:25747,t7:77007,t8:37839,t9:77200,t10:64850,t11:29956,t12:34307},
{tenthuoc: "PAXUS PM 30MG",t1:66269,t2:0,t3:46111,t4:73351,t5:74789,t6:75645,t7:4737,t8:62145,t9:72531,t10:50280,t11:0,t12:34886},
{tenthuoc: "Heparin 25000 UI/5ml",t1:51424,t2:46682,t3:48969,t4:4422,t5:5265,t6:60882,t7:43195,t8:36724,t9:28022,t10:50851,t11:51475,t12:2639},
{tenthuoc: "Meronem 1g",t1:70252,t2:20158,t3:75495,t4:23254,t5:11029,t6:0,t7:56349,t8:2470,t9:71079,t10:30168,t11:68278,t12:41757},
{tenthuoc: "Rifaxon",t1:56376,t2:18034,t3:38284,t4:16360,t5:18638,t6:63980,t7:71429,t8:20026,t9:78521,t10:5428,t11:14980,t12:68155},
{tenthuoc: "Vimotram",t1:76903,t2:37969,t3:0,t4:47005,t5:0,t6:67505,t7:69973,t8:69263,t9:109,t10:64820,t11:79075,t12:39627},
{tenthuoc: "Paxus PM 30 mg",t1:911,t2:59324,t3:48840,t4:25079,t5:72982,t6:35396,t7:66358,t8:0,t9:52365,t10:26031,t11:33062,t12:16860},
{tenthuoc: "Meronem Inj 1g 10's",t1:76829,t2:3296,t3:22016,t4:46102,t5:7847,t6:13627,t7:24058,t8:49789,t9:69075,t10:76182,t11:33158,t12:0},
{tenthuoc: "Meronem Inj 1g 10's",t1:58806,t2:66441,t3:0,t4:46325,t5:59944,t6:69977,t7:39034,t8:53668,t9:77741,t10:26501,t11:15537,t12:11509},
{tenthuoc: "Unasyn INJ",t1:75485,t2:44921,t3:38719,t4:67812,t5:61049,t6:40912,t7:7104,t8:67008,t9:14362,t10:16083,t11:55724,t12:6466},
{tenthuoc: "Rocephin",t1:63402,t2:3117,t3:39616,t4:59004,t5:66257,t6:14733,t7:1326,t8:66188,t9:38632,t10:53404,t11:17652,t12:21039},
{tenthuoc: "Rifaxon",t1:5564,t2:50307,t3:62388,t4:48869,t5:59572,t6:34873,t7:30475,t8:5936,t9:43595,t10:10819,t11:72180,t12:30630},
{tenthuoc: "Vimotram",t1:59159,t2:2132,t3:41427,t4:0,t5:1483,t6:26709,t7:67714,t8:0,t9:47050,t10:65020,t11:0,t12:28719},
{tenthuoc: "Scilin M30 (30/70)",t1:11997,t2:36352,t3:70770,t4:33616,t5:75986,t6:55469,t7:78873,t8:12649,t9:38871,t10:42660,t11:36365,t12:20969},
{tenthuoc: "Twynsta 40+5mg",t1:25287,t2:74329,t3:0,t4:76100,t5:51449,t6:65148,t7:76259,t8:50944,t9:37404,t10:40909,t11:7963,t12:17048},
{tenthuoc: "Tiepanem 1g",t1:15168,t2:69783,t3:36071,t4:34965,t5:1646,t6:27823,t7:43496,t8:73890,t9:10865,t10:17522,t11:21832,t12:22886},
{tenthuoc: "Paxus PM (Paclitaxel) 100mg",t1:29239,t2:4345,t3:69923,t4:64305,t5:68717,t6:73346,t7:69200,t8:21998,t9:42716,t10:24971,t11:15493,t12:39271},
{tenthuoc: "Plavix 75mg",t1:62699,t2:37795,t3:47780,t4:13675,t5:68615,t6:62179,t7:39456,t8:70921,t9:33218,t10:567,t11:63940,t12:43621},
{tenthuoc: "Paxus PM",t1:69585,t2:54951,t3:53265,t4:16197,t5:52383,t6:54109,t7:23397,t8:59691,t9:55029,t10:79055,t11:14578,t12:33483},
{tenthuoc: "Klamentin 1g",t1:50005,t2:66433,t3:60761,t4:26078,t5:68001,t6:39333,t7:1102,t8:19478,t9:43284,t10:45536,t11:61071,t12:40556},
{tenthuoc: "Ebitac 25",t1:29673,t2:68833,t3:52266,t4:60944,t5:0,t6:75562,t7:21010,t8:70759,t9:64720,t10:14741,t11:59671,t12:520},
{tenthuoc: "Scilin N",t1:51257,t2:33335,t3:36796,t4:28985,t5:28039,t6:43973,t7:52616,t8:55626,t9:38514,t10:44752,t11:47485,t12:52933},
{tenthuoc: "Scilin N",t1:70742,t2:55067,t3:35616,t4:23034,t5:70910,t6:14958,t7:7450,t8:79606,t9:4140,t10:33816,t11:7998,t12:37362},
{tenthuoc: "Levogol",t1:5403,t2:37975,t3:38163,t4:18511,t5:43366,t6:28788,t7:242,t8:46456,t9:72491,t10:28721,t11:19383,t12:47718},
{tenthuoc: "Proxacin 1%",t1:78057,t2:15907,t3:8568,t4:68055,t5:18610,t6:0,t7:61754,t8:48252,t9:56519,t10:42809,t11:55858,t12:72449},
{tenthuoc: "Anzatax",t1:52329,t2:36348,t3:13757,t4:75362,t5:21426,t6:66649,t7:55196,t8:78025,t9:52915,t10:11300,t11:46744,t12:50531},
{tenthuoc: "Lovenox 0,6ml,6000UI",t1:76122,t2:62357,t3:30121,t4:25932,t5:32478,t6:42991,t7:76161,t8:2625,t9:78817,t10:0,t11:13503,t12:8001},
{tenthuoc: "Tadocel 80mg/2ml",t1:79768,t2:74747,t3:22482,t4:13850,t5:32746,t6:31427,t7:69299,t8:54972,t9:17874,t10:9794,t11:76136,t12:32001},
{tenthuoc: "Unasyn Inj 1500mg 1's Ampicillin 1g, Sulba",t1:72300,t2:46956,t3:75098,t4:68410,t5:37407,t6:0,t7:47382,t8:8983,t9:10890,t10:0,t11:34780,t12:0},
{tenthuoc: "Aristin- C",t1:7984,t2:65373,t3:43605,t4:32984,t5:27888,t6:22155,t7:42554,t8:22460,t9:10710,t10:13418,t11:19362,t12:3581},
{tenthuoc: "Ceftriaxone  Panpharma [Chỉ dành cho kho Nhi]",t1:50721,t2:34344,t3:60176,t4:14728,t5:19896,t6:71292,t7:72394,t8:79731,t9:33613,t10:52939,t11:28544,t12:51088},
{tenthuoc: "Xeloda",t1:48149,t2:30845,t3:33328,t4:20066,t5:51060,t6:6540,t7:2480,t8:56648,t9:6191,t10:77936,t11:17184,t12:30670},
{tenthuoc: "Ceftriaxone panpharma",t1:38059,t2:14024,t3:71194,t4:233,t5:9797,t6:30889,t7:33995,t8:46035,t9:40592,t10:32900,t11:21177,t12:36626},
{tenthuoc: "Cerecaps",t1:55548,t2:36095,t3:33616,t4:37635,t5:19054,t6:60498,t7:69787,t8:55528,t9:78194,t10:13603,t11:36069,t12:39695},
{tenthuoc: "Bupivacaine 20mg 0.5% 5 mg/ml 0,5%/4ml",t1:48668,t2:66830,t3:71266,t4:37381,t5:11994,t6:0,t7:37192,t8:74062,t9:45942,t10:9278,t11:63160,t12:77688},
{tenthuoc: "Scilin N 400UI/10ml",t1:28975,t2:57537,t3:34433,t4:64641,t5:16017,t6:15717,t7:70517,t8:75842,t9:11431,t10:0,t11:75668,t12:72107},
{tenthuoc: "Vimotram",t1:52170,t2:65727,t3:5502,t4:33267,t5:9947,t6:10925,t7:27269,t8:0,t9:54191,t10:68273,t11:25867,t12:57618},
{tenthuoc: "Heparin (natri) 5ml.5000UI/ml",t1:1459,t2:29863,t3:15841,t4:12941,t5:735,t6:65434,t7:32030,t8:11265,t9:76279,t10:23120,t11:72325,t12:0},
{tenthuoc: "Infartan 75",t1:46944,t2:77954,t3:9344,t4:75557,t5:11122,t6:44382,t7:26317,t8:44981,t9:43013,t10:45677,t11:51098,t12:27361},
{tenthuoc: "Meronem Inj 500mg 10's",t1:1204,t2:78510,t3:32398,t4:78256,t5:62435,t6:56676,t7:365,t8:75162,t9:66786,t10:53639,t11:67212,t12:47448},
{tenthuoc: "Anzatax",t1:24780,t2:21534,t3:2722,t4:31691,t5:58337,t6:60734,t7:66891,t8:71218,t9:9667,t10:37934,t11:72522,t12:77579},
{tenthuoc: "Merugold I.V 1g",t1:1847,t2:24489,t3:59267,t4:71181,t5:26072,t6:50028,t7:51829,t8:38227,t9:63935,t10:5768,t11:27391,t12:17964},
{tenthuoc: "Amlor 5mg",t1:79780,t2:68915,t3:67801,t4:33559,t5:33617,t6:22632,t7:65630,t8:18363,t9:55985,t10:72452,t11:71659,t12:69609},
{tenthuoc: "Meiunem 0,5g",t1:77033,t2:63727,t3:31439,t4:8616,t5:41423,t6:0,t7:21394,t8:17965,t9:43080,t10:10603,t11:77164,t12:36848},
{tenthuoc: "Tadocel 20mg/0,5ml",t1:54060,t2:386,t3:30997,t4:20109,t5:17643,t6:158,t7:76693,t8:6000,t9:45701,t10:42723,t11:58378,t12:52938},
{tenthuoc: "DBL Irinotecan Injection 100mg/5ml",t1:8898,t2:57998,t3:62507,t4:33417,t5:11249,t6:33633,t7:1433,t8:42778,t9:21566,t10:30033,t11:6592,t12:40710},
{tenthuoc: "Klamentin",t1:49225,t2:5364,t3:2402,t4:21344,t5:39583,t6:14358,t7:77199,t8:42307,t9:37328,t10:48338,t11:23811,t12:32827},
{tenthuoc: "Huma  Globin 2,5g/50ml",t1:70866,t2:711,t3:44886,t4:65462,t5:17226,t6:27901,t7:47367,t8:38832,t9:481,t10:5564,t11:33136,t12:46528},
{tenthuoc: "Tadocel 80mg/2ml",t1:54846,t2:43116,t3:48933,t4:49679,t5:76604,t6:7054,t7:34487,t8:29925,t9:7222,t10:43292,t11:34868,t12:51817},
{tenthuoc: "Rocephin 1g",t1:36029,t2:25899,t3:63780,t4:76078,t5:75633,t6:42384,t7:74445,t8:34404,t9:14100,t10:64624,t11:52744,t12:22373},
{tenthuoc: "Fudcime 200mg",t1:49140,t2:66301,t3:52026,t4:10951,t5:62146,t6:152,t7:72542,t8:30008,t9:26911,t10:36064,t11:47558,t12:11597},
{tenthuoc: "Tavanic",t1:5133,t2:30483,t3:15383,t4:78394,t5:41868,t6:66940,t7:73575,t8:28550,t9:74955,t10:40573,t11:10339,t12:13996},
{tenthuoc: "Coveram (5+5)mg",t1:75922,t2:53622,t3:72870,t4:51790,t5:48638,t6:44717,t7:44290,t8:66853,t9:36474,t10:22058,t11:48350,t12:31295},
{tenthuoc: "Coveram 5mg/5mg",t1:26873,t2:1539,t3:26501,t4:34480,t5:42923,t6:28424,t7:29819,t8:8524,t9:2606,t10:15180,t11:45227,t12:6894},
{tenthuoc: "Meronem",t1:47696,t2:40792,t3:12220,t4:2514,t5:71839,t6:73049,t7:69215,t8:40042,t9:73159,t10:36439,t11:39231,t12:66050},
{tenthuoc: "Vigamox",t1:58369,t2:45990,t3:13869,t4:9613,t5:48861,t6:37404,t7:12918,t8:9089,t9:41485,t10:9285,t11:37187,t12:362},
{tenthuoc: "Cozaar 50mg",t1:53379,t2:65562,t3:24253,t4:45112,t5:46722,t6:54920,t7:50913,t8:38327,t9:12269,t10:73107,t11:471,t12:37133},
{tenthuoc: "Tiepanem",t1:9308,t2:61092,t3:79181,t4:15578,t5:73425,t6:4281,t7:3706,t8:23337,t9:32415,t10:51567,t11:78510,t12:12350},
{tenthuoc: "Tiepanem 1g",t1:69999,t2:52902,t3:45757,t4:44101,t5:2909,t6:56765,t7:3487,t8:827,t9:63540,t10:65840,t11:37996,t12:11294},
{tenthuoc: "Vimotram",t1:22996,t2:56042,t3:11005,t4:50188,t5:13492,t6:43633,t7:24207,t8:61557,t9:64624,t10:30554,t11:5177,t12:71954},
{tenthuoc: "I.V.-Globulin SN inj.",t1:52242,t2:53751,t3:1645,t4:35722,t5:28136,t6:65976,t7:20475,t8:8087,t9:74771,t10:76294,t11:12556,t12:8812},
{tenthuoc: "Novomix 30 Flexpen",t1:16694,t2:34410,t3:25112,t4:68013,t5:1861,t6:25639,t7:27869,t8:6327,t9:55070,t10:36617,t11:75494,t12:44411},
{tenthuoc: "Vimotram",t1:78813,t2:67301,t3:78521,t4:57395,t5:12498,t6:6699,t7:9905,t8:26770,t9:57801,t10:46349,t11:20072,t12:53560},
{tenthuoc: "Proxacin 1%",t1:51504,t2:25482,t3:15157,t4:63744,t5:62752,t6:47731,t7:50993,t8:53930,t9:25354,t10:56852,t11:44215,t12:54390},
{tenthuoc: "Moxilen (amoxicillin) 500mg",t1:69097,t2:27113,t3:37131,t4:52871,t5:54574,t6:58900,t7:59464,t8:55219,t9:35483,t10:63480,t11:78196,t12:70876},
{tenthuoc: "Ebitac 12.5",t1:79407,t2:17029,t3:53043,t4:70130,t5:39453,t6:72882,t7:37618,t8:59824,t9:5979,t10:37964,t11:11550,t12:32867},
{tenthuoc: "Ebitac 25",t1:61503,t2:29421,t3:42193,t4:60472,t5:17727,t6:20007,t7:20990,t8:35153,t9:54330,t10:29096,t11:57002,t12:31089},
{tenthuoc: "Ceftriaxone Panpharma 1g",t1:16482,t2:10988,t3:63181,t4:41783,t5:44166,t6:14145,t7:58949,t8:77329,t9:56131,t10:14772,t11:35479,t12:18730},
{tenthuoc: "PROPOFOL 1% KABI",t1:33556,t2:44605,t3:77770,t4:57006,t5:36345,t6:18066,t7:67142,t8:26022,t9:24693,t10:22242,t11:47517,t12:18421},
{tenthuoc: "Vismed",t1:74309,t2:79920,t3:41253,t4:62723,t5:43616,t6:14521,t7:38967,t8:26733,t9:70882,t10:71765,t11:37809,t12:21036},
{tenthuoc: "Medphadion Drops 100mg/5ml",t1:7632,t2:67007,t3:37931,t4:68099,t5:46742,t6:15069,t7:60671,t8:18073,t9:28717,t10:63867,t11:12638,t12:6557},
{tenthuoc: "Coveram 5-5 Tab 5mg/5mg 30's",t1:60921,t2:15789,t3:45088,t4:63060,t5:57938,t6:63476,t7:34109,t8:24764,t9:9637,t10:58275,t11:14265,t12:31982},
{tenthuoc: "Meiunem 0,5g",t1:77560,t2:29926,t3:27706,t4:52369,t5:72328,t6:6629,t7:79082,t8:105,t9:16101,t10:77800,t11:21740,t12:57952},
{tenthuoc: "Vimotram",t1:79151,t2:27444,t3:76700,t4:2390,t5:40026,t6:11787,t7:66606,t8:25542,t9:62017,t10:16524,t11:1402,t12:79990},
{tenthuoc: "Diprivan Inj 20ml 5's 10mg/ml (1%) - 20ml",t1:51440,t2:36176,t3:74377,t4:2048,t5:62723,t6:42122,t7:29553,t8:41321,t9:2857,t10:39714,t11:71273,t12:56280},
{tenthuoc: "DBL Irinotecan Injection 40mg/2ml",t1:6942,t2:66184,t3:74866,t4:38943,t5:20992,t6:10941,t7:55355,t8:19448,t9:6019,t10:35019,t11:40545,t12:15144},
{tenthuoc: "Proxacin 200mg",t1:49243,t2:59232,t3:64429,t4:58663,t5:72637,t6:4411,t7:9019,t8:137,t9:21056,t10:8967,t11:31545,t12:55747},
{tenthuoc: "Prospan cough syrup",t1:43692,t2:60308,t3:47688,t4:20172,t5:18977,t6:19513,t7:4113,t8:927,t9:1505,t10:58892,t11:5075,t12:69077},
{tenthuoc: "Ceftriaxone panpharma",t1:55241,t2:53229,t3:14233,t4:59754,t5:67945,t6:75891,t7:48776,t8:16729,t9:49303,t10:50267,t11:57024,t12:32135},
{tenthuoc: "Vismed 0.18",t1:59936,t2:33369,t3:78342,t4:20295,t5:54132,t6:28033,t7:28757,t8:53030,t9:4716,t10:14949,t11:59757,t12:28206},
{tenthuoc: "Rocephin 1g I.V.",t1:28883,t2:57810,t3:30360,t4:5455,t5:48775,t6:6228,t7:55772,t8:55773,t9:0,t10:20940,t11:21302,t12:48210},
{tenthuoc: "Bupivacaine Aguettant 5mg/ml x 20ml 0,5%/20ml",t1:61717,t2:58824,t3:75113,t4:46758,t5:13366,t6:58037,t7:50886,t8:36372,t9:62489,t10:78929,t11:39029,t12:17919},
{tenthuoc: "Vimotram",t1:75838,t2:33261,t3:44388,t4:25456,t5:18201,t6:18710,t7:47755,t8:9451,t9:49033,t10:9665,t11:79305,t12:29426},
{tenthuoc: "Meiunem 0,5g",t1:2377,t2:56466,t3:0,t4:10046,t5:60175,t6:28590,t7:52084,t8:11979,t9:76666,t10:77654,t11:54380,t12:4948},
{tenthuoc: "Polhumin mix-2",t1:8125,t2:62315,t3:50279,t4:67793,t5:11210,t6:67868,t7:28453,t8:23299,t9:57355,t10:55624,t11:48731,t12:40832},
{tenthuoc: "Levoflex",t1:54352,t2:10781,t3:67054,t4:58678,t5:78211,t6:45553,t7:29239,t8:28049,t9:73070,t10:45189,t11:63919,t12:10807},
{tenthuoc: "Paxus PM",t1:47431,t2:55298,t3:16339,t4:2458,t5:66887,t6:23397,t7:71082,t8:14185,t9:13310,t10:33211,t11:76859,t12:11832},
{tenthuoc: "Levoflex",t1:1958,t2:45959,t3:18623,t4:75273,t5:58512,t6:75941,t7:68239,t8:7130,t9:66702,t10:37229,t11:65766,t12:972},
{tenthuoc: "Twynsta",t1:56254,t2:28270,t3:65698,t4:21298,t5:54274,t6:4733,t7:34539,t8:11070,t9:33644,t10:8003,t11:70457,t12:15139},
{tenthuoc: "Unasyl",t1:76895,t2:17079,t3:60592,t4:13645,t5:50587,t6:23421,t7:2091,t8:35996,t9:19553,t10:63660,t11:35646,t12:22218},
{tenthuoc: "Aldacton tab",t1:63673,t2:7469,t3:36320,t4:46328,t5:18987,t6:5735,t7:30189,t8:25578,t9:7266,t10:16915,t11:42392,t12:24780},
{tenthuoc: "Adalat LA 30mg",t1:37438,t2:63139,t3:0,t4:62458,t5:20363,t6:65289,t7:47547,t8:31215,t9:72165,t10:37933,t11:61350,t12:51673},
{tenthuoc: "Progut",t1:47643,t2:8685,t3:37500,t4:34168,t5:74508,t6:71470,t7:11821,t8:11189,t9:21707,t10:64594,t11:18576,t12:47934},
{tenthuoc: "Concor 5mg",t1:63041,t2:51887,t3:3441,t4:61978,t5:33267,t6:18777,t7:53318,t8:27150,t9:41590,t10:35447,t11:11554,t12:27443},
{tenthuoc: "Exforge 5mg+80mg",t1:5918,t2:56964,t3:867,t4:71898,t5:64801,t6:51862,t7:48231,t8:35051,t9:35147,t10:0,t11:10874,t12:65698},
{tenthuoc: "Meronem 500mg",t1:78727,t2:1779,t3:31781,t4:45124,t5:59118,t6:5028,t7:76120,t8:3384,t9:49373,t10:69926,t11:30923,t12:14362},
{tenthuoc: "Indocollyre",t1:21070,t2:40286,t3:21113,t4:9631,t5:70796,t6:52196,t7:34745,t8:74002,t9:65812,t10:14837,t11:41141,t12:24444},
{tenthuoc: "Sanbeclaneksi (Amoxicillin+acid clavulanic) 1.2g",t1:42671,t2:735,t3:45487,t4:68786,t5:42884,t6:51111,t7:33459,t8:52665,t9:1658,t10:67605,t11:60355,t12:67115},
{tenthuoc: "Nexium",t1:63347,t2:38612,t3:32957,t4:44682,t5:44476,t6:42117,t7:75526,t8:61157,t9:17881,t10:33660,t11:61905,t12:29201},
{tenthuoc: "Rocephin 1g I.V.",t1:70991,t2:9230,t3:62588,t4:37914,t5:48711,t6:43144,t7:10579,t8:28066,t9:57590,t10:51836,t11:3408,t12:8926},
{tenthuoc: "Taxotere (Docetaxel) 80mg/4ml",t1:56677,t2:28357,t3:57878,t4:46211,t5:2623,t6:46135,t7:41438,t8:71095,t9:75115,t10:53015,t11:24546,t12:7709},
{tenthuoc: "Xalvobin (capecitabin) 500mg",t1:62361,t2:24822,t3:25614,t4:32895,t5:12430,t6:56669,t7:27883,t8:27214,t9:12662,t10:14027,t11:27487,t12:67871},
{tenthuoc: "Adalat LA Tab 20mg",t1:49352,t2:47749,t3:50021,t4:69877,t5:76953,t6:41696,t7:58994,t8:44794,t9:64462,t10:37590,t11:35585,t12:54234},
{tenthuoc: "Concor",t1:38590,t2:77065,t3:41030,t4:76885,t5:5339,t6:77883,t7:77185,t8:32512,t9:51473,t10:4986,t11:63375,t12:2360},
{tenthuoc: "Infartan (Clopidogrel) 75mg",t1:67597,t2:53257,t3:22845,t4:34893,t5:24802,t6:4707,t7:50774,t8:24223,t9:9538,t10:77952,t11:64935,t12:44809},
{tenthuoc: "Gardenal 100mg",t1:61847,t2:37145,t3:56138,t4:31955,t5:1243,t6:61080,t7:46448,t8:62853,t9:7941,t10:43814,t11:22859,t12:77406},
{tenthuoc: "Sanlein 0,1% x 5ml",t1:55484,t2:30072,t3:45015,t4:24962,t5:62391,t6:71847,t7:3226,t8:13942,t9:51763,t10:640,t11:54047,t12:42474},
{tenthuoc: "Unasyn Inj 1500mg 1's",t1:62302,t2:28286,t3:54854,t4:28985,t5:58903,t6:33073,t7:47644,t8:34506,t9:8808,t10:15778,t11:79261,t12:57456},
{tenthuoc: "Aristin-C (Ciprofloxacin 200mg/100ml)",t1:28286,t2:55937,t3:33686,t4:68962,t5:73685,t6:77888,t7:44382,t8:77603,t9:39025,t10:8798,t11:59137,t12:44026},
{tenthuoc: "Resines (Amlodipin) 5mg",t1:50659,t2:22970,t3:55876,t4:66495,t5:65642,t6:45777,t7:17725,t8:37273,t9:33085,t10:62441,t11:13668,t12:0},
{tenthuoc: "Kedrigamma",t1:71068,t2:26561,t3:14818,t4:25748,t5:52569,t6:66702,t7:5128,t8:6401,t9:6166,t10:42056,t11:56546,t12:10298},
{tenthuoc: "Ebitac 25",t1:32757,t2:60585,t3:19019,t4:71777,t5:77006,t6:56376,t7:5349,t8:19140,t9:45235,t10:12982,t11:60351,t12:42348},
{tenthuoc: "Tartriakson  1g",t1:66834,t2:22806,t3:58197,t4:42050,t5:46370,t6:16119,t7:41920,t8:2200,t9:73960,t10:19714,t11:33704,t12:57193},
{tenthuoc: "Ceftriaxon",t1:56853,t2:56613,t3:32799,t4:63785,t5:33744,t6:42986,t7:40651,t8:10912,t9:3918,t10:60041,t11:30708,t12:65292},
{tenthuoc: "Prospan 70ml",t1:37797,t2:26968,t3:57107,t4:40749,t5:33744,t6:68334,t7:19128,t8:11303,t9:39080,t10:18310,t11:40758,t12:41601},
{tenthuoc: "Tercef 1g",t1:43086,t2:30936,t3:58002,t4:55010,t5:29811,t6:24206,t7:78999,t8:72385,t9:65237,t10:78030,t11:79758,t12:5267},
{tenthuoc: "Sanbeclaneksi (Amoxicillin+acid clavulanic) 1.2g",t1:1754,t2:38289,t3:25248,t4:45653,t5:57494,t6:53408,t7:1160,t8:61541,t9:3472,t10:32073,t11:56927,t12:46198},
{tenthuoc: "Novomix 30/70 Flexpen 100UI 3ml",t1:1761,t2:37552,t3:51806,t4:52041,t5:6338,t6:43875,t7:10143,t8:30798,t9:1504,t10:39084,t11:37891,t12:11285},
{tenthuoc: "Rocephin 1g I.V.",t1:6753,t2:9412,t3:13006,t4:71162,t5:78856,t6:25947,t7:42448,t8:19094,t9:68208,t10:77339,t11:5546,t12:73359},
{tenthuoc: "Coveram 5-5 Tab 5mg/5mg 30's",t1:29500,t2:8371,t3:52248,t4:47860,t5:40405,t6:22808,t7:46721,t8:0,t9:41557,t10:78435,t11:39477,t12:28945},
{tenthuoc: "LOVENOX 40mg Inj B/ 2 syringes x0,4ml",t1:53255,t2:22580,t3:75133,t4:5634,t5:79477,t6:16922,t7:17721,t8:33806,t9:46244,t10:21604,t11:50230,t12:39178},
{tenthuoc: "LOVENOX 40mg Inj B/ 2 syringes x0,4ml",t1:53381,t2:0,t3:38916,t4:14498,t5:0,t6:25223,t7:10863,t8:5268,t9:6983,t10:64120,t11:74892,t12:18904},
{tenthuoc: "Remeclar 500",t1:48222,t2:31133,t3:156,t4:21069,t5:20162,t6:4361,t7:49081,t8:31902,t9:4364,t10:36201,t11:39711,t12:22098},
{tenthuoc: "Remeclar 500",t1:64314,t2:46120,t3:75232,t4:38885,t5:27871,t6:78180,t7:22661,t8:75233,t9:67032,t10:10082,t11:47545,t12:34645},
{tenthuoc: "Remeclar 500mg",t1:74343,t2:21763,t3:60208,t4:7169,t5:54137,t6:211,t7:18847,t8:56709,t9:64934,t10:33050,t11:26896,t12:2446},
{tenthuoc: "Mixtard 30 Flexpen 100IU/ml x 3ml",t1:15643,t2:4636,t3:75823,t4:65502,t5:1878,t6:63231,t7:42870,t8:20588,t9:72378,t10:65411,t11:20478,t12:57653},
{tenthuoc: "Bisoprolol 2,5mg",t1:65301,t2:67768,t3:8103,t4:4784,t5:68951,t6:26080,t7:12657,t8:71500,t9:51077,t10:1248,t11:6874,t12:249},
{tenthuoc: "Paclispec 30",t1:51715,t2:19946,t3:33039,t4:47278,t5:70454,t6:36675,t7:73507,t8:48287,t9:13635,t10:57790,t11:7262,t12:49950},
{tenthuoc: "Hyđan",t1:46789,t2:43391,t3:5943,t4:48714,t5:19733,t6:6707,t7:77023,t8:45230,t9:79936,t10:54571,t11:61975,t12:22640},
{tenthuoc: "Ebitac (Enalapril 10mg+Hydroclorothiazid 25mg)",t1:57457,t2:61788,t3:37408,t4:68438,t5:22398,t6:40560,t7:43538,t8:49203,t9:56892,t10:287,t11:41926,t12:46218},
{tenthuoc: "Polhumin mix-2",t1:65575,t2:2760,t3:67465,t4:47668,t5:32998,t6:47667,t7:74915,t8:16453,t9:13529,t10:57676,t11:75744,t12:28949},
{tenthuoc: "Boganic",t1:23212,t2:74238,t3:23286,t4:42425,t5:195,t6:66314,t7:73090,t8:30448,t9:18416,t10:55503,t11:40538,t12:30839},
{tenthuoc: "Insulin H  Mix 100UI",t1:22516,t2:34615,t3:5592,t4:13813,t5:27434,t6:1476,t7:47112,t8:4046,t9:61031,t10:18213,t11:71958,t12:67414},
{tenthuoc: "Nexium mups",t1:37168,t2:28250,t3:18171,t4:62139,t5:44095,t6:62288,t7:67373,t8:21035,t9:6122,t10:49424,t11:75735,t12:46056},
{tenthuoc: "Chirocain 5mg/ml Ampoule 10x10ml 5mg/ml",t1:66177,t2:18454,t3:25820,t4:21462,t5:23020,t6:45119,t7:56066,t8:57680,t9:17319,t10:32573,t11:27620,t12:56082},
{tenthuoc: "Levemir Flexpen",t1:67976,t2:63901,t3:3156,t4:52412,t5:30890,t6:15054,t7:3870,t8:16820,t9:44592,t10:17177,t11:42839,t12:32852},
{tenthuoc: "Coveram (5+10)mg",t1:67889,t2:9467,t3:28737,t4:27233,t5:44095,t6:47042,t7:37861,t8:17348,t9:66437,t10:29690,t11:16758,t12:38296},
{tenthuoc: "Azipowder",t1:28791,t2:1374,t3:63569,t4:59005,t5:61750,t6:45946,t7:20966,t8:67425,t9:8498,t10:6258,t11:23239,t12:51596},
{tenthuoc: "Cardilopin",t1:44338,t2:33933,t3:21431,t4:66365,t5:32902,t6:58388,t7:10749,t8:38381,t9:8108,t10:53594,t11:34954,t12:32509},
{tenthuoc: "Humulin N 100IU 10ml",t1:9570,t2:55801,t3:78740,t4:59647,t5:57970,t6:15511,t7:10207,t8:14672,t9:40378,t10:22836,t11:36637,t12:3674},
{tenthuoc: "Vitamin B1-B6-B12",t1:78375,t2:57990,t3:75351,t4:2752,t5:71808,t6:62991,t7:30700,t8:9378,t9:49329,t10:3306,t11:60799,t12:21004},
{tenthuoc: "Vitamin B1-B6-B12",t1:69920,t2:73740,t3:55763,t4:60403,t5:69439,t6:68738,t7:48572,t8:76730,t9:32127,t10:52743,t11:68594,t12:0},
{tenthuoc: "Concor 2,5mg",t1:68273,t2:8498,t3:1001,t4:50433,t5:6731,t6:40226,t7:7279,t8:15449,t9:3875,t10:12600,t11:77918,t12:72236},
{tenthuoc: "Ebitac12.5",t1:59086,t2:18482,t3:22147,t4:26575,t5:22398,t6:48474,t7:12535,t8:68845,t9:59759,t10:44817,t11:14237,t12:43078},
{tenthuoc: "Sanlein",t1:69085,t2:64600,t3:5855,t4:22889,t5:57591,t6:5074,t7:71972,t8:29126,t9:15283,t10:0,t11:29704,t12:41481},
{tenthuoc: "Tercef",t1:74553,t2:35275,t3:32911,t4:76460,t5:63430,t6:43888,t7:0,t8:55566,t9:38563,t10:19190,t11:11139,t12:26569},
{tenthuoc: "Propofol 1 % Kabi",t1:1379,t2:27227,t3:42923,t4:32699,t5:53966,t6:49045,t7:63436,t8:20758,t9:15495,t10:69204,t11:9151,t12:52797},
{tenthuoc: "Unasyn Inj 1500mg 1's",t1:78911,t2:35370,t3:68702,t4:17133,t5:32019,t6:37966,t7:7310,t8:52581,t9:20983,t10:67231,t11:21849,t12:23069},
{tenthuoc: "Unasyn Inj 1500mg 1's",t1:56654,t2:71465,t3:51613,t4:74230,t5:4672,t6:25086,t7:76881,t8:79072,t9:35886,t10:70598,t11:40027,t12:27501},
{tenthuoc: "Insulin H Mix 100IU",t1:77406,t2:68871,t3:37671,t4:12753,t5:42345,t6:40562,t7:28674,t8:60748,t9:8417,t10:39263,t11:20936,t12:71480},
{tenthuoc: "Oflovid ophthalmic ointment",t1:46586,t2:39867,t3:28907,t4:2110,t5:75210,t6:49289,t7:25592,t8:76212,t9:69801,t10:1099,t11:32974,t12:51430},
{tenthuoc: "Rocephin 1g I.V.",t1:1374,t2:16863,t3:39922,t4:39371,t5:9278,t6:41293,t7:23687,t8:14590,t9:62941,t10:58329,t11:41609,t12:62767},
{tenthuoc: "Franilax",t1:66664,t2:22666,t3:54397,t4:28889,t5:30969,t6:20948,t7:29496,t8:64729,t9:47061,t10:66825,t11:69233,t12:15307},
{tenthuoc: "Fudcime",t1:40125,t2:16863,t3:50767,t4:15991,t5:29860,t6:53011,t7:17086,t8:18477,t9:63062,t10:25283,t11:64511,t12:29935},
{tenthuoc: "Fudcime",t1:63585,t2:68478,t3:49732,t4:74565,t5:63201,t6:7971,t7:4895,t8:16650,t9:31259,t10:37900,t11:10026,t12:60674},
{tenthuoc: "Paclitaxel  Ebewe  Inj 100MG/16.7ML 1's",t1:31565,t2:50990,t3:71091,t4:31940,t5:75365,t6:37905,t7:11605,t8:37776,t9:64064,t10:67422,t11:64563,t12:63434},
{tenthuoc: "Ucyrin 75mg",t1:33614,t2:46321,t3:22271,t4:0,t5:31987,t6:49669,t7:17072,t8:8146,t9:67701,t10:44634,t11:28920,t12:56236},
{tenthuoc: "Vigamox",t1:57815,t2:24071,t3:17747,t4:30096,t5:10367,t6:54601,t7:55461,t8:36002,t9:76263,t10:32378,t11:23556,t12:25567},
{tenthuoc: "Levonor 4mg/4ml",t1:61537,t2:37852,t3:62918,t4:13863,t5:67979,t6:10362,t7:20128,t8:17472,t9:9393,t10:79751,t11:59317,t12:72633},
{tenthuoc: "Scilin M30 (30/70)",t1:74842,t2:64426,t3:79130,t4:39278,t5:27151,t6:41415,t7:51722,t8:44458,t9:24778,t10:69805,t11:62629,t12:45059},
{tenthuoc: "Mixtard 30 FlexPen",t1:5874,t2:58766,t3:64462,t4:41160,t5:13719,t6:18525,t7:21797,t8:746,t9:29344,t10:37150,t11:55271,t12:59499},
{tenthuoc: "Xeloda (Capecitabin) 500mg",t1:46569,t2:3952,t3:45119,t4:74540,t5:62991,t6:15052,t7:58901,t8:59388,t9:44787,t10:58588,t11:16806,t12:31738},
{tenthuoc: "Emas",t1:24067,t2:48463,t3:16108,t4:41475,t5:25976,t6:53334,t7:9099,t8:57582,t9:5065,t10:61345,t11:26166,t12:73044},
{tenthuoc: "Exforge 5/80mg",t1:14747,t2:38778,t3:21067,t4:37340,t5:67601,t6:59886,t7:30714,t8:57709,t9:36147,t10:19070,t11:7480,t12:54639},
{tenthuoc: "Unasyn 1500mg",t1:2526,t2:37754,t3:10424,t4:43513,t5:75012,t6:16531,t7:17705,t8:77431,t9:71808,t10:65687,t11:46976,t12:38513},
{tenthuoc: "Unasyn Inj 1500mg 1's",t1:47095,t2:40877,t3:44701,t4:51822,t5:34752,t6:53344,t7:49617,t8:72327,t9:44426,t10:72920,t11:72197,t12:60362},
{tenthuoc: "Diprivan 20ml/200mg",t1:63229,t2:55119,t3:16108,t4:78666,t5:36096,t6:29485,t7:22279,t8:63660,t9:42748,t10:38918,t11:58252,t12:19016},
{tenthuoc: "Tobradex 5ml",t1:16213,t2:40543,t3:60433,t4:60131,t5:26827,t6:70801,t7:48122,t8:69547,t9:56374,t10:10782,t11:33443,t12:47028},
{tenthuoc: "Azipowder",t1:68258,t2:45614,t3:12160,t4:63770,t5:19828,t6:67689,t7:59261,t8:57273,t9:41122,t10:35403,t11:75457,t12:57108},
{tenthuoc: "Heparin 25000 UI/5ml",t1:48876,t2:17553,t3:24829,t4:75979,t5:54352,t6:72355,t7:48299,t8:76671,t9:37556,t10:78854,t11:23453,t12:62088},
{tenthuoc: "Parocontin",t1:15123,t2:37960,t3:26259,t4:43830,t5:65776,t6:71917,t7:4697,t8:38937,t9:78579,t10:15303,t11:63392,t12:27003},
{tenthuoc: "Sastan-H",t1:52292,t2:64583,t3:73337,t4:56064,t5:2323,t6:36717,t7:32171,t8:25805,t9:23984,t10:8016,t11:33893,t12:3153},
{tenthuoc: "Mixtard 30 Flexpen 100IU/ml x 3ml",t1:19800,t2:25511,t3:5839,t4:3571,t5:53146,t6:38914,t7:20511,t8:59443,t9:1165,t10:62739,t11:31451,t12:57466},
{tenthuoc: "Coveram 5-10 Tab 5mg-10mg 30's",t1:22755,t2:71675,t3:59296,t4:67800,t5:68022,t6:1247,t7:3602,t8:63857,t9:61978,t10:64322,t11:60277,t12:60825},
{tenthuoc: "Ebitac 12.5",t1:18821,t2:36916,t3:7189,t4:68700,t5:1048,t6:24365,t7:74159,t8:42023,t9:21143,t10:71113,t11:38137,t12:32762},
{tenthuoc: "Bloza",t1:41765,t2:79454,t3:79431,t4:32522,t5:34777,t6:66442,t7:38850,t8:29210,t9:47935,t10:27542,t11:67737,t12:23858},
{tenthuoc: "Twynsta 40/5mg",t1:31830,t2:36470,t3:59713,t4:69555,t5:73724,t6:67194,t7:54229,t8:67316,t9:76143,t10:425,t11:10552,t12:22558},
{tenthuoc: "Rocephin 1g I.V.",t1:1971,t2:28098,t3:25844,t4:2543,t5:29937,t6:73717,t7:3149,t8:29839,t9:25281,t10:70565,t11:63249,t12:19881},
{tenthuoc: "Exforge HCT 5/160/12.5mg",t1:40249,t2:329,t3:64599,t4:44141,t5:2710,t6:33773,t7:55483,t8:41875,t9:9231,t10:25141,t11:21759,t12:29067},
{tenthuoc: "Diamisu-N 10ml",t1:8244,t2:26071,t3:59026,t4:24586,t5:32167,t6:62427,t7:70634,t8:72046,t9:48667,t10:35875,t11:50829,t12:1320},
{tenthuoc: "Cravit 5mg/ ml",t1:40505,t2:55492,t3:75832,t4:20980,t5:72134,t6:22121,t7:50668,t8:30269,t9:67110,t10:30410,t11:60251,t12:12465},
{tenthuoc: "Polhumin mix-2",t1:26226,t2:11618,t3:8917,t4:68697,t5:12133,t6:30864,t7:70697,t8:50567,t9:50015,t10:15383,t11:76939,t12:43582},
{tenthuoc: "Bupivacaine 20mg 0.5% 5 mg/ml 0,5%/4ml",t1:56672,t2:25143,t3:62700,t4:2085,t5:36326,t6:50449,t7:15395,t8:43109,t9:60076,t10:43161,t11:45368,t12:30997},
{tenthuoc: "Unasyn Inj 1500mg 1's",t1:43488,t2:45972,t3:56657,t4:15202,t5:37097,t6:2849,t7:2374,t8:56632,t9:3207,t10:55130,t11:0,t12:14385},
{tenthuoc: "Reditux 100mg/10ml",t1:28386,t2:9303,t3:42376,t4:0,t5:68807,t6:19821,t7:21356,t8:77316,t9:12525,t10:928,t11:50294,t12:64883},
{tenthuoc: "Coveram",t1:2288,t2:69338,t3:53983,t4:74861,t5:19113,t6:29930,t7:51864,t8:57321,t9:68498,t10:3574,t11:18111,t12:20575},
{tenthuoc: "Bibiso",t1:16847,t2:73184,t3:3272,t4:55656,t5:40186,t6:13392,t7:52997,t8:29781,t9:49912,t10:28053,t11:15134,t12:51200},
{tenthuoc: "Coveram 5-10 Tab 5mg-10mg 30's",t1:42659,t2:11045,t3:69708,t4:12023,t5:15687,t6:28,t7:37925,t8:45756,t9:13648,t10:5349,t11:67826,t12:72672},
{tenthuoc: "Insulin H Mix 100IU",t1:54052,t2:26516,t3:42342,t4:48771,t5:26764,t6:51111,t7:65975,t8:70678,t9:79709,t10:14808,t11:65730,t12:33936},
{tenthuoc: "Diprivan Pre-Filled Syring 1% 10mg/ml (1%) - 50ml",t1:11749,t2:79499,t3:1175,t4:45617,t5:39588,t6:58361,t7:64535,t8:5504,t9:1517,t10:67046,t11:74468,t12:48503},
{tenthuoc: "Mixtard 30 Flexpen 100ui/mlx3ml",t1:21073,t2:30771,t3:4516,t4:53305,t5:19919,t6:56697,t7:7761,t8:39226,t9:32382,t10:50964,t11:23642,t12:6104},
{tenthuoc: "Exforge tab 5mg/ 80mg 2x14's",t1:31211,t2:62907,t3:72500,t4:32327,t5:65558,t6:29869,t7:44176,t8:39284,t9:55415,t10:27595,t11:27070,t12:35986},
{tenthuoc: "Fudcime (Cefixim) 200mg",t1:100,t2:45500,t3:51643,t4:54118,t5:48848,t6:12925,t7:58316,t8:65951,t9:51353,t10:57511,t11:20387,t12:39088},
{tenthuoc: "Savi Losartan plus HCT 50/12.5",t1:58676,t2:3385,t3:48139,t4:67660,t5:27804,t6:69302,t7:30428,t8:50889,t9:72095,t10:10021,t11:72543,t12:44861},
{tenthuoc: "Vigamox",t1:20462,t2:55152,t3:24676,t4:75660,t5:9229,t6:18689,t7:24840,t8:64882,t9:58922,t10:33865,t11:5791,t12:31517},
{tenthuoc: "Heparin-Belmed",t1:59321,t2:63882,t3:78871,t4:64167,t5:28320,t6:38423,t7:35482,t8:16091,t9:28945,t10:47860,t11:59574,t12:17203},
{tenthuoc: "Scilin M30 (30/70)",t1:53129,t2:67897,t3:20833,t4:29446,t5:57194,t6:5553,t7:67905,t8:54546,t9:57368,t10:53386,t11:79484,t12:70161},
{tenthuoc: "Cravit",t1:51588,t2:2785,t3:61235,t4:56658,t5:11669,t6:74500,t7:57288,t8:72647,t9:39330,t10:5483,t11:69035,t12:36978},
{tenthuoc: "Losartan Stada 50mg",t1:0,t2:38340,t3:51370,t4:60810,t5:16099,t6:22998,t7:67994,t8:38132,t9:30879,t10:23973,t11:65580,t12:53269},
{tenthuoc: "Resines 5mg(amlodipin)",t1:0,t2:35391,t3:7740,t4:51725,t5:61788,t6:31508,t7:16913,t8:53987,t9:26617,t10:46203,t11:29453,t12:75977},
{tenthuoc: "Meronem 1g (Meropenem)",t1:0,t2:72992,t3:0,t4:2130,t5:36974,t6:20565,t7:28,t8:4798,t9:65712,t10:43561,t11:20399,t12:37835},
{tenthuoc: "Insulin H Mix 100IU",t1:0,t2:56474,t3:60974,t4:50844,t5:35715,t6:75418,t7:14527,t8:40966,t9:41207,t10:72361,t11:0,t12:6635},
{tenthuoc: "Bimoxine",t1:0,t2:75678,t3:55908,t4:1899,t5:33491,t6:72255,t7:1477,t8:75048,t9:59491,t10:71186,t11:2561,t12:32110},
{tenthuoc: "Fudcime 200mg",t1:0,t2:78560,t3:25317,t4:5678,t5:66779,t6:2188,t7:31656,t8:23634,t9:76564,t10:4256,t11:20308,t12:44432},
{tenthuoc: "Novorapid",t1:0,t2:77920,t3:74389,t4:30746,t5:16778,t6:78979,t7:52724,t8:74567,t9:58659,t10:41839,t11:55081,t12:36473},
{tenthuoc: "Bactirid 100mg/5ml",t1:0,t2:73808,t3:1485,t4:63777,t5:45413,t6:44643,t7:33641,t8:51946,t9:35396,t10:59834,t11:5087,t12:57549},
{tenthuoc: "Paringold Injection 25000Ui/5ml",t1:0,t2:79853,t3:22804,t4:32336,t5:3725,t6:12761,t7:77686,t8:63024,t9:60720,t10:24574,t11:5827,t12:63953},
{tenthuoc: "Mixtard 30 FlexPen",t1:0,t2:44043,t3:14557,t4:73394,t5:19789,t6:65613,t7:75610,t8:67263,t9:48282,t10:27685,t11:28152,t12:67853},
{tenthuoc: "Mixtard 30 Flexpen 100IU/ml x 3ml",t1:0,t2:66784,t3:13504,t4:70874,t5:59261,t6:67454,t7:42557,t8:35219,t9:78970,t10:23456,t11:1241,t12:36416},
{tenthuoc: "Lotemax",t1:0,t2:47864,t3:49293,t4:17006,t5:14779,t6:24777,t7:67964,t8:31868,t9:31457,t10:46843,t11:73569,t12:62693},
{tenthuoc: "Paracetamol 500mg( Panadol Caplet)",t1:0,t2:47138,t3:23143,t4:60859,t5:10340,t6:7353,t7:52695,t8:3219,t9:1632,t10:38189,t11:5068,t12:11167},
{tenthuoc: "Cravit 5mg/ ml",t1:0,t2:71430,t3:56672,t4:32057,t5:24402,t6:46603,t7:67823,t8:42950,t9:61287,t10:60410,t11:24920,t12:49513},
{tenthuoc: "Clocardigel (Clopidogrel) 75mg",t1:0,t2:6849,t3:31622,t4:14333,t5:18670,t6:63289,t7:70941,t8:69772,t9:44498,t10:43875,t11:59484,t12:54021},
{tenthuoc: "Cozaar XQ 5mg+100mg",t1:0,t2:1174,t3:22895,t4:64026,t5:68646,t6:34096,t7:18286,t8:71803,t9:65114,t10:76313,t11:16774,t12:14061},
{tenthuoc: "Nifedipin Hasan 20 Retard",t1:0,t2:41779,t3:5463,t4:62591,t5:48757,t6:7919,t7:40452,t8:41870,t9:13224,t10:45121,t11:0,t12:52517},
{tenthuoc: "Venrutine",t1:0,t2:78827,t3:8412,t4:2822,t5:11665,t6:18947,t7:53478,t8:26577,t9:8831,t10:37937,t11:77731,t12:21786},
{tenthuoc: "Mecefix-B.E",t1:0,t2:53705,t3:30995,t4:74275,t5:49118,t6:52467,t7:77293,t8:32334,t9:51008,t10:5241,t11:63060,t12:60075},
{tenthuoc: "Twynsta",t1:0,t2:58780,t3:19735,t4:4653,t5:42588,t6:60890,t7:2285,t8:48858,t9:72597,t10:54319,t11:42728,t12:23967},
{tenthuoc: "Tamifine",t1:0,t2:57651,t3:27907,t4:69827,t5:62355,t6:21805,t7:606,t8:67621,t9:38087,t10:22965,t11:46387,t12:17301},
{tenthuoc: "Heparin-Belmed 25.000UI/5ml",t1:0,t2:49351,t3:24344,t4:35713,t5:44878,t6:19457,t7:37745,t8:53741,t9:50148,t10:30738,t11:26472,t12:40941},
{tenthuoc: "Xalvobin",t1:17467,t2:61542,t3:40818,t4:1395,t5:29088,t6:55417,t7:77717,t8:4251,t9:9391,t10:31035,t11:58073,t12:28865},
{tenthuoc: "Ofmantine-Domesco 625mg",t1:10926,t2:56770,t3:24778,t4:10744,t5:61587,t6:24177,t7:34146,t8:16282,t9:50894,t10:70283,t11:72667,t12:36424},
{tenthuoc: "Nifedipine Hasan 20 retard",t1:49486,t2:44293,t3:44652,t4:68529,t5:50915,t6:50442,t7:56701,t8:19232,t9:33336,t10:61239,t11:8956,t12:3147},
{tenthuoc: "Resines 5mg",t1:18667,t2:31816,t3:54894,t4:65498,t5:28026,t6:58618,t7:12718,t8:5412,t9:348,t10:79453,t11:21336,t12:34748},
{tenthuoc: "Exforge 5/80mg",t1:56366,t2:64678,t3:8109,t4:50151,t5:17575,t6:11290,t7:7506,t8:26990,t9:72463,t10:15951,t11:29949,t12:73760},
{tenthuoc: "Adalat LA Tab 20mg 30's",t1:10728,t2:30158,t3:49556,t4:70294,t5:29710,t6:48236,t7:48842,t8:6668,t9:45211,t10:46451,t11:71316,t12:75459},
{tenthuoc: "Bibiso",t1:31128,t2:4565,t3:75134,t4:77300,t5:67471,t6:70750,t7:46699,t8:73190,t9:78401,t10:50907,t11:27183,t12:686},
{tenthuoc: "Ebitac 25mg",t1:13035,t2:79053,t3:7149,t4:28148,t5:72064,t6:60357,t7:76991,t8:53017,t9:5711,t10:46661,t11:73906,t12:58875},
{tenthuoc: "Diprivan 200mg/10ml",t1:11082,t2:16087,t3:63006,t4:54033,t5:71356,t6:33979,t7:3224,t8:58358,t9:11964,t10:62156,t11:59620,t12:55309},
{tenthuoc: "Scilin M30 (30/70)",t1:3975,t2:21159,t3:69310,t4:5014,t5:19469,t6:78710,t7:45818,t8:16408,t9:69129,t10:69262,t11:79586,t12:65810},
{tenthuoc: "Scilin R",t1:18816,t2:3758,t3:28705,t4:0,t5:34927,t6:59097,t7:16828,t8:11277,t9:40780,t10:53486,t11:48941,t12:71192},
{tenthuoc: "Plavix",t1:66815,t2:38152,t3:60703,t4:0,t5:15164,t6:72336,t7:16501,t8:63327,t9:9120,t10:8557,t11:66780,t12:39382},
{tenthuoc: "Amlor Cap 5mg 30's",t1:68319,t2:78298,t3:67551,t4:0,t5:11739,t6:14654,t7:77281,t8:9793,t9:49658,t10:69242,t11:72389,t12:23384},
{tenthuoc: "Nifedipin T20 Stada retard",t1:45423,t2:26013,t3:53,t4:0,t5:64660,t6:44433,t7:4805,t8:62515,t9:25239,t10:65712,t11:76778,t12:49524},
{tenthuoc: "Loratadin 5mg/5ml*30ml(Ganusa)",t1:74777,t2:15893,t3:0,t4:0,t5:3922,t6:15159,t7:71681,t8:42873,t9:7921,t10:32314,t11:3242,t12:19893},
{tenthuoc: "Noradrenalin Base Aguettant  4mg/4ml",t1:38269,t2:2903,t3:0,t4:0,t5:59721,t6:35670,t7:24733,t8:6857,t9:38599,t10:40504,t11:52751,t12:63853},
{tenthuoc: "Tadocel 20mg/0,5ml",t1:75895,t2:30934,t3:0,t4:0,t5:18725,t6:39791,t7:50437,t8:43554,t9:34582,t10:14865,t11:59054,t12:59650},
{tenthuoc: "Sotstop",t1:68937,t2:74571,t3:0,t4:0,t5:51695,t6:28340,t7:68201,t8:34470,t9:30717,t10:4034,t11:12865,t12:70281},
{tenthuoc: "Fabapoxim",t1:71572,t2:35140,t3:0,t4:0,t5:750,t6:64708,t7:17149,t8:33740,t9:2478,t10:69423,t11:59560,t12:39180},
{tenthuoc: "Klamentin 1g",t1:0,t2:18942,t3:0,t4:73412,t5:59511,t6:2051,t7:39654,t8:76422,t9:6285,t10:50209,t11:26088,t12:56621},
{tenthuoc: "Aldacton 25mg",t1:0,t2:39693,t3:76699,t4:36433,t5:8025,t6:16710,t7:59095,t8:0,t9:40663,t10:23062,t11:14175,t12:63684},
{tenthuoc: "Unasyn 1,5g",t1:0,t2:15498,t3:69267,t4:23846,t5:34744,t6:55344,t7:34792,t8:0,t9:49015,t10:27778,t11:47502,t12:5379},
{tenthuoc: "Coveram 5-5 Tab 5mg/5mg 30's",t1:0,t2:76368,t3:70134,t4:8122,t5:4458,t6:0,t7:42311,t8:0,t9:21931,t10:63985,t11:31167,t12:450},
{tenthuoc: "Coveram 5-5 Tab 5mg/5mg 30's",t1:0,t2:6650,t3:10471,t4:25230,t5:18666,t6:0,t7:9146,t8:0,t9:65931,t10:28899,t11:1327,t12:39830},
{tenthuoc: "Coveram Tab 5mg/5mg 30's",t1:47103,t2:62956,t3:45357,t4:42742,t5:39731,t6:0,t7:41116,t8:0,t9:65595,t10:44944,t11:79477,t12:10499},
{tenthuoc: "Amlor cap",t1:27024,t2:59534,t3:40907,t4:35144,t5:76689,t6:0,t7:31412,t8:0,t9:45144,t10:39948,t11:77559,t12:77279},
{tenthuoc: "Systan Ultra",t1:13829,t2:47615,t3:10653,t4:74668,t5:22065,t6:0,t7:67649,t8:0,t9:71526,t10:56301,t11:65342,t12:68324},
{tenthuoc: "Tartriakson (Ceftriaxon) 1g",t1:26959,t2:38231,t3:59078,t4:31605,t5:8748,t6:0,t7:75318,t8:0,t9:78188,t10:78262,t11:7618,t12:48194},
{tenthuoc: "Toxaxine Inj",t1:46649,t2:32594,t3:7107,t4:66734,t5:64004,t6:0,t7:67136,t8:0,t9:54041,t10:16283,t11:6045,t12:0},
{tenthuoc: "Cozaar",t1:57221,t2:29719,t3:72616,t4:77276,t5:14307,t6:0,t7:62226,t8:0,t9:23875,t10:36131,t11:78794,t12:67204},
{tenthuoc: "Resines",t1:1015,t2:12367,t3:74794,t4:29403,t5:75888,t6:0,t7:54514,t8:0,t9:524,t10:4687,t11:40411,t12:51112},
{tenthuoc: "Moxilen 500mg",t1:72245,t2:76291,t3:28593,t4:1323,t5:68795,t6:0,t7:7443,t8:0,t9:45720,t10:45045,t11:24510,t12:3892},
{tenthuoc: "Hometex",t1:77663,t2:23642,t3:39171,t4:33104,t5:35210,t6:0,t7:71451,t8:33166,t9:46364,t10:21771,t11:42910,t12:9831},
{tenthuoc: "Ebitac 12.5",t1:22912,t2:59909,t3:4161,t4:28647,t5:33734,t6:50301,t7:76219,t8:31015,t9:22160,t10:52058,t11:16510,t12:13649},
{tenthuoc: "Amlor Tab 5mg 30's",t1:12532,t2:2208,t3:66819,t4:25174,t5:46903,t6:78656,t7:15232,t8:5207,t9:77355,t10:35366,t11:20467,t12:28895},
{tenthuoc: "Sanlein",t1:7377,t2:73056,t3:958,t4:15969,t5:66988,t6:17795,t7:14417,t8:10828,t9:44450,t10:33340,t11:3605,t12:18933},
{tenthuoc: "Medsamic",t1:51440,t2:17813,t3:29153,t4:6121,t5:15747,t6:15678,t7:52198,t8:18368,t9:63014,t10:10050,t11:21097,t12:78766},
{tenthuoc: "Moxacin",t1:54656,t2:61833,t3:47640,t4:78969,t5:27545,t6:3934,t7:42385,t8:63165,t9:14795,t10:46020,t11:58504,t12:64156},
{tenthuoc: "Thiên sứ hộ tâm đan",t1:19448,t2:10738,t3:9389,t4:33450,t5:54702,t6:42070,t7:23031,t8:2561,t9:28559,t10:43583,t11:15454,t12:2200},
{tenthuoc: "Mecefix (cefixim 150mg)",t1:46789,t2:45207,t3:52317,t4:10593,t5:51425,t6:46964,t7:14849,t8:73081,t9:78995,t10:70672,t11:73139,t12:17293},
{tenthuoc: "Scilin M30 400IU",t1:18798,t2:18408,t3:38582,t4:42040,t5:71466,t6:34863,t7:28836,t8:22534,t9:43211,t10:43376,t11:72669,t12:77298},
{tenthuoc: "Scilin M30 (30/70)",t1:53778,t2:11242,t3:18217,t4:68728,t5:46279,t6:6267,t7:6777,t8:13032,t9:40438,t10:32078,t11:30018,t12:20812},
{tenthuoc: "Campto Inj 100mg 5ml",t1:65762,t2:14568,t3:26678,t4:41236,t5:9100,t6:74793,t7:18773,t8:47689,t9:67225,t10:45629,t11:50466,t12:35458},
{tenthuoc: "Campto (Irinotecan) 100mg",t1:43016,t2:13931,t3:38272,t4:73131,t5:44332,t6:36744,t7:6225,t8:57894,t9:65000,t10:66651,t11:45360,t12:72801},
{tenthuoc: "LOVENOX 60mg Inj B/ 2 syringes x  0,6ml",t1:30350,t2:4897,t3:33414,t4:34694,t5:75703,t6:68876,t7:17847,t8:43400,t9:47005,t10:0,t11:1682,t12:43108},
{tenthuoc: "LOVENOX 60mg Inj B/ 2 syringes x0,6ml",t1:9227,t2:21273,t3:23959,t4:40713,t5:49188,t6:0,t7:68170,t8:9597,t9:29350,t10:0,t11:0,t12:43427},
{tenthuoc: "Rocephin 1g",t1:53627,t2:57032,t3:41196,t4:67701,t5:25997,t6:0,t7:24827,t8:19230,t9:24665,t10:0,t11:0,t12:41832},
{tenthuoc: "Rocephin 1g I.V.",t1:19480,t2:15685,t3:15271,t4:6484,t5:30644,t6:0,t7:5713,t8:9053,t9:34610,t10:0,t11:0,t12:71571},
{tenthuoc: "Paclitaxel  Ebewe  Inj 30MG/5ML 1's",t1:35947,t2:55693,t3:6162,t4:41479,t5:68729,t6:0,t7:19899,t8:15336,t9:71038,t10:0,t11:0,t12:54789},
{tenthuoc: "Lisonorm 5mg+10mg",t1:74373,t2:9980,t3:75022,t4:45820,t5:26374,t6:0,t7:53271,t8:68933,t9:6080,t10:0,t11:0,t12:71644},
{tenthuoc: "Fabapoxim",t1:36173,t2:62727,t3:68472,t4:17335,t5:1202,t6:66340,t7:28319,t8:44576,t9:78958,t10:0,t11:0,t12:72313},
{tenthuoc: "EDNYT 10MG Tablet(s) nén",t1:8961,t2:18598,t3:30079,t4:34768,t5:3546,t6:45231,t7:57843,t8:32711,t9:4681,t10:0,t11:0,t12:38577},
{tenthuoc: "Adalat LA 20mg",t1:77687,t2:30996,t3:57907,t4:0,t5:60197,t6:15321,t7:47585,t8:38021,t9:9565,t10:0,t11:0,t12:24017},
{tenthuoc: "Tercef",t1:71681,t2:73947,t3:12464,t4:15418,t5:40486,t6:58322,t7:63644,t8:79803,t9:68790,t10:0,t11:0,t12:13201},
{tenthuoc: "Isotic Moxisone",t1:47888,t2:10465,t3:4157,t4:21106,t5:29168,t6:64593,t7:14861,t8:72039,t9:43768,t10:0,t11:0,t12:35340},
{tenthuoc: "Isotic Moxisone",t1:56247,t2:35305,t3:30591,t4:33099,t5:0,t6:35343,t7:3463,t8:68215,t9:0,t10:0,t11:0,t12:6678},
{tenthuoc: "Bactirid 40ml",t1:43820,t2:0,t3:58002,t4:6623,t5:0,t6:22548,t7:20369,t8:75303,t9:0,t10:0,t11:0,t12:748},
{tenthuoc: "Hoạt huyết dưỡng não",t1:48391,t2:14363,t3:31069,t4:812,t5:0,t6:36170,t7:34022,t8:40063,t9:0,t10:29811,t11:0,t12:55124},
{tenthuoc: "Bisoprolol fumarat",t1:53817,t2:0,t3:55327,t4:24954,t5:0,t6:14207,t7:32567,t8:67174,t9:0,t10:30599,t11:0,t12:22001},
{tenthuoc: "Ebitac 12,5",t1:0,t2:30917,t3:47876,t4:72136,t5:0,t6:18859,t7:45206,t8:7641,t9:0,t10:42287,t11:0,t12:19986},
{tenthuoc: "Venrutine",t1:0,t2:78848,t3:73520,t4:42060,t5:0,t6:20567,t7:35856,t8:48398,t9:0,t10:46272,t11:0,t12:57616},
{tenthuoc: "Propofol 1%/20mL.",t1:0,t2:64868,t3:0,t4:40939,t5:70087,t6:41004,t7:5608,t8:0,t9:0,t10:35630,t11:0,t12:43212},
{tenthuoc: "Zitromax 600mg/15ml",t1:0,t2:59442,t3:8266,t4:48666,t5:27244,t6:44624,t7:7210,t8:25384,t9:0,t10:44366,t11:0,t12:8568},
{tenthuoc: "Heparin 25000UI/5ml",t1:0,t2:17326,t3:54461,t4:60380,t5:79399,t6:27743,t7:53887,t8:24121,t9:0,t10:16340,t11:57332,t12:13794},
{tenthuoc: "CIPROFLOXACIN KABI",t1:0,t2:22402,t3:61016,t4:2770,t5:28097,t6:75538,t7:39972,t8:2043,t9:0,t10:61919,t11:61428,t12:25715},
{tenthuoc: "Oflovid 3mg/ml x 5ml",t1:0,t2:48247,t3:46534,t4:49,t5:32823,t6:53959,t7:42270,t8:57294,t9:0,t10:25907,t11:32958,t12:2762},
{tenthuoc: "Ebitac 12,5",t1:73647,t2:24872,t3:73636,t4:53021,t5:30074,t6:0,t7:65302,t8:0,t9:0,t10:24479,t11:73816,t12:23782},
{tenthuoc: "Bloza",t1:66828,t2:41361,t3:8233,t4:41853,t5:15241,t6:57242,t7:18674,t8:0,t9:0,t10:19063,t11:69002,t12:18105},
{tenthuoc: "Unasyn Inj 1500mg 1's",t1:61747,t2:59562,t3:60032,t4:57233,t5:21807,t6:7950,t7:36155,t8:0,t9:0,t10:56664,t11:9262,t12:43035},
{tenthuoc: "Levonor 1mg/ml",t1:51411,t2:56488,t3:23766,t4:9707,t5:54318,t6:75598,t7:59230,t8:0,t9:0,t10:49742,t11:17669,t12:9420},
{tenthuoc: "Levonor 4mg/4ml",t1:40062,t2:47212,t3:0,t4:72821,t5:26627,t6:2561,t7:13803,t8:0,t9:0,t10:52284,t11:7677,t12:45071},
{tenthuoc: "Paclitaxel  Ebewe  Inj 100MG/16.7ML 1's",t1:75203,t2:73514,t3:0,t4:59979,t5:40606,t6:14944,t7:12444,t8:0,t9:0,t10:67153,t11:67583,t12:3834},
{tenthuoc: "Bidilucil 500",t1:12076,t2:50531,t3:0,t4:11033,t5:23890,t6:72615,t7:23793,t8:0,t9:0,t10:19920,t11:74762,t12:4846},
{tenthuoc: "Mixtard Flexpen 30/70 100UI/ml,3ml",t1:17927,t2:34208,t3:0,t4:25979,t5:56195,t6:33126,t7:54881,t8:0,t9:0,t10:71551,t11:50809,t12:76001},
{tenthuoc: "Scanneuron 300,2mg",t1:70436,t2:47521,t3:0,t4:32680,t5:18500,t6:778,t7:64709,t8:0,t9:0,t10:35303,t11:13516,t12:1044},
{tenthuoc: "Scanneuron",t1:42731,t2:68645,t3:0,t4:28137,t5:74723,t6:40864,t7:25050,t8:0,t9:0,t10:36119,t11:60981,t12:67509},
{tenthuoc: "Duotrav",t1:455,t2:62019,t3:0,t4:71703,t5:25903,t6:0,t7:22490,t8:0,t9:0,t10:18526,t11:9884,t12:0},
{tenthuoc: "3bpluzs (vitamin b1+b6+b12)",t1:51412,t2:6698,t3:0,t4:30417,t5:3103,t6:0,t7:44879,t8:0,t9:0,t10:14656,t11:44559,t12:0},
{tenthuoc: "Paclitaxel 100mg/16,7ml",t1:76062,t2:57983,t3:0,t4:37608,t5:8539,t6:0,t7:42856,t8:0,t9:0,t10:33772,t11:34857,t12:0},
{tenthuoc: "Morphini Sulfas Wzf 0,1% 2mg 2ml Spinal 2mg/2ml",t1:10268,t2:29893,t3:0,t4:17681,t5:64138,t6:0,t7:69493,t8:0,t9:0,t10:61571,t11:7008,t12:0},
{tenthuoc: "Curam 625mg",t1:59247,t2:58641,t3:0,t4:47047,t5:12560,t6:0,t7:21858,t8:0,t9:0,t10:46273,t11:44769,t12:0},
{tenthuoc: "Mixtard 30 FlexPen",t1:79244,t2:63348,t3:0,t4:34050,t5:23144,t6:0,t7:45015,t8:0,t9:0,t10:10160,t11:71118,t12:0},
{tenthuoc: "Mixtard 30 flexpen (insulin human)",t1:69973,t2:77565,t3:28141,t4:4848,t5:39341,t6:0,t7:78085,t8:0,t9:0,t10:22780,t11:3312,t12:0},
{tenthuoc: "Lovenox 0,4ml,4000UI",t1:61290,t2:18643,t3:61607,t4:77581,t5:8238,t6:0,t7:60240,t8:0,t9:0,t10:59084,t11:51765,t12:3281},
{tenthuoc: "Lotemax",t1:43564,t2:72078,t3:49465,t4:8122,t5:18408,t6:0,t7:26108,t8:0,t9:0,t10:67867,t11:6086,t12:4089},
{tenthuoc: "Insulin H  Mix 100UI",t1:77502,t2:60954,t3:16005,t4:31677,t5:32918,t6:0,t7:67812,t8:0,t9:0,t10:58425,t11:32508,t12:6802},
{tenthuoc: "Scilin R 400IU",t1:28094,t2:22944,t3:6523,t4:57880,t5:661,t6:0,t7:17227,t8:0,t9:0,t10:17641,t11:26504,t12:70621},
{tenthuoc: "Vigadexa 5ml",t1:57741,t2:47638,t3:16139,t4:60786,t5:55031,t6:0,t7:6173,t8:0,t9:0,t10:11185,t11:27826,t12:49476},
{tenthuoc: "Plofed",t1:40393,t2:57208,t3:56254,t4:49928,t5:66543,t6:0,t7:77506,t8:0,t9:0,t10:28956,t11:77217,t12:43721},
{tenthuoc: "Plofed (Propofol) 1%/20ml",t1:55300,t2:33664,t3:51045,t4:13577,t5:7454,t6:0,t7:69386,t8:0,t9:0,t10:19297,t11:48715,t12:34062},
{tenthuoc: "Bloza",t1:55143,t2:29033,t3:48521,t4:69705,t5:45129,t6:0,t7:45810,t8:0,t9:0,t10:54297,t11:35933,t12:23543},
{tenthuoc: "Moxacin",t1:61158,t2:7235,t3:0,t4:9893,t5:24673,t6:0,t7:35945,t8:0,t9:0,t10:74154,t11:26758,t12:36863},
{tenthuoc: "Mecefix (Cefixim) 150mg",t1:8160,t2:22518,t3:14419,t4:20100,t5:43222,t6:0,t7:78139,t8:0,t9:66094,t10:44317,t11:32485,t12:16458},
{tenthuoc: "Claritek 125mg/5mlx50mL",t1:73813,t2:22905,t3:24970,t4:30846,t5:24017,t6:0,t7:2188,t8:0,t9:41814,t10:59521,t11:51722,t12:19446},
{tenthuoc: "Heparin Belmed",t1:8663,t2:20292,t3:71716,t4:2200,t5:39968,t6:0,t7:20056,t8:0,t9:65254,t10:11884,t11:37627,t12:35904},
{tenthuoc: "Clocardigel",t1:3238,t2:15228,t3:50153,t4:34506,t5:67086,t6:0,t7:44284,t8:0,t9:79129,t10:3778,t11:67366,t12:57022},
{tenthuoc: "Adalat LA Tab 20mg 30's",t1:9211,t2:59877,t3:38849,t4:36606,t5:64787,t6:0,t7:14246,t8:0,t9:75759,t10:77410,t11:13388,t12:1870},
{tenthuoc: "Systane Ultra 5ml",t1:59549,t2:3113,t3:0,t4:46753,t5:39133,t6:0,t7:20027,t8:0,t9:63521,t10:33316,t11:34086,t12:3386},
{tenthuoc: "Campto (Irinotecan) 100mg",t1:33169,t2:8255,t3:3451,t4:74225,t5:46971,t6:0,t7:41075,t8:0,t9:33843,t10:56848,t11:1317,t12:29686},
{tenthuoc: "Thiên sứ hộ tâm đan",t1:16168,t2:38545,t3:5997,t4:10297,t5:39297,t6:0,t7:0,t8:16347,t9:76890,t10:17871,t11:7269,t12:39233},
{tenthuoc: "Polhumin mix-2",t1:69721,t2:47278,t3:38848,t4:50953,t5:28093,t6:0,t7:0,t8:63821,t9:72560,t10:67152,t11:73639,t12:28076},
{tenthuoc: "Polhumin mix-2",t1:3300,t2:78830,t3:37804,t4:54115,t5:41849,t6:0,t7:0,t8:4191,t9:5812,t10:0,t11:72098,t12:0},
{tenthuoc: "I.V-globulin S 2500mg",t1:27133,t2:5820,t3:20668,t4:55425,t5:59592,t6:0,t7:0,t8:7110,t9:66150,t10:61376,t11:72453,t12:60454},
{tenthuoc: "Heparin (Paringold) 25000IU/5ml",t1:7096,t2:56634,t3:10930,t4:11556,t5:2153,t6:0,t7:0,t8:48064,t9:74554,t10:15526,t11:26411,t12:1188},
{tenthuoc: "Asgizole (Esomeprazol) 40mg",t1:48046,t2:10135,t3:78245,t4:77659,t5:34096,t6:0,t7:0,t8:28694,t9:24538,t10:40937,t11:69934,t12:31614},
{tenthuoc: "Scanneuron",t1:60920,t2:75775,t3:28642,t4:48252,t5:39914,t6:0,t7:0,t8:23,t9:15238,t10:31627,t11:62503,t12:52731},
{tenthuoc: "Augmentin BD Tab 625mg 14's",t1:42838,t2:32311,t3:50604,t4:39491,t5:54899,t6:0,t7:0,t8:50694,t9:6813,t10:60862,t11:1492,t12:70072},
{tenthuoc: "Oflovid Oint",t1:32275,t2:52925,t3:47189,t4:2066,t5:0,t6:62457,t7:0,t8:13185,t9:16066,t10:36952,t11:18799,t12:50555},
{tenthuoc: "Bibiso",t1:64838,t2:55798,t3:67990,t4:21503,t5:0,t6:590,t7:0,t8:3726,t9:75854,t10:76728,t11:45598,t12:79961},
{tenthuoc: "Taxotere (Docetaxel) 20mg/1ml",t1:21642,t2:64506,t3:56183,t4:79735,t5:0,t6:69775,t7:0,t8:70346,t9:26152,t10:67243,t11:4878,t12:56785},
{tenthuoc: "Ebitac 25mg",t1:68568,t2:20320,t3:50991,t4:2757,t5:0,t6:45360,t7:0,t8:37187,t9:53165,t10:22470,t11:37877,t12:35406},
{tenthuoc: "Sastan-H",t1:66357,t2:44538,t3:13818,t4:8100,t5:0,t6:16261,t7:0,t8:32586,t9:11029,t10:20216,t11:13754,t12:49374},
{tenthuoc: "Vitamin B1+B6+B12",t1:47515,t2:21971,t3:34035,t4:7126,t5:0,t6:49573,t7:0,t8:21298,t9:20176,t10:43573,t11:28778,t12:64392},
{tenthuoc: "PROPOFOL 1% KABI",t1:38636,t2:77856,t3:47896,t4:33534,t5:0,t6:15888,t7:0,t8:55286,t9:11931,t10:53603,t11:18223,t12:50092},
{tenthuoc: "Campto (Irinotecan) 40mg",t1:61996,t2:33978,t3:68323,t4:30024,t5:0,t6:60526,t7:0,t8:72208,t9:73750,t10:15138,t11:43696,t12:12926},
{tenthuoc: "Bibiso",t1:57896,t2:29946,t3:64954,t4:69284,t5:0,t6:8800,t7:0,t8:64905,t9:12160,t10:47264,t11:55215,t12:32392},
{tenthuoc: "Coveram 5-5 Tab 5mg/5mg 30's",t1:29384,t2:70133,t3:28020,t4:56056,t5:0,t6:12721,t7:0,t8:12987,t9:19379,t10:62648,t11:69062,t12:49226},
{tenthuoc: "Ebitac",t1:72081,t2:66453,t3:9834,t4:40802,t5:0,t6:66027,t7:0,t8:31161,t9:54679,t10:19582,t11:43026,t12:30382},
{tenthuoc: "Ebitac 12.5",t1:7708,t2:3198,t3:66177,t4:731,t5:0,t6:4236,t7:0,t8:35331,t9:78909,t10:39716,t11:15878,t12:49273},
{tenthuoc: "Medoclav 1g 875mg+ 125mg",t1:34865,t2:28011,t3:72435,t4:40251,t5:0,t6:25109,t7:0,t8:6934,t9:23843,t10:61411,t11:60901,t12:14920},
{tenthuoc: "Tartriakson  1g",t1:62265,t2:51061,t3:52012,t4:53837,t5:0,t6:13853,t7:0,t8:37775,t9:60363,t10:13007,t11:48915,t12:56930},
{tenthuoc: "Levonor 1mg/ml H10",t1:5490,t2:70229,t3:15786,t4:72376,t5:0,t6:55207,t7:0,t8:27696,t9:35870,t10:73915,t11:43240,t12:65043},
{tenthuoc: "Scilin M30 (30/70)",t1:4993,t2:25825,t3:11363,t4:20699,t5:0,t6:37604,t7:0,t8:17801,t9:36136,t10:70606,t11:28357,t12:2869},
{tenthuoc: "Travatan",t1:29267,t2:1184,t3:18800,t4:40858,t5:0,t6:13231,t7:0,t8:16000,t9:50421,t10:54845,t11:34385,t12:31536},
{tenthuoc: "Humulin 70/30",t1:10356,t2:0,t3:47998,t4:69190,t5:0,t6:41868,t7:0,t8:77098,t9:54655,t10:14708,t11:44986,t12:49879},
{tenthuoc: "Lidocain 40mg/ 2ml",t1:74758,t2:0,t3:30555,t4:42564,t5:0,t6:948,t7:62875,t8:42943,t9:8551,t10:39978,t11:56740,t12:16426},
{tenthuoc: "Panadol Caplet (Paracetamol) 500mg",t1:0,t2:0,t3:12877,t4:25350,t5:0,t6:60508,t7:28345,t8:46764,t9:11903,t10:45469,t11:15398,t12:76349},
{tenthuoc: "3Bpluzs",t1:0,t2:0,t3:33941,t4:52145,t5:0,t6:6764,t7:72620,t8:33813,t9:70314,t10:33152,t11:13256,t12:72466},
{tenthuoc: "Levobact 0,5% eye drops",t1:0,t2:0,t3:62905,t4:36263,t5:0,t6:2009,t7:73254,t8:18954,t9:24390,t10:52726,t11:18891,t12:22530},
{tenthuoc: "Unasyn Inj 1500mg 1's",t1:0,t2:0,t3:77616,t4:26057,t5:0,t6:74771,t7:16621,t8:77455,t9:28960,t10:53631,t11:59050,t12:53473},
{tenthuoc: "Unasyn Inj 1500mg 1's",t1:0,t2:0,t3:57481,t4:22326,t5:0,t6:46021,t7:47256,t8:42860,t9:62539,t10:75430,t11:12882,t12:11},
{tenthuoc: "Azicine",t1:0,t2:0,t3:62044,t4:6065,t5:0,t6:47298,t7:7641,t8:71338,t9:46113,t10:31889,t11:15466,t12:58554},
{tenthuoc: "Coveram 5-5 Tab 5mg/5mg 30's",t1:0,t2:0,t3:30232,t4:49653,t5:0,t6:74247,t7:18486,t8:45478,t9:40451,t10:25798,t11:22702,t12:72307},
{tenthuoc: "Coveram Tab 5mg/5mg 30's",t1:0,t2:0,t3:69090,t4:23136,t5:0,t6:15449,t7:21839,t8:34357,t9:5398,t10:33886,t11:40362,t12:61038},
{tenthuoc: "Fudcime (Cefixim) 200mg",t1:0,t2:0,t3:9254,t4:22474,t5:3416,t6:73136,t7:55793,t8:25657,t9:10197,t10:59970,t11:66511,t12:78119},
{tenthuoc: "Medoclav 1g",t1:0,t2:0,t3:74613,t4:56073,t5:1563,t6:27109,t7:69284,t8:45695,t9:75751,t10:36853,t11:29816,t12:75819},
{tenthuoc: "Panadol caplet",t1:0,t2:0,t3:63125,t4:0,t5:70959,t6:63530,t7:58188,t8:26235,t9:50971,t10:11572,t11:79961,t12:79650},
{tenthuoc: "Scilin M30",t1:0,t2:0,t3:34269,t4:0,t5:32049,t6:25284,t7:51577,t8:12459,t9:14359,t10:76492,t11:61740,t12:53007},
{tenthuoc: "Scanneuron 300,2mg",t1:0,t2:0,t3:64627,t4:0,t5:75775,t6:57045,t7:70802,t8:74559,t9:72308,t10:65881,t11:55379,t12:35281},
{tenthuoc: "Moxilen 500mg",t1:0,t2:0,t3:67980,t4:0,t5:58316,t6:11101,t7:61414,t8:46687,t9:52541,t10:70485,t11:15759,t12:2977},
{tenthuoc: "Medopiren 500mg",t1:0,t2:0,t3:71872,t4:0,t5:20198,t6:34765,t7:36672,t8:21620,t9:53763,t10:24816,t11:53815,t12:41586},
{tenthuoc: "Bloza (Losartan) 50mg",t1:0,t2:0,t3:8501,t4:0,t5:12587,t6:43051,t7:23140,t8:49731,t9:60890,t10:79892,t11:62013,t12:25311},
{tenthuoc: "Cloramphenicol",t1:0,t2:0,t3:16134,t4:0,t5:6775,t6:54702,t7:8308,t8:49824,t9:4611,t10:61122,t11:21761,t12:5087},
{tenthuoc: "Coveram Tab 5mg/5mg 30's",t1:0,t2:0,t3:0,t4:0,t5:30225,t6:30885,t7:79518,t8:35391,t9:31983,t10:51811,t11:14884,t12:5626},
{tenthuoc: "Denxif",t1:0,t2:0,t3:0,t4:0,t5:11195,t6:41031,t7:45354,t8:52214,t9:63800,t10:39398,t11:52127,t12:6124},
{tenthuoc: "Franilax",t1:0,t2:0,t3:0,t4:0,t5:19111,t6:7854,t7:26757,t8:6513,t9:59970,t10:67865,t11:62675,t12:58830},
{tenthuoc: "Tobrex Eye Ointment",t1:0,t2:0,t3:0,t4:0,t5:65969,t6:15381,t7:66029,t8:4818,t9:4089,t10:57905,t11:60359,t12:56095},
{tenthuoc: "Ceftriaxone - Panpharma",t1:0,t2:0,t3:0,t4:0,t5:37080,t6:40153,t7:52415,t8:58259,t9:36984,t10:49030,t11:9300,t12:76262},
{tenthuoc: "Scilin M30 400IU",t1:0,t2:0,t3:0,t4:0,t5:17499,t6:52186,t7:24339,t8:58606,t9:63365,t10:47594,t11:57546,t12:68010},
{tenthuoc: "Mixtard 30 FlexPen",t1:0,t2:0,t3:0,t4:23648,t5:28823,t6:24844,t7:30911,t8:1818,t9:53312,t10:64834,t11:76486,t12:63266},
{tenthuoc: "Moxilen 500mg",t1:0,t2:0,t3:0,t4:49996,t5:33923,t6:66872,t7:65786,t8:13588,t9:14782,t10:67081,t11:6683,t12:63042},
{tenthuoc: "Hoạt huyết thông mạch P/H",t1:0,t2:0,t3:0,t4:42297,t5:8800,t6:39420,t7:23290,t8:66009,t9:23161,t10:28328,t11:61866,t12:41291},
{tenthuoc: "Heparin (Paringold) 25000IU/5ml",t1:0,t2:0,t3:0,t4:951,t5:50456,t6:10753,t7:52813,t8:49501,t9:39580,t10:13080,t11:27285,t12:69668},
{tenthuoc: "Mecefix-B.E",t1:0,t2:0,t3:0,t4:54800,t5:46303,t6:75558,t7:51573,t8:842,t9:31364,t10:28367,t11:25209,t12:16319},
{tenthuoc: "Mecefix-B.E",t1:0,t2:0,t3:0,t4:10430,t5:70175,t6:75658,t7:20822,t8:68295,t9:7342,t10:51250,t11:25028,t12:35094},
{tenthuoc: "Augmentin BD Tab 625mg 14's",t1:0,t2:0,t3:0,t4:55139,t5:49983,t6:7056,t7:70359,t8:13956,t9:7392,t10:55400,t11:48273,t12:70282},
{tenthuoc: "Nifedipin Hasan 20 Retard",t1:0,t2:0,t3:0,t4:79525,t5:45863,t6:36396,t7:18627,t8:73288,t9:29894,t10:23042,t11:345,t12:8041},
{tenthuoc: "Exforge HCT 10/160/12.5mg",t1:0,t2:0,t3:0,t4:12951,t5:79719,t6:33105,t7:77534,t8:16152,t9:37019,t10:72092,t11:2604,t12:46693},
{tenthuoc: "Enaminal (elalapril 10mg)",t1:0,t2:0,t3:0,t4:36221,t5:24435,t6:75274,t7:20284,t8:49493,t9:54494,t10:11514,t11:72063,t12:13062},
{tenthuoc: "Enamigal 5mg",t1:0,t2:0,t3:0,t4:12453,t5:62054,t6:10805,t7:4911,t8:66038,t9:17887,t10:71291,t11:55142,t12:14618},
{tenthuoc: "Tobrin 0,3%/5ml",t1:0,t2:0,t3:0,t4:36876,t5:59487,t6:19364,t7:56939,t8:60335,t9:15585,t10:55111,t11:15297,t12:23695},
{tenthuoc: "Enamigal (enalapril 5mg)",t1:0,t2:0,t3:0,t4:60354,t5:47178,t6:42551,t7:50142,t8:33508,t9:53176,t10:40296,t11:5321,t12:40412},
{tenthuoc: "Enap (Enalapril) 5mg",t1:0,t2:0,t3:0,t4:5953,t5:39111,t6:6871,t7:45125,t8:45117,t9:16759,t10:13466,t11:47707,t12:14998},
{tenthuoc: "Medopiren 500mg",t1:0,t2:0,t3:0,t4:67175,t5:77214,t6:0,t7:12998,t8:15748,t9:75132,t10:41787,t11:33376,t12:38963},
{tenthuoc: "Osaki",t1:0,t2:0,t3:0,t4:71503,t5:78445,t6:31768,t7:31673,t8:52520,t9:41001,t10:31277,t11:70153,t12:52751},
{tenthuoc: "Bloza",t1:0,t2:0,t3:0,t4:23930,t5:1650,t6:18380,t7:79764,t8:22296,t9:8336,t10:16675,t11:64044,t12:58228},
{tenthuoc: "Bloza",t1:0,t2:0,t3:0,t4:66024,t5:9364,t6:59174,t7:23772,t8:4617,t9:11629,t10:14231,t11:58680,t12:2411},
{tenthuoc: "Novomix 30 Flexpen",t1:0,t2:0,t3:0,t4:55350,t5:7462,t6:3223,t7:59144,t8:20172,t9:74191,t10:31948,t11:4915,t12:0}];

var lstNgayHH = [{tenthuoc: "Aristin - C",year:2019,month:"Aug",day:7,tenHC:"Ciprofloxacin"},
{tenthuoc: "Vimotram",year:2019,month:"Nov",day:22,tenHC:"Amoxicilin+Suib actam"},
{tenthuoc: "Xalvobin 500mg film coated-tablet",year:2019,month:"Aug",day:15,tenHC:"Capecitabin"},
{tenthuoc: "Paxus PM 100 mg",year:2019,month:"Dec",day:8,tenHC:"Paclitaxel"},
{tenthuoc: "Duoplavin 75mg+100mg",year:2019,month:"Oct",day:31,tenHC:"Acetylsalic acid + Clopidogrel"},
{tenthuoc: "Meronem Inj 1g 10's",year:2019,month:"Oct",day:5,tenHC:"Meropenem"},
{tenthuoc: "Rifaxon",year:2019,month:"Oct",day:28,tenHC:"Paracetamol"},
{tenthuoc: "Taxotere 80mg/1ml",year:2019,month:"Dec",day:24,tenHC:"Docetacel"},
{tenthuoc: "Concor 5mg",year:2019,month:"Dec",day:4,tenHC:"Bisoprolol"},
{tenthuoc: "Scilin M30 (30/70)",year:2019,month:"Dec",day:13,tenHC:"Insulin trộn"},
{tenthuoc: "REDITUX",year:2019,month:"Oct",day:1,tenHC:"Rituximab"},
{tenthuoc: "Scilin N",year:2019,month:"Nov",day:11,tenHC:"Insulin tác dụng trung bình, trung gian"},
{tenthuoc: "Ebitac 25",year:2019,month:"Sep",day:3,tenHC:"Enalapril maleat + Hydrochlorothiazid"},
{tenthuoc: "Taxotere 20mg/1ml",year:2019,month:"Aug",day:18,tenHC:"Docetacel"},
{tenthuoc: "Heparin 25000 UI/5ml",year:2019,month:"Jun",day:11,tenHC:"Heparin (Natri)"},
{tenthuoc: "Unasyn Inj 1500mg 1's",year:2019,month:"Jun",day:2,tenHC:"Ampicilin + sulbactam"},
{tenthuoc: "Rocephin 1g I.V.",year:2019,month:"Dec",day:23,tenHC:"Ceftriaxon"},
{tenthuoc: "Paclitaxelum Actavis",year:2019,month:"Aug",day:16,tenHC:"Paclitaxel"},
{tenthuoc: "Coveram 5-5 Tab 5mg/5mg 30's",year:2019,month:"Nov",day:20,tenHC:"Perindopril + amlodipin"},
{tenthuoc: "Coveram 5-5 Tab 5mg/5mg 30's",year:2019,month:"Sep",day:3,tenHC:"Perindopril Arginine 5mg, Amlodipine besylate 5mg"},
{tenthuoc: "Tiepanem 1g",year:2019,month:"Jun",day:4,tenHC:"Meropenem"},
{tenthuoc: "PAXUS PM 30MG",year:2019,month:"Dec",day:30,tenHC:"Paclitacel-Polymeric Micelle"},
{tenthuoc: "Meiunem 0,5g",year:2019,month:"Nov",day:8,tenHC:"Meropenem"},
{tenthuoc: "Anzatax",year:2019,month:"Aug",day:22,tenHC:"Paclitacel"},
{tenthuoc: "Tadocel 80mg/2ml",year:2019,month:"Jun",day:30,tenHC:"Docetaxel"},
{tenthuoc: "Ceftriaxone panpharma",year:2019,month:"Sep",day:8,tenHC:"Ceftriaxon"},
{tenthuoc: "Proxacin 1%",year:2019,month:"Sep",day:2,tenHC:"Ciprofloxacin"},
{tenthuoc: "Paxus PM",year:2019,month:"Aug",day:30,tenHC:"Paclitaxel"},
{tenthuoc: "Meronem 1g",year:2019,month:"Dec",day:22,tenHC:"Meropenem"},
{tenthuoc: "Paxus PM 30 mg",year:2019,month:"Dec",day:3,tenHC:"Paclitaxel"},
{tenthuoc: "Polhumin mix-2",year:2019,month:"Nov",day:20,tenHC:"Insulin trộn, hỗn hợp (Mixtard-acting, Dual- acting)"},
{tenthuoc: "Unasyn INJ",year:2019,month:"Dec",day:5,tenHC:"Ampicilin + sulbactam"},
{tenthuoc: "Klamentin 1g",year:2019,month:"Nov",day:24,tenHC:"Amoxicilin + Acid clavulanic"},
{tenthuoc: "Vigamox",year:2019,month:"Oct",day:23,tenHC:"Moxifloxacin"},
{tenthuoc: "Rocephin",year:2019,month:"Jul",day:22,tenHC:"Ceftriaxone"},
{tenthuoc: "Twynsta 40+5mg",year:2019,month:"Jul",day:1,tenHC:"Telmisartan + Amlodipine"},
{tenthuoc: "Ebitac 12.5",year:2019,month:"Oct",day:17,tenHC:"Enalapril+ Hydrochlorothiazid"},
{tenthuoc: "Bupivacaine 20mg 0.5% 5 mg/ml 0,5%/4ml",year:2019,month:"Nov",day:7,tenHC:"Bupivacain (hydroclorid)"},
{tenthuoc: "Mixtard 30 Flexpen 100IU/ml x 3ml",year:2019,month:"Jul",day:20,tenHC:"Insulin người"},
{tenthuoc: "Levogol",year:2019,month:"Oct",day:31,tenHC:"Levofloxacin"},
{tenthuoc: "Fudcime 200mg",year:2019,month:"Aug",day:11,tenHC:"Cefixim"},
{tenthuoc: "Levoflex",year:2019,month:"Jun",day:10,tenHC:"Levofloxacin"},
{tenthuoc: "Vitamin B1-B6-B12",year:2019,month:"Nov",day:17,tenHC:"Vitamin B1+Vitamin B6+Vitamin B12."},
{tenthuoc: "Insulin H Mix 100IU",year:2019,month:"Jun",day:22,tenHC:"Insulin trộn, hỗn hợp (Mixtard-acting, Dual-acting)"},
{tenthuoc: "Lovenox 0,6ml,6000UI",year:2019,month:"Jul",day:1,tenHC:"Enoxaparin (natri)"},
{tenthuoc: "Unasyn Inj 1500mg 1's Ampicillin 1g, Sulba",year:2019,month:"Jun",day:24,tenHC:"Ampicillin- sulbactam"},
{tenthuoc: "Tadocel 20mg/0,5ml",year:2019,month:"Dec",day:12,tenHC:"Docetacel"},
{tenthuoc: "PROPOFOL 1% KABI",year:2019,month:"Dec",day:11,tenHC:"Propofol"},
{tenthuoc: "Cerecaps",year:2019,month:"Dec",day:7,tenHC:"Cao khô hồng hoa +cao khô đương quy +cao khô sinh địa +cao khô sài hồ +cao khô cam thảo +cao khô xuyên khung +cao khô xích thược +cao khô chỉ xác +cao khô ngưu tất +cao khô bạch quả"},
{tenthuoc: "Infartan 75",year:2019,month:"Sep",day:14,tenHC:"Clopidogrel"},
{tenthuoc: "Mixtard 30 FlexPen",year:2019,month:"Dec",day:28,tenHC:"Insulin trộn, hỗn hợp (Mixtard-acting, Dual- acting)"},
{tenthuoc: "Ceftriaxone  Panpharma [Chỉ dành cho kho Nhi]",year:2019,month:"Sep",day:1,tenHC:"Ceftriaxone"},
{tenthuoc: "Xeloda",year:2019,month:"Nov",day:3,tenHC:"Capecitabin"},
{tenthuoc: "Rocephin 1g",year:2019,month:"Oct",day:25,tenHC:"Ceftriaxon"},
{tenthuoc: "Scilin M30 400IU",year:2019,month:"Jul",day:11,tenHC:"Insulin trộn (M) 30/70 400IU/10ml"},
{tenthuoc: "Scilin N 400UI/10ml",year:2019,month:"Sep",day:5,tenHC:"Insulin chậm, kéo dài"},
{tenthuoc: "Sanbeclaneksi (Amoxicillin+acid clavulanic) 1.2g",year:2019,month:"Nov",day:16,tenHC:"Amoxicilin+Acid Clavulanic"},
{tenthuoc: "Bloza",year:2019,month:"Dec",day:29,tenHC:"Losartan"},
{tenthuoc: "Heparin (natri) 5ml.5000UI/ml",year:2019,month:"Oct",day:20,tenHC:"Heparin"},
{tenthuoc: "Twynsta",year:2019,month:"Oct",day:19,tenHC:"Telmisartan + Amlodipine"},
{tenthuoc: "Adalat LA Tab 20mg 30's",year:2019,month:"Jul",day:21,tenHC:"Nifedipine 20 mg"},
{tenthuoc: "Bibiso",year:2019,month:"Sep",day:22,tenHC:"Cao Actiso, Cao rau đắng đất, Cao bìm bìm"},
{tenthuoc: "Amlor 5mg",year:2019,month:"Jul",day:12,tenHC:"Amlodipin 5mg"},
{tenthuoc: "LOVENOX 40mg Inj B/ 2 syringes x0,4ml",year:2019,month:"Sep",day:10,tenHC:"Enoxaparin natri"},
{tenthuoc: "Meronem Inj 500mg 10's",year:2019,month:"Aug",day:13,tenHC:"Meropenem"},
{tenthuoc: "Merugold I.V 1g",year:2019,month:"Oct",day:7,tenHC:"Meropenem"},
{tenthuoc: "Remeclar 500",year:2019,month:"Nov",day:30,tenHC:"Clarithromycin"},
{tenthuoc: "Scanneuron",year:2019,month:"Oct",day:22,tenHC:"Vitamin B1 + Vitamin B6 + Vitamin B12"},
{tenthuoc: "DBL Irinotecan Injection 100mg/5ml",year:2019,month:"Nov",day:30,tenHC:"Irinotecan"},
{tenthuoc: "Fudcime",year:2019,month:"Oct",day:21,tenHC:"Cefixim"},
{tenthuoc: "Novomix 30 Flexpen",year:2019,month:"Aug",day:10,tenHC:"Insulin aspart Biphasic (DNA tái tổ hợp)"},
{tenthuoc: "Indocollyre",year:2019,month:"Dec",day:12,tenHC:"Indomethacin"},
{tenthuoc: "Sotstop",year:2019,month:"Jun",day:8,tenHC:"Ibuprofen"},
{tenthuoc: "Amlor cap",year:2019,month:"Jun",day:6,tenHC:"Amlodipin"},
{tenthuoc: "Systan Ultra",year:2019,month:"Oct",day:28,tenHC:"Polyethylen glycol + Propylen glycol"},
{tenthuoc: "Meiunem (meropenem) 500mg",year:2019,month:"Sep",day:7,tenHC:"Meropenem"},
{tenthuoc: "Cloramphenicol",year:2019,month:"Dec",day:18,tenHC:"Cloramphenicol"},
{tenthuoc: "Tartriakson (Ceftriaxon) 1g",year:2019,month:"Dec",day:8,tenHC:"Ceftriaxon"},
{tenthuoc: "Toxaxine Inj",year:2019,month:"Dec",day:29,tenHC:"Tranexamic acid"},
{tenthuoc: "Cozaar",year:2019,month:"Nov",day:17,tenHC:"Losartan"},
{tenthuoc: "Hometex",year:2019,month:"Jun",day:30,tenHC:"Cao đặc actiso"},
{tenthuoc: "Thiên sứ hộ tâm đan",year:2019,month:"Jun",day:28,tenHC:"Cao Đan Sâm, Cao Tam Thất, Borneol"},
{tenthuoc: "Mecefix (cefixim 150mg)",year:2019,month:"Nov",day:19,tenHC:"Cefixim"},
{tenthuoc: "Oflovid",year:2019,month:"Jul",day:1,tenHC:"Ofloxacin"},
{tenthuoc: "Concor Tab 5mg 30's",year:2019,month:"Aug",day:11,tenHC:"Bisoprolol"},
{tenthuoc: "Campto Inj 100mg 5ml",year:2019,month:"Jun",day:1,tenHC:"Irinotecan"},
{tenthuoc: "LOVENOX 60mg Inj B/ 2 syringes x  0,6ml",year:2019,month:"Nov",day:17,tenHC:"Enoxaparin"},
{tenthuoc: "Lisonorm 5mg+10mg",year:2019,month:"Oct",day:4,tenHC:"Amlodipin + Lisinopril"},
{tenthuoc: "EDNYT 10MG Tablet(s) nén",year:2019,month:"Nov",day:1,tenHC:"Enalapril"},
{tenthuoc: "Albis",year:2019,month:"Dec",day:5,tenHC:"Ranitidin + bismuth + sucralfat"},
{tenthuoc: "Paracetamol",year:2019,month:"Jul",day:30,tenHC:"Paracetamol"},
{tenthuoc: "Bactirid 40ml",year:2019,month:"Jun",day:18,tenHC:"Cefixim"},
{tenthuoc: "Adalat LA Tab 30mg 30's",year:2019,month:"Nov",day:27,tenHC:"Nifedipin"},
{tenthuoc: "Vitamin B1+B6+B12",year:2019,month:"Jul",day:10,tenHC:"Vitamin B1+ vitamin B6+ vitamin B12 (115mg+115mg+50mcg)"},
{tenthuoc: "Bisoprolol fumarat",year:2019,month:"Aug",day:13,tenHC:"Bisoprolol"},
{tenthuoc: "Propofol 1%/20mL.",year:2019,month:"Sep",day:23,tenHC:"Propofol"},
{tenthuoc: "Zitromax 600mg/15ml",year:2019,month:"Sep",day:15,tenHC:"Azithromycin"},
{tenthuoc: "Heparin 25000UI/5ml",year:2019,month:"Aug",day:24,tenHC:"Heparin (Natri)"},
{tenthuoc: "Tartriakson 1g",year:2019,month:"Jul",day:28,tenHC:"Ceftriaxon"},
{tenthuoc: "CIPROFLOXACIN KABI",year:2019,month:"Oct",day:4,tenHC:"Ciprofloxacin"},
{tenthuoc: "Oflovid 3mg/ml x 5ml",year:2019,month:"Jul",day:24,tenHC:"Ofloxacin"},
{tenthuoc: "PLAVIX 75mg B/ 1bl x 14 Tabs",year:2019,month:"Nov",day:14,tenHC:"Clopidogrel"},
{tenthuoc: "Travatan",year:2019,month:"Aug",day:28,tenHC:"Travoprost"},
{tenthuoc: "Vitamin B1",year:2019,month:"Aug",day:15,tenHC:"vitamin B1"},
{tenthuoc: "Bidilucil 500",year:2019,month:"Oct",day:30,tenHC:"Meclofenoxat hydroclorid"},
{tenthuoc: "Lidocain 2% 10ml",year:2019,month:"Nov",day:23,tenHC:"Lidocain (hydroclorid)"},
{tenthuoc: "Enamigal",year:2019,month:"Sep",day:11,tenHC:"Enalapril"},
{tenthuoc: "Mixtard Flexpen 30/70 100UI/ml,3ml",year:2019,month:"Jun",day:17,tenHC:"Insulin trộn (M) 30:70"},
{tenthuoc: "Enap (Enalapril) 5mg",year:2019,month:"Jul",day:21,tenHC:"Enalapril"},
{tenthuoc: "Tobradex 0.3% 3.5g",year:2019,month:"Sep",day:19,tenHC:"Tobramycin+Dexamethason"},
{tenthuoc: "Plofed (Propofol) 1%/20ml",year:2019,month:"Jul",day:8,tenHC:"Propofol"},
{tenthuoc: "Panadol caplet 500mg",year:2019,month:"Aug",day:25,tenHC:"Paracetamol (acetaminophen)"},
{tenthuoc: "Bloza (Losartan) 50mg",year:2019,month:"Sep",day:13,tenHC:"Losartan"},
{tenthuoc: "Duotrav",year:2019,month:"Nov",day:6,tenHC:"Travoprost+Timolol"},
{tenthuoc: "3bpluzs (vitamin b1+b6+b12)",year:2019,month:"Jun",day:12,tenHC:"vitamin b1+b6+b12"},
{tenthuoc: "Utrogestan 100mg",year:2019,month:"Nov",day:7,tenHC:"Progesteron"},
{tenthuoc: "Tobradex",year:2019,month:"Dec",day:21,tenHC:"Tobramycin + Dexamethasone"},
{tenthuoc: "Tobrin 0,3%/5ml",year:2019,month:"Nov",day:14,tenHC:"Tobramycin"},
{tenthuoc: "Paclitaxel 100mg/16,7ml",year:2019,month:"Jul",day:17,tenHC:"Paclitaxel"},
{tenthuoc: "Morphini Sulfas Wzf 0,1% 2mg 2ml Spinal 2mg/2ml",year:2019,month:"Sep",day:17,tenHC:"Morphin sulfat"},
{tenthuoc: "Curam 625mg",year:2019,month:"Aug",day:10,tenHC:"Amoxicilin+Acid Clavulanic"},
{tenthuoc: "Dalacin C Cap 300mg 16's",year:2019,month:"Jun",day:16,tenHC:"Clindamycin"},
{tenthuoc: "DUOPLAVIN 75/100mg B/ 3bls  x 10 Tabs",year:2019,month:"Nov",day:19,tenHC:"Clopidogrel + acetylsalicylic"},
{tenthuoc: "Humulin 70/30",year:2019,month:"Aug",day:6,tenHC:"Insulin người (rDNA)"},
{tenthuoc: "Neo-Tergynan",year:2019,month:"Sep",day:24,tenHC:"Metronidazol +Neomycin+ Nystatin"},
{tenthuoc: "Mixtard 30 flexpen (insulin human)",year:2019,month:"Dec",day:7,tenHC:"Insulin người, rADN"},
{tenthuoc: "Tobrex Eye Ointment 3,5g",year:2019,month:"Nov",day:23,tenHC:"Tobramycin"},
{tenthuoc: "Lovenox 0,4ml,4000UI",year:2019,month:"Sep",day:4,tenHC:"Enoxaparin (natri)"},
{tenthuoc: "Vigadexa 5ml",year:2019,month:"Sep",day:26,tenHC:"Moxifloxacin + Dexamethason"},
{tenthuoc: "Plofed",year:2019,month:"Oct",day:13,tenHC:"Propofol"},
{tenthuoc: "Levonor 1mg/ml H10",year:2019,month:"Dec",day:14,tenHC:"Nor epinephrin (Nor adrenalin)"},
{tenthuoc: "Mecefix (Cefixim) 150mg",year:2019,month:"Aug",day:23,tenHC:"Cefixim"},
{tenthuoc: "Heparin Belmed",year:2019,month:"Dec",day:18,tenHC:"Heparin"},
{tenthuoc: "Clocardigel",year:2019,month:"Aug",day:1,tenHC:"Clopidogrel"},
{tenthuoc: "Unasyn",year:2019,month:"Nov",day:9,tenHC:"Ampicillin + Sulbactam"},
{tenthuoc: "Systane Ultra 5ml",year:2019,month:"Dec",day:13,tenHC:"Polyethylen glycol+ Propylen glycol"},
{tenthuoc: "Seduxen 5mg",year:2019,month:"Jun",day:8,tenHC:"Diazepam"},
{tenthuoc: "I.V-globulin S 2500mg",year:2019,month:"Dec",day:12,tenHC:"I.V-globulin S 2500mg"},
{tenthuoc: "Ambelin 5mg",year:2019,month:"Oct",day:4,tenHC:"Amlodipin"},
{tenthuoc: "Asgizole (Esomeprazol) 40mg",year:2019,month:"Aug",day:23,tenHC:"Esomeprazole"},
{tenthuoc: "Oflovid Oint",year:2019,month:"Jun",day:4,tenHC:"Ofloxacin"},
{tenthuoc: "Taxotere (Docetaxel) 20mg/1ml",year:2019,month:"Jul",day:28,tenHC:"Docetaxel"},
{tenthuoc: "Campto (Irinotecan) 40mg",year:2019,month:"Dec",day:17,tenHC:"Irinotecan"},
{tenthuoc: "Transamin Injection",year:2019,month:"Aug",day:18,tenHC:"Tranexamic acid"},
{tenthuoc: "Ebitac",year:2019,month:"Oct",day:7,tenHC:"Enalapril maleat+ Hydroclorothiazid"},
{tenthuoc: "Medoclav 1g 875mg+ 125mg",year:2019,month:"Nov",day:23,tenHC:"Amoxycillin + Acid Clavulanic"},
{tenthuoc: "Clarithromycin 500mg",year:2019,month:"Sep",day:10,tenHC:"Clarithromycin"},
{tenthuoc: "VG5",year:2019,month:"Aug",day:8,tenHC:"Cao diệp hạ châu, cao nhân trần, cao nhọ nồi, cao râu bắp"},
{tenthuoc: "Levobact 0,5% eye drops",year:2019,month:"Sep",day:19,tenHC:"Levofloxacin"},
{tenthuoc: "Tobiwel",year:2019,month:"Sep",day:21,tenHC:"Natri chondroitin sulfat + retinol palmitat + cholin hydrotartrat + riboflavin (vitamin B2) + thiamin hydroclorid (vitamin B1)"},
{tenthuoc: "Enap 5",year:2019,month:"Jun",day:19,tenHC:"Enalapril"},
{tenthuoc: "Aerius Syr 0.5mg/ ml 1's",year:2019,month:"Jul",day:30,tenHC:"Desloratadine"},
{tenthuoc: "Losartan 25mg",year:2019,month:"Dec",day:26,tenHC:"Losartan"},
{tenthuoc: "Vigamox 0,5%/5ml",year:2019,month:"Dec",day:18,tenHC:"Moxifloxacin"},
{tenthuoc: "Tranecid 250",year:2019,month:"Nov",day:9,tenHC:"Tranexamic acid"},
{tenthuoc: "Exforge HCT 10mg/160mg/12.5mg",year:2019,month:"Oct",day:19,tenHC:"Amlodipin+Valsartan+Hydrochlorothiazide"},
{tenthuoc: "Clarithromycin 250mg",year:2019,month:"Nov",day:4,tenHC:"Clarithromycin 250mg"},
{tenthuoc: "Vitamin E",year:2019,month:"Aug",day:15,tenHC:"Vitamin E"},
{tenthuoc: "Paclitaxel  Ebewe ",year:2019,month:"Dec",day:12,tenHC:"Paclitaxel"},
{tenthuoc: "Medsamic 250mg/5ml",year:2019,month:"Jul",day:21,tenHC:"Tranexamic acid"},
{tenthuoc: "Hoạt huyết dưỡng não Cebraton S",year:2019,month:"Oct",day:4,tenHC:"Cao đinh lăng, cao bạch quả"},
{tenthuoc: "Midactam",year:2019,month:"Aug",day:19,tenHC:"Ampicilin +Subactam"},
{tenthuoc: "Midactam 1,5g",year:2019,month:"Jun",day:21,tenHC:"Ampicilin +Sulbactam"},
{tenthuoc: "Novorapid Flexpen 100U/ml",year:2019,month:"Oct",day:2,tenHC:"Insulin tác dụng ngắn (S) (Insulin aspart)"},
{tenthuoc: "Diopolol (Bisoprolol) 5mg",year:2019,month:"Sep",day:24,tenHC:"Bisoprolol"},
{tenthuoc: "Transamin Injection 250mg /5ml",year:2019,month:"Aug",day:22,tenHC:"Tranexamic acid"},
{tenthuoc: "Fabamox 500",year:2019,month:"Dec",day:8,tenHC:"Amoxicilin"},
{tenthuoc: "Klamentin 250",year:2019,month:"Sep",day:28,tenHC:"Amoxicilin + Acidclavulanic"},
{tenthuoc: "Klamentin 250",year:2019,month:"Aug",day:5,tenHC:"Amoxicilin + Acid clavulanic"},
{tenthuoc: "Systane Ultra",year:2019,month:"Jul",day:1,tenHC:"Polyethylene glycol 400 + Propylen glycol"},
{tenthuoc: "Tobrex 0.3% 5ml",year:2019,month:"Nov",day:1,tenHC:"Tobramycin"},
{tenthuoc: "Anginovag 10ml",year:2019,month:"Aug",day:30,tenHC:"Beta-glycyrrhetinic acid + Dequalinium chlorid + Tyrothricin + Hydrocortison acetat + Lidocain hydrochlorid"},
{tenthuoc: "Anginovag 10ml",year:2019,month:"Jul",day:10,tenHC:"Beta-glycyrrhetinic acid +dequalinium clorid + tyrothricin+ hydrocortison acetat +lidocain hydroclorid"},
{tenthuoc: "Esomeprazol Stada 40mg",year:2019,month:"Dec",day:21,tenHC:"Esomeprazole"},
{tenthuoc: "Cesyrup",year:2019,month:"Oct",day:29,tenHC:"Vitamin C"},
{tenthuoc: "Erolin 10mg (Loratadin)",year:2019,month:"Dec",day:22,tenHC:"Loratadin"},
{tenthuoc: "Vitamin C",year:2019,month:"Sep",day:22,tenHC:"Vitamin C"},
{tenthuoc: "Mediplex",year:2019,month:"Jul",day:17,tenHC:"Aciclovir"},
{tenthuoc: "Cefdyvax-200",year:2019,month:"Dec",day:12,tenHC:"Cefixim"},
{tenthuoc: "Losec Mups Tab",year:2019,month:"Jul",day:25,tenHC:"Omeprazole"},
{tenthuoc: "Hydrocortison",year:2019,month:"Aug",day:15,tenHC:"Hydrocortison"},
{tenthuoc: "Hydrocortison",year:2019,month:"Jun",day:22,tenHC:"Lidocain (hydroclorid) + Hydrocortison"},
{tenthuoc: "Transamin 250mg/5ml",year:2019,month:"Oct",day:24,tenHC:"Tranexamic acid"},
{tenthuoc: "Amlaxopin 10mg",year:2019,month:"Jul",day:30,tenHC:"Amlodipin"},
{tenthuoc: "Bestdocel 20",year:2019,month:"Nov",day:19,tenHC:"Docetaxel"},
{tenthuoc: "Hydrocortion- Lidocain- Richter",year:2019,month:"Oct",day:16,tenHC:"Hydrocortison acetat + Lidocain (Hydroclorid)"},
{tenthuoc: "Refresh Tears Lubricant eye Drops 15ml",year:2019,month:"Aug",day:26,tenHC:"Natri carboxymethylcellulose"},
{tenthuoc: "Refresh Tears Lubricant eye Drops 15ml",year:2019,month:"Aug",day:14,tenHC:"Natricarboxy methylcellulose"},
{tenthuoc: "Refresh Tears Lubricant eye Drops 15ml",year:2019,month:"Jul",day:22,tenHC:"Natri carboxy methylcellulose"},
{tenthuoc: "Imidu 60mg",year:2019,month:"Jun",day:3,tenHC:"Isosorbid 5 - mononitrat"},
{tenthuoc: "Nexium 40 mg",year:2019,month:"Dec",day:25,tenHC:"Esomeprazole"},
{tenthuoc: "Cerecaps(Hoạt huyết dưỡng não )",year:2019,month:"Jul",day:9,tenHC:"Cao khô hỗn hợp (tương ứng với :Hồng hoaĐương quyXuyên khungSinh địaCam thảoXích thượcSài hồChỉ xácNgưu tất) Bạch quả"},
{tenthuoc: "Bổ gan P/H",year:2019,month:"Dec",day:25,tenHC:"Cao đặc Diệp hạ châu; Cao đặc Bồ bồ; Cao đặc Chi tử."},
{tenthuoc: "Maxitrol",year:2019,month:"Nov",day:25,tenHC:"Dexamethasone Sulfate + Neomycin Sulfate + Polymycin B Sulfate"},
{tenthuoc: "Maxitrol",year:2019,month:"Aug",day:4,tenHC:"Dexamethason+ Neomycin+ Polymycin B"},
{tenthuoc: "Maxitrol",year:2019,month:"Oct",day:20,tenHC:"Neomycin + polymyxin B + dexamethason"},
{tenthuoc: "Rocephin 250mg I.V 250mg",year:2019,month:"Oct",day:13,tenHC:"Ceftriaxone"},
{tenthuoc: "Hyzaar Tab",year:2019,month:"Jun",day:1,tenHC:"Losartan + Hydrochlothiazide"},
{tenthuoc: "Paclispec (Paclitaxel) 30mg/5ml",year:2019,month:"Aug",day:30,tenHC:"Paclitaxel"},
{tenthuoc: "Amohexine",year:2019,month:"Sep",day:18,tenHC:"Amoxicilin + Bromhexin"},
{tenthuoc: "Lidocain",year:2019,month:"Jun",day:14,tenHC:"Lidocain (hydroclorid)"},
{tenthuoc: "Lidocain",year:2019,month:"Jun",day:16,tenHC:"Lidocain"},
{tenthuoc: "Lidocain",year:2019,month:"Jun",day:2,tenHC:"Lidocain ( hydroclorid)"},
{tenthuoc: "Lidocain",year:2019,month:"Sep",day:23,tenHC:"Lidocaine"},
{tenthuoc: "Augmentin 250/31.25 Sac 250mg 12's",year:2019,month:"Oct",day:30,tenHC:"Amoxicillin  250mg, Acid Clavulanic  31.25mg"},
{tenthuoc: "Campto Inj 40mg 2ml",year:2019,month:"Aug",day:29,tenHC:"Irinotecan"},
{tenthuoc: "Mitotax 30mg",year:2019,month:"Oct",day:22,tenHC:"Paclitaxel"},
{tenthuoc: "Usalota",year:2019,month:"Nov",day:24,tenHC:"Loratadin"},
{tenthuoc: "Danotan 100mg/ml",year:2019,month:"Nov",day:26,tenHC:"Phenobarbital"},
{tenthuoc: "Đại tràng hoàn P/H",year:2019,month:"Aug",day:6,tenHC:"Bột Bạch truật; Bột Mộc hương; Bột Hoàng đằng; Bột Hoài sơn; Bột Trần bì; Bột Hoàng liên; Bột Bạch linh; Bột Sa nhân; Bột Bạch thược; Cao đặc Cam thảo; Cao đặc Đảng sâm."},
{tenthuoc: "Gardenal 100mg V10",year:2019,month:"Jul",day:19,tenHC:"Phenobacbital"},
{tenthuoc: "Gardenal 100mg V10",year:2019,month:"Sep",day:7,tenHC:"Phenobarbital"},
{tenthuoc: "Transamin Inj",year:2019,month:"Aug",day:31,tenHC:"Tranexamic acid"},
{tenthuoc: "Rolnadez",year:2019,month:"Dec",day:12,tenHC:"Tamoxifen"},
{tenthuoc: "Cozaar XQ (Amlodipin+Losartan) (5+100)mg",year:2019,month:"Jul",day:8,tenHC:"Amlodipin + Losartan"},
{tenthuoc: "Ebelgiumtac 12.5",year:2019,month:"Dec",day:22,tenHC:"Enalapril maleat +Hydroclorothiazid"},
{tenthuoc: "Azicine  250mg (Sacchet(s))",year:2019,month:"Nov",day:29,tenHC:"Azithromycin"},
{tenthuoc: "Thấp khớp hoàn P/H",year:2019,month:"Jun",day:12,tenHC:"Cao Tần giao; Cao Đỗ trọng; Cao Ngưu tất; Cao Độc hoạt; Bột Phòng phong; Bột Phục linh; Bột Xuyên khung;  Bột Tục đoạn; Bột Hoàng kỳ; Bột Bạch thược; Bột Cam thảo; Bột Đương quy; Bột Thiên niên kiện."},
{tenthuoc: "Thấp khớp hoàn P/H",year:2019,month:"Jun",day:2,tenHC:"Cao tần giaoCao đỗ trọngCao ngưu tấtCao độc hoạtBột phòng phongBột phục linhBột xuyên khungBột tục đoạnBột hoàng kỳBột bạch thượcBột cam thảoBột đương quyBột thiên niên kiện"},
{tenthuoc: "Danotan",year:2019,month:"Sep",day:16,tenHC:"Phenobarbital"},
{tenthuoc: "EDNYT 5MG",year:2019,month:"Oct",day:9,tenHC:"Enalapril"},
{tenthuoc: "Crila Forte",year:2019,month:"Dec",day:28,tenHC:"Cao khô Trinh nữ hoàng cung"},
{tenthuoc: "Caldihasan",year:2019,month:"Jul",day:24,tenHC:"Calci carbonat+ Vitamin D3 1250mg+125UI"},
{tenthuoc: "Caldihasan",year:2019,month:"Nov",day:22,tenHC:"Calci carbonat + Vitamin D3"},
{tenthuoc: "Caldihasan",year:2019,month:"Aug",day:4,tenHC:"Calci cacbonat+ Vitamin D3"},
{tenthuoc: "Liposic",year:2019,month:"Sep",day:22,tenHC:"Carbomer"},
{tenthuoc: "Noradrenalin 1mg/1ml",year:2019,month:"Dec",day:2,tenHC:"Nor epinephrin (Nor adrenalin)"},
{tenthuoc: "Propofol 200 mg/ống 20mL",year:2019,month:"Aug",day:11,tenHC:"PROPOFOL 1% KABI"},
{tenthuoc: "Amogentine",year:2019,month:"Jul",day:12,tenHC:"Amoxicilin+Acid Clavulanic"},
{tenthuoc: "Profol 1%",year:2019,month:"Dec",day:1,tenHC:"Propofol"},
{tenthuoc: "CORDARONE 200mg B/   2bls x 15 Tabs",year:2019,month:"Nov",day:25,tenHC:"Amiodarone hydrochloride"},
{tenthuoc: "CORDARONE 200mg B/   2bls x 15 Tabs",year:2019,month:"Nov",day:21,tenHC:"Amiodaron (hydrochlorid)"},
{tenthuoc: "Humulin R 100IU 10ml",year:2019,month:"Jun",day:3,tenHC:"Insulin tác dụng nhUK, ngắn"},
{tenthuoc: "Humulin R 100IU 10ml",year:2019,month:"Sep",day:21,tenHC:"Insulin tác dụng nhanh, ngắn"},
{tenthuoc: "Humulin R 100IU 10ml",year:2019,month:"Jun",day:7,tenHC:"Insulin tác dụng nhanh, ngắn (Fast-acting, Short-acting)"},
{tenthuoc: "Humulin R 100IU 10ml",year:2019,month:"Jun",day:26,tenHC:"Insulin người, rADN"},
{tenthuoc: "Boliveric",year:2019,month:"Nov",day:28,tenHC:"Cao Actiso,Cao biển xúc,cao bìm bìm biếc"},
{tenthuoc: "Mezathion (spironolacton) 25mg",year:2019,month:"Jun",day:16,tenHC:"Spironolacton"},
{tenthuoc: "Scilin M30 (30/70) 1000IU/10ml",year:2019,month:"Sep",day:15,tenHC:"Insulin trộn, hỗn hợp"},
{tenthuoc: "Insulin H Mix 100 IU",year:2019,month:"Jul",day:13,tenHC:"Insulin trộn, hỗn hợp (Mixtard-acting, Dual-acting)"},
{tenthuoc: "Insulin H Mix 100 IU",year:2019,month:"Sep",day:6,tenHC:"Insulin trộn, hỗn hợp"},
{tenthuoc: "Dưỡng cốt  5g",year:2019,month:"Dec",day:12,tenHC:"Cao xương hỗn hợp/Cao quy bản, Hoàng bá, Tri mẫu, Trần bì, Bạch thược, Can khương, Thục địa."},
{tenthuoc: "Cozaar 100mg",year:2019,month:"Dec",day:15,tenHC:"Losartan"},
{tenthuoc: "Otrivin Dro",year:2019,month:"Aug",day:26,tenHC:"Xylometazoline"},
{tenthuoc: "Amlodipin Stada 5mg",year:2019,month:"Jul",day:15,tenHC:"Amlodipin"},
{tenthuoc: "Amlodipin Stada 5mg",year:2019,month:"Jul",day:7,tenHC:"Amlodipine"},
{tenthuoc: "Cornergel",year:2019,month:"Aug",day:23,tenHC:"Dexpanthenol ( Vitamin B5 )"},
{tenthuoc: "Hydrocortison-Lidocain-Richter125mg/5ml",year:2019,month:"Oct",day:13,tenHC:"Hydrocortison acetat + Lidocain (Hydroclorid)"},
{tenthuoc: "Hydrocortison +Lidocain",year:2019,month:"Jun",day:29,tenHC:"Hydrocortison natri succinat + Lidocain (hydroclorid)"},
{tenthuoc: "Levonor 1 mg/ml",year:2019,month:"Oct",day:2,tenHC:"Nor epinephrin (Nor adrenalin)"},
{tenthuoc: "Paringold",year:2019,month:"Aug",day:3,tenHC:"Heparin (Natri)"},
{tenthuoc: "Examin INJ 250mg/5ml",year:2019,month:"Oct",day:17,tenHC:"Acid Tranexamic"},
{tenthuoc: "Examin INJ 250mg/5ml",year:2019,month:"Jun",day:23,tenHC:"Tranexamic acid"},
{tenthuoc: "Aleradin 5mg",year:2019,month:"Jun",day:8,tenHC:"Desloratadin 5mg"},
{tenthuoc: "Dorokit",year:2019,month:"Dec",day:18,tenHC:"Clarithromycin"},
{tenthuoc: "Dorokit",year:2019,month:"Nov",day:7,tenHC:"Clarithromycin 250mg; Tinidazol 500mg; Omeprazol 20mg"},
{tenthuoc: "Duoplavin (75+100)mg",year:2019,month:"Jun",day:20,tenHC:"Clopidogrel + Acetylsalicylic acid"},
{tenthuoc: "Augclamox 250mg",year:2019,month:"Nov",day:4,tenHC:"Amoxicilin+ Acid clavulanic"},
{tenthuoc: "Klacid",year:2019,month:"Jul",day:9,tenHC:"Clarithromycin"},
{tenthuoc: "BFS-Noradrenalin 1mg",year:2019,month:"Jul",day:30,tenHC:"Nor epinephrin (Nor adrenalin)"},
{tenthuoc: "Adrenalin 1mg/ml VP",year:2019,month:"Oct",day:3,tenHC:"Epinephrin"},
{tenthuoc: "Cefixim",year:2019,month:"Sep",day:10,tenHC:"Cefixim"},
{tenthuoc: "Bisoprolol 5mg Stada",year:2019,month:"Sep",day:24,tenHC:"Bisoprolol"},
{tenthuoc: "Lovenox 4000IU",year:2019,month:"Oct",day:26,tenHC:"Natri Enoxaparin"},
{tenthuoc: "Adalat LA Tab 20 mg",year:2019,month:"Sep",day:18,tenHC:"Nifedipin"},
{tenthuoc: "Vigamox 0.5% 5ml",year:2019,month:"Jul",day:22,tenHC:"Moxifloxacin"},
{tenthuoc: "Celetran 1g",year:2019,month:"Nov",day:8,tenHC:"Ceftriaxon*"},
{tenthuoc: "Oraptic 40mg",year:2019,month:"Nov",day:14,tenHC:"Omeprazole 40 mg"},
{tenthuoc: "Diprivan 20ml",year:2019,month:"Sep",day:5,tenHC:"Propofol (EDTA)"},
{tenthuoc: "PMS-Claminat250mg/ 31,25mg",year:2019,month:"Aug",day:11,tenHC:"Amoxicilin + Acid clavulanic"},
{tenthuoc: "PMS-Claminat250mg/ 31,25mg",year:2019,month:"Sep",day:29,tenHC:"Amoxicilin + Acidclavulanic"},
{tenthuoc: "Aspirin 100mg",year:2019,month:"Sep",day:30,tenHC:"Aspirin"},
{tenthuoc: "Ravonol",year:2019,month:"Oct",day:20,tenHC:"Paracetamol + Loratadin + Dextromethophan"},
{tenthuoc: "Ravonol",year:2019,month:"Nov",day:16,tenHC:"Paracetamol Loratadin Dextromethophan HBr"},
{tenthuoc: "Propofol KABI 1% 20ml",year:2019,month:"Oct",day:16,tenHC:"Propofol lipuro"},
{tenthuoc: "Vitamin A-D 5400UI",year:2019,month:"Oct",day:11,tenHC:"Vitamin A+Vitamin D"},
{tenthuoc: "Bổ gan Thephaco",year:2019,month:"Oct",day:14,tenHC:"Cao Actiso, Cao rau đắng đất, cao diệp hạ châu, Powder mịn bìm bìm"},
{tenthuoc: "Tobrex",year:2019,month:"Aug",day:8,tenHC:"Tobramycin"},
{tenthuoc: "Tobrex",year:2019,month:"Oct",day:19,tenHC:"Tobramycin 0.3% 5ml"},
{tenthuoc: "Tobrex",year:2019,month:"Jun",day:23,tenHC:"Tobramycin 0,3%/5ml"},
{tenthuoc: "Bupivacaine for spinal anaesthesia Aguettant 5mg/ml - 4ml",year:2019,month:"Dec",day:3,tenHC:"Bupivacain"},
{tenthuoc: "Bloza 50mg",year:2019,month:"Nov",day:25,tenHC:"Losartan"},
{tenthuoc: "Bupivacaine 0.5%",year:2019,month:"Dec",day:14,tenHC:"Bupivacaine HCl"},
{tenthuoc: "Renapril (Enalapril) 5mg",year:2019,month:"Aug",day:1,tenHC:"Enalapril"},
{tenthuoc: "Briozcal",year:2019,month:"Jun",day:6,tenHC:"Calci cacbonat+ Vitamin D3"},
{tenthuoc: "Cordaron 200mg",year:2019,month:"Nov",day:4,tenHC:"Amiodaron (hydroclorid)"},
{tenthuoc: "Vaginapoly",year:2019,month:"Sep",day:5,tenHC:"Nystatin + Neomycin + Polymyxin B"},
{tenthuoc: "Vaginapoly",year:2019,month:"Aug",day:27,tenHC:"Nystatin+Neomycin+Polymyxin B"},
{tenthuoc: "Trimoxtal 250/250",year:2019,month:"Jun",day:8,tenHC:"Amoxicilin + Sulbactam"},
{tenthuoc: "Lantus (Insulin 300UI/3ml)",year:2019,month:"Jul",day:5,tenHC:"Insulin"},
{tenthuoc: "Clarithromycin Stada 500 mg",year:2019,month:"Sep",day:23,tenHC:"Clarithromycin"},
{tenthuoc: "Scaneuron",year:2019,month:"Jun",day:14,tenHC:"Vitamin B1+Vitamin B6+Vitamin B12."},
{tenthuoc: "Tobrex 5ml",year:2019,month:"Dec",day:19,tenHC:"Tobramycin"},
{tenthuoc: "Hemblood",year:2019,month:"Nov",day:24,tenHC:"Vitamin B1+vitamin B6+ vitamin B12"},
{tenthuoc: "Novomix 30 Flexpen",year:2019,month:"Aug",day:24,tenHC:"Insulin"},
{tenthuoc: "Adalat LA Tab 20mg 30's",year:2019,month:"Aug",day:15,tenHC:"Nifedipine"},
{tenthuoc: "Adalat LA Tab 20mg 30's",year:2019,month:"Jun",day:7,tenHC:"Nifedipin"},
{tenthuoc: "LOVENOX 40mg Inj B/ 2 syringes x0,4ml",year:2019,month:"Dec",day:13,tenHC:"Natri  Enoxaparin"},
{tenthuoc: "LOVENOX 40mg Inj B/ 2 syringes x0,4ml",year:2019,month:"Nov",day:3,tenHC:"Enoxaparin"},
{tenthuoc: "Scilin R 400IU",year:2019,month:"Nov",day:14,tenHC:"Insulin tác dụng ngắn (S) 400IU/10ml"},
{tenthuoc: "3Bpluzs",year:2019,month:"Dec",day:21,tenHC:"Vitamin Bl+Vitamin B6+Vitamin B12."},
{tenthuoc: "3Bpluzs",year:2019,month:"Jun",day:10,tenHC:"Vitamin B1 + Vitamin B6 + Vitamin B12"},
{tenthuoc: "3Bpluzs",year:2019,month:"Jun",day:29,tenHC:"Vitamin B1 + B6 + B12"},
{tenthuoc: "3Bpluzs",year:2019,month:"Oct",day:27,tenHC:"Vitamin B1+Vitamin B6+Vitamin B12."},
{tenthuoc: "3Bpluzs",year:2019,month:"Aug",day:6,tenHC:"Vitamin B1+ vitamin B6+ vitamin B12"},
{tenthuoc: "I.V.-Globulin SN inj.",year:2019,month:"Dec",day:25,tenHC:"Immunoglobulin G"},
{tenthuoc: "Meronem",year:2019,month:"Jun",day:27,tenHC:"Meropenem"},
{tenthuoc: "Tartriakson  1g",year:2019,month:"Jul",day:20,tenHC:"Ceftriaxon"},
{tenthuoc: "Cozaar 50mg",year:2019,month:"Dec",day:13,tenHC:"Losartan"},
{tenthuoc: "Azipowder",year:2019,month:"Jun",day:17,tenHC:"Azithromycin"},
{tenthuoc: "Tiepanem",year:2019,month:"Oct",day:11,tenHC:"Meropenem"},
{tenthuoc: "Sastan-H",year:2019,month:"Jul",day:19,tenHC:"Losartan+hydroclorothiazid"},
{tenthuoc: "Moxilen (amoxicillin) 500mg",year:2019,month:"Nov",day:5,tenHC:"Amoxicilin"},
{tenthuoc: "Moxacin",year:2019,month:"Nov",day:6,tenHC:"Amoxicilin"},
{tenthuoc: "Ceftriaxone Panpharma 1g",year:2019,month:"Aug",day:15,tenHC:"Ceftriaxon"},
{tenthuoc: "Amoxicilin 500mg",year:2019,month:"Sep",day:3,tenHC:"Amoxicilin"},
{tenthuoc: "Amoxicilin 500mg",year:2019,month:"Dec",day:16,tenHC:"Amoxicillin"},
{tenthuoc: "Vismed",year:2019,month:"Jul",day:12,tenHC:"Natri hyaluronat"},
{tenthuoc: "Tercef 1g",year:2019,month:"Aug",day:9,tenHC:"Ceftriaxon"},
{tenthuoc: "Medphadion Drops 100mg/5ml",year:2019,month:"Dec",day:25,tenHC:"Phytomenadione (Vitamin K1)"},
{tenthuoc: "Coveram 5-10 Tab 5mg-10mg 30's",year:2019,month:"Jun",day:4,tenHC:"Perindopril + Amlodipine"},
{tenthuoc: "Coveram 5-10 Tab 5mg-10mg 30's",year:2019,month:"Nov",day:29,tenHC:"Perindopril  +  Amlodipin"},
{tenthuoc: "Coveram 5-10 Tab 5mg-10mg 30's",year:2019,month:"Oct",day:12,tenHC:"Perindopril Arginine 5mg, Amlodipine besylate 10mg"},
{tenthuoc: "Coveram 5-10 Tab 5mg-10mg 30's",year:2019,month:"Jul",day:8,tenHC:"Amlodipin +Perindopril"},
{tenthuoc: "Insulin H  Mix 100UI",year:2019,month:"Jun",day:14,tenHC:"Insulin trộn, hỗn hợp"},
{tenthuoc: "Prospan cough syrup",year:2019,month:"Nov",day:13,tenHC:"Cao khô lá thường xuân"},
{tenthuoc: "Sanlein",year:2019,month:"Nov",day:3,tenHC:"Natri hyaluronate"},
{tenthuoc: "Sanlein",year:2019,month:"Dec",day:4,tenHC:"Natri hyaluronat"},
{tenthuoc: "Diprivan Inj 20ml 5's 10mg/ml (1%) - 20ml",year:2019,month:"Aug",day:1,tenHC:"Propofol"},
{tenthuoc: "DBL Irinotecan Injection 40mg/2ml",year:2019,month:"Aug",day:30,tenHC:"Irinotecan"},
{tenthuoc: "Proxacin 200mg",year:2019,month:"Oct",day:13,tenHC:"Ciprofloxacin"},
{tenthuoc: "Moxilen 500mg",year:2019,month:"Aug",day:28,tenHC:"Amoxicilin 500mg"},
{tenthuoc: "Moxilen 500mg",year:2019,month:"Sep",day:21,tenHC:"Amoxicilin"},
{tenthuoc: "Taxotere (Docetaxel) 80mg/4ml",year:2019,month:"Jun",day:1,tenHC:"Docetaxel"},
{tenthuoc: "Vismed 0.18",year:2019,month:"Jul",day:7,tenHC:"Natri hyaluronat"},
{tenthuoc: "Isotic Moxisone",year:2019,month:"Dec",day:13,tenHC:"Moxifloxacin + Dexamethason"},
{tenthuoc: "Tercef",year:2019,month:"Aug",day:6,tenHC:"Ceftriaxon"},
{tenthuoc: "Tercef",year:2019,month:"Nov",day:4,tenHC:"Ceftriaxon*"},
{tenthuoc: "Bupivacaine Aguettant 5mg/ml x 20ml 0,5%/20ml",year:2019,month:"Sep",day:29,tenHC:"Bupivacain (hydroclorid)"},
{tenthuoc: "Cravit 5mg/ ml",year:2019,month:"Nov",day:24,tenHC:"Levofloxacin"},
{tenthuoc: "Exforge 5/80mg",year:2019,month:"Oct",day:16,tenHC:"Amlodipin + Valsartan"},
{tenthuoc: "Fabapoxim",year:2019,month:"Aug",day:13,tenHC:"Cefpodoxim"},
{tenthuoc: "Unasyl",year:2019,month:"Oct",day:2,tenHC:"Ampicilin + sulbactam"},
{tenthuoc: "Paclitaxel  Ebewe  Inj 100MG/16.7ML 1's",year:2019,month:"Jul",day:9,tenHC:"Paclitaxel,6mg/ml"},
{tenthuoc: "Paclitaxel  Ebewe  Inj 100MG/16.7ML 1's",year:2019,month:"Jun",day:12,tenHC:"Paclitacel"},
{tenthuoc: "Levonor 4mg/4ml",year:2019,month:"Nov",day:27,tenHC:"Nor epinephrin (Nor adrenalin)"},
{tenthuoc: "Aldacton tab",year:2019,month:"Dec",day:21,tenHC:"Spironolactone"},
{tenthuoc: "Adalat LA 30mg",year:2019,month:"Jul",day:24,tenHC:"Nifedipin"},
{tenthuoc: "Progut",year:2019,month:"Aug",day:5,tenHC:"Esomeprazole"},
{tenthuoc: "Adalat LA Tab 20mg",year:2019,month:"Jun",day:28,tenHC:"Nifedipin"},
{tenthuoc: "Exforge 5mg+80mg",year:2019,month:"Jul",day:9,tenHC:"Amlodipin + Valsartan"},
{tenthuoc: "Ebitac (Enalapril 10mg+Hydroclorothiazid 25mg)",year:2019,month:"Dec",day:18,tenHC:"Enalapril 10mg + Hydroclorothiazid 25mg"},
{tenthuoc: "Ebitac (Enalapril 10mg+Hydroclorothiazid 25mg)",year:2019,month:"Jun",day:30,tenHC:"Enalapril maleate 10mg + Hydrochlorothiazide 25mg"},
{tenthuoc: "Boganic",year:2019,month:"Jun",day:27,tenHC:"Cao đặc Actiso + Cao đặc Rau đắng đất + Cao đặc Bìm bìm"},
{tenthuoc: "Boganic",year:2019,month:"Oct",day:17,tenHC:"Cao Actiso + Cao đặc rau đắng đất + Cao đặc bìm bìm"},
{tenthuoc: "Boganic",year:2019,month:"Sep",day:5,tenHC:"Cao khô Artiso+Cao khô rau đắng đất+Cao khô bìm bìm"},
{tenthuoc: "Mecefix-B.E",year:2019,month:"Dec",day:5,tenHC:"Cefixim"},
{tenthuoc: "Meronem 500mg",year:2019,month:"Aug",day:3,tenHC:"Meropenem"},
{tenthuoc: "Nexium",year:2019,month:"Aug",day:7,tenHC:"Esomeprazole"},
{tenthuoc: "Franilax",year:2019,month:"Jul",day:24,tenHC:"Furosemide + Spironolacton"},
{tenthuoc: "Franilax",year:2019,month:"Jul",day:12,tenHC:"Spironolacton + Furosemid"},
{tenthuoc: "Franilax",year:2019,month:"Jul",day:30,tenHC:"Furosemid + Spironolacton"},
{tenthuoc: "Gardenal 100mg",year:2019,month:"Jul",day:5,tenHC:"Phenobarbital"},
{tenthuoc: "Gardenal 100mg",year:2019,month:"Jul",day:18,tenHC:"Phenobacbital"},
{tenthuoc: "Coveram Tab 5mg/5mg 30's",year:2019,month:"Oct",day:14,tenHC:"Perindopril Arginine 5mg, Amlodipine besylate 5mg"},
{tenthuoc: "Coveram Tab 5mg/5mg 30's",year:2019,month:"Aug",day:11,tenHC:"Perindopril  +  Amlodipin"},
{tenthuoc: "Xalvobin (capecitabin) 500mg",year:2019,month:"Nov",day:2,tenHC:"Capecitabin"},
{tenthuoc: "Levonor 1mg/ml",year:2019,month:"Sep",day:25,tenHC:"Nor epinephrin (Nor adrenalin)"},
{tenthuoc: "Sanlein 0,1% x 5ml",year:2019,month:"Jun",day:6,tenHC:"Natri hyaluronate 1mg/ml"},
{tenthuoc: "Sanlein 0,1% x 5ml",year:2019,month:"Aug",day:15,tenHC:"Natri hyaluronat"},
{tenthuoc: "Concor",year:2019,month:"Jul",day:19,tenHC:"Bisoprolol"},
{tenthuoc: "Infartan (Clopidogrel) 75mg",year:2019,month:"Aug",day:12,tenHC:"Clopidogrel"},
{tenthuoc: "Nifedipin Hasan 20 Retard",year:2019,month:"Sep",day:10,tenHC:"Nifedipin"},
{tenthuoc: "Nifedipin Hasan 20 Retard",year:2019,month:"Sep",day:27,tenHC:"Nifedipine"},
{tenthuoc: "Aristin-C (Ciprofloxacin 200mg/100ml)",year:2019,month:"Sep",day:16,tenHC:"Ciprofloxacin"},
{tenthuoc: "Resines (Amlodipin) 5mg",year:2019,month:"Aug",day:14,tenHC:"Amlodipin"},
{tenthuoc: "Kedrigamma",year:2019,month:"Sep",day:23,tenHC:"Immune Globulin"},
{tenthuoc: "Tobradex 5ml",year:2019,month:"Nov",day:11,tenHC:"Tobramycin+Dexamethason"},
{tenthuoc: "Tobradex 5ml",year:2019,month:"Nov",day:19,tenHC:"Tobramycin0,3% Dexamethason 0,1%"},
{tenthuoc: "Tobradex 5ml",year:2019,month:"Aug",day:7,tenHC:"Tobramycin + Dexamethason"},
{tenthuoc: "Tobradex 5ml",year:2019,month:"Dec",day:23,tenHC:"Tobramycin + Dexamethasone"},
{tenthuoc: "Prospan 70ml",year:2019,month:"Oct",day:29,tenHC:"Cao lá thường xuân"},
{tenthuoc: "Prospan 70ml",year:2019,month:"Jun",day:20,tenHC:"Cao khô lá thường xuân"},
{tenthuoc: "Ceftriaxon",year:2019,month:"Sep",day:17,tenHC:"Ceftriaxon"},
{tenthuoc: "Amlor Tab 5mg 30's",year:2019,month:"Oct",day:29,tenHC:"Amlodipin"},
{tenthuoc: "Amlor Tab 5mg 30's",year:2019,month:"Jul",day:22,tenHC:"Amlodipine besilate (amlodipine 5mg)"},
{tenthuoc: "Amlor Tab 5mg 30's",year:2019,month:"Sep",day:13,tenHC:"Amlodipine"},
{tenthuoc: "Novomix 30/70 Flexpen 100UI 3ml",year:2019,month:"Jul",day:2,tenHC:"Insulin trộn (M) 30:70"},
{tenthuoc: "Lotemax",year:2019,month:"Nov",day:20,tenHC:"Lotepredol etabonat"},
{tenthuoc: "Lotemax",year:2019,month:"Jun",day:1,tenHC:"Loteprednol etabonat"},
{tenthuoc: "Lotemax",year:2019,month:"Sep",day:22,tenHC:"Loteprednol Etabonate"},
{tenthuoc: "Concor 2,5mg",year:2019,month:"Jul",day:16,tenHC:"Bisoprolol"},
{tenthuoc: "Ebitac 12,5",year:2019,month:"Dec",day:23,tenHC:"Enalapril maleate + Hydrochlorothiazide"},
{tenthuoc: "Ebitac 12,5",year:2019,month:"Sep",day:9,tenHC:"Enalapril maleat +Hydroclorothiazid"},
{tenthuoc: "Ebitac 12,5",year:2019,month:"Oct",day:28,tenHC:"Enalapril maleat + Hydroclorothiazid"},
{tenthuoc: "Remeclar 500mg",year:2019,month:"Jun",day:17,tenHC:"Clarithromycin"},
{tenthuoc: "Amlor Cap 5mg 30's",year:2019,month:"Sep",day:17,tenHC:"Amlodipin"},
{tenthuoc: "Fudcime (Cefixim) 200mg",year:2019,month:"Jul",day:14,tenHC:"Cefixim"},
{tenthuoc: "Bisoprolol 2,5mg",year:2019,month:"Oct",day:11,tenHC:"Bisoprolol"},
{tenthuoc: "Paclispec 30",year:2019,month:"Aug",day:23,tenHC:"Paclitaxel"},
{tenthuoc: "Augmentin BD Tab 625mg 14's",year:2019,month:"Sep",day:18,tenHC:"Amoxicillin trihydrate 500mg + Acid Clavulanic 125mg"},
{tenthuoc: "Augmentin BD Tab 625mg 14's",year:2019,month:"Jun",day:21,tenHC:"Amoxicilin + Acid clavulanic"},
{tenthuoc: "Augmentin BD Tab 625mg 14's",year:2019,month:"Oct",day:5,tenHC:"Amoxicillin + Acid Clavulanic"},
{tenthuoc: "Medopiren 500mg",year:2019,month:"Aug",day:7,tenHC:"Ciprofloxacin"},
{tenthuoc: "Azicine",year:2019,month:"Sep",day:28,tenHC:"Azithromycin"},
{tenthuoc: "Hyđan",year:2019,month:"Nov",day:6,tenHC:"Bột mã tiền chế, hy thiêm, ngũ gia bì"},
{tenthuoc: "Ucyrin 75mg",year:2019,month:"Oct",day:25,tenHC:"Clopidogrel"},
{tenthuoc: "Ucyrin 75mg",year:2019,month:"Oct",day:8,tenHC:"Clopidogrel 75mg"},
{tenthuoc: "Campto (Irinotecan) 100mg",year:2019,month:"Jun",day:21,tenHC:"Irinotecan"},
{tenthuoc: "Scilin R",year:2019,month:"Aug",day:14,tenHC:"Insulin tác dụng nhanh ngắn"},
{tenthuoc: "Scilin R",year:2019,month:"Dec",day:22,tenHC:"Insulin tác dụng nhanh (fast-acting, short-acting)"},
{tenthuoc: "Scilin R",year:2019,month:"Nov",day:21,tenHC:"Insulin tác dụng nhanh, ngắn"},
{tenthuoc: "Scilin R",year:2019,month:"Sep",day:15,tenHC:"Insulin tác dụng nhanh, ngắn (Fast-acting, Short-acting)"},
{tenthuoc: "Scilin R",year:2019,month:"Sep",day:19,tenHC:"Insulin ( tác dụng nhanh)"},
{tenthuoc: "Resines",year:2019,month:"Nov",day:9,tenHC:"Amlodipin"},
{tenthuoc: "Resines",year:2019,month:"Nov",day:18,tenHC:"Amlodipine"},
{tenthuoc: "Nexium mups",year:2019,month:"Jul",day:26,tenHC:"Esomeprazole"},
{tenthuoc: "Chirocain 5mg/ml Ampoule 10x10ml 5mg/ml",year:2019,month:"Jul",day:10,tenHC:"Levobupivacaine hydrochloride,  levobupivacaine 5mg/ml"},
{tenthuoc: "Scilin N 400IU",year:2019,month:"Dec",day:25,tenHC:"Insulin tác dụng trung Bottle(s) (insulin NPH) 400IU/10ml"},
{tenthuoc: "Levemir Flexpen",year:2019,month:"Aug",day:16,tenHC:"Insulin determir"},
{tenthuoc: "Coveram (5+10)mg",year:2019,month:"Jul",day:8,tenHC:"Perindopril + amlodipin"},
{tenthuoc: "Cardilopin",year:2019,month:"Jul",day:14,tenHC:"Amlodipin"},
{tenthuoc: "Humulin N 100IU 10ml",year:2019,month:"Dec",day:20,tenHC:"Insulin tác dụng trung bình, trung gian"},
{tenthuoc: "Ebitac 25mg",year:2019,month:"Jun",day:22,tenHC:"Enalapril maleate 10mg+hydroclorothiazid 25mg"},
{tenthuoc: "Ebitac12.5",year:2019,month:"Sep",day:7,tenHC:"Enalapril"},
{tenthuoc: "Scanneuron 300,2mg",year:2019,month:"Nov",day:8,tenHC:"Vitamin B1+Vitamin B6+Vitamin B12."},
{tenthuoc: "Coveram",year:2019,month:"Jun",day:25,tenHC:"Perindopril + amlodipin"},
{tenthuoc: "Propofol 1 % Kabi",year:2019,month:"Dec",day:22,tenHC:"Propofol"},
{tenthuoc: "Oflovid ophthalmic ointment",year:2019,month:"Jun",day:1,tenHC:"Ofloxacin"},
{tenthuoc: "Mixtard 30",year:2019,month:"Jun",day:3,tenHC:"Insulin trộn, hỗn hợp (Mixtard-acting, Dual-acting)"},
{tenthuoc: "Mixtard 30",year:2019,month:"Sep",day:11,tenHC:"Insulin người, rADN"},
{tenthuoc: "Mixtard 30",year:2019,month:"Sep",day:22,tenHC:"Insulin trộn, hỗn hợp"},
{tenthuoc: "Xeloda (Capecitabin) 500mg",year:2019,month:"Jul",day:19,tenHC:"Capecitabin"},
{tenthuoc: "Resines 5mg",year:2019,month:"Oct",day:6,tenHC:"Amlodipin 5mg"},
{tenthuoc: "Resines 5mg",year:2019,month:"Oct",day:24,tenHC:"Amlodipine"},
{tenthuoc: "Resines 5mg",year:2019,month:"Oct",day:5,tenHC:"Amlodipin"},
{tenthuoc: "Emas",year:2019,month:"Dec",day:16,tenHC:"Glycerin"},
{tenthuoc: "Scilin M30",year:2019,month:"Jun",day:24,tenHC:"Insulin trộn, hỗn hợp"},
{tenthuoc: "Scilin M30",year:2019,month:"Nov",day:27,tenHC:"Insulin trộn, hỗn hợp (Mixtard-acting, Dual-acting)"},
{tenthuoc: "Scilin M30",year:2019,month:"Jun",day:18,tenHC:"Insulin (30/70)"},
{tenthuoc: "Unasyn 1500mg",year:2019,month:"Oct",day:5,tenHC:"Amoxicilin + Sulbactam"},
{tenthuoc: "Ofmantine-Domesco 625mg",year:2019,month:"Nov",day:28,tenHC:"Amoxicilin + Acid clavulanic"},
{tenthuoc: "Cozaar Tab 50mg 30's",year:2019,month:"Jun",day:30,tenHC:"Losartan"},
{tenthuoc: "Cozaar Tab 50mg 30's",year:2019,month:"Jun",day:7,tenHC:"Losartan Kali"},
{tenthuoc: "Cozaar Tab 50mg 30's",year:2019,month:"Dec",day:28,tenHC:"Losartan K 50mg"},
{tenthuoc: "Diprivan 20ml/200mg",year:2019,month:"Nov",day:8,tenHC:"Propofol"},
{tenthuoc: "Parocontin",year:2019,month:"Dec",day:6,tenHC:"Methocarbamol + Paracetamol"},
{tenthuoc: "Medsamic",year:2019,month:"Oct",day:19,tenHC:"Acid Tranexamic"},
{tenthuoc: "Medsamic",year:2019,month:"Aug",day:4,tenHC:"Tranexamic acid"},
{tenthuoc: "Claritek 125mg/5mlx50mL",year:2019,month:"Sep",day:8,tenHC:"Clarithromycin"},
{tenthuoc: "Twynsta 40/5mg",year:2019,month:"Dec",day:8,tenHC:"Telmisartan + Amlodipin"},
{tenthuoc: "Paclitaxel  Ebewe  Inj 30MG/5ML 1's",year:2019,month:"Jul",day:18,tenHC:"Paclitacel"},
{tenthuoc: "Exforge HCT 5/160/12.5mg",year:2019,month:"Jul",day:12,tenHC:"Amlodipin+Valsartan+Hydrochlorothiazide"},
{tenthuoc: "Diamisu-N 10ml",year:2019,month:"Aug",day:28,tenHC:"Insulin chậm, kéo dài"},
{tenthuoc: "Heparin (Paringold) 25000IU/5ml",year:2019,month:"Dec",day:6,tenHC:"Heparin (Natri)"},
{tenthuoc: "Reditux 100mg/10ml",year:2019,month:"Jun",day:24,tenHC:"Rituximab"},
{tenthuoc: "Unasyn 1,5g",year:2019,month:"Nov",day:16,tenHC:"Ampicilin + sulbactam"},
{tenthuoc: "Unasyn 1,5g",year:2019,month:"Nov",day:8,tenHC:"Ampicillin+Sulbactam"},
{tenthuoc: "Dicortineff 5ml",year:2019,month:"Aug",day:24,tenHC:"Neomycin; gramicidin; Fludrocortisone acetate"},
{tenthuoc: "Dicortineff 5ml",year:2019,month:"Jul",day:14,tenHC:"Neomycin sulfat + gramicidin + 9-alpha fluohydrocortison acetat"},
{tenthuoc: "Diprivan Pre-Filled Syring 1% 10mg/ml (1%) - 50ml",year:2019,month:"Sep",day:5,tenHC:"Propofol"},
{tenthuoc: "Mixtard 30 Flexpen 100ui/mlx3ml",year:2019,month:"Jun",day:1,tenHC:"Insulin người (rDNA)"},
{tenthuoc: "Exforge tab 5mg/ 80mg 2x14's",year:2019,month:"Jun",day:7,tenHC:"Amlodipine + Valsartan"},
{tenthuoc: "Vitamin B1 + B6 + B12",year:2019,month:"Sep",day:11,tenHC:"Vitamin B1+Vitamin B6+Vitamin B12."},
{tenthuoc: "Vitamin B1 + B6 + B12",year:2019,month:"Jul",day:26,tenHC:"Vitamin B1 + B6 + B12"},
{tenthuoc: "Vitamin B1 + B6 + B12",year:2019,month:"Oct",day:23,tenHC:"Vitamin B1 + Vitamin B6 + Vitamin B12"},
{tenthuoc: "Medoclav 1g",year:2019,month:"Oct",day:27,tenHC:"Amoxicilin + Acid clavulanic"},
{tenthuoc: "Medoclav 1g",year:2019,month:"Sep",day:16,tenHC:"Amoxicillin + Acid Clavulanic"},
{tenthuoc: "Tamifine",year:2019,month:"Jul",day:26,tenHC:"Tamoxifen"},
{tenthuoc: "Savi Losartan plus HCT 50/12.5",year:2019,month:"Jul",day:2,tenHC:"Losartan + Hydrochlorothiazid"},
{tenthuoc: "Panadol Caplet (Paracetamol) 500mg",year:2019,month:"Nov",day:19,tenHC:"Paracetamol"},
{tenthuoc: "Heparin-Belmed",year:2019,month:"Sep",day:2,tenHC:"Heparin (Natri)"},
{tenthuoc: "Cravit",year:2019,month:"Oct",day:12,tenHC:"Levofloxacin"},
{tenthuoc: "Tobrin",year:2019,month:"Nov",day:9,tenHC:"Tobramycin"},
{tenthuoc: "Tobrin",year:2019,month:"Dec",day:12,tenHC:"Tobramycin 0,3%/5ml"},
{tenthuoc: "Losartan Stada 50mg",year:2019,month:"Oct",day:14,tenHC:"Losartan"},
{tenthuoc: "Resines 5mg(amlodipin)",year:2019,month:"Dec",day:25,tenHC:"Amlodipin"},
{tenthuoc: "Hoạt huyết thông mạch P/H",year:2019,month:"Jul",day:30,tenHC:"Bột Đương quy; Cao đặc các dược liệu (tương đương với Đương quy; Sinh địa; Xuyên khung; Ngưu tất; Ích mẫu; Đan sâm)."},
{tenthuoc: "Hoạt huyết thông mạch P/H",year:2019,month:"Nov",day:1,tenHC:"Mỗi 200ml cao lỏng chứa: Đương quy; Bạch thược; Ngưu tất; Thục địa; Xuyên khung; Cao đặc ích mẫu (10:1)"},
{tenthuoc: "Meronem 1g (Meropenem)",year:2019,month:"Dec",day:23,tenHC:"Meropenem"},
{tenthuoc: "Aldacton 25mg",year:2019,month:"Jul",day:2,tenHC:"Spironolacton"},
{tenthuoc: "Bimoxine",year:2019,month:"Oct",day:13,tenHC:"Amoxicilin + Cloxacilin"},
{tenthuoc: "Plavix",year:2019,month:"Jun",day:30,tenHC:"Clopidogrel"},
{tenthuoc: "Novorapid",year:2019,month:"Jun",day:30,tenHC:"Insulin ( tác dụng nhanh)"},
{tenthuoc: "Bactirid 100mg/5ml",year:2019,month:"Nov",day:17,tenHC:"Cefixim"},
{tenthuoc: "Paringold Injection 25000Ui/5ml",year:2019,month:"Nov",day:19,tenHC:"Heparin (Natri)"},
{tenthuoc: "Xalvobin",year:2019,month:"Aug",day:11,tenHC:"Capecitabin"},
{tenthuoc: "Aleradin",year:2019,month:"Oct",day:13,tenHC:"Desloratadine"},
{tenthuoc: "Aleradin",year:2019,month:"Nov",day:23,tenHC:"Desloratadin"},
{tenthuoc: "Paracetamol 500mg( Panadol Caplet)",year:2019,month:"Aug",day:14,tenHC:"Paracetamol"},
{tenthuoc: "Clocardigel (Clopidogrel) 75mg",year:2019,month:"Aug",day:24,tenHC:"Clopidogrel"},
{tenthuoc: "Cozaar XQ 5mg+100mg",year:2019,month:"Oct",day:24,tenHC:"Amlodipin + Losartan"},
{tenthuoc: "LOVENOX 60mg Inj B/ 2 syringes x0,6ml",year:2019,month:"Jul",day:14,tenHC:"Enoxaparin (natri)"},
{tenthuoc: "LOVENOX 60mg Inj B/ 2 syringes x0,6ml",year:2019,month:"Dec",day:17,tenHC:"Enoxaparin"},
{tenthuoc: "Lantus Solostar",year:2019,month:"Aug",day:30,tenHC:"Insulin tác dụng chậm, kéo dài (Slow-acting, Long-acting)"},
{tenthuoc: "Nifedipin T20 Stada retard",year:2019,month:"Sep",day:18,tenHC:"Nifedipin"},
{tenthuoc: "Enamigal 5mg",year:2019,month:"Nov",day:30,tenHC:"Enalapril"},
{tenthuoc: "Enamigal 5mg",year:2019,month:"Nov",day:19,tenHC:"Enalapril 5mg"},
{tenthuoc: "Tobrex Eye Ointment",year:2019,month:"Oct",day:15,tenHC:"Tobramycin"},
{tenthuoc: "Noradrenalin Base Aguettant  4mg/4ml",year:2019,month:"Nov",day:24,tenHC:"Nor epinephrin (Nor adrenalin)"},
{tenthuoc: "Hoạt huyết dưỡng não",year:2019,month:"Sep",day:7,tenHC:"Cao đinh lăng, cao bạch quả"},
{tenthuoc: "Hoạt huyết dưỡng não",year:2019,month:"Nov",day:17,tenHC:"Ginkgo biloba + Cao đan sâm"},
{tenthuoc: "Hoạt huyết dưỡng não",year:2019,month:"Aug",day:9,tenHC:"Cao đinh lăng +Cao bạch quả"},
{tenthuoc: "Panadol caplet",year:2019,month:"Jul",day:14,tenHC:"Paracetamol   500mg"},
{tenthuoc: "Actrapid",year:2019,month:"Sep",day:19,tenHC:"Insulin người,rADN"},
{tenthuoc: "Actrapid",year:2019,month:"Sep",day:19,tenHC:"Insulin người, rADN"},
{tenthuoc: "Actrapid",year:2019,month:"Dec",day:19,tenHC:"Insulin trộn, hỗn hợp"},
{tenthuoc: "Kalecin 250",year:2019,month:"Aug",day:8,tenHC:"Clarithromycin"},
{tenthuoc: "Denxif",year:2019,month:"Jun",day:3,tenHC:"Digoxin WZF 0.25mg"},
{tenthuoc: "Ceftriaxone - Panpharma",year:2019,month:"Jul",day:20,tenHC:"Ceftriaxone"},
{tenthuoc: "VEROSPIRON 25MG",year:2019,month:"Nov",day:19,tenHC:"Spironolacton"},
{tenthuoc: "Hadokit",year:2019,month:"Jul",day:6,tenHC:"Omeprazol + Tinidazol + Clarithromycin"},
{tenthuoc: "Hadokit",year:2019,month:"Jun",day:5,tenHC:"Omeprazol+Tinidazol+Clarithromycin"},
{tenthuoc: "Paringold Injection",year:2019,month:"Aug",day:19,tenHC:"Heparin (Natri)"},
{tenthuoc: "Erolin",year:2019,month:"Nov",day:1,tenHC:"Loratadin"},
{tenthuoc: "Clarithromycin Stada 250mg",year:2019,month:"Jun",day:21,tenHC:"Clarithromycin"},
{tenthuoc: "Utrogestan 200mg",year:2019,month:"Jun",day:5,tenHC:"Progesteron"},
{tenthuoc: "Exforge HCT 10/160/12.5mg",year:2019,month:"Aug",day:10,tenHC:"Amlodipin+Valsartan+Hydrochlorothiazide"},
{tenthuoc: "Ebitac Forte",year:2019,month:"Dec",day:19,tenHC:"Enalapril+ Hydrochlorothiazid"},
{tenthuoc: "Ebitac Forte",year:2019,month:"Aug",day:3,tenHC:"Enalapril + Hydroclorothiazid"},
{tenthuoc: "Clarithromycin Stada 500mg",year:2019,month:"Aug",day:1,tenHC:"Clarithromycin"},
{tenthuoc: "Enaminal (elalapril 10mg)",year:2019,month:"Dec",day:11,tenHC:"Enalapril"},
{tenthuoc: "Enamigal (enalapril 5mg)",year:2019,month:"Dec",day:22,tenHC:"Enalapril"},
{tenthuoc: "Medopiren",year:2019,month:"Sep",day:1,tenHC:"Ciprofloxacin"},
{tenthuoc: "Osaki",year:2019,month:"Dec",day:30,tenHC:"Lysin+ Vitamin+ Khoáng chất"},
{tenthuoc: "Acuvail",year:2019,month:"Aug",day:4,tenHC:"Ketorolac tromethamine"},
{tenthuoc: "Acuvail",year:2019,month:"Sep",day:6,tenHC:"Ketorolac tromethamin"},
{tenthuoc: "Tartriakson",year:2019,month:"Jun",day:29,tenHC:"Ceftriaxone"},
{tenthuoc: "Bupivacaine 20mg 0.5% 5 mg/ml",year:2019,month:"Jul",day:20,tenHC:"Bupivacain (hydroclorid)"},
{tenthuoc: "Bupivacaine 20mg 0.5% 5 mg/ml",year:2019,month:"Jul",day:13,tenHC:"Bupivacain"},
{tenthuoc: "Cefixime 100mg",year:2019,month:"Jul",day:2,tenHC:"Cefixim"},
{tenthuoc: "Neo- Tergynan",year:2019,month:"Oct",day:17,tenHC:"Metronidazol + Neomycin+Nystatin"},
{tenthuoc: "Neo- Tergynan",year:2019,month:"Oct",day:1,tenHC:"Nystatin + metronidazol + neomycin"},
{tenthuoc: "Sastan-H (Losartan+Hydroclorothiazid) (25+12,5)mg",year:2019,month:"Dec",day:2,tenHC:"Losartan+hydroclorothiazid"},
{tenthuoc: "Vitamin A-D",year:2019,month:"Aug",day:14,tenHC:"Vitamin A+Vitamin D"},
{tenthuoc: "Vitamin A-D",year:2019,month:"Jun",day:22,tenHC:"Vitamin A+ Vitamin D"},
{tenthuoc: "Vitamin A-D",year:2019,month:"Jun",day:19,tenHC:"Vitamin A + D"},
{tenthuoc: "Vitamin A-D",year:2019,month:"Aug",day:18,tenHC:"Vitamin A + Vitamin D"},
{tenthuoc: "CORDARONE 150mg/3ml Inj B/ 6 Amps x 3ml",year:2019,month:"Jun",day:21,tenHC:"Amiodarone HCL 150mg"},
{tenthuoc: "CORDARONE 150mg/3ml Inj B/ 6 Amps x 3ml",year:2019,month:"Aug",day:9,tenHC:"Amiodarone HCl"},
{tenthuoc: "CORDARONE 150mg/3ml Inj B/ 6 Amps x 3ml",year:2019,month:"Dec",day:10,tenHC:"Amiodarone HCL 150mg/ 3ml"},
{tenthuoc: "CORDARONE 150mg/3ml Inj B/ 6 Amps x 3ml",year:2019,month:"Jul",day:30,tenHC:"Amiodaron (hydrochlorid)"},
{tenthuoc: "CORDARONE 150mg/3ml Inj B/ 6 Amps x 3ml",year:2019,month:"Nov",day:5,tenHC:"Amiodaron"},
{tenthuoc: "Diprivan Inj 20ml 5's",year:2019,month:"Sep",day:10,tenHC:"Propofol"},
{tenthuoc: "Diprivan Inj 20ml 5's",year:2019,month:"Oct",day:30,tenHC:"Propofol 10mg/ml (1%)"},
{tenthuoc: "Nexium 40mg",year:2019,month:"Sep",day:24,tenHC:"Esomeprazole"},
{tenthuoc: "Maxitrol 5ml",year:2019,month:"Nov",day:13,tenHC:"Neomycin + Polymicin + Dexamethason"},
{tenthuoc: "Maxitrol 5ml",year:2019,month:"Jul",day:20,tenHC:"Dexamethasone+neomycin+polymycin B 0,1%+3500IU/ml+6000IU/g"},
{tenthuoc: "Maxitrol 5ml",year:2019,month:"Jun",day:6,tenHC:"Dexamethason+ Neomycin+ Polymycin B"},
{tenthuoc: "Osmofundin",year:2019,month:"Dec",day:12,tenHC:"Mannitol"},
{tenthuoc: "Omeprem",year:2019,month:"Dec",day:7,tenHC:"Omeprazole"},
{tenthuoc: "3B-Medi",year:2019,month:"Oct",day:18,tenHC:"Vitamin B1 + Vitamin B6 + Vitamin B12"},
{tenthuoc: "Medoclav 625mg",year:2019,month:"Jun",day:22,tenHC:"Amoxicilin + Acid clavulanic"},
{tenthuoc: "Medoclav 625mg",year:2019,month:"Sep",day:8,tenHC:"Amoxicilin+Acid Clavulanic"},
{tenthuoc: "Fabafixim",year:2019,month:"Dec",day:10,tenHC:"Cefixim"},
{tenthuoc: "Hyzaar Tab 50/12.5 30's",year:2019,month:"Jun",day:12,tenHC:"Losartan + hydroclorothiazid"},
{tenthuoc: "Hyzaar Tab 50/12.5 30's",year:2019,month:"Nov",day:12,tenHC:"Losartan potassium; Hydrochlorothiazide"},
{tenthuoc: "Adrenalin",year:2019,month:"Nov",day:25,tenHC:"Epinephrin (adrenalin)"},
{tenthuoc: "Adrenalin",year:2019,month:"Aug",day:10,tenHC:"Adrenalin(Epinephrin)"},
{tenthuoc: "Adrenalin",year:2019,month:"Jun",day:28,tenHC:"Epinephrin(Adrenalin)"},
{tenthuoc: "Adrenalin",year:2019,month:"Jun",day:2,tenHC:"Epinephrine (Adrenaline)"},
{tenthuoc: "Cosyndo B",year:2019,month:"Jun",day:30,tenHC:"Vitamin B1 + B6 + B12"},
{tenthuoc: "Dicortinef",year:2019,month:"Oct",day:21,tenHC:"neomycin sulfat + gramicidin+ 9-alpha fluohydrocortison"},
{tenthuoc: "Ambelin",year:2019,month:"Aug",day:12,tenHC:"Amlodipin"},
{tenthuoc: "Bupivacaine Aguettant 5mg/ml",year:2019,month:"Sep",day:14,tenHC:"Bupivacaine hydrochloride"},
{tenthuoc: "Exforge HCT 10/160/12,5 mg",year:2019,month:"Jul",day:16,tenHC:"Amlodipin + Valsartan"},
{tenthuoc: "Vitamin 3B",year:2019,month:"Jul",day:13,tenHC:"Vitamin B1 + B6 +B12"},
{tenthuoc: "Vitamin 3B",year:2019,month:"Jul",day:5,tenHC:"Vitamin B1 + B6 + B12"},
{tenthuoc: "Ostocare",year:2019,month:"Nov",day:13,tenHC:"Calci Gluconate, Vitamin D3"},
{tenthuoc: "Cammic",year:2019,month:"Sep",day:30,tenHC:"Tranexamic acid"},
{tenthuoc: "Cammic",year:2019,month:"Jul",day:19,tenHC:"Acid Tranexamic"},
{tenthuoc: "Ambelin (Amlodipin) 5mg",year:2019,month:"Nov",day:16,tenHC:"Amlodipin"},
{tenthuoc: "Cordaflex",year:2019,month:"Jul",day:13,tenHC:"Nifedipin"},
{tenthuoc: "Isotic Moxicin",year:2019,month:"Sep",day:18,tenHC:"Moxifloxacin"},
{tenthuoc: "Dalacin C Inj 600mg 4ml",year:2019,month:"Oct",day:10,tenHC:"Clindamycin  phosphate"},
{tenthuoc: "Dalacin C Inj 600mg 4ml",year:2019,month:"Oct",day:4,tenHC:"Clindamycin"},
{tenthuoc: "Dropstar",year:2019,month:"Aug",day:11,tenHC:"Levofloxacin"},
{tenthuoc: "Hydrocortison-Lidocain-Richter 125mg/5ml",year:2019,month:"Jul",day:28,tenHC:"Hydrocortison"},
{tenthuoc: "Hydrocortison-Lidocain-Richter 125mg/5ml",year:2019,month:"Oct",day:3,tenHC:"Hydrocortison + Lidocain"},
{tenthuoc: "Diệp hạ châu TP",year:2019,month:"Oct",day:25,tenHC:"Cao khô diệp hạ châu,lá khô diệp hạ châu"},
{tenthuoc: "Albis (ranitidin+tripotassium)",year:2019,month:"Sep",day:6,tenHC:"Ranitidine  + Tripotassium Bismuth Dicitrate + Sucralfate"},
{tenthuoc: "Clamoxyl",year:2019,month:"Jul",day:24,tenHC:"Amoxicilin"},
{tenthuoc: "Tioga",year:2019,month:"Jul",day:20,tenHC:"Cao actiso, Sài đất, Thương nhĩ tử, Kim ngân hoa, Hạ khô thảo, tá dược vừa đủ"},
{tenthuoc: "Tioga",year:2019,month:"Jul",day:20,tenHC:"Cao Actiso, Sài đất, Thương nhĩ tử, Kim ngân hoa, Hạ khô thảo"},
{tenthuoc: "Ucyrin",year:2019,month:"Dec",day:1,tenHC:"Clopidogrel"},
{tenthuoc: "Hoạt huyết dưỡng não TP",year:2019,month:"Jun",day:20,tenHC:"Cao đinh lăng,cao bạch quả"},
{tenthuoc: "Polyclox 750mg (amoxicilin+cloxacilin)",year:2019,month:"Aug",day:30,tenHC:"amoxicilin+cloxacilin"},
{tenthuoc: "Levofloxacin Stada 500mg",year:2019,month:"Jul",day:24,tenHC:"Levofloxacin"},
{tenthuoc: "Clarithromycin",year:2019,month:"Oct",day:5,tenHC:"Clarithromycin"},
{tenthuoc: "Dicortineff",year:2019,month:"Sep",day:4,tenHC:"Neomycin sulfat + gramicidin + 9-alpha fluohydrocortison acetat"},
{tenthuoc: "Dicortineff",year:2019,month:"Dec",day:17,tenHC:"Neomycin+Gramicidin+Fludrococortisone"},
{tenthuoc: "Mediclovir 5g",year:2019,month:"Jul",day:22,tenHC:"Aciclovir"},
{tenthuoc: "Cefix",year:2019,month:"Oct",day:1,tenHC:"Cefixime"},
{tenthuoc: "Milflox",year:2019,month:"Jul",day:8,tenHC:"Moxifloxacin"},
{tenthuoc: "Tetracycllin 1% 5g",year:2019,month:"Jul",day:20,tenHC:"Tetracycllin"},
{tenthuoc: "Vitamin B1+B6+B13",year:2019,month:"Jul",day:7,tenHC:"Vitamin B1+ vitamin B6+ vitamin B12"},
{tenthuoc: "Augmentin BD 625mg",year:2019,month:"Dec",day:4,tenHC:"Amoxicillin trihydrate, Acid Clavulanic"},
{tenthuoc: "Venrutin (rutin 500mg + vitamin c 100mg)",year:2019,month:"Jul",day:30,tenHC:"rutin  + vitamin c"},
{tenthuoc: "Ednyt (Enalapril) 5mg",year:2019,month:"Sep",day:15,tenHC:"Enalapril"},
{tenthuoc: "Praverix 500 mg",year:2019,month:"Nov",day:4,tenHC:"Amoxicilin"},
{tenthuoc: "Heparin 25000 UI",year:2019,month:"Dec",day:14,tenHC:"Heparin (Natri)"},
{tenthuoc: "Amlaxopin 5mg",year:2019,month:"Sep",day:25,tenHC:"Amlodipin"},
{tenthuoc: "Amlaxopin 5mg",year:2019,month:"Nov",day:9,tenHC:"Amlodipine"},
{tenthuoc: "Meronem 500mg (Meropenem)",year:2019,month:"Sep",day:3,tenHC:"Meropenem"},
{tenthuoc: "Novorapid Flexpen",year:2019,month:"Nov",day:17,tenHC:"Insulin tác dụng nhUK, ngắn (Fast-acting, Short- acting)"},
{tenthuoc: "Scanax 500",year:2019,month:"Oct",day:21,tenHC:"Ciprofloxacin"},
{tenthuoc: "Mixtad",year:2019,month:"Jun",day:18,tenHC:"Insulin người,rADN"},
{tenthuoc: "Bidilucil 250",year:2019,month:"Oct",day:4,tenHC:"Meclophenoxat"},
{tenthuoc: "Panalganeffer 150mg",year:2019,month:"Sep",day:12,tenHC:"Paracetamol (acetaminophen)"},
{tenthuoc: "Claritek granules",year:2019,month:"Jun",day:27,tenHC:"Clarithromycin"},
{tenthuoc: "Crila forte 500mg",year:2019,month:"Oct",day:12,tenHC:"Cao khô Trinh nữ hoàng cung"},
{tenthuoc: "Dex-Tobrin 5ml",year:2019,month:"Sep",day:5,tenHC:"Tobramycin + Dexamethason"},
{tenthuoc: "Visulin 0,75g",year:2019,month:"Jul",day:13,tenHC:"Ampicilin +Sulbactam"},
{tenthuoc: "Morphin 10mg/1ml",year:2019,month:"Dec",day:5,tenHC:"Morphin (hydroclorid)"},
{tenthuoc: "Morphin 10mg/1ml",year:2019,month:"Sep",day:15,tenHC:"Morphin hydroclorid"},
{tenthuoc: "Morphin 10mg/1ml",year:2019,month:"Sep",day:10,tenHC:"Morphin"},
{tenthuoc: "Levonor 1mg",year:2019,month:"Jun",day:27,tenHC:"Nor epinephrin (Nor adrenalin)"},
{tenthuoc: "Diệp hạ châu - V",year:2019,month:"Jun",day:10,tenHC:"Lá diệp hạ châu, cao khô diệp hạ châu toàn cây"},
{tenthuoc: "Augmentin 250/31.25",year:2019,month:"Aug",day:21,tenHC:"Amoxicillin, Acid Clavulanic"},
{tenthuoc: "Vipocef 200",year:2019,month:"Oct",day:13,tenHC:"Cefpodoxim"},
{tenthuoc: "Tranexamic acid 250mg/5ml",year:2019,month:"Aug",day:12,tenHC:"Tranexamic acid 250mg/5ml"},
{tenthuoc: "Nifedipin Hasan 20 Retart",year:2019,month:"Nov",day:23,tenHC:"Nifedipin"},
{tenthuoc: "Concor cor 2.5mg",year:2019,month:"Jun",day:4,tenHC:"Bisoprolol"},
{tenthuoc: "Seduxen",year:2019,month:"Sep",day:4,tenHC:"Diazepam"},
{tenthuoc: "Morphin",year:2019,month:"Dec",day:22,tenHC:"Morphin (hydroclorid)"},
{tenthuoc: "Morphin",year:2019,month:"Nov",day:10,tenHC:"Morphin"},
{tenthuoc: "Partamol Tab 500mg",year:2019,month:"Jul",day:10,tenHC:"Paracetamol"},
{tenthuoc: "Diazepam Injection BP 10mg 2ml H10",year:2019,month:"Jul",day:4,tenHC:"Diazepam"},
{tenthuoc: "Lovenox 40mg/0,4ml",year:2019,month:"Aug",day:20,tenHC:"Natri  Enoxaparin  40mg/ 0,4ml"},
{tenthuoc: "Pataday",year:2019,month:"Dec",day:17,tenHC:"Pataday"},
{tenthuoc: "Pataday",year:2019,month:"Jun",day:23,tenHC:"Olopatadine hydrochloride"},
{tenthuoc: "Cefpodoxime 100mg",year:2019,month:"Sep",day:1,tenHC:"Cefpodoxim"},
{tenthuoc: "Amlodipin 5 mg",year:2019,month:"Aug",day:26,tenHC:"Amlodipine"},
{tenthuoc: "Eyewise (Moxifloxacin) 0,5%/3ml",year:2019,month:"Oct",day:27,tenHC:"Moxifloxacin"},
{tenthuoc: "Ebitac (Enalapril + Hydroclorothiazide) (10+25)",year:2019,month:"Jun",day:22,tenHC:"Enalapril + Hydroclorothiazide"},
{tenthuoc: "Lisonorm",year:2019,month:"Oct",day:17,tenHC:"Lisinopril + Amlodipin"},
{tenthuoc: "Amlodipin (Domesco) 5mg",year:2019,month:"Dec",day:1,tenHC:"Amlodipin"},
{tenthuoc: "LANTUS Solostar 100IU/ml B/ 5 pens x 3ml",year:2019,month:"Nov",day:25,tenHC:"Insulin tác dụng chậm, kéo dài"},
{tenthuoc: "Lantus Solostar 100IU/ml B/5 pensx3ml",year:2019,month:"Nov",day:23,tenHC:"Insulin glargine"},
{tenthuoc: "Hyzaar 50mg+12.5mg",year:2019,month:"Jul",day:13,tenHC:"Losartan + hydroclorothiazid"},
{tenthuoc: "Bisoprolol Fumarate 2.5mg",year:2019,month:"Aug",day:25,tenHC:"Bisoprolol"},
{tenthuoc: "Bipisyn",year:2019,month:"Oct",day:11,tenHC:"Ampicilin +Sulbactam"},
{tenthuoc: "Savi 3B",year:2019,month:"Jul",day:24,tenHC:"Vitamin B1      Vitamin B6      Vitamin B12"},
{tenthuoc: "Azithromycin(Azicine) 250mg",year:2019,month:"Jun",day:18,tenHC:"Azithromycin"},
{tenthuoc: "Cefixim 100",year:2019,month:"Jun",day:8,tenHC:"Cefixim"},
{tenthuoc: "Acular 5ml",year:2019,month:"Jul",day:25,tenHC:"Ketorolac"},
{tenthuoc: "Vitamin AD",year:2019,month:"Jun",day:6,tenHC:"Vitamin A + Vitamin D"},
{tenthuoc: "Vitamin AD",year:2019,month:"Sep",day:24,tenHC:"Vitamin A Vitamin D"},
{tenthuoc: "Vitamin AD",year:2019,month:"Jun",day:6,tenHC:"Vitamin A + D"},
{tenthuoc: "syrup  Bổ tỳ P/H",year:2019,month:"Dec",day:12,tenHC:"Mỗi 100ml syrup  chứa cao lỏng dược liệu chiết từ: Đẳng sâm; Bạch linh; Bạch truật; Cát cánh; Mạch nha; Cam thảo; Long nhãn; Trần bì; Liên nhục; Sa nhân; Sử quân tử; Bán hạ."},
{tenthuoc: "Cordaflex 20mg",year:2019,month:"Sep",day:15,tenHC:"Nifedipin"},
{tenthuoc: "Coveram 5mg/5mg",year:2019,month:"Aug",day:24,tenHC:"Perindopril Arginine5mg+ Amlodipin 5mg"},
{tenthuoc: "Pamilonor",year:2019,month:"Dec",day:7,tenHC:"Amlodipine"},
{tenthuoc: "Eyewise",year:2019,month:"Sep",day:4,tenHC:"Moxifloxacin"},
{tenthuoc: "Pataday 0,2% 2.5ml",year:2019,month:"Dec",day:2,tenHC:"Olopatadine hydrochloride"},
{tenthuoc: "Refresh Liquigel 15ml",year:2019,month:"Dec",day:21,tenHC:"Natri carboxy methylcellulose"},
{tenthuoc: "Bisoprolol fumarate 2,5mg",year:2019,month:"Nov",day:2,tenHC:"Bisoprolol"},
{tenthuoc: "Rutin C 100mg",year:2019,month:"Jun",day:19,tenHC:"Vitamin C+Rutine"},
{tenthuoc: "Lovenox",year:2019,month:"Oct",day:21,tenHC:"Enoxaparin"},
{tenthuoc: "Hoạt huyết dưỡng não (ACP)",year:2019,month:"Dec",day:12,tenHC:"Cao đinh lăng,cao bạch quả"},
{tenthuoc: "Prospan Cough Liquid",year:2019,month:"Jul",day:14,tenHC:"Cao khô lá thường xuân"},
{tenthuoc: "Aldan tablets 5mg",year:2019,month:"Jun",day:9,tenHC:"Amlodipin"},
{tenthuoc: "Cordaflex 20mg (Nifedipin)",year:2019,month:"Oct",day:16,tenHC:"Nifedipin"},
{tenthuoc: "Troysar (Losartan) 50mg",year:2019,month:"Sep",day:7,tenHC:"Losartan"},
{tenthuoc: "Azithromycin 250mg",year:2019,month:"Jun",day:3,tenHC:"Azithromycin"},
{tenthuoc: "Tobradex 3,5g",year:2019,month:"Jun",day:5,tenHC:"Tobramycin + Dexamethason"},
{tenthuoc: "Tobradex 3,5g",year:2019,month:"Sep",day:10,tenHC:"Tobramycin"},
{tenthuoc: "Tobradex 3,5g",year:2019,month:"Aug",day:12,tenHC:"Tobramycin + Dexamethasone"},
{tenthuoc: "Kavasdin 5",year:2019,month:"Dec",day:28,tenHC:"Amlodipin"},
{tenthuoc: "Rutin - Vitamin C",year:2019,month:"Aug",day:19,tenHC:"Vitamin C+Rutin"},
{tenthuoc: "Rutin - Vitamin C",year:2019,month:"Oct",day:18,tenHC:"Vitamin C + rutine"},
{tenthuoc: "Levonor 1mg/1ml",year:2019,month:"Nov",day:25,tenHC:"Nor-epinephrin (Nor- adrenalin)"},
{tenthuoc: "Levonor 1mg/1ml",year:2019,month:"Nov",day:6,tenHC:"Nor epinephrin (Nor adrenalin)"},
{tenthuoc: "Cesyrup (Vitamin C) 1200mg/60ml",year:2019,month:"Dec",day:8,tenHC:"Vitamin C"},
{tenthuoc: "Natriclorid 0,9%",year:2019,month:"Jun",day:11,tenHC:"Natriclorid"},
{tenthuoc: "Dalacin C Inj 300mg 2ml",year:2019,month:"Jul",day:23,tenHC:"Clindamycin"},
{tenthuoc: "Azicin 250 mg",year:2019,month:"Oct",day:20,tenHC:"Azithromycin"},
{tenthuoc: "Aldan tablets",year:2019,month:"Jun",day:5,tenHC:"Amlodipin"},
{tenthuoc: "Polyclox 1000 (amoxicilin 500mg+cloxacilin 500mg)",year:2019,month:"Jun",day:17,tenHC:"amoxicilin+cloxacilin"},
{tenthuoc: "Lovenox 6000IU/0,6ml",year:2019,month:"Sep",day:18,tenHC:"Enoxaparin (natri)"},
{tenthuoc: "Azopt",year:2019,month:"Jun",day:9,tenHC:"Brinzolamid"},
{tenthuoc: "Azopt",year:2019,month:"Aug",day:6,tenHC:"Brinzolamide"},
{tenthuoc: "Azopt 1% 5ml",year:2019,month:"Jun",day:4,tenHC:"Brinzolamid"},
{tenthuoc: "Augclmox 250",year:2019,month:"Jun",day:15,tenHC:"Amoxicilin + Acid clavulanic"},
{tenthuoc: "Enalapril 5mg",year:2019,month:"Sep",day:9,tenHC:"Enalapril"},
{tenthuoc: "Ambelin 5",year:2019,month:"Oct",day:31,tenHC:"Amlodipin"},
{tenthuoc: "Scanax",year:2019,month:"Dec",day:16,tenHC:"Ciprofloxacin"},
{tenthuoc: "VITAMIN A & D",year:2019,month:"Jun",day:26,tenHC:"Vitamin A+Vitamin D"},
{tenthuoc: "VITAMIN A & D",year:2019,month:"Jun",day:18,tenHC:"Vitamin A + D"},
{tenthuoc: "VITAMIN A & D",year:2019,month:"Sep",day:2,tenHC:"Vitamin A Vitamin D"},
{tenthuoc: "Clopistad",year:2019,month:"Nov",day:9,tenHC:"Clopidogrel"},
{tenthuoc: "Clarithromycin  stada",year:2019,month:"Aug",day:13,tenHC:"Clarithromycin"},
{tenthuoc: "Corneregel",year:2019,month:"Jul",day:4,tenHC:"Dexpanthenol (panthenol)"},
{tenthuoc: "Vitamin B1 25mg",year:2019,month:"Sep",day:22,tenHC:"vitamin B1"},
{tenthuoc: "Mezathion",year:2019,month:"Aug",day:31,tenHC:"Spironolactone"},
{tenthuoc: "Mezathion",year:2019,month:"Dec",day:23,tenHC:"Spironolacton"},
{tenthuoc: "Levemir Flexpen 300IU/3ml",year:2019,month:"Jun",day:11,tenHC:"Insulin tác dụng chậm, kéo dài (Slow-acting, Long-acting)"},
{tenthuoc: "Panalganeffer Codein",year:2019,month:"Jul",day:13,tenHC:"Paracetamol + codein phosphat"},
{tenthuoc: "Maxitrol drop",year:2019,month:"Nov",day:10,tenHC:"Dexamethason+ Neomycin+ Polymycin B"},
{tenthuoc: "Midagentin 250/31,25",year:2019,month:"Aug",day:11,tenHC:"Amoxicilin + Acid clavulanic"},
{tenthuoc: "Renapril 10mg",year:2019,month:"Jun",day:2,tenHC:"Enalapril"},
{tenthuoc: "Renapril 10mg",year:2019,month:"Sep",day:19,tenHC:"Enalapril 10mg"},
{tenthuoc: "Vitamin K1 1mg/1ml",year:2019,month:"Aug",day:3,tenHC:"Phytomenadion (Vitamin K1)"},
{tenthuoc: "Vitamin B1 100mg",year:2019,month:"Oct",day:21,tenHC:"vitamin B1"},
{tenthuoc: "Verospiron",year:2019,month:"Dec",day:29,tenHC:"Spironolacton"},
{tenthuoc: "SCILIN R 40IU/ml",year:2019,month:"Jun",day:1,tenHC:"Insulin tác dụng nhanh, ngắn (Fast-acting, Short-acting)"},
{tenthuoc: "Biloxcin Eye",year:2019,month:"Sep",day:17,tenHC:"Ofloxacin"},
{tenthuoc: "Biloxcin Eye",year:2019,month:"Jun",day:16,tenHC:"Ofloxacin 0.3%/5ml"},
{tenthuoc: "Biloxcin Eye",year:2019,month:"Nov",day:30,tenHC:"Omeprazole"},
{tenthuoc: "KALBENOX",year:2019,month:"Aug",day:4,tenHC:"Enoxaparin"},
{tenthuoc: "Cefpodoxim 100mg",year:2019,month:"Jun",day:29,tenHC:"Cefpodoxim"},
{tenthuoc: "Alcaine",year:2019,month:"Jul",day:7,tenHC:"Proparacain"},
{tenthuoc: "Alcaine",year:2019,month:"Nov",day:6,tenHC:"Proparacain hydrochlorid"},
{tenthuoc: "Alcaine",year:2019,month:"Oct",day:7,tenHC:"Proparacaine Hydrochloride (Proxymetacaine hydrochloride)"},
{tenthuoc: "Aerius Tab 5mg 10's",year:2019,month:"Aug",day:23,tenHC:"Desloratadin"},
{tenthuoc: "Bupivacain 0.5%/4ml",year:2019,month:"Sep",day:15,tenHC:"Bupivacain"},
{tenthuoc: "Morphin HCl",year:2019,month:"Sep",day:17,tenHC:"Morphin (clohydrat)"},
{tenthuoc: "Propofol-Lipuro 1% (10mg/ ml)20ml 5's",year:2019,month:"Aug",day:19,tenHC:"Propofol - Lipuro 10mg/ml MCT& LCT, 20ml"},
{tenthuoc: "Cefpodoxime 100",year:2019,month:"Nov",day:11,tenHC:"Cefpodoxim"},
{tenthuoc: "Tobramycin",year:2019,month:"Oct",day:19,tenHC:"Tobramycin"},
{tenthuoc: "Gardenal 10mg V10",year:2019,month:"Nov",day:18,tenHC:"Phenobarbital"},
{tenthuoc: "Agicipro",year:2019,month:"Aug",day:4,tenHC:"Ciprofloxacin"},
{tenthuoc: "Vitamin K",year:2019,month:"Jul",day:12,tenHC:"Vitamin K"},
{tenthuoc: "Adrenalin Aguettant 5ml/5mg",year:2019,month:"Aug",day:12,tenHC:"Epinephrin"},
{tenthuoc: "Vigamox 0.5% 5m",year:2019,month:"Jun",day:5,tenHC:"Moxifloxacin"},
{tenthuoc: "Vitamin B6 100mg",year:2019,month:"Jul",day:24,tenHC:"Vitamin B6"},
{tenthuoc: "Neo-codion",year:2019,month:"Dec",day:19,tenHC:"Codein camphosulphonat + Sulfoguaiacol + Cao mềm Grindelia"},
{tenthuoc: "Pharmaton",year:2019,month:"Jul",day:20,tenHC:"Ginseng G115, vitamins"},
{tenthuoc: "Bestdocel (Docetaxel) 20mg",year:2019,month:"Jul",day:19,tenHC:"Docetaxel"},
{tenthuoc: "Furosemide Salf",year:2019,month:"Nov",day:7,tenHC:"Furosemide"},
{tenthuoc: "Azicin  250 mg",year:2019,month:"Sep",day:22,tenHC:"Azithromycin"},
{tenthuoc: "Transamin 250mg",year:2019,month:"Sep",day:15,tenHC:"Tranexamic acid"},
{tenthuoc: "Thập toàn đại bổ P/H",year:2019,month:"Nov",day:11,tenHC:"Bột Bạch thược; Bột Bạch truật; Bột Cam thảo; Bột Đương Quy; Cao Đảng sâm; Bột Phục linh; Bột Quế; Bột Thục địa; Bột Xuyên Khung; Cao Hoàng kỳ."},
{tenthuoc: "Levobact 0,5%5ml (levofloxacin)",year:2019,month:"Nov",day:16,tenHC:"Levofloxacin"},
{tenthuoc: "Gentamicin",year:2019,month:"Sep",day:21,tenHC:"Gentamicin"},
{tenthuoc: "Otrivin 0.1% Nasal Spray 10ml",year:2019,month:"Sep",day:26,tenHC:"Xylometazoline hydrochloride"},
{tenthuoc: "Biloxcin (ofloxacin) 0,3%/5ml",year:2019,month:"Dec",day:18,tenHC:"Ofloxacin"},
{tenthuoc: "Azicine 250 mg",year:2019,month:"Sep",day:2,tenHC:"Azithromycin"},
{tenthuoc: "Cammic 250mg/5ml",year:2019,month:"Oct",day:19,tenHC:"Tranexamic acid"},
{tenthuoc: "Aerius 60ml",year:2019,month:"Oct",day:23,tenHC:"Desloratadin"},
{tenthuoc: "Omeprem 20",year:2019,month:"Nov",day:23,tenHC:"Omeprazole"},
{tenthuoc: "Insunova 30/70",year:2019,month:"Jun",day:11,tenHC:"Insulin hỗn hợp 30/70 100IU/ml 10ml"},
{tenthuoc: "Thuốc ho K/H",year:2019,month:"Dec",day:5,tenHC:"Ma hoàng, hạnh nhân/khô hạnh nhân, quế chi/thạch cao, cam thảo."},
{tenthuoc: "VIDLOX 200 (Cefpodoxim 200mg)",year:2019,month:"Oct",day:18,tenHC:"Cefpodoxim"},
{tenthuoc: "Vitamin B12",year:2019,month:"Oct",day:7,tenHC:"Vitamin B12"},
{tenthuoc: "Vitamin B12",year:2019,month:"Jun",day:20,tenHC:"Vitamin B12 (Cyanocobalamin, Hydroxocobalamin)"},
{tenthuoc: "Vitamin B12",year:2019,month:"Aug",day:16,tenHC:"Vitamin B12(Cyanocobalamin, Hydroxocobalamin)"},
{tenthuoc: "Ucyrin 75mg (Clopidogrel)",year:2019,month:"Nov",day:2,tenHC:"Clopidogrel"},
{tenthuoc: "Propofol Kabelgium",year:2019,month:"Dec",day:16,tenHC:"Propofol"},
{tenthuoc: "Adrenalin 1mg/ml",year:2019,month:"Oct",day:26,tenHC:"Epinephrin (adrenalin)"},
{tenthuoc: "Propofol 1% Kabelgium",year:2019,month:"Jul",day:1,tenHC:"Propofol"},
{tenthuoc: "Loratadin Stada 10mg",year:2019,month:"Aug",day:21,tenHC:"Losartan"},
{tenthuoc: "Exforge HCT Tab",year:2019,month:"Nov",day:9,tenHC:"Amlodipine + Hydrochlorothiazide + Valsartan"},
{tenthuoc: "Sastan - H",year:2019,month:"Oct",day:4,tenHC:"Losartan+hydroclorothiazid"},
{tenthuoc: "Digoxin Richter 0.25mg",year:2019,month:"Dec",day:1,tenHC:"Digoxin"},
{tenthuoc: "Venrutine (100mg+500mg)",year:2019,month:"Nov",day:22,tenHC:"Rutin+vitamin C"},
{tenthuoc: "Tobradex Oint 0,3% +0,1%/3,5g",year:2019,month:"Dec",day:24,tenHC:"Tobramycin + Dexamethason"},
{tenthuoc: "Tobradex onit",year:2019,month:"Sep",day:1,tenHC:"Tobramycin + Dexamethason"},
{tenthuoc: "(kiên 2)",year:2019,month:"Sep",day:23,tenHC:""},
{tenthuoc: "Lidocain 2%",year:2019,month:"Jul",day:10,tenHC:"Lidocain"},
{tenthuoc: "Lidocain 2%",year:2019,month:"Dec",day:15,tenHC:"Lidocaine (hydrocloride)"},
{tenthuoc: "Midagentin 250/62,5",year:2019,month:"Jul",day:18,tenHC:"Amoxicilin + Acid clavulanic"},
{tenthuoc: "Midagentin 250/62,5",year:2019,month:"Jul",day:28,tenHC:"Amoxicilin+ Acid clavulanic"},
{tenthuoc: "Neostimine-hameln",year:2019,month:"Jul",day:15,tenHC:"Neostigmin Bromid"},
{tenthuoc: "Paracetamol 500mg",year:2019,month:"Jun",day:2,tenHC:"Paracetamol"},
{tenthuoc: "Vitfermin",year:2019,month:"Oct",day:19,tenHC:"Sắt fumarat + acid folic + vitamin B12"},
{tenthuoc: "PMS-Claminat 250mg/31,25mg",year:2019,month:"Jun",day:14,tenHC:"Amoxicilin + Acidclavulanic"},
{tenthuoc: "Valygyno",year:2019,month:"Oct",day:16,tenHC:"Nystatin + Neomycin + Polymyxin B"},
{tenthuoc: "Valygyno",year:2019,month:"Jul",day:11,tenHC:"Nystatin + Neomycin+ Polymicin B"},
{tenthuoc: "Propofol 1% Kabi 10mg/ml 20ml",year:2019,month:"Dec",day:21,tenHC:"Propofol"},
{tenthuoc: "Mitafix",year:2019,month:"Aug",day:4,tenHC:"Cefixim"},
{tenthuoc: "Travatan 2.5ml",year:2019,month:"Jun",day:23,tenHC:"Travoprost"},
{tenthuoc: "Babi B.O.N",year:2019,month:"Oct",day:10,tenHC:"Vitamin D3"},
{tenthuoc: "Scilin N 100IU/ml 10ml",year:2019,month:"Nov",day:28,tenHC:"Insulin chậm (Insulin isophane)"},
{tenthuoc: "Boligenax Soft capsules",year:2019,month:"Oct",day:27,tenHC:"Nystatin + neomycin + polymycin B"},
{tenthuoc: "CORDARONE TAB 200mg B/ 2bls  x 15 Tabs",year:2019,month:"Dec",day:25,tenHC:"Amiodarone HCl"},
{tenthuoc: "CORDARONE TAB 200mg B/ 2bls  x 15 Tabs",year:2019,month:"Jun",day:14,tenHC:"Amiodarone hydrochloride  200 mg"},
{tenthuoc: "Hydrocortison + Lidocain",year:2019,month:"Dec",day:13,tenHC:"Hydrocortison + Lidocain"},
{tenthuoc: "Kim tiền thảo",year:2019,month:"Oct",day:10,tenHC:"Cao khô kim tiền thảo 120mg"},
{tenthuoc: "Kim tiền thảo",year:2019,month:"Jun",day:21,tenHC:"Cao khô Kim tiền thảo"},
{tenthuoc: "Lorista H 50mg+12.5mg",year:2019,month:"Sep",day:23,tenHC:"Losartan+hydroclorothiazid"},
{tenthuoc: "Clarithromycin tablets 250mg",year:2019,month:"Oct",day:30,tenHC:"Clarithromycin"},
{tenthuoc: "Lostad HCT 50/12,5 mg",year:2019,month:"Aug",day:13,tenHC:"Losartan+hydroclorothiazid"},
{tenthuoc: "Amdirel",year:2019,month:"Jul",day:29,tenHC:"Amlodipin"},
{tenthuoc: "Tranecid (acid tranexamic) 250mg/5ml",year:2019,month:"Jul",day:23,tenHC:"Acid Tranexamic"},
{tenthuoc: "Concor Cor Tab 2.5mg 3x10's",year:2019,month:"Dec",day:11,tenHC:"Bisoprolol fumarate"},
{tenthuoc: "Concor Cor Tab 2.5mg 3x10's",year:2019,month:"Dec",day:10,tenHC:"Bisoprolol"},
{tenthuoc: "Adalat retard 20mg",year:2019,month:"Aug",day:21,tenHC:"Nifedipin"},
{tenthuoc: "Nolvadex (Tamoxifen) 10mg",year:2019,month:"Nov",day:19,tenHC:"Tamoxifen"},
{tenthuoc: "Panalganeffer 500",year:2019,month:"Jun",day:10,tenHC:"Paracetamol"},
{tenthuoc: "Combigan",year:2019,month:"Dec",day:17,tenHC:"Brimonidine tartrate + Timolol maleat"},
{tenthuoc: "Combigan",year:2019,month:"Jul",day:8,tenHC:"Brimonidin tartrat + Timolol"},
{tenthuoc: "Combigan",year:2019,month:"Jun",day:20,tenHC:"Brimonidin tartra + Timonol maleate"},
{tenthuoc: "INSUNOVA -30/70 (BIPHASIC)",year:2019,month:"Oct",day:10,tenHC:"Insulin trộn, hỗn hợp (Mixtard-acting, Dual-acting)"},
{tenthuoc: "Scilin M30       ( 30/70)",year:2019,month:"Sep",day:4,tenHC:"Insulin trộn, hỗn hợp"},
{tenthuoc: "Nifedipin T20 Standa retard",year:2019,month:"Jul",day:9,tenHC:"Nifedipin"},
{tenthuoc: "Aldactone Tab 25mg 100's",year:2019,month:"Nov",day:6,tenHC:"Spironolactone"},
{tenthuoc: "Aldactone Tab 25mg 100's",year:2019,month:"Aug",day:11,tenHC:"Spironolactone 25mg"},
{tenthuoc: "Morphin HCl 0.01g H 25",year:2019,month:"Dec",day:6,tenHC:"Morphin (hydroclorid)"},
{tenthuoc: "Morphin HCl 0.01g H 25",year:2019,month:"Aug",day:9,tenHC:"Morphin"},
{tenthuoc: "Mibeviru 400mg",year:2019,month:"Jun",day:8,tenHC:"Aciclovir"}]


